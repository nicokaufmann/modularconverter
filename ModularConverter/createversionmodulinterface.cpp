#include "createversionmodulinterface.h"

#include "createversionmodul.h"

CreateVersionModulInterface::CreateVersionModulInterface( CreateVersionModul* modul, QWidget *parent, Qt::WFlags flags )
	: StandardModulInterface(modul, parent, flags), importInterface(new ImportFileModulInterface(this))
{
	addModulWidget(importInterface);

	QPushButton* button = (QPushButton*)importInterface->importButton();
	button->setText("Version erstellen");

	QObject::connect(button, SIGNAL(clicked()), this, SLOT(createVersion()));
}

CreateVersionModulInterface::~CreateVersionModulInterface()
{

}

void CreateVersionModulInterface::finished( bool status )
{
	userMessageWidget->updateMessages();
}

void CreateVersionModulInterface::createVersion()
{
	modul->clearUserMessages();
	userMessageWidget->updateMessages();

	CreateVersionModul* versionModul = (CreateVersionModul*)modul;
	versionModul->setVersionDescription(importInterface->getVersionDescription());

	emit execute();
}
