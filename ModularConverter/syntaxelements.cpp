#include "syntaxelements.h"

SyntaxElements::SyntaxElements()
	: logicalFunctions(new QStringList), functions(new QMap<QString, QStringList>), keywords(new QStringList), typeList(new QStringList), typeMapping(new QMap<int, QString>),
	  sharedFieldLeadDelimiter(new QString()), sharedFieldTrailDelimiter(new QString()), sharedTableLeadDelimiter(new QString()), sharedTableTrailDelimiter(new QString()), sharedTextDelimiter(new QString()),
	  sharedSpecialChars(new QRegExp()), sharedSpecialCharsReplacement(new QString()), sharedDateDelimiter(new QString())
{
}

SyntaxElements::~SyntaxElements()
{

}

SyntaxElements::SyntaxElements( const SyntaxElements& obj )
{
	logicalFunctions = obj.logicalFunctions;
	functions = obj.functions;
	keywords = obj.keywords;
	typeList = obj.typeList;
	typeMapping = obj.typeMapping;

	sharedFieldLeadDelimiter = obj.sharedFieldLeadDelimiter;
	sharedFieldTrailDelimiter = obj.sharedFieldTrailDelimiter;
	sharedTableLeadDelimiter = obj.sharedTableLeadDelimiter;
	sharedTableTrailDelimiter = obj.sharedTableTrailDelimiter;
	sharedTextDelimiter = obj.sharedTextDelimiter;
	sharedDateDelimiter = obj.sharedDateDelimiter;

	sharedSpecialChars = obj.sharedSpecialChars;
	sharedSpecialCharsReplacement = obj.sharedSpecialCharsReplacement;
}

SyntaxElements& SyntaxElements::operator=( const SyntaxElements& obj )
{
	logicalFunctions = obj.logicalFunctions;
	functions = obj.functions;
	keywords = obj.keywords;
	typeList = obj.typeList;
	typeMapping = obj.typeMapping;

	sharedFieldLeadDelimiter = obj.sharedFieldLeadDelimiter;
	sharedFieldTrailDelimiter = obj.sharedFieldTrailDelimiter;
	sharedTableLeadDelimiter = obj.sharedTableLeadDelimiter;
	sharedTableTrailDelimiter = obj.sharedTableTrailDelimiter;
	sharedTextDelimiter = obj.sharedTextDelimiter;
	sharedDateDelimiter = obj.sharedDateDelimiter;

	sharedSpecialChars = obj.sharedSpecialChars;
	sharedSpecialCharsReplacement = obj.sharedSpecialCharsReplacement;

	return *this;
}

void SyntaxElements::initializeSyntax()
{
	*sharedFieldLeadDelimiter = "`";
	*sharedFieldTrailDelimiter = "`";
	*sharedTableLeadDelimiter = "`";
	*sharedTableTrailDelimiter = "`";
	*sharedTextDelimiter = "'";
	*sharedDateDelimiter = "#";

	*sharedSpecialCharsReplacement = "";
	sharedSpecialChars->setPattern("[,.'*+~�`?=})\\[\\(/\\\\&%$�""!:;#\\]]");

	keywords->clear();
	*keywords << "SELECT" << "FROM" << "DELETE" <<"WHERE" << "UPDATE" << "INSERT" << "INTO" << "VALUES"
		<< "INNER" << "TABLE" << "CREATE"<< "OUTER" << "JOIN" << "ON" << "AND" << "OR" << "LEFT" << "RIGHT"
		<< "SET" << "AS";

	functions->clear();
	functions->insert("left", (QStringList()<<"str"<<"n"));
	functions->insert("right", (QStringList()<<"str"<<"n"));
	functions->insert("instr", (QStringList()<<"str"<<"pos"));
	functions->insert("nz", (QStringList()<<"value"<<"replacement"));

	logicalFunctions->clear();
	*logicalFunctions << "AND" << "OR" << "=" << ">" << ">=" << "<" << "<=";

	typeMapping->clear();
	typeMapping->insert(QVariant::Bool, "TINYINT");
	typeMapping->insert(QVariant::Int, "INT");
	typeMapping->insert(QVariant::UInt,"INT UNSIGNED");
	typeMapping->insert(QVariant::LongLong, "BIGINT");
	typeMapping->insert(QVariant::ULongLong,"INT UNSIGNED");
	typeMapping->insert(QVariant::Double,"DOUBLE");
	typeMapping->insert(QVariant::Char,"VARCHAR(255)");
	typeMapping->insert(QVariant::Map,"BLOB");
	typeMapping->insert(QVariant::List,"BLOB");
	typeMapping->insert(QVariant::String,"VARCHAR(255)");
	typeMapping->insert(QVariant::StringList, "BLOB");
	typeMapping->insert(QVariant::ByteArray, "BLOB");
	typeMapping->insert(QVariant::BitArray, "BLOB");
	typeMapping->insert(QVariant::Date, "DATE");
	typeMapping->insert(QVariant::Time, "TIME");
	typeMapping->insert(QVariant::DateTime, "DATETIME");

	typeList->clear();
	*typeList<<"TINYINT"<<"INT"<<"INT UNSIGNED"<<"BIGINT"<<"DOUBLE"<<"VARCHAR(255)"<<"DATE"<<"TIME"<<"DATETIME"<<"TIMESTAMP"<<"BLOB";
}

QStringList SyntaxElements::getKeywords() const
{
	return *keywords;
}

QStringList SyntaxElements::getLogicalFunctions() const
{
	return *logicalFunctions;
}

QStringList SyntaxElements::getTypeList() const
{
	return *typeList;
}

QString SyntaxElements::getType( int val ) const
{
	if(typeMapping->contains(val)) {
		return typeMapping->value(val);
	} 
	return QString();
}

QStringList SyntaxElements::getFunctions() const
{
	return functions->keys();
}

QStringList SyntaxElements::getFunctionParameter(const QString& functionname) const
{
	if(functions->contains(functionname)) {
		return functions->value(functionname);
	}
	return QStringList();
}
 
int SyntaxElements::getFunctionParamterCount(const QString& functionname) const
{
	if(functions->contains(functionname)) {
		return functions->value(functionname).count();
	}
	return 0;
}

QString SyntaxElements::delimitTablename(const QString& tablename ) const
{
	return QString(tablename).append(*sharedTableTrailDelimiter).prepend(*sharedTableLeadDelimiter);
}

QString SyntaxElements::delimitFieldname(const QString& fieldname ) const
{
	return QString(fieldname).append(*sharedFieldTrailDelimiter).prepend(*sharedFieldLeadDelimiter);
}

QString SyntaxElements::delimitText(const QString& text ) const
{
	return QString(text).append(*sharedTextDelimiter).prepend(*sharedTextDelimiter);
}

QString SyntaxElements::fieldLeadDelimiter() const
{
	return *sharedFieldLeadDelimiter;
}

QString SyntaxElements::fieldTrailDelimiter() const
{
	return *sharedFieldTrailDelimiter;
}

QString SyntaxElements::tableLeadDelimiter() const
{
	return *sharedTableLeadDelimiter;
}

QString SyntaxElements::tableTrailDelimiter() const
{
	return *sharedTableTrailDelimiter;
}

QString SyntaxElements::textDelimiter() const
{
	return *sharedTextDelimiter;
}

QString SyntaxElements::dateDelimiter() const
{
	return *sharedDateDelimiter;
}



QString SyntaxElements::filterSpecialChars( const QString& string )
{
	return QString(string).replace(*sharedSpecialChars,*sharedSpecialCharsReplacement);
}

QString SyntaxElements::insertLimit( const QString& statement,const int& limit ) const
{
	QString result(statement);
	QRegExp hasLimit("(top [0-9]+)|(limit [0-9]+)",Qt::CaseInsensitive);
	if(!(hasLimit.indexIn(result) >= 0)) {
		QRegExp keyword("(select)|(delete)|(update)",Qt::CaseInsensitive);
		int index=keyword.indexIn(result);
		if(index >= 0) {
			return result.insert(index+6," top " + QString::number(limit)+" ");
		}
	}

	return result;
}

QString SyntaxElements::delimitDate( const QString& date ) const
{
	return QString(date).append(*sharedDateDelimiter).prepend(*sharedDateDelimiter);
}

QString SyntaxElements::delimitDate( const QDate& date ) const
{
	return delimitDate(date.toString("yyyy-MM-dd"));
}