#include "reportconfigmodul.h"

ReportConfigModul::ReportConfigModul( ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager , QString context, bool inheritVersion)
	: ConverterModul(parameterMap, versionMap, connectionManager, context, inheritVersion)
{
}

ReportConfigModul::~ReportConfigModul()
{
}

QWidget* ReportConfigModul::createWidget()
{
	return new ReportConfigurationModul(this);
}

QStringList ReportConfigModul::getRequestedParameter()
{
	return (QStringList()<<"Report Config"<<"Report Statements");
}

QStringList ReportConfigModul::getContextList()
{
	QStringList results;

	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Report Config"].first));
	QString table = parameterMap["Report Config"].second;

	if(connection->isOpen()) {

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		QSqlQuery data(*connection);
		if(data.exec("select distinct context from "+ syntax->delimitTablename(table))) {
			while(data.next()) {
				results<<data.record().value("context").toString();
			}
		}
		data.finish();
	}
	return results;
}

bool ReportConfigModul::loadReportConfig( QString context, QMap<QString, QVariant>& cfg )
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Report Config"].first));
	QString table = parameterMap["Report Config"].second;

	if(connection->isOpen()) {
		QSqlQuery data(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		data.prepare("select ident, configvalue from " + syntax->delimitTablename(table) + " where context=:context");
		data.bindValue(0,context);

		if(data.exec()) {
			while(data.next()) {
				cfg[data.record().value("ident").toString()] = data.record().value("configvalue");
			}

			data.finish();
			return true;
		} else {
			lastError = "Konfigurationsdaten konnten nicht geladen werden. Abfrage fehlgeschlagen.\n\n" + data.lastError().text();
			return false;
		}
	} else {
		lastError = "Konfigurationsdaten konnten nicht geladen werden. Problem mit Datenbankverbindung.\n\n" + connection->lastError().text();
		return false;
	}
}

bool ReportConfigModul::getStatementIds( QString context, QStringList& statementIds )
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Report Statements"].first));
	QString table = parameterMap["Report Statements"].second;


	if(connection->isOpen()) {
		QSqlQuery data(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		data.prepare("select distinct statement_id from "+ syntax->delimitTablename(table) + " where context=:context");
		data.bindValue(0,context);
		if(data.exec()) {
			while(data.next()) {
				statementIds<<data.record().value("statement_id").toString();
			}

			data.finish();
			return true;
		} else { 
			lastError = "Statements konnten nicht geladen werden. Abfrage fehlgeschlagen.\n\n" + data.lastError().text();
			return false;
		}
	} else {
		lastError = "Statements konnten nicht geladen werden. Problem mit Datenbankverbindung.\n\n" + connection->lastError().text();
		return false;
	}
}

bool ReportConfigModul::loadStatementConfig( QString context, QString statementId, QMap<QString, QVariant>& statementCfg )
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Report Statements"].first));
	QString table = parameterMap["Report Statements"].second;


	if(connection->isOpen()) {
		QSqlQuery data(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		data.prepare("select statementvalue, ident from "+ syntax->delimitTablename(table) + " where context=:context and statement_id=:statement_id");
		data.bindValue(0,context);
		data.bindValue(1,statementId);

		if(data.exec()) {
			while(data.next()) {
				statementCfg[data.record().value("ident").toString()] = data.record().value("statementvalue");
			}
			data.finish();
			return true;
		} else {
			lastError = "Statement konnte nicht geladen werden. Abfrage fehlgeschlagen.\n\n" + data.lastError().text();
			return false;
		}
	} else {
		lastError = "Statement konnte nicht geladen werden. Problem mit Datenbankverbindung.\n\n" + connection->lastError().text();
		return false;
	}
}

QStringList ReportConfigModul::getDatabaseNames()
{
	return connectionManager->connectionList();
}

bool ReportConfigModul::execute(int cmd)
{
	return true;
}

QStringList ReportConfigModul::getFieldNames( QString connectionName, QString statement )
{
	QStringList result;
	if(connectionManager->containsConnection(connectionName)) {
		AcquiredConnection connection(connectionManager->acquireConnection(connectionName));

		if(connection->isOpen()) {
			QSqlQuery attributes(*connection);

			SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

			if(attributes.exec(syntax->insertLimit(statement,1))) {
				for(int i=0; i<attributes.record().count();++i) {
					result<<attributes.record().fieldName(i);
				}
				attributes.finish();
			} else {
				lastError = "Das Statement hat einen Fehler verursacht. " + attributes.lastError().text();
			}
		} else {
			lastError = "Problem mit der Datenbankverbindung. " + connection->lastError().text();
		}
	}
	return result;
}

bool ReportConfigModul::saveReportConfig( QString context, QMap<QString, QVariant>& cfg )
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Report Config"].first));
	QString table = parameterMap["Report Config"].second;


	if(connection->isOpen()) {
		QSqlQuery insert(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		insert.prepare("insert into "+ syntax->delimitTablename(table) + " (context, ident, configvalue) VALUES (:context, :ident, :configvalue)");
		insert.bindValue(0,context);

		for(int i=0; i < cfg.keys().count(); ++i) {
			insert.bindValue(1,cfg.keys()[i]);
			insert.bindValue(2,cfg[cfg.keys()[i]]);

			if(!insert.exec()) {
				lastError = insert.lastError().text() + "\n" + insert.lastQuery();
				insert.finish();
				return false;
			} else {
				insert.finish();
			}
		}

		return true;
	} else {
		lastError = "Kontext konnte nicht geladen werden. Problem mit Datenbankverbindung.\n\n" + connection->lastError().text();
		return false;
	}
}

bool ReportConfigModul::saveStatementConfig( QString context, QString statementId, QMap<QString, QVariant>& statementCfg )
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Report Statements"].first));
	QString table = parameterMap["Report Statements"].second;


	if(connection->isOpen()) {
		QSqlQuery insert(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		insert.prepare("insert into "+ syntax->delimitTablename(table) + " (context, statement_id, ident, statementvalue) VALUES (:context, :statement_id, :ident, :statementvalue)");
		insert.bindValue(0,context);
		insert.bindValue(1,statementId);

		for(int i=0; i < statementCfg.keys().count(); ++i) {
			insert.bindValue(2,statementCfg.keys()[i]);
			insert.bindValue(3,statementCfg[statementCfg.keys()[i]]);

			if(!insert.exec()) {
				lastError = insert.lastError().text();
				insert.finish();
				return false;
			} else {
				insert.finish();
			}
		}

		return true;
	} else {
		lastError = "Kontext konnte nicht gespeichert werden. Problem mit Datenbankverbindung.\n\n" + connection->lastError().text();
		return false;
	}
}

bool ReportConfigModul::deleteContext( QString context )
{
	{
		AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Report Statements"].first));
		QString table = parameterMap["Report Statements"].second;


		if(connection->isOpen()) {
			QSqlQuery deleteStatement(*connection);

			SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

			deleteStatement.prepare("delete from " + syntax->delimitTablename(table)+ " where context=:context");
			deleteStatement.bindValue(0, context);
			if(!deleteStatement.exec()) {
				lastError = deleteStatement.lastError().text();
				return false;
			}
			deleteStatement.finish();
		} else {
			lastError = "Verbindung zur Datenbank konnte nicht hergestellt werden.\n\n" + connection->lastError().text();
			return false;
		}
	}

	{
		AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Report Config"].first));
		QString table = parameterMap["Report Config"].second;


		if(connection->isOpen()) {
			QSqlQuery deleteStatement(*connection);

			SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

			deleteStatement.prepare("delete from " + syntax->delimitTablename(table)+ " where context=:context");
			deleteStatement.bindValue(0, context);
			if(!deleteStatement.exec()) {
				lastError = deleteStatement.lastError().text();
				return false;
			}
			deleteStatement.finish();
		} else {
			lastError = "Verbindung zur Datenbank konnte nicht hergestellt werden.\n\n" + connection->lastError().text();
			return false;
		}
	}
	return true;
}
