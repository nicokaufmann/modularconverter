#include "stringeditdialog.h"



StringEditDialog::StringEditDialog( QString text, QString context, QStringList parts, QWidget *parent, Qt::WFlags flags )
	: QDialog(parent,flags)
{
	this->setWindowTitle("Connection String erstellen");
	ui.setupUi(this);
	ui.lDesc->setText(context);
	ui.leString->setText(text);
	ui.listParts->addItems(parts);

	QObject::connect(ui.bApply, SIGNAL(clicked()), this, SLOT(accept()));
	QObject::connect(ui.bAddPath, SIGNAL(clicked()), this, SLOT(addPath()));
	QObject::connect(ui.listParts, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(itemDoubleClicked(QListWidgetItem*)));
}

StringEditDialog::~StringEditDialog()
{

}

QString StringEditDialog::getEditedText()
{
	return ui.leString->text();
}

void StringEditDialog::addPath()
{
	QString path = QFileDialog::getOpenFileName(this,"Pfad ausw�hlen","","Alle Dateien (*)");
	ui.leString->setText(ui.leString->text().insert(ui.leString->cursorPosition(),path));
}

void StringEditDialog::itemDoubleClicked( QListWidgetItem* item )
{
	ui.leString->setText(ui.leString->text().insert(ui.leString->cursorPosition(),item->text()));
}


