#include "executeversionmodulinterface.h"

#include "convertermodul.h"

ExecuteVersionModulInterface::ExecuteVersionModulInterface( ConverterModul* modul, QWidget *parent, Qt::WFlags flags )
	: StandardModulInterface(modul, parent, flags), executeInterface(new ExecuteModulInterface(this))
{
	addModulWidget(new SelectVersionModulInterface(modul->getConnectionManager(), modul->getGuiVersionRequests(), modul->getParameterMap()["Versionierung"],this));
	addModulWidget(executeInterface);

	QObject::connect(executeInterface->executeButton(), SIGNAL(clicked()), this, SLOT(executeModul()));
}

ExecuteVersionModulInterface::~ExecuteVersionModulInterface()
{

}

void ExecuteVersionModulInterface::finished( bool status )
{
	userMessageWidget->updateMessages();
}

void ExecuteVersionModulInterface::executeModul()
{
	modul->clearUserMessages();
	userMessageWidget->updateMessages();

	emit execute();
}
