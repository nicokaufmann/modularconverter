#include "importfilemodulinterface.h"

ImportFileModulInterface::ImportFileModulInterface(QWidget *parent, Qt::WFlags flags )
	: QWidget(parent, flags)
{
	ui.setupUi(this);
}

ImportFileModulInterface::~ImportFileModulInterface()
{
}

QString ImportFileModulInterface::getVersionDescription()
{
	return ui.ptDescription->toPlainText();
}

QWidget* ImportFileModulInterface::importButton()
{
	return ui.bImport;
}
