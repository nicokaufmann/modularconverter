#include "sqlquerymodel.h"

#include <QDateTime>

SqlQueryModel::SqlQueryModel( QSqlQuery* sqlQuery, QObject *parent )
	: QAbstractItemModel(parent), sqlQuery(sqlQuery), bufferStart(0), bufferEnd(0), bufferedRows(0)
{
	if(sqlQuery->isActive() && sqlQuery->isSelect()) {
		columns = sqlQuery->record().count();
		for(int i=0; i < columns; ++i) {
			columnData<<sqlQuery->record().fieldName(i);
		}

		setBuffer(1,1000);
	} else {
		columns = 0;
	}
}

SqlQueryModel::~SqlQueryModel()
{
}

Qt::DropActions SqlQueryModel::supportedDropActions() const
{
	return Qt::IgnoreAction;
}
void SqlQueryModel::setBuffer( int startRow, int endRow )
{
	if(startRow <= endRow) {
		/*int deltaTop = bufferStart-startRow;
		int deltaBottom = endRow-bufferEnd;
		int currentBufferSize = bufferEnd-bufferStart; 

		if(deltaBottom*-1 > currentBufferSize || deltaTop*-1 > currentBufferSize) {
			//remove all
			if(bufferStart > 0 && bufferEnd > 0) {
				beginRemoveRows(QModelIndex(),0,bufferEnd-bufferStart);
				endRemoveRows();
			}

			//insert new area
			if(startRow > 0 && endRow > 0) {
				beginInsertRows(QModelIndex(),0,endRow-startRow);
				endInsertRows();
			}
			
			bufferStart = startRow;
			bufferEnd = endRow;
		} else {
			if(deltaTop < 0) {
				//remove
				beginRemoveRows(QModelIndex(),0,deltaTop*-1);
				endRemoveRows();
			} else if(deltaTop > 0) {
				//insert new area
				beginInsertRows(QModelIndex(),0,deltaTop);
				endInsertRows();
			}

			bufferStart = startRow;

			if(deltaBottom < 0) {
				beginRemoveRows(QModelIndex(),bufferEnd-bufferStart+deltaBottom,bufferEnd-bufferStart);
				endRemoveRows();
			} else if( deltaBottom > 0) {
				//insert new area
				beginInsertRows(QModelIndex(),bufferEnd-bufferStart,bufferEnd-bufferStart+deltaBottom);
				endInsertRows();
			}

			bufferEnd = endRow;
		}*/

		if(bufferStart > 0 && bufferEnd > 0) {
			beginRemoveRows(QModelIndex(),0,bufferEnd-bufferStart);
			endRemoveRows();
		}

		//insert new area
		if(startRow > 0 && endRow > 0) {
			beginInsertRows(QModelIndex(),0,endRow-startRow);
			endInsertRows();
		}
		bufferStart = startRow;
		bufferEnd = endRow;

		if(bufferEnd > bufferedRows) {
			expandValuebuffer(bufferEnd);
		}
	}
}

int SqlQueryModel::rowCount( const QModelIndex & parent ) const
{
	if(bufferStart > 0) {
		return bufferEnd-bufferStart+1;
	} else {
		return 0;
	}
}

int SqlQueryModel::columnCount( const QModelIndex & parent ) const
{
	return columns;
}

QVariant SqlQueryModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if(role == Qt::DisplayRole) {
		//section relative to buffer position in
		if(orientation == Qt::Horizontal) {
			if(section < columns) {
				return columnData[section];
			}
		} else if(orientation == Qt::Vertical) {
			return QVariant(QString::number(bufferStart+section));
		}
	}
	
	return invalidVariant;
}

QVariant SqlQueryModel::data( const QModelIndex & index, int role ) const
{
	if(role == Qt::DisplayRole) {
		if(index.isValid() && index.column() < columns) {
			if(bufferStart > 0 && bufferEnd > 0) {
				int queryRow = bufferStart-1+index.row();
				if(queryRow<bufferedRows) {
					return valueBuffer[queryRow*columns+ index.column()];
				}
			}
		}
	}

	return invalidVariant;
}

QModelIndex SqlQueryModel::index( int row, int column, const QModelIndex &parent ) const
{
	return createIndex(row, column);
}

QModelIndex SqlQueryModel::parent( const QModelIndex &index ) const
{
	return invalidIndex;
}

int SqlQueryModel::getStartRow()
{
	return bufferStart;
}

int SqlQueryModel::getEndRow()
{
	return bufferEnd;
}

void SqlQueryModel::expandValuebuffer( int endrow )
{
	while(bufferedRows <= endrow && sqlQuery->next()) {
		for(int i=0; i < columns; ++i) {
			const QVariant temp(sqlQuery->value(i));

			if(temp.type() < 7 ) {
				valueBuffer.append(temp.toString());
			} else if (temp.type() == QVariant::DateTime && temp.toDateTime().time().second() == 0) {
				valueBuffer.append(temp.toDate());
			} else { 
				valueBuffer.append(temp);
			}
		}
		bufferedRows++;
	}

	if(bufferedRows < endrow) {
		sqlQuery->finish();

		if(bufferStart > bufferedRows) {
			setBuffer(0,0);
		} else {
			setBuffer(bufferStart,bufferedRows);
		}
	}
}

QVector<QVariant>& SqlQueryModel::getValueBuffer()
{
	return valueBuffer;
}

QVector<QVariant>& SqlQueryModel::getColumnHeader()
{
	return columnData;
}
