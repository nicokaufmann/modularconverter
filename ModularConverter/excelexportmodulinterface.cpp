#include "excelexportmodulinterface.h"

#include "excelexportmodul.h"

ExcelExportModulInterface::ExcelExportModulInterface( BasePathModul* modul, QWidget *parent, Qt::WFlags flags )
	: StandardModulInterface(modul, parent, flags), exportInterface(new ExportFileModulInterface(this))
{
	addModulWidget(new SelectVersionModulInterface(modul->getConnectionManager(), modul->getGuiVersionRequests(), modul->getParameterMap()["Versionierung"],this));
	addModulWidget(exportInterface);
	
	QObject::connect(exportInterface->exportButton(), SIGNAL(clicked()), this, SLOT(exportFile()));
}

ExcelExportModulInterface::~ExcelExportModulInterface()
{

}

void ExcelExportModulInterface::finished( bool status )
{
	userMessageWidget->updateMessages();
}

void ExcelExportModulInterface::exportFile()
{
	QString path = QFileDialog::getSaveFileName(this,"Exportdatei angeben","","Excel Arbeitsmappe (*.xlsx);;Excel Arbeitsmappe 97-2003 (*.xls)");

	if(path.length() > 0) {
		BasePathModul* pathModul = (BasePathModul*)modul;
		pathModul->setPath(path);
		pathModul->clearUserMessages();
		emit execute();
	} else {
		QMessageBox msgBox;
		msgBox.setWindowTitle("Fehler");
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.setText("Es muss ein Pfad angegeben werden.");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.exec();
	}
}
