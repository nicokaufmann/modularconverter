#ifndef excelexportmodul_h__
#define excelexportmodul_h__

#include <QString>
#include <QVariant>
#include <QDate>
#include <QMap>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>

#include "excelexportmodulinterface.h"
#include "basepathmodul.h"

#include "xlutil.h"
#include "xl_buffered_writer.h"

class ExcelExportModul : public BasePathModul {
public:
	ExcelExportModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~ExcelExportModul();

	bool execute(int cmd=0);
	bool initialize();
	QWidget* createWidget();

	static QStringList getRequestedParameter();
private:
	bool exportSheet(XL::WorksheetsPtr sheets, QString versionId);
	bool writeSheet(XL::WorksheetPtr sheet, QSqlQuery& query, int& rows);

	VersionRequest exportVersion;
};

#endif // excelexportmodul_h__