#include "completionitemmodel.h"


CompletionItemModel::CompletionItemModel(QObject *parent)
	: QAbstractItemModel(parent)
{
}


Qt::ItemFlags CompletionItemModel::flags( const QModelIndex & index ) const
{
	return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant CompletionItemModel::data( const QModelIndex & index, int role) const
{
	if(role == Qt::DecorationRole) {
		if(index.isValid() && index.row() < completionList.size() && completionIcons.contains(completionList[index.row()].toString())) {
			return completionIcons[completionList[index.row()].toString()];
		}
	} else if(role == Qt::DisplayRole || role == Qt::EditRole) {
		if(index.isValid()) {
			if(index.row() < completionList.size()) {
				return completionList[index.row()];
			} 
		}
	}

	return invalidVariant;
}

QVariant CompletionItemModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	return invalidVariant;
}

int CompletionItemModel::rowCount( const QModelIndex & parent /*= QModelIndex() */ ) const
{
	return completionList.size();
}

int CompletionItemModel::columnCount( const QModelIndex & parent /*= QModelIndex() */ ) const
{
	return 1;
}

QModelIndex CompletionItemModel::parent( const QModelIndex &index ) const
{
	return invalidModelIndex;
}

QModelIndex CompletionItemModel::index( int row, int column, const QModelIndex &parent ) const
{
	if (parent.isValid() && parent.column() != 0) {
		return invalidModelIndex;
	} else {
		if(row < completionList.size()) {
			return createIndex(row, column);
		}
	}

	return invalidModelIndex;
}


void CompletionItemModel::setCompletionElements( SyntaxElements syntaxElements, DatabaseElements databaseElements )
{
	beginRemoveRows(invalidModelIndex, 0 , completionList.size());
	endRemoveRows();

	completionList.clear();
	completionIcons.clear();

	QStringList tables(databaseElements.getTables());

	for(int i=0; i < tables.size();++i) {
		completionList<<tables[i];
		completionIcons[tables[i]] = QIcon(":/ModularConverter/bulletpoint_t.png");

		QStringList attributes(databaseElements.getAttributes(tables[i]));

		for(int j=0; j < attributes.size();++j) {
			completionList<<attributes[j];
			completionIcons[attributes[j]] =  QIcon(":/ModularConverter/bulletpoint_a.png");
		}

		for(int j=0; j < attributes.size();++j) {
			completionList<<tables[i] + "." + attributes[j];
			completionIcons[tables[i] + "." + attributes[j]] =  QIcon(":/ModularConverter/bulletpoint_a.png");
		}
	}

	for(int i=0; i < syntaxElements.getFunctions().size();++i) {
		completionList<<syntaxElements.getFunctions()[i];
		completionIcons[syntaxElements.getFunctions()[i]] = QIcon(":/ModularConverter/bulletpoint_f.png");
	}

	for(int i=0; i < syntaxElements.getKeywords().size();++i) {
		completionList<<syntaxElements.getKeywords()[i];
		completionIcons[syntaxElements.getKeywords()[i]] = QIcon(":/ModularConverter/bulletpoint_s.png");
	}

	beginInsertRows(invalidModelIndex, 0 , completionList.size());
	endInsertRows();
}
