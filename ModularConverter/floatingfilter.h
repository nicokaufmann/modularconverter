#ifndef floatingfilter_h__
#define floatingfilter_h__

#include <QString>

#include "ui_floatingfilter.h"

class FloatingFilter  : public QWidget
{
	Q_OBJECT

public:
	FloatingFilter(int id, const QString& fieldname, QWidget *parent = 0, Qt::WFlags flags = 0);
	~FloatingFilter();

	int getId();
	QString getFieldname();
	QString getMethod();
	QString getCheckString();

public slots:
	void removeFilter();

signals:
	void removed(int id);
	void removed(FloatingFilter* label);

private:
	int id;
	QString fieldname;

	Ui::FloatingFilter ui;
};
#endif // floatingfilter_h__
