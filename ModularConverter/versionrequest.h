#ifndef versionrequest_h__
#define versionrequest_h__

#include <QString>
#include <QSharedPointer>

class VersionRequest {
public:
	VersionRequest();
	VersionRequest(QString connectionname, QString tablename);
	VersionRequest(QString connectionname, QString tablename, QString context);
	VersionRequest(const VersionRequest& obj);
	VersionRequest& operator= (const VersionRequest& obj);

	void setConnectionname(QString connectionname);
	QString connectionname();

	void setTablename(QString tablename);
	QString tablename();

	void setContext(QString context);
	QString context();

	void setVersionId(unsigned int versionId);
	unsigned int versionId();

private:
	QSharedPointer<QString> sharedConnectionname;
	QSharedPointer<QString> sharedTablename;
	QSharedPointer<QString> sharedContext;
	QSharedPointer<unsigned int> sharedVersionId;
};
#endif // versionrequest_h__