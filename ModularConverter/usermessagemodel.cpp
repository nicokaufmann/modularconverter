#include "usermessagemodel.h"

#include "completionitemmodel.h"


UserMessageModel::UserMessageModel(ConverterModul* converterModul, QObject *parent)
	: QAbstractItemModel(parent), converterModul(converterModul), warningIcon(QIcon(":/ModularConverter/warning.png")), errorIcon(QIcon(":/ModularConverter/error.png"))
{
	updateMessages();
}


Qt::ItemFlags UserMessageModel::flags( const QModelIndex & index ) const
{
	return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant UserMessageModel::data( const QModelIndex & index, int role) const
{
	if(role == Qt::DecorationRole) {
		if(index.isValid() && index.row() < warningList.count()) {
			if(warningList[index.row()] == 1) {
				return warningIcon;
			} else if(warningList[index.row()] > 1) {
				return errorIcon;
			}
		}
	} else if(role == Qt::DisplayRole || role == Qt::EditRole) {
		if(index.isValid() && index.row() < messageList.count()) {
			return messageList[index.row()];
		}
	}

	return invalidVariant;
}

QVariant UserMessageModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	return invalidVariant;
}

int UserMessageModel::rowCount( const QModelIndex & parent /*= QModelIndex() */ ) const
{

	return messageList.count();

}

int UserMessageModel::columnCount( const QModelIndex & parent /*= QModelIndex() */ ) const
{
	return 1;
}

QModelIndex UserMessageModel::parent( const QModelIndex &index ) const
{
	return invalidModelIndex;
}

QModelIndex UserMessageModel::index( int row, int column, const QModelIndex &parent ) const
{
	if (parent.isValid() && parent.column() != 0) {
		return invalidModelIndex;
	} else {
		if(row < messageList.count()) {
			return createIndex(row, column);
		}
	}

	return invalidModelIndex;
}

void UserMessageModel::setConverterModul( ConverterModul* converterModul )
{
	this->converterModul = converterModul;
	updateMessages();
}

void UserMessageModel::updateMessages()
{
	if(converterModul) {
		beginRemoveRows(invalidModelIndex, 0 , messageList.count());
		endRemoveRows();

		messageList.clear();
		warningList.clear();

		for(int i=0; i < converterModul->userMessageCount(); ++i) {
			messageList<<converterModul->getUserMessage(i);
			warningList<<converterModul->getWarningLevel(i);
		}

		beginInsertRows(invalidModelIndex, 0 , messageList.count());
		endInsertRows();
	}
	
}

