#include "excelreportmodul.h"

#include "excelexportmodul.h"

ExcelReportModul::ExcelReportModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion)
	: BasePathModul(parameterMap, versionMap, connectionManager, context, inheritVersion)
{
}

ExcelReportModul::~ExcelReportModul()
{
}

bool ExcelReportModul::initialize()
{
	loadContext(context);
	generateVersionRequests();
	return true;
}

QWidget* ExcelReportModul::createWidget()
{
	return new ExcelExportModulInterface(this);
}

QStringList ExcelReportModul::getRequestedParameter()
{
	return (QStringList()<<"Report Config"<<"Report Statements"<<"Versionierung");
}

bool ExcelReportModul::useContext()
{
	return true;
}

bool ExcelReportModul::loadContext( QString context )
{
	if(!loadReportConfig(context, reportCfg)) {
		return false;
	}

	QStringList statementIds;
	if(!getStatementIds(context,statementIds)) {
		return false;
	}

	for(int i=0; i < statementIds.size();++i) {
		statementCfgs.append(QMap<QString, QVariant>());
		
		if(!loadStatementConfig(context,statementIds[i],statementCfgs.last())) {
		}
	}

	return true;
}

void ExcelReportModul::generateVersionRequests()
{
	for(int i=0; i < statementCfgs.count(); ++i) {
		if(statementCfgs[i]["versioning.enabled"].toBool()) {
			connectionManager->getDatabaseElementsUpdater()->updateElementsBlocking(statementCfgs[i]["connectionname"].toString());
			
			DatabaseElements databaseElements(*connectionManager->getConnection(statementCfgs[i]["connectionname"].toString()).databaseElements());
			SyntaxElements syntaxElements(*connectionManager->getConnection(statementCfgs[i]["connectionname"].toString()).syntaxElements());

			SqlInterpreter interpreter(databaseElements,syntaxElements);
			StatementNode statement(0,"");
			interpreter.interpret(statementCfgs[i]["statement"].toString(),&statement);

			SyntaxAnalyzer analyzer(&statement);
			QVector<SyntaxNode*> fromNodes = analyzer.getAllDepth("from");
			for(int j=0; j < fromNodes.count(); ++j) {
				SyntaxAnalyzer fromAnalyzer(fromNodes[j]);
				QVector<SyntaxNode*> tableNodes = fromAnalyzer.getAllDepth("table");
			
				SyntaxNode* whereNode = fromAnalyzer.getCorresponding("where");

				if(!whereNode) {
					whereNode = new WhereNode(0,"where");
					fromNodes[j]->parent()->add(whereNode,fromNodes[j]->parent()->index(fromNodes[j])+1);
				}

				for(int k=0; k < tableNodes.count(); ++k) {
					//check if table has alias
					SyntaxNode* currentNode = tableNodes[k];
					QString tablealias(currentNode->value());
					QString tablename(currentNode->value());

					if(currentNode->parent() != NULL && currentNode->parent()->type()=="link") {
						if(currentNode->parent()->count() > 1) {
							tablealias = tableNodes[k]->parent()->child(1)->value();
						}
					}

					SyntaxNode* condition = new LogicalNode(0,"=");
					condition->add(new LinkNode(0,"."));
					condition->add(new FieldNode(0,":version_id_" + QString::number(k)));
					condition->children()[0]->add(new TableNode(0,tablealias));
					condition->children()[0]->add(new FieldNode(0,"version_id"));
					
					SyntaxBuilder::addConjunction(whereNode, condition);

					statementVersions[i].append(VersionRequest(statementCfgs[i]["connectionname"].toString(),tablename));
					addVersionRequestInherited(statementVersions[i].last());
				}
			}
			statementCfgs[i]["statement"] = QVariant(statement.toString());
		}
	}

	processInheritedRequests();
}

bool ExcelReportModul::execute(int cmd)
{

	XL::ApplicationPtr xlapp(XL::GetApplication()); 
	if(xlapp->good()) {
		XL::WorkbooksPtr books(xlapp->getWorkbooks());
		XL::IWorkbook* tempBook;

		if(reportCfg["template.enabled"].toBool()) {
			tempBook = books->open(reportCfg["template.path"].toString().replace("/","\\").toStdString().c_str());
		} else {
			tempBook = books->add();
		}

		if(tempBook) {
			XL::WorkbookPtr exportBook(tempBook);
			XL::WorksheetsPtr exportSheets(exportBook->sheets());

			QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 5));

			if(exportSheet(exportSheets)) {
				XL::Util::deleteEmptySheets(exportBook.get());

				if(exportBook->saveAs(exportPath.replace("/","\\").toStdString().c_str())) {
					QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 100));
					addUserMessage("Excel Report erfolgreich gespeichert",0);
					return true;
				} else {
					addUserMessage("Excel Arbeitsmappe kann nicht gespeichert werden",2);
					lastError="Excel Arbeitsmappe kann nicht gespeichert werden:\n" +QString(exportBook->getLastError()->getErrorMessage());
					return false;
				}
			} else {
				return false;
			}
		} else {
			addUserMessage("Excel Arbeitsmappe konnte nicht ge�ffnet werden",2);
			lastError="Excel Arbeitsmappe konnte nicht ge�ffnet werden:\n" +QString(books->getLastError()->getErrorMessage());
			return false;
		}
	} else {
		addUserMessage("Problem mit Excel Schnittstelle",2);
		lastError="Problem mit Excel Schnittstelle:\n" +QString(xlapp->getLastError()->getErrorMessage());
		return false;
	}
}

bool ExcelReportModul::exportSheet( XL::WorksheetsPtr sheets)
{
	for(int i=0; i < statementCfgs.count(); ++i) {
		if(connectionManager->containsConnection(statementCfgs[i]["connectionname"].toString())) {

			AcquiredConnection connection(connectionManager->acquireConnection(statementCfgs[i]["connectionname"].toString()));

			if(connection->isOpen()) {
				QSqlQuery data(*connection);
				data.setForwardOnly(true);
				data.prepare(statementCfgs[i]["statement"].toString());

				if(statementVersions.contains(i)) {
					for(int j=0; j < statementVersions[i].count(); ++j) {
						data.bindValue(j, statementVersions[i][j].versionId());
					}
				}

				if(data.exec()) {
					writeStatement(sheets,i,data);
				} else {
					addUserMessage("Abfrage fehlgeschlagen",2);
					lastError="Abfrage fehlgeschlagen:\n"+data.lastError().text() + "\nQuery:\n"+data.executedQuery();
					return false;
				}
			} else {
				addUserMessage("Problem mit Datenbankverbindung",2);
				lastError="Problem mit Datenbankverbindung:\n"+connection->lastError().text();
				return false;
			}
		}
	}

	//version synchronize
	for(int i=0; i < statementCfgs.count(); ++i) {
		for(int j=0; j < statementVersions[i].count(); ++j) {
			versionMap[statementVersions[i][j].connectionname()][statementVersions[i][j].tablename()] = statementVersions[i][j].versionId();
		}
	}

	return true;
}

bool ExcelReportModul::writeStatement( XL::WorksheetsPtr sheets, int statementId, QSqlQuery& query )
{
	//set start
	
	int row = (statementCfgs[statementId]["row"].toInt() <= 0) ? 1 : statementCfgs[statementId]["row"].toInt();
	int col = (statementCfgs[statementId]["column"].toInt() <= 0) ? 1 : statementCfgs[statementId]["column"].toInt();
	

	//get relevant indexes
	QList<int> relevantIndexes;
	{
		const QSqlRecord record(query.record());
		for(int i=0; i < record.count();++i) {

			if(record.fieldName(i) != "version_id") {
				relevantIndexes<<i;
			}
		}
	}
	

	//int nrRows = (query.size()-query.at());
	XL::WorksheetPtr sheet(sheets->getItem(1));

	XL::BufferedWriter writer(sheet.get(),1,col+relevantIndexes.size());
	XL::VariantPtr variant(XL::GetVariant());

	if(statementCfgs[statementId]["header.enabled"].toBool()) {
		for(int i=0; i < relevantIndexes.size(); ++i) {
			variant->setChar(query.record().fieldName(relevantIndexes[i]).toStdString().c_str());
			if(statementCfgs[statementId]["mapping.enabled"].toBool() && statementCfgs[statementId].contains("map." + query.record().fieldName(relevantIndexes[i]))) {
				variant->setChar( statementCfgs[statementId]["map." + query.record().fieldName(relevantIndexes[i])].toString().toStdString().c_str());
			}
			writer.setValue(row,col+i,variant.get());
		}

		//format header
		if(statementCfgs[statementId]["header.changeFormat"].toBool()) {
			XL::RangePtr cells(sheet->cells());
			XL::RangePtr headerRange(cells->getRangeFrom(col-1,row-1,1,relevantIndexes.size()));
			
			if(statementCfgs[statementId].contains("header.fontcolor") || statementCfgs[statementId].contains("header.fontname") ) {
				XL::FontPtr font(headerRange->getFont());
				if(statementCfgs[statementId].contains("header.fontcolor")) {
					XL::VariantPtr varFontColor(XL::GetVariant());
					varFontColor->setUInt(statementCfgs[statementId]["header.fontcolor"].toUInt());
					font->setColor(varFontColor.get());
				}
				if(statementCfgs[statementId].contains("header.fontname")) {
					XL::VariantPtr varFontname(XL::GetVariant());
					varFontname->setChar(statementCfgs[statementId]["header.fontname"].toString().toStdString().c_str());
					font->setName(varFontname.get());
				}
			}

			if(statementCfgs[statementId].contains("header.color")) {
				XL::InteriorPtr interior(headerRange->getInterior());
				XL::VariantPtr varInteriorColor(XL::GetVariant());
				varInteriorColor->setUInt(statementCfgs[statementId]["header.color"].toUInt());
				interior->setColor(varInteriorColor.get());
			}
		}

		row++;
	}

	int startRow = row;

	//write data
	while(query.next()) {
		const QSqlRecord record(query.record());

		for(int i=0; i < relevantIndexes.size(); ++i) {
			const QVariant val(record.value(relevantIndexes[i]));

			if(val.type() == QVariant::Date) {
				variant->setChar(val.toDate().toString("dd.MM.yyyy").toStdString().c_str());
			} else if(val.type() == QVariant::DateTime) {
				QDateTime tempTime = val.toDateTime();
				if(tempTime.toString("hh:mm:ss") == QString("00:00:00")) {
					variant->setChar(val.toDateTime().toString("dd.MM.yyyy").toStdString().c_str());
				} else {
					variant->setChar(val.toDateTime().toString("dd.MM.yyyy hh:mm:ss").toStdString().c_str());
				}
			} else {
				variant->setChar(val.toString().toStdString().c_str());
			}
			writer.setValue(row,col+i,variant.get());
		}
		row++;
	}
	writer.writeBuffer();

	//format data
	if(row > startRow) {
		if(statementCfgs[statementId]["values.changeFormat"].toBool()) {
			XL::RangePtr cells(sheet->cells());
			XL::RangePtr valueRange(cells->getRangeFrom(col-1,startRow-1,row-startRow,relevantIndexes.size()));

			if(statementCfgs[statementId].contains("values.fontcolor") || statementCfgs[statementId].contains("values.fontname") ) {
				XL::FontPtr font(valueRange->getFont());
				if(statementCfgs[statementId].contains("values.fontcolor")) {
					XL::VariantPtr varFontColor(XL::GetVariant());
					varFontColor->setUInt(statementCfgs[statementId]["values.fontcolor"].toUInt());
					font->setColor(varFontColor.get());
				}
				if(statementCfgs[statementId].contains("values.fontname")) {
					XL::VariantPtr varFontname(XL::GetVariant());
					varFontname->setChar(statementCfgs[statementId]["values.fontname"].toString().toStdString().c_str());
					font->setName(varFontname.get());
				}
			}

			if(statementCfgs[statementId].contains("values.color")) {
				XL::InteriorPtr interior(valueRange->getInterior());
				XL::VariantPtr varInteriorColor(XL::GetVariant());
				varInteriorColor->setUInt(statementCfgs[statementId]["values.color"].toUInt());
				interior->setColor(varInteriorColor.get());
			}
		}
	}

	return true;
}


bool ExcelReportModul::loadReportConfig( QString context, QMap<QString, QVariant>& cfg )
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Report Config"].first));
	QString table = parameterMap["Report Config"].second;


	if(connection->isOpen()) {
		QSqlQuery data(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		data.prepare("select ident, configvalue from " + syntax->delimitTablename(table) + " where context=:context");
		data.bindValue(0,context);

		if(data.exec()) {
			while(data.next()) {
				cfg[data.record().value("ident").toString()] = data.record().value("configvalue");
			}
			return true;
		} else {
			addUserMessage("Konfigurationsdaten konnten nicht geladen werden. Abfrage fehlgeschlagen.",2);
			lastError = "Konfigurationsdaten konnten nicht geladen werden. Abfrage fehlgeschlagen.";
			return false;
		}
	} else {
		addUserMessage("Konfigurationsdaten konnten nicht geladen werden. Problem mit Datenbankverbindung.",2);
		lastError = "Konfigurationsdaten konnten nicht geladen werden. Problem mit Datenbankverbindung.";
		return false;
	}
}

bool ExcelReportModul::getStatementIds( QString context, QStringList& statementIds )
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Report Statements"].first));
	QString table = parameterMap["Report Statements"].second;

	if(connection->isOpen()) {
		QSqlQuery data(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		data.prepare("select distinct statement_id from "+ syntax->delimitTablename(table) + " where context=:context");
		data.bindValue(0,context);
		if(data.exec()) {
			while(data.next()) {
				statementIds<<data.record().value("statement_id").toString();
			}
			return true;
		} else {
			addUserMessage("Statements konnten nicht geladen werden. Abfrage fehlgeschlagen.",2);
			lastError = "Statements konnten nicht geladen werden. Abfrage fehlgeschlagen.";
			return false;
		}
	} else {
		addUserMessage("Statements konnten nicht geladen werden. Problem mit Datenbankverbindung.",2);
		lastError = "Statements konnten nicht geladen werden. Problem mit Datenbankverbindung.";
		return false;
	}
}

bool ExcelReportModul::loadStatementConfig( QString context, QString statementId, QMap<QString, QVariant>& statementCfg )
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Report Statements"].first));
	QString table = parameterMap["Report Statements"].second;


	if(connection->isOpen()) {
		QSqlQuery data(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		data.prepare("select statementvalue, ident from "+ syntax->delimitTablename(table) + " where context=:context and statement_id=:statement_id");
		data.bindValue(0,context);
		data.bindValue(1,statementId);

		if(data.exec()) {
			while(data.next()) {
				statementCfg[data.record().value("ident").toString()] = data.record().value("statementvalue");
			}
			return true;
		} else { 
			addUserMessage("Statement konnte nicht geladen werden. Abfrage fehlgeschlagen.",2);
			lastError = "Statement konnte nicht geladen werden. Abfrage fehlgeschlagen.";
			return false;
		}
	} else {
		addUserMessage("Statement konnte nicht geladen werden. Problem mit Datenbankverbindung.",2);
		lastError = "Statement konnte nicht geladen werden. Problem mit Datenbankverbindung.";
		return false;
	}
}
