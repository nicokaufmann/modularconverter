#include "workflowtab.h"

WorkflowTab::WorkflowTab( ApplicationDatabase* applicationDatabase, ModulManager* modulManager, QWidget *parent, Qt::WFlags flags )
	: QWidget(parent,flags), applicationDatabase(applicationDatabase), workflowManager(applicationDatabase->getWorkflowManager()), modulManager(modulManager)
{
	ui.setupUi(this);

	workflowsUpdated();

	QObject::connect(ui.cbWorkflows, SIGNAL(activated(int)), this, SLOT(selectWorkflow(int)));
}

WorkflowTab::~WorkflowTab()
{
	clearModulList();
}

void WorkflowTab::execute( int index )
{
	Workflow* currentWorkflow = workflowManager->getWorkflow(ui.cbWorkflows->currentText());

	if(currentWorkflow) {
		if(windowMap.contains(currentWorkflow->getUniqueId())) {
			windowMap[currentWorkflow->getUniqueId()]->startItem(index);
			windowMap[currentWorkflow->getUniqueId()]->show();
		} else {
			WorkflowWindow* workflowWindow = new WorkflowWindow(modulManager, applicationDatabase->getConnectionManager(),currentWorkflow);
			workflowWindow->show();
			workflowWindow->startItem(index);
			windowMap.insert(currentWorkflow->getUniqueId(), workflowWindow);

			QObject::connect(workflowWindow, SIGNAL(closing(WorkflowWindow*)), this, SLOT(closing(WorkflowWindow*)));
			QObject::connect(workflowWindow, SIGNAL(statusChanged(Workflow*, int)), this, SLOT(statusChanged(Workflow*,int)));
		}
	}
}

void WorkflowTab::closing( WorkflowWindow* window )
{
	if(windowMap.contains(window->getWorkflow()->getUniqueId())) {
		windowMap.remove(window->getWorkflow()->getUniqueId());
		window->deleteLater();
	}
}

void WorkflowTab::workflowsUpdated()
{
	QVector<Workflow*> flowList = workflowManager->getWorkflowList();
	
	ApplicationUser currentUser =  applicationDatabase->getCurrentUser();

	ui.cbWorkflows->clear();
	for(int i=0; i < flowList.size(); ++i) {
		if(currentUser.hasWorkflowPermission(flowList[i]->getUniqueId())) {
			ui.cbWorkflows->addItem(flowList[i]->getName());
		}
	}

	selectWorkflow(0);
}

void WorkflowTab::selectWorkflow( int index )
{
	Workflow* currentFlow = workflowManager->getWorkflow(ui.cbWorkflows->currentText());

	if(currentFlow) {
		clearModulListLayout();
		clearModulList();

		ui.lDescription->setText(currentFlow->getDescription());

		QList<WorkflowItem*> itemList = currentFlow->getItems();
		for(int itemIdx=0; itemIdx < itemList.size();++itemIdx) {

			WorkflowElement* temp = new WorkflowElement(itemIdx,itemList[itemIdx]->getDescription() + " (" + itemList[itemIdx]->getModulName()+ ")",(!currentFlow->getForceOrder() || itemIdx==0),this);

			QObject::connect(temp, SIGNAL(execute(int)), this, SLOT(execute(int)));

			elementList.push_back(temp);
		}

		buildModulListLayout();
	}
}

void WorkflowTab::clearModulList() 
{
	// clear widgets
	for(int i=0; i < elementList.size(); ++i) {
		QObject::disconnect(elementList[i], SIGNAL(execute(int)), this, SLOT(execute(int)));

		delete elementList[i];
	}
	elementList.clear();
}

void WorkflowTab::clearModulListLayout()
{
	// clear layout
	QLayoutItem *child;
	while ((child = ui.modulList->takeAt(0)) != 0) {
		delete child;
	}
}

void WorkflowTab::buildModulListLayout()
{
	for(int i=0; i < elementList.size(); ++i) {
		elementList[i]->setIndex(i);
		ui.modulList->addWidget(elementList[i]);
	}
}

void WorkflowTab::statusChanged( Workflow* workflow, int activeIndex )
{
	if(ui.cbWorkflows->currentText() == workflow->getName()) {
		for(int i=0; (i < elementList.count()); ++i) {
			elementList[i]->setStartable(true);
			elementList[i]->setStatus((i<activeIndex));
		}
	}
}
