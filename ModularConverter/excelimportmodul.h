#ifndef excelimportmodul_h__
#define excelimportmodul_h__

#include <QString>
#include <QVariant>
#include <QDate>
#include <QMap>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QSqlError>


#include "excelimportmodulinterface.h"
#include "convertermodul.h"

#include "xlutil.h"
#include "xl_buffered_reader.h"

class ExcelImportModul : public ConverterModul {
public:
	ExcelImportModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~ExcelImportModul();

	bool execute(int cmd=0);
	QWidget* createWidget();
	
	void setPath(QString path);
	void setSheetnames(QStringList sheetNames);
	void setVersionDescription(QString versionDescription);

	static QStringList getRequestedParameter();
protected:
	bool loadInstructions(XL::WorksheetPtr sheet, QVector<int>& mappedColumns, QVector<int>& mappedTypes, QString& statement);
	bool importSheet( XL::WorksheetPtr sheet, int versionId, QVector<int>& mappedColumns, QVector<int>& mappedTypes, QString& statement );

	QString formatValueODBC(const QVariant &field, bool trimStrings);
	QString formatValue(const QVariant &field, bool trimStrings);

	int dataRow;
	int titleRow;

	QStringList sheetNames;
	QString path;
	QString versionDesc;
};

#endif // excelimportmodul_h__