#ifndef DATABASELISTDIALOG_H
#define DATABASELISTDIALOG_H

#include <QtGui/QDialog>
#include <QStringListModel>
#include <QMessageBox>

#include "ui_databaselist.h"

#include "databaseinformation.h"

#include "applicationdatabase.h"

class DatabaseListDialog : public QDialog
{
	Q_OBJECT

public:
	DatabaseListDialog(ApplicationDatabase* applicationDatabase, QWidget *parent = 0, Qt::WFlags flags = 0);
	~DatabaseListDialog();

	
public slots:
	void addDatabase();
	void removeDatabase();
	void editDatabase();
private:
	
	ApplicationDatabase* applicationDatabase;
	ConnectionManager* connectionManager;
	QStringListModel* listModel;

	Ui::DatabaseListDialog ui;
};

#endif // DATABASELISTDIALOG_H
