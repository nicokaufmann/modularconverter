#ifndef filterwindow_h__
#define filterwindow_h__


#include <QMainWindow>
#include <QMessageBox>
#include <QCloseEvent>
#include <QStringListModel>

#include "floatinglabel.h"
#include "filtergroupframe.h"
#include "ui_filterwindow.h"

#include "connectionmanager.h"
#include "syntaxitemmodel.h"

class SimpleSqlEditor;

class FilterWindow : public QMainWindow
{
	Q_OBJECT

public:
	FilterWindow(SimpleSqlEditor* sqlEditor, ConnectionManager* connectionManager, QWidget *parent = 0, Qt::WFlags flags = 0);
	~FilterWindow();

	void selectFrame(int idx);

public slots:
	void clearFilter();
	void createStatement();

	void connectionsUpdated();

	void databaseSelected(int index);
	void tableSelected(int index);

	void addField(const QModelIndex& index);
	void removeFilterGroup(FilterGroupFrame* filterGroup);
	void removeField(FloatingLabel* label);

	bool eventFilter(QObject *object, QEvent *event);
signals:
	void closing(FilterWindow* window);

protected:
	void closeEvent(QCloseEvent *event);

private:
	SimpleSqlEditor* sqlEditor;
	ConnectionManager* connectionManager;

	QList<FloatingLabel*> selectedFields;
	QList<FilterGroupFrame*> filterFrames;
	QList<QPushButton*> buttonList;

	int selectedFrame;

	QStringListModel* databaseListModel;
	SyntaxItemModel* tableListModel;
	SyntaxItemModel* attributeListModel;

	Ui::FilterWindow ui;
};
#endif // filterwindow_h__
