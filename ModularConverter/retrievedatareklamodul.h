#ifndef retrievedatareklamodul_h__
#define retrievedatareklamodul_h__

#include "retrievedatamodul.h"

class RetrieveDataReklaModul : public RetrieveDataModul {
public:
	RetrieveDataReklaModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~RetrieveDataReklaModul();

	bool execute(int cmd=0);
};
#endif // retrievedatareklamodul_h__
