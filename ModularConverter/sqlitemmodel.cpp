#include "SqlItemModel.h"

SqlItemModel::SqlItemModel(SqlInterpreter* interpreter, QObject *parent)
	: QAbstractItemModel(parent), rootNode(NULL), interpreter(interpreter)
{
	iconMap["select"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["distinct"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["top"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["where"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["delete"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["update"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["set"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["insert"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["values"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["from"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["into"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["join"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["union"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["group"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["order"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["field"]=":/ModularConverter/bulletpoint_a.png";
	iconMap["table"]=":/ModularConverter/bulletpoint_t.png";
	iconMap["text"]=":/ModularConverter/bulletpoint_a.png";
	iconMap["value"]=":/ModularConverter/bulletpoint_a.png";
	iconMap["alias"]=":/ModularConverter/bulletpoint_a.png";
	iconMap["link"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["brace"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["negate"]=":/ModularConverter/bulletpoint_l.png";
	iconMap["logical"]=":/ModularConverter/bulletpoint_l.png";
	iconMap["between"]=":/ModularConverter/bulletpoint_l.png";
	iconMap["operator"]=":/ModularConverter/bulletpoint_l.png";
	iconMap["on"]=":/ModularConverter/bulletpoint_s.png";
	iconMap["function"]=":/ModularConverter/bulletpoint_f.png";
}

SqlItemModel::~SqlItemModel()
{
}

void SqlItemModel::setRootNode( SyntaxNode* root )
{
	if(rootNode) {
		beginRemoveRows(QModelIndex(),0,0);
		endRemoveRows();
	}

	beginInsertRows(QModelIndex(),0,0);
	rootNode = root;
	endInsertRows();
}

SyntaxNode* SqlItemModel::getRootNode()
{
	return rootNode;
}

Qt::ItemFlags SqlItemModel::flags( const QModelIndex & index ) const
{
	return Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled;
}

int SqlItemModel::rowCount( const QModelIndex & parent ) const
{
	if(parent.isValid()) {
		//childnode
		SyntaxNode* node = static_cast<SyntaxNode*>(parent.internalPointer());
		if(node != NULL) {
			return node->children().size();
		}
	} else if(rootNode != NULL) {
		//parent
		return 1;
	}

	return 0;
}

int SqlItemModel::columnCount( const QModelIndex & parent) const
{
	return 1;
}

QVariant SqlItemModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	return QVariant(QString("Statement"));
}

QVariant SqlItemModel::data( const QModelIndex & index, int role) const
{
	if(role == Qt::DecorationRole) {
		if(index.isValid()) {
			SyntaxNode* node = static_cast<SyntaxNode*>(index.internalPointer());
			if(node != NULL) {
				if(iconMap.contains(node->type())) {
					return QIcon(iconMap[node->type()]);
				}
			}
		}
	} else if(role == Qt::DisplayRole || role == Qt::EditRole) {
		if(index.isValid()) {
			SyntaxNode* node = static_cast<SyntaxNode*>(index.internalPointer());
			if(node != NULL) {
				if(role == Qt::DisplayRole) {
					return QVariant(node->value() + " (" + node->type()+ ")");
				} else {
					return QVariant(node->value());
				}
				
			}
		}
	}

	return QVariant();
}

bool SqlItemModel::setData( const QModelIndex & index, const QVariant & value, int role /*= Qt::EditRole*/ )
{
	emit dataChanged(index,index);
	return true;
}

QModelIndex SqlItemModel::parent( const QModelIndex &index ) const
{
	if(index.isValid()) {
		SyntaxNode* node = static_cast<SyntaxNode*>(index.internalPointer());

		if(node != NULL && node->parent() != NULL) {
			return nodeIndex(node->parent());
		}
	}

	return QModelIndex();
}

QModelIndex SqlItemModel::index( int row, int column, const QModelIndex &parent ) const
{
	if(!parent.isValid()) {
		//rootnode?
		if(column == 0 && rootNode != NULL) {
			return createIndex(row, column, rootNode);
		}
	} else {
		//childnode
		SyntaxNode* node = static_cast<SyntaxNode*>(parent.internalPointer());

		if(node != NULL && row < node->children().size()) {
			return createIndex(row, column, node->children()[row]);
		}
	}

	return QModelIndex();
}

QModelIndex SqlItemModel::nodeIndex( SyntaxNode* node ) const
{
	if(node->parent()==NULL) {
		return createIndex(0,0,node);
	} else {
		return createIndex(node->parent()->index(node),0,node);
	}
}

void SqlItemModel::addSyntaxNode( SyntaxNode* target, SyntaxNode* node )
{
	beginInsertRows(nodeIndex(target),target->count(),target->count());
	target->add(node);
	endInsertRows();
}

void SqlItemModel::addSyntaxNode( SyntaxNode* target, SyntaxNode* node,int at )
{
	beginInsertRows(nodeIndex(target),at,at);
	target->add(node,at);
	endInsertRows();
}

void SqlItemModel::removeSyntaxNode( SyntaxNode* node )
{
	if(node->parent()!= NULL) {
		beginRemoveRows(nodeIndex(node->parent()),node->parent()->children().indexOf(node,0),node->parent()->children().indexOf(node,0));
		node->parent()->remove(node);
		endRemoveRows();
	} else {
		beginRemoveRows(QModelIndex(),0,0);
		endRemoveRows();
	}
}

Qt::DropActions SqlItemModel::supportedDropActions() const
{
	return Qt::CopyAction | Qt::MoveAction;
}

QStringList SqlItemModel::mimeTypes() const
{
	return (QStringList() << "text/plain" << "application/typeid" << "application/internalmove");
}

QMimeData* SqlItemModel::mimeData( const QModelIndexList & indexes ) const
{
	if(indexes.size() == 0) {
		return 0;
	} else {
		QMimeData* data = new QMimeData;
		QByteArray encodedData;
		QDataStream stream(&encodedData, QIODevice::WriteOnly);
		
		for(int i=0; i < indexes.size(); ++i) {
			if (indexes[i].isValid()) {
				stream<<(quint32)indexes[i].internalPointer();

				data->setText(data->text() + indexes[i].data(Qt::DisplayRole).toString());
				data->setData("application/internalmove", encodedData);
			}
		}
		return data;
	}
}

bool SqlItemModel::dropMimeData( const QMimeData * data, Qt::DropAction action, int row, int column, const QModelIndex & parent )
{
	if(action == Qt::CopyAction || action == Qt::MoveAction ) {
		if(data->hasText() && !data->hasFormat("application/internalmove")) {
			//get node from string
			operationSource = interpreter->getNode(data->text(),NULL);

			//get selected interaction node
			if(parent.isValid()) {
				operationTarget = static_cast<SyntaxNode*>(parent.internalPointer());
			} else {
				operationTarget = rootNode;
			}

			if(operationSource->needsIntermediate() && (!operationTarget || operationTarget->type()!=operationSource->intermediate()->type())) {
				operationSource = operationSource->intermediate();
			}

			if(operationTarget == NULL) {
				beginInsertRows(QModelIndex(),0,0);
				rootNode = operationSource;
				endInsertRows();
			} else {
				checkDropOptions(false);
			}
			return true;
		} else if(data->hasFormat("application/internalmove")) {
			//get node from pos
			QByteArray encodedData = data->data("application/internalmove");
			QDataStream stream(&encodedData, QIODevice::ReadOnly);

			//nasty - double check this
			quint32 tempNode;
			stream >> tempNode;
			operationSource = (SyntaxNode*)tempNode;
				
			if(operationSource != NULL) {
				if(parent.isValid()) {
					operationTarget = static_cast<SyntaxNode*>(parent.internalPointer());
				} else {
					operationTarget = rootNode;
				}

				if(operationTarget != operationSource) {
						
					checkDropOptions(true);
					return true;
				}
			}
		}
	}
	return false;
}

void SqlItemModel::setSqlInterpreter( SqlInterpreter* interpreter )
{
	this->interpreter=interpreter;
}

void SqlItemModel::checkDropOptions( bool internalMove )
{
	if(operationSource != NULL && operationTarget != NULL) {
		QMenu menu;

		if(operationTarget->accepts(operationSource) && operationTarget->hasSpace()) {
			//check if target hasSpace
			menu.addAction(QIcon(":/ModularConverter/add.png"),"unterordnen")->setData(QVariant(addNode));
		}

		if(operationTarget->parent()!=NULL && operationTarget->parent()->accepts(operationSource)) {
			//check if target->parent accepts source
			menu.addAction(QIcon(":/ModularConverter/replace.png"),"ersetzen")->setData(QVariant(replaceNode));
			menu.addAction(QIcon(":/ModularConverter/preorder.png"),"einordnen (davor)")->setData(QVariant(preorderNode));

			if( internalMove && 
				operationSource->parent() && operationSource->parent()->accepts(operationTarget)) {
					//check if internal move
					//check if source->parent accepts target
					menu.addAction(QIcon(":/ModularConverter/swap.png"),"tauschen")->setData(QVariant(swapNodes));
			}
			
			if(operationSource->accepts(operationTarget) && operationSource->hasSpace()) {
					//check if source accepts target
					//check if operation source hasSpace()
					menu.addAction(QIcon(":/ModularConverter/insert.png"),"einf�gen")->setData(QVariant(insertNode));
			}
		}
		
		if(!menu.isEmpty()) {
			QObject::connect(&menu,SIGNAL(triggered(QAction*)), this, SLOT(dropOperation(QAction*)));
			menu.exec(QCursor::pos());
			QObject::disconnect(&menu,SIGNAL(triggered(QAction*)), this, SLOT(dropOperation(QAction*)));
		}
	}
}


void SqlItemModel::dropOperation( QAction * action )
{
	if(operationSource != NULL && operationTarget != NULL) {

		if(action->data().toInt() == replaceNode) {
			//check if target->parent accepts source
			if(operationSource->parent()) {
				removeSyntaxNode(operationSource);
			}
			SyntaxNode* tempTarget = operationTarget->parent();
			int index = tempTarget->index(operationTarget);

			removeSyntaxNode(operationTarget);
			delete operationTarget;
			addSyntaxNode(tempTarget,operationSource,index);
		} else if(action->data().toInt() == preorderNode) {
			if(operationSource->parent()) {
				removeSyntaxNode(operationSource);
			}

			addSyntaxNode(operationTarget->parent(),operationSource,operationTarget->parent()->index(operationTarget));
		} else if(action->data().toInt() == swapNodes) {
			//check if source->parent accepts target
			//check if target->parent accepts source
			SyntaxNode* tempSource = operationSource->parent();
			SyntaxNode* tempTarget = operationTarget->parent();
			int sourceIndex = tempSource->index(operationSource);
			int targetIndex = tempTarget->index(operationTarget);

			removeSyntaxNode(operationSource);
			removeSyntaxNode(operationTarget);

			if(sourceIndex > targetIndex) {
				addSyntaxNode(tempTarget, operationSource, targetIndex);
				addSyntaxNode(tempSource, operationTarget, sourceIndex);
			} else {
				addSyntaxNode(tempSource, operationTarget, sourceIndex);
				addSyntaxNode(tempTarget, operationSource, targetIndex);
			}
		} else if(action->data().toInt() == addNode) {
			//check if target accepts source
			//check if target hasSpace
			if(operationSource->parent()) {
				removeSyntaxNode(operationSource);
			}

			addSyntaxNode(operationTarget,operationSource);
		} else if(action->data().toInt() == insertNode) {
			//check if target->parent accepts source
			//check if source accepts target
			//check if operation source hasSpace()
			if(operationSource->parent()) {
				removeSyntaxNode(operationSource);
			}
			SyntaxNode* tempParent = operationTarget->parent();
			removeSyntaxNode(operationTarget);
			addSyntaxNode(tempParent,operationSource);
			addSyntaxNode(operationSource,operationTarget);
		}
	}
}

void SqlItemModel::contextOperation( QAction* action )
{
	QByteArray encodedData = action->data().toByteArray();
	QDataStream stream(&encodedData, QIODevice::ReadOnly);

	int operation;
	stream >> operation;

	if(operation != noopNode) {
		quint32 tempNode;
		stream >> tempNode;
		SyntaxNode* targetNode = (SyntaxNode*)tempNode;

		if(targetNode) {
			if(operation == removeNode) {
				if(!targetNode->parent()) {
					rootNode = NULL;
				}
				removeSyntaxNode(targetNode);
				delete targetNode;
			} else if(operation == editNode) {
				QInputDialog dlg;
				dlg.setLabelText("Wert:");
				dlg.setTextValue(targetNode->value());
				if(dlg.exec() == QDialog::Accepted) {
					targetNode->setValue(dlg.textValue());
				}
			}
		}
	}
}
