#ifndef lineeditdelegate_h__
#define lineeditdelegate_h__

#include <QItemDelegate>
#include <QLineEdit>

class LineEditDelegate : public QItemDelegate
{
	Q_OBJECT
public:
	LineEditDelegate(QObject *parent = 0);

	QWidget* createEditor (QWidget * parent, const QStyleOptionViewItem& option, const QModelIndex& index ) const;
	void setEditorData(QWidget *editor, const QModelIndex &index) const;
	void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
	void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};
#endif // lineeditdelegate_h__