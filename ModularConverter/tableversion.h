#ifndef tableversion_h__
#define tableversion_h__

#include <QMap>
#include <QPair>
#include <QString>
#include <QDateTime>
#include <QStringList>

#include "dataversion.h"
#include "versionrequest.h"

#include "ui_tableversion.h"

class TableVersion : public QWidget
{
	Q_OBJECT

public:
	TableVersion(const QMap<QString,DataVersion>& versionMap, QStringList relevantVersions, VersionRequest requestedVersion, QWidget *parent = 0, Qt::WFlags flags = 0);
	~TableVersion();
	
public slots:
	void selectVersion(int index);

protected:
	const QMap<QString,DataVersion>& versionMap;
	VersionRequest requestedVersion;

	Ui::TableVersion ui;
};
#endif tableversion_h__
