#ifndef basemodulwidget_h__
#define basemodulwidget_h__

#include <QWidget>

class BaseModulWidget : public QWidget {
	Q_OBJECT
public:
	BaseModulWidget(QWidget *parent = 0, Qt::WFlags flags = 0);

	virtual void finished(bool status);
signals:
	void execute(int cmd=0);
};
#endif // basemodulwidget_h__