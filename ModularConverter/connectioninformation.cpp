#include "connectioninformation.h"

#include "connectionmanager.h"
#include <QThread>

ConnectionInformation::ConnectionInformation()
	: sharedConnectionname(new QString("")), sharedHostname(new QString("")), sharedDatabasename(new QString("")), sharedOptions(new QString("")), 
	   sharedUsername(new QString("")), sharedSyntaxname(new QString("")) , sharedPassword(new QString("")), sharedSynchronousUse(new bool), sharedUpdateTables(new bool), lastError(new QString("")), 
	   connectionLock(new QReadWriteLock), sharedDatabaseElements(new DatabaseElements())
{
	*sharedSynchronousUse = true;
	*sharedUpdateTables = true;
}

ConnectionInformation::ConnectionInformation( DriverInformation driver, QString syntaxname, QString connection, QString hostname, QString databasename, QString options, QString username, QString password, bool synchronousUse, bool updateTables )
	: driverInformation(driver), sharedSyntaxname(new QString(syntaxname)), sharedConnectionname(new QString(connection)), sharedHostname(new QString(hostname)), sharedDatabasename(new QString(databasename)), sharedOptions(new QString(options)), 
	   sharedUsername(new QString(username)), sharedPassword(new QString(password)), sharedSynchronousUse(new bool), sharedUpdateTables(new bool), lastError(new QString("")), 
	   connectionLock(new QReadWriteLock), sharedDatabaseElements(new DatabaseElements(NULL, connection))
{
	*sharedSynchronousUse = synchronousUse;
	*sharedUpdateTables = updateTables;
}

ConnectionInformation::ConnectionInformation( const ConnectionInformation& obj )
{
	driverInformation = obj.driverInformation;
	sharedSyntaxname = obj.sharedSyntaxname;
	sharedConnectionname = obj.sharedConnectionname;
	sharedHostname = obj.sharedHostname;
	sharedDatabasename = obj.sharedDatabasename;
	sharedOptions = obj.sharedOptions;
	sharedUsername = obj.sharedUsername;
	sharedPassword = obj.sharedPassword;
	sharedSynchronousUse = obj.sharedSynchronousUse;
	sharedUpdateTables = obj.sharedUpdateTables;
	sharedDatabaseElements = obj.sharedDatabaseElements;
	connectionLock = obj.connectionLock;
	lastError = obj.lastError;
}

ConnectionInformation& ConnectionInformation::operator=( const ConnectionInformation& obj )
{
	driverInformation = obj.driverInformation;
	sharedSyntaxname = obj.sharedSyntaxname;
	sharedConnectionname = obj.sharedConnectionname;
	sharedHostname = obj.sharedHostname;
	sharedDatabasename = obj.sharedDatabasename;
	sharedOptions = obj.sharedOptions;
	sharedUsername = obj.sharedUsername;
	sharedPassword = obj.sharedPassword;
	sharedSynchronousUse = obj.sharedSynchronousUse;
	sharedUpdateTables = obj.sharedUpdateTables;
	sharedDatabaseElements = obj.sharedDatabaseElements;
	connectionLock = obj.connectionLock;
	lastError = obj.lastError;

	return *this;
}

ConnectionInformation::~ConnectionInformation()
{
}

QSqlDatabase ConnectionInformation::database()
{
	const QString tempName((*sharedConnectionname) + "_" + driverInformation.alias() + "_" + QString::number((unsigned long)QThread::currentThreadId()));
	QSqlDatabase temp;

	if(QSqlDatabase::contains(tempName)) {
		temp = QSqlDatabase::database(tempName);
	} else {
		temp = QSqlDatabase::addDatabase(driverInformation.alias(),tempName);
	}

	temp.setHostName(*sharedHostname);
	temp.setDatabaseName(*sharedDatabasename);
	temp.setUserName(*sharedUsername);
	temp.setPassword(*sharedPassword);
	temp.setConnectOptions(*sharedOptions);

	return temp;
}

void ConnectionInformation::setLastError( QString lastError )
{
	this->lastError->clear();
	this->lastError->append(lastError);
}

QString ConnectionInformation::getLastError()
{
	return *lastError;
}

QReadWriteLock* ConnectionInformation::lock()
{
	return connectionLock.data();
}

DriverInformation ConnectionInformation::driver() const
{
	return driverInformation;
}

QString ConnectionInformation::syntaxname() const
{
	return *sharedSyntaxname;
}

QString ConnectionInformation::connectionname() const
{
	return *sharedConnectionname;
}

QString ConnectionInformation::hostname() const
{
	return *sharedHostname;
}

QString ConnectionInformation::databasename() const
{
	return *sharedDatabasename;
}

QString ConnectionInformation::options() const
{
	return *sharedOptions;
}

QString ConnectionInformation::username() const
{
	return *sharedUsername;
}

QString ConnectionInformation::password() const
{
	return *sharedPassword;
}

bool ConnectionInformation::synchronousUse() const
{
	return *sharedSynchronousUse;
}

void ConnectionInformation::setDriver( DriverInformation val )
{
	driverInformation = val;
}

void ConnectionInformation::setSyntaxname( QString val )
{
	*sharedSyntaxname = val;
}


void ConnectionInformation::setConnectionName( QString val )
{
	*sharedConnectionname = val;
}

void ConnectionInformation::setHostname( QString val )
{
	*sharedHostname = val;
}

void ConnectionInformation::setDatabasename( QString val )
{
	*sharedDatabasename = val;
}

void ConnectionInformation::setOptions( QString val )
{
	*sharedOptions = val;
}

void ConnectionInformation::setUsername( QString val )
{
	*sharedUsername = val;
}

void ConnectionInformation::setPassword( QString val )
{
	*sharedPassword = val;
}

void ConnectionInformation::setSynchronousUse( bool val )
{
	*sharedSynchronousUse = val;
}

bool ConnectionInformation::updateTables() const
{
	return *sharedUpdateTables;
}

void ConnectionInformation::setUpdateTables( bool val )
{
	*sharedUpdateTables = val;
}

DatabaseElements* ConnectionInformation::databaseElements() const
{
	return sharedDatabaseElements.data();
}

SyntaxElements* ConnectionInformation::syntaxElements() const
{
	return ConnectionManager::getSyntax(*sharedSyntaxname);
}
