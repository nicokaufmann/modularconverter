#include "mergeretrievesmodul.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QStringBuilder>

MergeRetrievesModul::MergeRetrievesModul( ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion )
	: ConverterModul(parameterMap, versionMap, connectionManager, context, inheritVersion),targetVersion(parameterMap["Zieltabelle"].first, parameterMap["Zieltabelle"].second,"Zielversion")
{
}

MergeRetrievesModul::~MergeRetrievesModul()
{

}

bool MergeRetrievesModul::initialize()
{
	addVersionRequestInherited(targetVersion);
	processInheritedRequests();

	return true;
}

QStringList MergeRetrievesModul::getRequestedParameter()
{
	return (QStringList()<<"Zieltabelle"<<"Versionierung");
}

QWidget* MergeRetrievesModul::createWidget()
{
	return new ExecuteVersionModulInterface(this);
}

bool MergeRetrievesModul::execute(int cmd)
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Zieltabelle"].first));
	QString targetTable = parameterMap["Zieltabelle"].second;

	if(connection->isOpen()) {
		//check if field exists
		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		QList<int> versions;
		getVersionList(versions);

		if(versions.count() > 0) {
			const QSqlRecord record(connection->record(targetTable));
			if(record.contains("version_id") && record.contains("unique_id")) {

				const QString versionStr(QString::number(generateVersion(parameterMap["Versionierung"].first,parameterMap["Versionierung"].second,"Merge Retrieves basierend auf v. " + QString::number(targetVersion.versionId()))));

				//build fieldsstring
				QString fields("");
				for(int i=0; i < record.count(); ++i) {
					if(record.fieldName(i) != "version_id") {
						fields+=syntax->delimitFieldname(record.fieldName(i)) + ",";
					}
				}
				fields.chop(1);

				QSqlQuery insertData(*connection);
				insertData.setForwardOnly(true);

				const QString insertStatement("insert into " % syntax->delimitTablename(targetTable) % " (" % fields % ", version_id) " );

				//insert matched
				for(int i=0; i < versions.count(); ++i) {
					const QString selectMatchedStatement("select " % fields % ", "  % versionStr % " from " % syntax->delimitTablename(targetTable) % " where version_id=?");

					insertData.prepare(insertStatement % selectMatchedStatement);
					insertData.bindValue(0,versions[i]);

					if(!insertData.exec()) {
						addUserMessage("Abfrage fehlgeschlagen. Datens�tze konnten nicht zusammengefasst werden.",2);
						lastError="Abfrage fehlgeschlagen:\n"+insertData.lastError().text();
						return false;
					}

					QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, (100.0f/versions.count()*(i+1))));
				}

				//insert not matched
				const QString selectNotMatchedStatement("select distinct unique_id from " % syntax->delimitTablename(targetTable) % " where version_id=?");
				const QString selectStatement("select " % fields % ", "  % versionStr % " from " % syntax->delimitTablename(targetTable) % " where version_id=? and not unique_id in (" % selectNotMatchedStatement % ")");

				insertData.prepare(insertStatement % selectStatement);
				insertData.bindValue(0,targetVersion.versionId());
				insertData.bindValue(1,versionStr);

				if(!insertData.exec()) {
					addUserMessage("Abfrage fehlgeschlagen. Datens�tze konnten nicht zusammengefasst werden.",2);
					lastError="Abfrage fehlgeschlagen:\n"+insertData.lastError().text();
					return false;
				}
				
				QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, (100)));

				versionMap[parameterMap["Zieltabelle"].first][targetTable] = versionStr.toInt();

				return true;
			} else {
				addUserMessage("Pflichtfelder version_id + unique_id konnten nicht gefunden werden.",2);
				lastError="Pflichtfelder version_id + unique_id konnten nicht gefunden werden.";
				return false;
			}	
		} else {
			addUserMessage("Keine Versionen f�r Merge gefunden.",2);
			lastError="Keine Versionen f�r Merge gefunden.";
			return false;
		}
	} else {
		addUserMessage("Problem mit Datenbankverbindung.",2);
		lastError="Problem mit Datenbankverbindung:\n"+connection->lastError().text();
		return false;
	}
	return true;
}

void MergeRetrievesModul::getVersionList( QList<int>& versions )
{
	const QString connectionName(parameterMap["Zieltabelle"].first);
	const QString matchStr(parameterMap["Zieltabelle"].second+"#R");

	if(versionMap.contains(connectionName)) {
		const QStringList keys(versionMap[connectionName].keys());
		for(int i=0; i < keys.count(); ++i) {
			if(keys[i].indexOf(matchStr,0) == 0) {
				versions.append(versionMap[connectionName][keys[i]]);
			}
		}
	}
}
