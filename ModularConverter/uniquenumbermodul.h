#ifndef uniquenumbermodul_h__
#define uniquenumbermodul_h__

#include "convertermodul.h"
#include "executeversionmodulinterface.h"


class UniqueNumberModul : public ConverterModul{
public:
	UniqueNumberModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~UniqueNumberModul();

	bool execute(int cmd=0);
	bool initialize();
	QWidget* createWidget();

	static bool useContext();
	static QStringList getRequestedParameter();

protected:
	virtual int getUniqueId(int count);

	VersionRequest targetVersion;
};

class UniqueNumberNoConfigModul : public UniqueNumberModul {
public:
	UniqueNumberNoConfigModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion) 
		: UniqueNumberModul(parameterMap, versionMap, connectionManager, context, inheritVersion) {
	}

	static QStringList getRequestedParameter() {
		return (QStringList()<<"Zieltabelle"<<"Versionierung");
	}

	static bool useContext() {
		return false;
	}

protected:
	virtual int getUniqueId(int count) {
		return 1;
	}
};


#endif // uniquenumbermodul_h__