#ifndef mergeretrievesreklamodul_h__
#define mergeretrievesreklamodul_h__

#include "mergeretrievesmodul.h"

class MergeRetrievesReklaModul : public MergeRetrievesModul {
public:
	MergeRetrievesReklaModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~MergeRetrievesReklaModul();

	bool execute(int cmd=0);
};
#endif // mergeretrievesreklamodul_h__
