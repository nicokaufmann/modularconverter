#ifndef dataentrydialog_h__
#define dataentrydialog_h__

#include <QtGui/QDialog>
#include <QLineEdit>
#include <QLabel>

#include "ui_dataentry.h"

class DataEntryDialog : public QDialog
{
	Q_OBJECT

public:
	DataEntryDialog(QStringList fields, QWidget *parent = 0, Qt::WFlags flags = 0);
	~DataEntryDialog();

	QMap<QString,QString> getValueMap();

public slots:

private:

	Ui::DataEntryDialog ui;
};
#endif // dataentrydialog_h__
