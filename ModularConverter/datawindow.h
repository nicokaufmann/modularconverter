
#ifndef datawindow_h__
#define datawindow_h__

#include <QMainWindow>
#include <QMessageBox>
#include <QClipboard>
#include <QScrollBar>
#include <QCloseEvent>
#include <QtConcurrentRun>
#include <QFuture>
#include <QFutureWatcher>

#include "ui_datawindow.h"

#include "sqlquerymodel.h"
#include "connectionmanager.h"
#include "sqlinterpreter.h"
#include "syntaxanalyzer.h"

class DataWindow : public QMainWindow
{
	Q_OBJECT

public:
	DataWindow(ConnectionManager* connectionManager, QString connectionname, QString query, QWidget *parent = 0, Qt::WFlags flags = 0);
	~DataWindow();

	void init();
public slots:
	void initializationFinished();
	void changeSection();
	void countRecords();
	void prepareJump();
	void valueChanged(int val);
	void copyData();
	void showInExcel();
	void excelExportFinished();

signals:
	void closing(DataWindow* window);

protected:
	void closeEvent(QCloseEvent *event);
	bool queryInitialization();
	bool excelExport(QString path);

private:
	AcquiredConnection connection;

	QString initError;
	QString exportError;
	QString exportPath;

	QString connectionname;
	QString query;

	QSqlQuery* dbQuery;
	ConnectionManager* connectionManager;

	QFutureWatcher<bool> watchQueryInitialization;
	QFutureWatcher<bool> watchExcelExport;

	QAction* copyAction;
	QScrollBar* vScroll;
	Ui::DataWindow ui;
};
#endif // datawindow_h__