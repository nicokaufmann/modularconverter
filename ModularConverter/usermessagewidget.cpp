#include "usermessagewidget.h"


UserMessageWidget::UserMessageWidget( ConverterModul* modul, QWidget *parent, Qt::WFlags flags )
	: QWidget(parent, flags)
{
	ui.setupUi(this);

	ui.messageView->setModel(new UserMessageModel(modul, this));
}

UserMessageWidget::~UserMessageWidget()
{

}

void UserMessageWidget::updateMessages()
{
	UserMessageModel* modul = (UserMessageModel*)ui.messageView->model();
	modul->updateMessages();
}
