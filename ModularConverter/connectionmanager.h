#ifndef connectionmanager_h__
#define connectionmanager_h__

#include <QString>
#include <QStringList>
#include <QMap>
#include <QList>
#include <QMutex>

#include "connectioninformation.h"
#include "acquiredconnection.h"
#include "syntaxelements.h"
#include "databaseelementsupdater.h"

class ApplicationDatabase;

class ConnectionManager {
public:
	ConnectionManager(ApplicationDatabase* applicationDatabase);
	~ConnectionManager();

	void clearConnections();
	bool containsConnection(const QString connectionname);
	QStringList connectionList();

	void addConnection(ConnectionInformation connection);
	void removeConnection(ConnectionInformation connection);
	ConnectionInformation getConnection(const QString connectionname);
	
	AcquiredConnection acquireConnection(const QString connectionname);
	void checkRequests(const QString connectionname, const QString drivername);

	static DriverInformation getDriver(const QString alias);
	static QStringList getDriverList();
	
	static SyntaxElements* getSyntax(const QString syntax);
	static QStringList getSyntaxList();

	ApplicationDatabase* getApplicationDatabase();
	DatabaseElementsUpdater* getDatabaseElementsUpdater();
private:
	static QMap<QString, DriverInformation> initializeDrivers();
	static QMap<QString, SyntaxElements*> initializeSyntax();

	void removeInactiveConnections(const QString& connectionname);
	void removeInactiveDrivers(const QString& drivername);


	ApplicationDatabase* applicationDatabase;
	DatabaseElementsUpdater* databaseElementsUpdater;

	QMutex aquireInvocationLock;

	QStringList connectionRequests;
	QStringList driverRequests;

	QMap<unsigned long, QMap<QString, AcquiredConnection>> activeConnectionsMap;
	QMap<QString, ConnectionInformation> connectionMap;
	QStringList removedConnections;

	static QMap<QString, DriverInformation> driverMap;
	static QMap<QString, SyntaxElements*> syntaxMap;
};
#endif // connectionmanager_h__

