#ifndef retrievedatastageconfigmodulinterface_h__
#define retrievedatastageconfigmodulinterface_h__

#include <QString>
#include <QMap>

#include "comboboxdelegate.h"
#include "lineeditdelegate.h"
#include "ui_retrievedatastageconfigmodulinterface.h"

class RetrieveDataConfigModul;

class RetrieveDataStageConfigModulInterface : public QWidget
{
	Q_OBJECT

public:
	RetrieveDataStageConfigModulInterface(int index, int stage, RetrieveDataConfigModul* rdcm, const QMap<QString, QPair<QString, QString>>& stageMap, QWidget *parent = 0, Qt::WFlags flags = 0);
	~RetrieveDataStageConfigModulInterface();

	int getIndex();
	int getStage();
	void getStageMapping(QMap<QString, QPair<QString, QString>>& stageMap);
	void setStageMapping(const QMap<QString, QPair<QString, QString>>& stageMap);

public slots:
	void removeStage();

signals:
	void removeStage(int id);
private:
	int index;
	ComboBoxDelegate comboDelegate;
	LineEditDelegate lineDelegate;
	Ui::RetrieveDataStageConfigModulInterface ui;
};
#endif // retrievedatastageconfigmodulinterface_h__