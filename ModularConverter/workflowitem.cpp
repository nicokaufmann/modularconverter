#include "workflowitem.h"

WorkflowItem::WorkflowItem( int uniqueId, QString modulName )
	: uniqueId(uniqueId), modulName(modulName), inheritVersion(false)
{
}

WorkflowItem::WorkflowItem()
{
	uniqueId=0;
	modulName="";
}

WorkflowItem::~WorkflowItem()
{

}

int WorkflowItem::getUniqueId()
{
	return uniqueId;
}

void WorkflowItem::setUniqueId( int id )
{
	uniqueId = id;
}

QString WorkflowItem::getDescription()
{
	return description;
}

void WorkflowItem::setDescription( QString description )
{
	this->description = description;
}

QString WorkflowItem::getModulName()
{
	return modulName;
}

void WorkflowItem::setModulName( QString modulName )
{
	this->modulName = modulName;
}

ParameterMap& WorkflowItem::getParameterMap()
{
	return params;
}

void WorkflowItem::setParameterMap( ParameterMap params )
{
	this->params = params;
}

QString WorkflowItem::getModulContext()
{
	return modulContext;
}

void WorkflowItem::setModulContext( QString modulContext )
{
	this->modulContext = modulContext;
}

bool WorkflowItem::getInheritVersion()
{
	return inheritVersion;
}

void WorkflowItem::setInheritVersion( bool inheritVersion )
{
	this->inheritVersion = inheritVersion;
}


