#include "workflowwindow.h"

WorkflowWindow::WorkflowWindow(ModulManager* modulManager, ConnectionManager* connectionManager, Workflow* workflow, QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags), modulManager(modulManager), connectionManager(connectionManager), workflow(workflow), currentModul(0), currentIndex(0)
{
	ui.setupUi(this);
	ui.bNextModul->setEnabled(false);
	this->setWindowTitle("Workflow - " + workflow->getName());

	QList<WorkflowItem*> itemList = workflow->getItems();
	for(int itemIdx=0; itemIdx < itemList.size();++itemIdx) {
		WorkflowElement* temp = new WorkflowElement(itemIdx,itemList[itemIdx]->getDescription() + " (" + itemList[itemIdx]->getModulName()+ ")",(!workflow->getForceOrder() || itemIdx==0),this);
		ui.workflowContent->addWidget(temp);
		
		QObject::connect(temp, SIGNAL(execute(int)), this, SLOT(startItem(int)));
	}

	ui.bAbort->hide();
	ui.lCheck->hide();
	ui.lModuldescription->hide();

	QObject::connect(&watchModulInitialization, SIGNAL(finished()),this, SLOT(initializationFinished()));
	QObject::connect(&watchModulExecution, SIGNAL(finished()),this, SLOT(executionFinished()));
	QObject::connect(ui.bNextModul, SIGNAL(clicked()),this, SLOT(nextItem()));
	QObject::connect(ui.bAbort, SIGNAL(clicked()),this, SLOT(abortExecution()));
}

WorkflowWindow::~WorkflowWindow()
{
	if(watchModulExecution.future().isRunning()) {
		watchModulExecution.future().waitForFinished();
	}

	if(watchModulInitialization.future().isRunning()) {
		watchModulInitialization.future().waitForFinished();
	}

	if(currentModul) {
		delete currentModul;
	}
}

void WorkflowWindow::startItem( int index )
{
	if(!watchModulExecution.future().isRunning()) {
		if(index < workflow->count()) {
			currentIndex = index;

			WorkflowItem* item = workflow->getItem(index);
			if(modulManager->contains(item->getModulName())) {
				ConverterModul* tempModul;

				if(currentModul) {
					tempModul = modulManager->modul(item->getModulName(),item->getParameterMap(),currentModul->getVersionMap(),connectionManager, item->getModulContext(),item->getInheritVersion());

					QObject::disconnect(currentModul->getWidget(), SIGNAL(execute(int)), this, SLOT(execute(int)));
					delete currentModul;
				} else {
					tempModul = modulManager->modul(item->getModulName(),item->getParameterMap(),VersionMap(),connectionManager,item->getModulContext(),item->getInheritVersion());
				}
		
				currentModul = tempModul;

				//remove old
				for(int i=0; i < ui.modulContent->count(); i++) {
					ui.modulContent->itemAt(i)->widget()->hide();
					ui.modulContent->removeItem(ui.modulContent->itemAt(i));
				}

				ui.lCheck->show();
				ui.lModuldescription->show();
				ui.lCheck->setEnabled(false);
				ui.lModuldescription->setText(item->getDescription());

				ui.modulContent->addWidget(new QLabel("Initialisiere Modul ...", this));

				init();
			}
		}
	} else {
		//message
	}
}

void WorkflowWindow::closeEvent( QCloseEvent *event )
{
	if(watchModulExecution.future().isRunning()) {
		event->ignore();
	} else {
		emit closing(this);
	}
}

Workflow* WorkflowWindow::getWorkflow()
{
	return workflow;
}

void WorkflowWindow::init()
{
	if(currentModul) {
		QFuture<bool> future = QtConcurrent::run(this, &WorkflowWindow::modulInitialization );
		watchModulInitialization.setFuture(future);
	}
}

void WorkflowWindow::initializationFinished()
{
	if(watchModulInitialization.future().result()) {
		//remove old
		for(int i=0; i < ui.modulContent->count(); i++) {
			ui.modulContent->itemAt(i)->widget()->hide();
			ui.modulContent->removeItem(ui.modulContent->itemAt(i));
		}

		ui.modulContent->addWidget(currentModul->getWidget());
		QObject::connect(currentModul->getWidget(), SIGNAL(execute(int)), this, SLOT(execute(int)));

		if(currentModul->executeImmediatly()) {
			execute(0);
		}
	} else {
		QMessageBox msgBox;
		msgBox.setWindowTitle("Fehler");
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.setText("Problem beim initialisieren des Moduls.");
		msgBox.setDetailedText(currentModul->getLastError());
		msgBox.setIcon(QMessageBox::Critical);
		msgBox.exec();

	}
}

void WorkflowWindow::execute(int cmd)
{
	if(currentModul) {
		QFuture<bool> future = QtConcurrent::run(this, &WorkflowWindow::modulExecution,cmd);
		watchModulExecution.setFuture(future);
		ui.bAbort->show();
	}
}

void WorkflowWindow::executionFinished()
{
	BaseModulWidget* modulWidget = (BaseModulWidget*)currentModul->getWidget();
	modulWidget->finished(watchModulExecution.future().result());

	ui.bAbort->hide();

	if(watchModulExecution.future().result()) {
		
		ui.lCheck->setEnabled(true);
		//progress
		WorkflowElement* tempStatus = (WorkflowElement*)ui.workflowContent->itemAt(currentIndex)->widget();
		tempStatus->setStatus(true);
		if(currentIndex < workflow->count()-1) {
			if(workflow->getForceOrder()) {
				WorkflowElement* tempStart = (WorkflowElement*)ui.workflowContent->itemAt(currentIndex+1)->widget();
				tempStart->setStartable(true);
			}
			
			ui.bNextModul->setEnabled(true);
			emit statusChanged(workflow, currentIndex+1);
		} else {
			emit statusChanged(workflow, currentIndex+1);
		}
	} else {
		if(currentModul > 0) {
			QMessageBox msgBox;
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::Ok);
			msgBox.setIcon(static_cast<QMessageBox::Icon>(currentModul->getErrorType()));

			if(currentModul->getErrorType()==1) {
				msgBox.setWindowTitle("Information");
				msgBox.setText(currentModul->getLastError());
			} else if(currentModul->getErrorType()==2) {
				msgBox.setWindowTitle("Warnung");
				msgBox.setText(currentModul->getLastError());
			} else if(currentModul->getErrorType() > 2) {
				msgBox.setWindowTitle("Fehler");
				msgBox.setText("Problem beim ausf�hren des Moduls.");
				msgBox.setDetailedText(currentModul->getLastError());
			}

			msgBox.exec();
		}
	}
}

bool WorkflowWindow::modulExecution(int cmd)
{
	return currentModul->execute(cmd);
}

bool WorkflowWindow::modulInitialization()
{
	return currentModul->initialize();
}

void WorkflowWindow::nextItem()
{
	ui.bNextModul->setEnabled(false);
	startItem(currentIndex+1);
}

void WorkflowWindow::abortExecution()
{
	if(watchModulExecution.future().isRunning() && currentModul) {
		currentModul->abort();
	}
}
