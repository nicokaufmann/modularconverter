#ifndef mergeretrievesmodul_h__
#define mergeretrievesmodul_h__

#include "convertermodul.h"
#include "executeversionmodulinterface.h"

class MergeRetrievesModul : public ConverterModul{
public:
	MergeRetrievesModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~MergeRetrievesModul();

	bool execute(int cmd=0);
	bool initialize();
	QWidget* createWidget();

	static QStringList getRequestedParameter();
protected:
	void getVersionList(QList<int>& versions);

	VersionRequest targetVersion;
};
#endif // mergeretrievesmodul_h__
