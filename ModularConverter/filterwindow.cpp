#include "filterwindow.h"

#include "simplesqleditor.h"
#include <QStringBuilder>

FilterWindow::FilterWindow( SimpleSqlEditor* sqlEditor, ConnectionManager* connectionManager, QWidget *parent, Qt::WFlags flags)
	: sqlEditor(sqlEditor), connectionManager(connectionManager)
{
	ui.setupUi(this);

	ui.fieldFrame->installEventFilter(this);
	ui.fieldGrid->setRowMinimumHeight(0,100);

	databaseListModel = new QStringListModel(connectionManager->connectionList());
	ui.cbDatabases->setModel(databaseListModel);

	tableListModel = new SyntaxItemModel(QStringList(),":/ModularConverter/bulletpoint_t.png","table",this);
	ui.cbTables->setModel(tableListModel);

	attributeListModel = new SyntaxItemModel(QStringList(),":/ModularConverter/bulletpoint_a.png","attribute",this);
	ui.attributeList->setModel(attributeListModel);

	filterFrames.append(new FilterGroupFrame());
	filterFrames.last()->installEventFilter(this);
	ui.filterLayout->addWidget(filterFrames.last());
	QObject::connect(filterFrames.last(), SIGNAL(empty(FilterGroupFrame*)),this, SLOT(removeFilterGroup(FilterGroupFrame*)));

	selectFrame(-1);

	QObject::connect(ui.attributeList, SIGNAL(doubleClicked(const QModelIndex&)),this, SLOT(addField(const QModelIndex&)));
	QObject::connect(ui.cbDatabases, SIGNAL(activated(int)),this, SLOT(databaseSelected(int)));
	QObject::connect(ui.cbTables, SIGNAL(activated(int)),this, SLOT(tableSelected(int)));
	QObject::connect(ui.actionClear, SIGNAL(triggered()), this, SLOT(clearFilter()));
	QObject::connect(ui.actionStatement, SIGNAL(triggered()), this, SLOT(createStatement()));
	QObject::connect(connectionManager->getApplicationDatabase(), SIGNAL(databasesUpdated()),this, SLOT(connectionsUpdated()));
}

FilterWindow::~FilterWindow()
{
	for(int i=0; i < selectedFields.count();++i) {
		delete selectedFields[i];
	}
}

void FilterWindow::closeEvent( QCloseEvent *event )
{
	emit closing(this);
}

void FilterWindow::connectionsUpdated()
{
	databaseListModel->setStringList(connectionManager->connectionList());
}

void FilterWindow::databaseSelected( int index )
{
	if(connectionManager->containsConnection(ui.cbDatabases->currentText())) {
		DatabaseElements databaseElements(*connectionManager->getConnection(ui.cbDatabases->currentText()).databaseElements());
		
		tableListModel->setItemList(databaseElements.getTables());
		ui.cbTables->setCurrentIndex(0);
		tableSelected(0);
	}
}

void FilterWindow::tableSelected( int index )
{
	if(connectionManager->containsConnection(ui.cbDatabases->currentText())) {
		DatabaseElements databaseElements(*connectionManager->getConnection(ui.cbDatabases->currentText()).databaseElements());
		
		if(databaseElements.getTables().contains(ui.cbTables->currentText())) {
			QStringList attributes(databaseElements.getAttributes(ui.cbTables->currentText()));
			attributes.push_front("*");

			attributeListModel->setItemList(attributes);
		}
	}
}

void FilterWindow::selectFrame( int idx )
{
	selectedFrame = idx;

	for(int i=0; i < filterFrames.count(); ++i) {
		if(i != selectedFrame) {
			filterFrames[i]->setFrameShadow(QFrame::Raised);
		} else {
			filterFrames[i]->setFrameShadow(QFrame::Sunken);
		}
	}

	if(selectedFrame > -1) {
		ui.fieldFrame->setFrameShadow(QFrame::Raised);
	} else {
		ui.fieldFrame->setFrameShadow(QFrame::Sunken);
	}
}

void FilterWindow::clearFilter()
{
	while(selectedFields.count() > 0) {
		removeField(selectedFields[0]);
	}

	for(int i=0; i < buttonList.count(); ++i) {
		ui.filterLayout->removeWidget(buttonList[i]);
		delete buttonList[i];
		buttonList.removeAt(i);
	}

	for(int i=1; i < filterFrames.count(); ++i) {
		ui.filterLayout->removeWidget(filterFrames[i]);
		delete filterFrames[i];
		filterFrames.removeAt(i);
	}
	
	filterFrames[0]->clearFilter();
	selectFrame(-1);
}

void FilterWindow::createStatement()
{
	if(connectionManager->containsConnection(ui.cbDatabases->currentText())) {
		DatabaseElements databaseElements(*connectionManager->getConnection(ui.cbDatabases->currentText()).databaseElements());
		SyntaxElements* syntaxElements = connectionManager->getConnection(ui.cbDatabases->currentText()).syntaxElements();

		QString statement("SELECT ");

		for(int i=0; i < selectedFields.count(); ++i) {
			if(selectedFields[i]->getLabelText() == "*") {
				statement+=selectedFields[i]->getLabelText();
			} else {
				statement+=syntaxElements->fieldLeadDelimiter() % selectedFields[i]->getLabelText() % syntaxElements->fieldTrailDelimiter();
			}

			if(i < selectedFields.count()-1) {
				statement+=", ";
			}
		}

		statement+=" FROM " % syntaxElements->delimitTablename(ui.cbTables->currentText()) % " WHERE ";

		for(int i=0; i < filterFrames.count(); ++i) {
			if(filterFrames[i]->filterCount() > 0) {
				statement+="(" % filterFrames[i]->filter(syntaxElements ,databaseElements.getAttributes(ui.cbTables->currentText())) % ")";
			}

			if(i < filterFrames.count()-1 && filterFrames[i+1]->filterCount() > 0) {
				statement+=" " % buttonList[i]->text() % " ";
			}
		}

		sqlEditor->addStatement(ui.cbDatabases->currentText(), statement);
		sqlEditor->show();
		QApplication::setActiveWindow(sqlEditor);
	}

}

bool FilterWindow::eventFilter( QObject *object, QEvent *event )
{
	if(event->type() == QEvent::MouseButtonRelease) {
		if(object == ui.fieldFrame) {
			selectFrame(-1);
		} else if(filterFrames.contains(dynamic_cast<FilterGroupFrame*>(object))) {
			selectFrame(filterFrames.indexOf(dynamic_cast<FilterGroupFrame*>(object)));
		} else {
			QPushButton* tempButton = dynamic_cast<QPushButton*>(object);
			if(buttonList.contains(tempButton)) {
				if(tempButton->text()!="AND") {
					tempButton->setText("AND");
				} else {
					tempButton->setText("OR");
				}
			}
		}
	}
	return false;
}

void FilterWindow::addField( const QModelIndex& index )
{
	if(selectedFrame > -1) {
		//filter
		filterFrames[selectedFrame]->addFilter(index.data().toString());

		if(filterFrames[selectedFrame]->filterCount() == 1) {
			//new button
			buttonList.push_back(new QPushButton("AND"));
			buttonList.last()->installEventFilter(this);
			ui.filterLayout->addWidget(buttonList.last());

			//new frame
			filterFrames.append(new FilterGroupFrame());
			filterFrames.last()->installEventFilter(this);
			ui.filterLayout->addWidget(filterFrames.last());
			filterFrames.last()->setMinimumHeight(100);
			QObject::connect(filterFrames.last(), SIGNAL(empty(FilterGroupFrame*)),this, SLOT(removeFilterGroup(FilterGroupFrame*)));
		}

	} else {
		//fields
		selectedFields.append(new FloatingLabel(0,index.data().toString()));
		QObject::connect(selectedFields.last(), SIGNAL(removed(FloatingLabel*)),this, SLOT(removeField(FloatingLabel*)));
		ui.fieldGrid->addWidget(selectedFields.last(),ui.fieldGrid->count()/3,ui.fieldGrid->count()%3);
		if(selectedFields.count() > 3) {
			ui.fieldGrid->setRowMinimumHeight(0,0);
		}
	}
}

void FilterWindow::removeField( FloatingLabel* label )
{
	if(selectedFields.contains(label)) {
		for(int i=0; i < selectedFields.count();++i) {
			ui.fieldGrid->removeWidget(selectedFields[i]);
		}
		
		selectedFields.removeOne(label);
		QObject::disconnect(label, SIGNAL(removed(FloatingLabel*)),this, SLOT(removeField(FloatingLabel*)));
		delete label;

		for(int i=0; i < selectedFields.count();++i) {
			ui.fieldGrid->addWidget(selectedFields[i],ui.fieldGrid->count()/3,ui.fieldGrid->count()%3);
		}
		if(selectedFields.count() <= 3) {
			ui.fieldGrid->setRowMinimumHeight(0,100);
		}
	}
}

void FilterWindow::removeFilterGroup( FilterGroupFrame* filterGroup )
{
	if(filterFrames.contains(filterGroup)) {
		for(int i=1; i < filterFrames.count(); ++i ) {
			if(filterFrames[i]->filterCount()==0 && (filterFrames[i-1]->filterCount()==0 || i < filterFrames.count()-1)) {
				//remove button
				ui.filterLayout->removeWidget(buttonList[i-1]);
				delete buttonList[i-1];
				buttonList.removeAt(i-1);
				
				//remove filtergroup
				QObject::disconnect(filterFrames[i], SIGNAL(empty(FilterGroupFrame*)),this, SLOT(removeFilterGroup(FilterGroupFrame*)));
				ui.filterLayout->removeWidget(filterFrames[i]);
				delete filterFrames[i];
				filterFrames.removeAt(i);
				--i;
			}
		}
	}
}
