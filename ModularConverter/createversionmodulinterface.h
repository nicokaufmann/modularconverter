#ifndef createversionmodulinterface_h__
#define createversionmodulinterface_h__

#include <QMessageBox>
#include <QFileDialog>

#include "standardmodulinterface.h"
#include "importfilemodulinterface.h"

class CreateVersionModul;

class CreateVersionModulInterface : public StandardModulInterface 
{
	Q_OBJECT

public:
	CreateVersionModulInterface(CreateVersionModul* modul, QWidget *parent = 0, Qt::WFlags flags = 0);
	~CreateVersionModulInterface();

	void finished(bool status);
public slots:
	void createVersion();

signals:
	void execute(int cmd=0);

private:
	ImportFileModulInterface* importInterface;
};
#endif // createversionmodulinterface_h__