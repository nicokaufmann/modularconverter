#ifndef createversionmodul_h__
#define createversionmodul_h__

#include <QString>
#include <QVariant>
#include <QDate>
#include <QMap>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QSqlError>


#include "createversionmodulinterface.h"
#include "convertermodul.h"

#include "xlutil.h"
#include "xl_buffered_reader.h"

class CreateVersionModul : public ConverterModul {
public:
	CreateVersionModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~CreateVersionModul();

	bool execute(int cmd=0);
	QWidget* createWidget();

	void setVersionDescription(QString versionDescription);

	static QStringList getRequestedParameter();
protected:
	QString versionDesc;
};
#endif // createversionmodul_h__

