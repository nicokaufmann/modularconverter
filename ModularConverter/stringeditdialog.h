#ifndef stringeditdialog_h__
#define stringeditdialog_h__

#include <QtGui/QDialog>
#include <QFileDialog>
#include <QListWidgetItem>
#include <QLineEdit>
#include <QLabel>

#include "ui_stringedit.h"

class StringEditDialog : public QDialog
{
	Q_OBJECT

public:
	StringEditDialog(QString text, QString context, QStringList parts,QWidget *parent = 0, Qt::WFlags flags = 0);
	~StringEditDialog();

	QString getEditedText();

public slots:
	void addPath();
	void itemDoubleClicked(QListWidgetItem* item);
private:

	Ui::StringEditDialog ui;
};
#endif // stringeditdialog_h__