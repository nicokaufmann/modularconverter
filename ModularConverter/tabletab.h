#ifndef tabletab_h__
#define tabletab_h__

#include <QWidget>
#include <QInputDialog>
#include <QFileDialog>
#include <QSqlDatabase>
#include <QSqlField>
#include <QSqlTableModel>
#include <QSqlError>
#include <QString>
#include <QMessageBox>
#include <QDebug>

#include "choseelementsdialog.h"


#include "ui_tabletab.h"
#include "applicationdatabase.h"

#include "xlutil.h"

class TableTab : public QWidget
{
	Q_OBJECT

public:
	TableTab(ApplicationDatabase* applicationDatabase, QWidget *parent = 0, Qt::WFlags flags = 0);
	~TableTab();

public slots:
	void addColumn();
	void removeColumn();
	void changeHeader();

	void tableDataUpdated(QString connectionname);

	void selectDatabase(int index);
	void selectTable(int index);
	
	void clearTable();
	void createTable();
	void createStatement();

	void databasesUpdated();

	void spreadsheetTemplate();

private:
	void proposeTableDefinition(XL::WorksheetPtr sheet);

	ApplicationDatabase* applicationDatabase;
	ConnectionManager* connectionManager;

	Ui::TableTab ui;
};
#endif // tabletab_h__