#ifndef workflowtab_h__
#define workflowtab_h__

#include <QtGui/QWidget>
#include <QtSql/QSqlDatabase>
#include <QString>
#include "ui_workflowtab.h"

#include "workflowwindow.h"

#include "applicationdatabase.h"
#include "workflowelement.h"
#include "modulmanager.h"


class WorkflowTab : public QWidget
{
	Q_OBJECT

public:
	WorkflowTab(ApplicationDatabase* applicationDatabase, ModulManager* modulManager, QWidget *parent = 0, Qt::WFlags flags = 0);
	~WorkflowTab();

public slots:
	void workflowsUpdated();
	void selectWorkflow(int index);

	void execute(int index);

	void closing(WorkflowWindow* window);
	void statusChanged(Workflow* workflow, int activeIndex);
signals:
	void startWorkflowModul(QList<WorkflowItem*> itemList,int index);

private:
	void clearModulList();
	void clearModulListLayout();
	void buildModulListLayout();

	ApplicationDatabase* applicationDatabase;
	WorkflowManager* workflowManager;
	ModulManager* modulManager;

	QList<WorkflowElement*> elementList;
	QMap<int, WorkflowWindow*> windowMap;

	Ui::WorkflowTab ui;
};
#endif // workflowtab_h__