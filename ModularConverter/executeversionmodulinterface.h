#ifndef executeversionmodulinterface_h__
#define executeversionmodulinterface_h__

#include "standardmodulinterface.h"
#include "selectversionmodulinterface.h"
#include "executemodulinterface.h"

class ConverterModul;

class ExecuteVersionModulInterface : public StandardModulInterface 
{
	Q_OBJECT

public:
	ExecuteVersionModulInterface(ConverterModul* modul, QWidget *parent = 0, Qt::WFlags flags = 0);
	~ExecuteVersionModulInterface();

	void finished(bool status);

public slots:
	void executeModul();

signals:
	void execute(int cmd=0);

private:
	ExecuteModulInterface* executeInterface;
};
#endif // executeversionmodulinterface_h__