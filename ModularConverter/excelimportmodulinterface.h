#ifndef excelimportmodulinterface_h__
#define excelimportmodulinterface_h__

#include <QMessageBox>
#include <QFileDialog>

#include "standardmodulinterface.h"
#include "importfilemodulinterface.h"
#include "choseelementsdialog.h"
#include "xlcom.h"

class ExcelImportModul;

class ExcelImportModulInterface : public StandardModulInterface 
{
	Q_OBJECT

public:
	ExcelImportModulInterface(ExcelImportModul* modul, QWidget *parent = 0, Qt::WFlags flags = 0);
	~ExcelImportModulInterface();

	void finished(bool status);
public slots:
	void import();

signals:
	void execute(int cmd=0);

private:
	ImportFileModulInterface* importInterface;
};
#endif // excelimportmodulinterface_h__