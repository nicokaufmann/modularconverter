#ifndef singlestatementmodul_h__
#define singlestatementmodul_h__

#include <QSqlDatabase>
#include <QSqlQuery>

#include "standardmodulinterface.h"
#include "convertermodul.h"

class SingleStatementModul : public ConverterModul {
public:
	SingleStatementModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~SingleStatementModul();

	bool execute(int cmd=0);
	QWidget* createWidget();

	static QStringList getRequestedParameter();
	static bool useContext();
};
#endif // singlestatementmodul_h__