#ifndef mandatoryfieldsmodul_h__
#define mandatoryfieldsmodul_h__

#include <QString>
#include <QVariant>
#include <QMap>

#include "excelmultifileexecutionmodulinterface.h"
#include "convertermodul.h"

#include "xlutil.h"
#include "xl_buffered_reader.h"

class MandatoryFieldsModul : public ConverterModul {
public:
	MandatoryFieldsModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~MandatoryFieldsModul();

	bool execute(int cmd=0);
	QWidget* createWidget();

	void setPaths(const QMap<QString, QStringList>& paths);

	static bool useContext();
	static QStringList getRequestedParameter();
protected:
	QString loadFields();

	QMap<QString, QStringList> paths;
};
#endif // mandatoryfieldsmodul_h__
