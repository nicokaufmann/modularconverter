#include "statementconfigurationmodul.h"

#include "reportconfigmodul.h"

StatementConfigurationModul::StatementConfigurationModul(int id, ReportConfigModul* rcm, QWidget *parent, Qt::WFlags flags)
	: QWidget(parent,flags), id(id), rcm(rcm)
{
	ui.setupUi(this);

	ui.lStatementId->setText(QString::number(id));
	ui.cbDatabases->addItems(rcm->getDatabaseNames());

	ui.tFormatMapping->setRowCount(2);
	ui.tFormatMapping->setVerticalHeaderItem(0,new QTableWidgetItem("Mapping"));
	ui.tFormatMapping->setVerticalHeaderItem(1,new QTableWidgetItem("Zahlenformat"));

	QObject::connect(ui.cbDatabases, SIGNAL(activated(int)), this, SLOT(selectDatabase(int)));
	QObject::connect(ui.bRemoveStatement, SIGNAL(clicked()), this, SLOT(removeStatement()));
	QObject::connect(ui.bRefreshView, SIGNAL(clicked()), this, SLOT(refreshView()));

	QObject::connect(ui.bFontColorHeader, SIGNAL(clicked()), this, SLOT(changeHeaderFontColor()));
	QObject::connect(ui.bFontColorValues, SIGNAL(clicked()), this, SLOT(changeValuesFontColor()));
	QObject::connect(ui.bBgColorHeader, SIGNAL(clicked()), this, SLOT(changeHeaderBgColor()));
	QObject::connect(ui.bBgColorValues, SIGNAL(clicked()), this, SLOT(changeValuesBgColor()));
}

StatementConfigurationModul::~StatementConfigurationModul()
{

}

void StatementConfigurationModul::selectDatabase( int index )
{

}

void StatementConfigurationModul::setStatementValues( QMap<QString, QVariant>& cfg )
{
	ui.cbDatabases->setCurrentIndex(ui.cbDatabases->findText((cfg["connectionname"].toString())));
	ui.ptStatement->setPlainText(cfg["statement"].toString());

	ui.cbShowHeader->setChecked(cfg["header.enabled"].toBool());
	ui.cbVersioning->setChecked(cfg["versioning.enabled"].toBool());

	ui.leRow->setText(cfg["row"].toString());
	ui.leColumn->setText(cfg["column"].toString());
	ui.leRowRelation->setText(cfg["row.relativeTo"].toString());
	ui.leColumnRelation->setText(cfg["column.relativeTo"].toString());

	ui.cbFormatHeader->setChecked(cfg["header.changeFormat"].toBool());
	ui.fontHeader->setCurrentIndex(ui.fontHeader->findText(cfg["header.fontname"].toString()));
	if(cfg.contains("header.fontcolor")) {
		ui.bFontColorHeader->setStyleSheet("background-color: rgb(" 
			+ QString::number(cfg["header.fontcolor"].toUInt()&0xff) +  "," 
			+ QString::number((cfg["header.fontcolor"].toUInt()>>8)&0xff) + "," 
			+ QString::number((cfg["header.fontcolor"].toUInt()>>16)&0xff) + ");");
	}
	if(cfg.contains("header.color")) {
		ui.bBgColorHeader->setStyleSheet("background-color: rgb(" 
			+ QString::number(cfg["header.color"].toUInt()&0xff) +  "," 
			+ QString::number((cfg["header.color"].toUInt()>>8)&0xff) + "," 
			+ QString::number((cfg["header.color"].toUInt()>>16)&0xff) + ");");
	}

	ui.cbFormatValues->setChecked(cfg["values.changeFormat"].toBool());
	ui.fontValues->setCurrentIndex(ui.fontValues->findText(cfg["values.fontname"].toString()));
	if(cfg.contains("values.fontcolor")) {
		ui.bFontColorValues->setStyleSheet("background-color: rgb(" 
			+ QString::number(cfg["values.fontcolor"].toUInt()&0xff) +  "," 
			+ QString::number((cfg["values.fontcolor"].toUInt()>>8)&0xff) + "," 
			+ QString::number((cfg["values.fontcolor"].toUInt()>>16)&0xff) + ");");
	}
	if(cfg.contains("values.color")) {
		ui.bBgColorValues->setStyleSheet("background-color: rgb(" 
			+ QString::number(cfg["values.color"].toUInt()&0xff) +  "," 
			+ QString::number((cfg["values.color"].toUInt()>>8)&0xff) + "," 
			+ QString::number((cfg["values.color"].toUInt()>>16)&0xff) + ");");
	}

	ui.cbNumberformat->setChecked(cfg["mapping.enabled"].toBool());

	getTableFromStatement();
	setCurrentMapping(cfg);
}

void StatementConfigurationModul::getStatementValues( QMap<QString, QVariant>& cfg )
{
	cfg["connectionname"] = ui.cbDatabases->currentText();
	cfg["statement"] = ui.ptStatement->toPlainText();
	cfg["versioning.enabled"] = ui.cbVersioning->isChecked();
	cfg["header.enabled"] = ui.cbShowHeader->isChecked();
	
	cfg["row"] = ui.leRow->text();
	cfg["column"] = ui.leColumn->text();
	cfg["row.relativeTo"] = ui.leRowRelation->text();
	cfg["column.relativeTo"] = ui.leColumnRelation->text();

	cfg["header.changeFormat"] = ui.cbFormatHeader->isChecked();
	cfg["header.fontname"] = ui.fontHeader->currentText();

	QRegExp filterNumber("[0-9]+");
	if(ui.bFontColorHeader->styleSheet().count(filterNumber) > 2) {
		cfg["header.fontcolor"] = getRgbFromStyle(ui.bFontColorHeader->styleSheet());
	}
	if(ui.bBgColorHeader->styleSheet().count(filterNumber) > 2) {
		cfg["header.color"] = getRgbFromStyle(ui.bBgColorHeader->styleSheet());
	}

	cfg["values.changeFormat"] = ui.cbFormatValues->isChecked();
	cfg["values.fontname"] = ui.fontValues->currentText();

	if(ui.bFontColorValues->styleSheet().count(filterNumber) > 2) {
		cfg["values.fontcolor"] = getRgbFromStyle(ui.bFontColorValues->styleSheet());
	}
	if(ui.bBgColorValues->styleSheet().count(filterNumber) > 2) {
		cfg["values.color"] = getRgbFromStyle(ui.bBgColorValues->styleSheet());
	}

	cfg["mapping.enabled"] = ui.cbNumberformat->isChecked();

	getCurrentMapping(cfg);
}

int StatementConfigurationModul::getId()
{
	return id;
}

void StatementConfigurationModul::removeStatement()
{
	emit removeStatement(id);
}

void StatementConfigurationModul::changeHeaderFontColor()
{
	QColor color = QColorDialog::getColor();
	ui.bFontColorHeader->setStyleSheet("background-color: rgb(" 
		+ QString::number(color.red()) + ","
		+ QString::number(color.green()) + "," 
		+ QString::number(color.blue()) + ");");
}

void StatementConfigurationModul::changeHeaderBgColor()
{
	QColor color = QColorDialog::getColor();
	ui.bBgColorHeader->setStyleSheet("background-color: rgb(" 
		+ QString::number(color.red()) + ","
		+ QString::number(color.green()) + "," 
		+ QString::number(color.blue()) + ");");
}

void StatementConfigurationModul::changeValuesFontColor()
{
	QColor color = QColorDialog::getColor();
	ui.bFontColorValues->setStyleSheet("background-color: rgb(" 
		+ QString::number(color.red()) + ","
		+ QString::number(color.green()) + "," 
		+ QString::number(color.blue()) + ");");
}

void StatementConfigurationModul::changeValuesBgColor()
{
	QColor color = QColorDialog::getColor();
	ui.bBgColorValues->setStyleSheet("background-color: rgb(" 
		+ QString::number(color.red()) + ","
		+ QString::number(color.green()) + "," 
		+ QString::number(color.blue()) + ");");
}

void StatementConfigurationModul::refreshView()
{
	 QMap<QString, QVariant> currentMapping;
	 getCurrentMapping(currentMapping);
	 getTableFromStatement();
	 setCurrentMapping(currentMapping);

}

void StatementConfigurationModul::getTableFromStatement()
{
	QStringList fieldNames = rcm->getFieldNames(ui.cbDatabases->currentText(),ui.ptStatement->toPlainText());

	ui.tFormatMapping->setColumnCount(fieldNames.size());
	for(int i=0; i < fieldNames.size(); ++i) {
		ui.tFormatMapping->setHorizontalHeaderItem(i,new QTableWidgetItem(fieldNames[i]));

		ui.tFormatMapping->setCellWidget(0,i,new QLineEdit);
		ui.tFormatMapping->setCellWidget(1,i,new QLineEdit);
	}

	if(fieldNames.count() == 0 && rcm->getLastError().length() > 0 && ui.ptStatement->toPlainText().length() > 0) {
		QMessageBox msgBox;
		msgBox.setWindowTitle("Fehler");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.setText("Das Statement verursacht einen Fehler.");
		msgBox.setDetailedText(rcm->getLastError());
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.exec();
	}
}

void StatementConfigurationModul::getCurrentMapping( QMap<QString, QVariant>& mapping )
{
	for(int i=0; i < ui.tFormatMapping->columnCount();++i){
		QLineEdit* temp = (QLineEdit*)ui.tFormatMapping->cellWidget(0,i);
		if(temp->text().length() > 0) {
			mapping["map."+ui.tFormatMapping->horizontalHeaderItem(i)->text()] = temp->text();
		}

		temp = (QLineEdit*)ui.tFormatMapping->cellWidget(1,i);
		if(temp->text().length() >0 ) {
			mapping["numberformat."+ui.tFormatMapping->horizontalHeaderItem(i)->text()] = temp->text();
		}
	}
}

void StatementConfigurationModul::setCurrentMapping( QMap<QString, QVariant>& mapping )
{
	for(int i=0; i < ui.tFormatMapping->columnCount();++i){
		QLineEdit* temp = (QLineEdit*)ui.tFormatMapping->cellWidget(0,i);
		if(mapping.contains("map."+ui.tFormatMapping->horizontalHeaderItem(i)->text())) {
			temp->setText(mapping["map."+ui.tFormatMapping->horizontalHeaderItem(i)->text()].toString());
		}

		temp = (QLineEdit*)ui.tFormatMapping->cellWidget(1,i);
		if(mapping.contains("numberformat."+ui.tFormatMapping->horizontalHeaderItem(i)->text())) {
			temp->setText(mapping["numberformat."+ui.tFormatMapping->horizontalHeaderItem(i)->text()].toString());
		}
	}
}

unsigned int StatementConfigurationModul::getRgbFromStyle( QString style )
{
	QRegExp filterNumber("[0-9]+");
	unsigned int result = 0;

	int shift = 0;
	int pos = 0;
	while ((pos = filterNumber.indexIn(style, pos)) != -1) {
		result+=(style.mid(pos,filterNumber.matchedLength()).toUInt()<<shift);
		pos += filterNumber.matchedLength();
		shift+=8;
		if(shift>16) {
			break;
		}
	}
	return result;
}




