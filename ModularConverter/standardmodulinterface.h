#ifndef standardmodul_h__
#define standardmodul_h__

#include "basemodulwidget.h"
#include "convertermodul.h"
#include "usermessagewidget.h"

#include "ui_standardmodulinterface.h"

class StandardModulInterface : public BaseModulWidget
{
	Q_OBJECT

public:
	StandardModulInterface(ConverterModul* modul, QWidget *parent = 0, Qt::WFlags flags = 0);
	~StandardModulInterface();

	void addModulWidget(QWidget* modulWidget);

public slots:
	void setProgress(int progress);

signals:

protected:
	ConverterModul* modul;
	UserMessageWidget* userMessageWidget;

private:
	Ui::StandardModulInterface ui;
};
#endif // standardmodul_h__