#include "modultab.h"

ModulTab::ModulTab( ConnectionManager* connectionManager, ModulManager* modulManager, QWidget *parent, Qt::WFlags flags )
	: QWidget(parent,flags), connectionManager(connectionManager), modulManager(modulManager)
{
	ui.setupUi(this);

	ui.cbModul->addItems(modulManager->modulList());

	QObject::connect(ui.cbModul, SIGNAL(activated(int)), this, SLOT(selected(int)));
	QObject::connect(ui.bExecute, SIGNAL(clicked()), this, SLOT(executeModul()));
}

ModulTab::~ModulTab()
{

}

void ModulTab::executeModul()
{
	if(modulManager->contains(ui.cbModul->currentText())) {

		ParameterMap pm;
		for(int i=0; i < itemList.size(); ++i) {
			pm[itemList[i]->getAlias()]=itemList[i]->getParameter();
		}
		
		WorkflowItem* tempItem = new WorkflowItem(0,ui.cbModul->currentText());
		tempItem->setParameterMap(pm);
		tempItem->setModulContext(ui.cbContext->currentText());
		tempItem->setInheritVersion(false);
		
		Workflow* tempWorkflow = new Workflow(0,"Einzelausführung","",false);
		tempWorkflow->getItems().append(tempItem);

		WorkflowWindow* workflowWindow = new WorkflowWindow(modulManager, connectionManager,tempWorkflow);
		workflowWindow->show();

		windowList.append(workflowWindow);
		QObject::connect(workflowWindow, SIGNAL(closing(WorkflowWindow*)), this, SLOT(closing(WorkflowWindow*)));
		
			/*QMessageBox msgBox;
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::Ok);
			msgBox.setWindowTitle("Fehler");
			msgBox.setText("Das Modul hat einen Fehler verursacht.");
			msgBox.setDetailedText(modul->getLastError());
			msgBox.setIcon(QMessageBox::Critical);
			msgBox.exec();*/
	
	}
}

void ModulTab::selected( int index )
{
	if(modulManager->contains(ui.cbModul->itemText(index))) {
		clearItemList();
		QStringList requested = modulManager->requestedParameter(ui.cbModul->itemText(index));
		for(int i=0; i < requested.size(); ++i) {
			ModulSlotSetup* tempSlot = new ModulSlotSetup(connectionManager, requested[i], this);
			QObject::connect(tempSlot, SIGNAL(tableSelected(ModulSlotSetup*)), this, SLOT(tableSelected(ModulSlotSetup*)));

			itemList.push_back(tempSlot);
		}

		buildItemListLayout();

		if(modulManager->useContext(ui.cbModul->itemText(index))) {
			ui.cbContext->show();
			ui.cbContext->clear();
			ui.lContext->show();
		} else {
			ui.cbContext->hide();
			ui.lContext->hide();
		}
	}
}

void ModulTab::clearItemList()
{
	// clear layout
	QLayoutItem *child;
	while ((child = ui.modulSlotList->takeAt(0)) != 0) {
		delete child;
	}

	//clear itemlist
	for(int i=0; i < itemList.size();++i) {
		QObject::disconnect(itemList[i], SIGNAL(tableSelected(ModulSlotSetup*)), this, SLOT(tableSelected(ModulSlotSetup*)));
		delete itemList[i];
	}
	itemList.clear();
}

void ModulTab::buildItemListLayout()
{
	for(int i=0; i < itemList.size(); ++i) {
		ui.modulSlotList->addWidget(itemList[i]);
	}
}

void ModulTab::tableSelected( ModulSlotSetup* modulSlot )
{
	if(modulManager->useContext(ui.cbModul->currentText())) {
		QStringList contextList = modulSlot->getContextList();

		if(contextList.size() > 0) {
			ui.cbContext->clear();
			ui.cbContext->addItems(contextList);
		}
	}
}

void ModulTab::closing( WorkflowWindow* window )
{
	if(windowList.contains(window)) {
		delete window->getWorkflow();
		windowList.removeOne(window);
		window->deleteLater();
	}
}


