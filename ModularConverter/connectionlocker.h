
#ifndef connectionlocker_h__
#define connectionlocker_h__

#include "connectioninformation.h"

class ConnectionLocker {
public:
	ConnectionLocker(ConnectionInformation connection);
	~ConnectionLocker();

private:
	ConnectionInformation connection;
};
#endif // connectionlocker_h__