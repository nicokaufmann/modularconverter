#ifndef retrievedatadebugmodul_h__
#define retrievedatadebugmodul_h__

#include <QString>
#include <QVariant>
#include <QDate>
#include <QMap>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QSqlError>

#include "retrievedatamodulinterface.h"
#include "retrievedatamodul.h"

#include "xlutil.h"
#include "xl_buffered_reader.h"

class RetrieveDataDebugModul : public RetrieveDataModul {
public:
	RetrieveDataDebugModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~RetrieveDataDebugModul();

	bool execute(int cmd=0);
};
#endif // retrievedatadebugmodul_h__
