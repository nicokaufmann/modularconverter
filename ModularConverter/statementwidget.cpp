#include "statementwidget.h"

StatementWidget::StatementWidget(ApplicationDatabase* applicationDatabase, QWidget *parent, Qt::WFlags flags)
	: QWidget(parent,flags), applicationDatabase(applicationDatabase), connectionManager(applicationDatabase->getConnectionManager())
{
	ui.setupUi(this);

	sqlTextEdit=new TextEdit(this);
	sqlTextEdit->setLineWrapMode(QTextEdit::NoWrap);
	ui.sqlStatementLayout->addWidget(sqlTextEdit);

	sqlCompletionModel = new CompletionItemModel();
	sqlCompleter = new QCompleter(this);
	sqlCompleter->setModel(sqlCompletionModel);
	sqlCompleter->setModelSorting(QCompleter::CaseSensitivelySortedModel);
	sqlCompleter->setCaseSensitivity(Qt::CaseInsensitive);
	sqlCompleter->setWrapAround(false);

	sqlTextEdit->setCompleter(sqlCompleter);

	QFont font;
	font.setFamily("Consolas");
	font.setFixedPitch(true);
	font.setPointSize(10);
	font.setBold(true);
	font.setStyleStrategy(QFont::PreferAntialias);

	sqlTextEdit->setFont(font);
	sqlTextEdit->setTabStopWidth(sqlTextEdit->fontMetrics().width(' ')*4);

	sqlHighlighter = new SqlHighlighter(sqlTextEdit->document());



	dbListModel = new QStringListModel(connectionManager->connectionList());
	ui.cbDatabases->setModel(dbListModel);

	sqlListItemModel = new SyntaxItemModel(QStringList(),":/ModularConverter/bulletpoint_s.png","syntax",this);
	ui.sqlListview->setModel(sqlListItemModel);

	functionListModel = new SyntaxItemModel(QStringList(),":/ModularConverter/bulletpoint_f.png","function", this);
	ui.functionsListview->setModel(functionListModel);

	logicalFunctionListModel = new SyntaxItemModel(QStringList(),":/ModularConverter/bulletpoint_l.png","logical",this);
	ui.logicListview->setModel(logicalFunctionListModel);

	tableListModel = new SyntaxItemModel(QStringList(),":/ModularConverter/bulletpoint_t.png","table",this);
	ui.tableListview->setModel(tableListModel);

	attributeListModel = new SyntaxItemModel(QStringList(),":/ModularConverter/bulletpoint_a.png","attribute",this);
	ui.fieldListview->setModel(attributeListModel);

	ui.tablesCombo->setModel(tableListModel);

	sqlTreeModel = new SqlItemModel(&interpreter, this);
	ui.statementTree->setModel(sqlTreeModel);
	ui.statementTree->setDragEnabled(true);
	ui.statementTree->viewport()->setAcceptDrops(true);
	ui.statementTree->setDropIndicatorShown(true);

	databaseSelected(0);

	QObject::connect(ui.cbDatabases, SIGNAL(activated(int)),this, SLOT(databaseSelected(int)));
	QObject::connect(ui.tablesCombo, SIGNAL(activated(int)),this, SLOT(tableSelected(int)));

	QObject::connect(ui.bGenTree, SIGNAL(clicked()),this, SLOT(createTree()));
	QObject::connect(ui.bGenStatement, SIGNAL(clicked()),this, SLOT(createStatement()));
	
	QObject::connect(ui.statementTree, SIGNAL(clicked(const QModelIndex&)),this, SLOT(selectedTreeNode(const QModelIndex&)));
	QObject::connect(connectionManager->getDatabaseElementsUpdater(), SIGNAL(databaseElementsUpdated(QString)),this, SLOT(databaseElementsUpdated(QString)));
	QObject::connect(applicationDatabase, SIGNAL(databasesUpdated()),this, SLOT(databaseListUpdate()));
}

StatementWidget::~StatementWidget()
{

}

void StatementWidget::databaseListUpdate()
{
	dbListModel->setStringList(connectionManager->connectionList());
}

void StatementWidget::databaseSelected( int index )
{
	if(connectionManager->containsConnection(ui.cbDatabases->currentText())) {
		currentDatabaseElements = *(connectionManager->getConnection(ui.cbDatabases->currentText()).databaseElements());
		currentSyntaxElements = *connectionManager->getConnection(ui.cbDatabases->currentText()).syntaxElements();

		sqlCompletionModel->setCompletionElements(currentSyntaxElements, currentDatabaseElements);

		sqlListItemModel->setItemList(currentSyntaxElements.getKeywords());
		tableListModel->setItemList(currentDatabaseElements.getTables());
		functionListModel->setItemList(currentSyntaxElements.getFunctions());
		logicalFunctionListModel->setItemList(currentSyntaxElements.getLogicalFunctions());
		
		interpreter.setSyntaxElements(currentSyntaxElements);
		interpreter.setDatabaseElements(currentDatabaseElements);
		sqlHighlighter->setHighlightingRules(currentSyntaxElements,currentDatabaseElements);
		
		tableSelected(0);
	}
}

void StatementWidget::tableSelected( int index )
{
	if(currentDatabaseElements.getTables().contains(ui.tablesCombo->currentText())) {
		attributeListModel->setItemList(currentDatabaseElements.getAttributes(ui.tablesCombo->currentText()));

		sqlHighlighter->setHighlightingRules(currentSyntaxElements,currentDatabaseElements);
	}
}

void StatementWidget::selectedTreeNode( const QModelIndex & index )
{
	QMenu menu;
	

	//nothing
	QByteArray encodedDataNoop;
	QDataStream streamNoop(&encodedDataNoop, QIODevice::WriteOnly);
	streamNoop<<noopNode;
	streamNoop<<(quint32)index.internalPointer();
	menu.addAction("nichts tun")->setData(QVariant(encodedDataNoop));

	//edit
	if(((SyntaxNode*)index.internalPointer())->isEditable()) {
		QByteArray encodedDataEdit;
		QDataStream streamEdit(&encodedDataEdit, QIODevice::WriteOnly);
		streamEdit<<editNode;
		streamEdit<<(quint32)index.internalPointer();
		menu.addAction(QIcon(":/ModularConverter/edit.png"),"bearbeiten")->setData(QVariant(encodedDataEdit));
	}
	
	//copy / paste

	//remove
	QByteArray encodedDataRemove;
	QDataStream streamRemove(&encodedDataRemove, QIODevice::WriteOnly);
	streamRemove<<removeNode;
	streamRemove<<(quint32)index.internalPointer();
	menu.addAction(QIcon(":/ModularConverter/remove.png"),"entfernen")->setData(QVariant(encodedDataRemove));

	QObject::connect( &menu, SIGNAL(triggered(QAction*)), sqlTreeModel, SLOT(contextOperation(QAction*)));
	menu.exec(QCursor::pos());
}

void StatementWidget::createTree()
{
	SyntaxNode* rootNode = new StatementNode(NULL,"");
	interpreter.interpret(sqlTextEdit->document()->toPlainText(),rootNode);
	
	sqlTreeModel->setRootNode(rootNode);
	ui.statementTree->expandAll();
}

void StatementWidget::createStatement()
{
	if(sqlTreeModel->getRootNode() != NULL) {
		sqlTextEdit->setText(sqlTreeModel->getRootNode()->toStringFormat(0));
	}
}

void StatementWidget::databaseElementsUpdated( QString connectionname )
{
	if(connectionname == ui.cbDatabases->currentText()) {
		databaseSelected(ui.cbDatabases->currentIndex());
	}
}

void StatementWidget::setStatement( QString statement )
{
	sqlTextEdit->setText(statement);
}

void StatementWidget::setDatabase( QString connectionname )
{
	int pos = ui.cbDatabases->findText(connectionname);
	if(pos > -1) {
		ui.cbDatabases->setCurrentIndex(pos);
		databaseSelected(pos);
	}
}

QString StatementWidget::getStatement()
{
	return sqlTextEdit->toPlainText();
}

QString StatementWidget::getDatabase()
{
	return ui.cbDatabases->currentText();
}

