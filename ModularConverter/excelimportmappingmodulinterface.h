#ifndef excelimportmodulinterface_h__
#define excelimportmodulinterface_h__

#include <QMessageBox>
#include <QFileDialog>
#include <QTableView>

#include "comboboxdelegate.h"
#include "standardmodulinterface.h"
#include "importfilemodulinterface.h"
#include "choseelementsdialog.h"
#include "xlcom.h"

class BaseUserMappedImportModul;

class ExcelImportMappingModulInterface : public StandardModulInterface 
{
	Q_OBJECT
public:
	ExcelImportMappingModulInterface(BaseUserMappedImportModul* modul, QWidget *parent = 0, Qt::WFlags flags = 0);
	~ExcelImportMappingModulInterface();

	void finished(bool status);

public slots:
	void import();

signals:
	void execute(int cmd=0);

private:
	ImportFileModulInterface* importInterface;

	QTableView* tableView;
	ComboBoxDelegate comboDelegate;

	int interactionState;
};
#endif // excelimportmodulinterface_h__