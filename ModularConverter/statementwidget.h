#ifndef statementwidget_h__
#define statementwidget_h__

#include <QStringListModel>
#include <QMenu>
#include <QCompleter>

#include "textedit.h"
#include "sqlhighlighter.h"
#include "ui_statement.h"

#include "applicationdatabase.h"

#include "syntaxitemmodel.h"
#include "sqlitemmodel.h"
#include "sqlinterpreter.h"
#include "completionitemmodel.h"

class StatementWidget : public QWidget
{
	Q_OBJECT

public:
	StatementWidget(ApplicationDatabase* applicationDatabase, QWidget *parent = 0, Qt::WFlags flags = 0);
	~StatementWidget();

	void setStatement(QString statement);
	QString getStatement();
	void setDatabase(QString connectionname);
	QString getDatabase();
	

public slots:
	void databaseListUpdate();
	void databaseSelected(int index);
	void tableSelected(int index);
	void selectedTreeNode(const QModelIndex & index);

	void createTree();
	void createStatement();

	void databaseElementsUpdated(QString connectionname);
private:
	ApplicationDatabase* applicationDatabase;
	ConnectionManager* connectionManager;
	
	DatabaseElements currentDatabaseElements;
	SyntaxElements currentSyntaxElements;

	QStringListModel* dbListModel;
	SyntaxItemModel* sqlListItemModel;
	SyntaxItemModel* tableListModel;
	SyntaxItemModel* attributeListModel;
	SyntaxItemModel* functionListModel;
	SyntaxItemModel* logicalFunctionListModel;

	SqlItemModel* sqlTreeModel;

	SqlInterpreter interpreter;

	TextEdit* sqlTextEdit;
	QCompleter *sqlCompleter;
	CompletionItemModel* sqlCompletionModel;
	SqlHighlighter *sqlHighlighter;

	Ui::StatementWidget ui;
};
#endif // statementwidget_h__