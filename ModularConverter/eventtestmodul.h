
#include "convertermodul.h"
#include "standardmodulinterface.h"


class EventTestModul : public ConverterModul{
public:
	EventTestModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~EventTestModul();

	bool execute(int cmd=0);
	QWidget* createWidget();
};