#include <QVector>
#include <QVariant>

#include "xlcom.h"
#include "buffersegment.h"

class BufferedExcelWriter {
public:
	BufferedExcelWriter(XL::IWorksheet* sheet, const int& rows, const int& columns, const int& chunkSize);
	~BufferedExcelWriter();

	void setValue(const int& row, const int& column, QVariant* value);
	void writeBuffer();

	void setChunkSize(const int& chunkSize);
	void resize(const int& rows, const int& columns);
private:
	void writeChunks();
	void writeSegment(const BufferSegment& segment);
	BufferSegment getSegment(const int& row, const int& column);
	BufferSegment getLineSegment(const int& row, const int& column);

	XL::IWorksheet* sheet;
	QVector<QVariant*> valueBuffer;

	int rows;
	int columns;
	int chunkSize;
	int writtenData;
};