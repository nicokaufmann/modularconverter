#include "tablelistdialog.h"

#include <QStringListModel>
#include <QInputDialog>

TablelistDialog::TablelistDialog( const QString& title, const QStringList& list,QWidget *parent, Qt::WFlags flags )
	: QDialog(parent, flags)
{
	ui.setupUi(this);
	ui.listView->setModel(new QStringListModel(list));
	setWindowTitle(title);

	QObject::connect(ui.bAdd, SIGNAL(clicked()), this, SLOT(addTable()));
	QObject::connect(ui.bRemove, SIGNAL(clicked()), this, SLOT(removeTable()));
}

TablelistDialog::~TablelistDialog()
{

}


void TablelistDialog::addTable()
{
	QInputDialog inputDlg;
	if(inputDlg.exec() == QInputDialog::Accepted) {
		QStringListModel* model = (QStringListModel*)ui.listView->model();
		QStringList tempLst = model->stringList();
		tempLst<<inputDlg.textValue();
		model->setStringList(tempLst);
	}
}

void TablelistDialog::removeTable()
{
	QStringListModel* model = (QStringListModel*)ui.listView->model();
	QItemSelectionModel* selectionModel = ui.listView->selectionModel();
	
	if(selectionModel->currentIndex().isValid()) {
		model->removeRows(selectionModel->currentIndex().row(),1);
	}
}

QStringList TablelistDialog::getList()
{
	QStringListModel* model = (QStringListModel*)ui.listView->model();
	return model->stringList();
}
