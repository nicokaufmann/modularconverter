#include "selectversionmodulinterface.h"

SelectVersionModulInterface::SelectVersionModulInterface(ConnectionManager* connectionManager, QVector<VersionRequest> versionRequests, QPair<QString, QString> versioningParameter, QWidget *parent, Qt::WFlags flags)
	: QWidget(parent, flags), connectionManager(connectionManager)
{
	ui.setupUi(this);

	updateVersionList(connectionManager, versioningParameter);
	buildLayout(versionRequests);
}

SelectVersionModulInterface::~SelectVersionModulInterface()
{
}

void SelectVersionModulInterface::updateVersionList( ConnectionManager* connectionManager, QPair<QString, QString> versioningParameter )
{
	AcquiredConnection connection(connectionManager->acquireConnection(versioningParameter.first));
	QString table = versioningParameter.second;

	if(connection->isOpen()) {
		QSqlQuery data(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		if(data.exec("select * from "+ syntax->delimitTablename(table))) {
			versionMap.clear();
			while(data.next()) {
				versionMap.insert(data.record().value("id").toString(),DataVersion(data.record().value("id").toUInt(),data.record().value("username").toString(),data.record().value("description").toString(),data.record().value("created").toDateTime()));
			}
		} else {
			lastError = "Abfrage von Versionierungstabelle fehlgeschlagen: " + data.lastError().text();
		}
	} else {
		lastError = "Verbindung zu Versionierungstabelle fehlgeschlagen: " + connection->lastError().text();
	}
}

void SelectVersionModulInterface::buildLayout( QVector<VersionRequest> versionRequests )
{
	for(int i=0; i < versionRequests.count();++i) {
		ui.layout->addWidget(new TableVersion(versionMap, getRelevantVersions(versionRequests[i]),versionRequests[i], this), i/2, i%2);
	}
}

QStringList SelectVersionModulInterface::getRelevantVersions( VersionRequest request )
{
	AcquiredConnection connection(connectionManager->acquireConnection(request.connectionname()));
	QString table = request.tablename();

	if(connection->isOpen()) {
		QSqlQuery data(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		if(data.exec("select version_id from "+ syntax->delimitTablename(table) + " group by version_id order by version_id desc")) {
			QStringList result;
			while(data.next()) {
				result<<data.record().value("version_id").toString();
			}
			return result;
		}
	}

	return QStringList();
}

