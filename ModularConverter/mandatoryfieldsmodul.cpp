#include "mandatoryfieldsmodul.h"

#include <QSqlQuery>
#include <QSqlError>

MandatoryFieldsModul::MandatoryFieldsModul( ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion )
	: ConverterModul(parameterMap,versionMap,connectionManager,context,inheritVersion)
{

}

MandatoryFieldsModul::~MandatoryFieldsModul()
{
}

QWidget* MandatoryFieldsModul::createWidget()
{
	return new ExcelMultifileExecutionModulInterface(this);
}

bool MandatoryFieldsModul::useContext()
{
	return true;
}

QStringList MandatoryFieldsModul::getRequestedParameter()
{
	return (QStringList()<<"Konfiguration");
}

void MandatoryFieldsModul::setPaths( const QMap<QString, QStringList>& paths )
{
	this->paths = paths;
}

bool MandatoryFieldsModul::execute( int cmd)
{
	QString mandatoryFields(loadFields());

	if(!mandatoryFields.isNull() && !mandatoryFields.isEmpty()) {
		const QStringList fieldList(mandatoryFields.split(';'));

		XL::ApplicationPtr app(XL::GetApplication());
		int useHeadline = 1;

		if(app->good()) {
			XL::WorkbooksPtr books(app->getWorkbooks());

			const QStringList files(paths.keys());
			for(int i=0; i < files.count(); ++i) {
				const QStringList sheetNames(paths[files[i]]);

				XL::IWorkbook* tempBook = books->open(files[i].toStdString().c_str());

				int filepercentage = (i/(double)files.count())*100;

				if(tempBook) {
					XL::WorkbookPtr importBook(tempBook);
					XL::WorksheetsPtr importSheets(importBook->sheets());

					for(int j=0; j < importSheets->count(); ++j) {
						XL::WorksheetPtr sheet(importSheets->getItem(j+1));

						int sheetpercentage = (j/(double)importSheets->count()/(double)files.count())*100;

						if(sheetNames.contains(QString(sheet->getName()))) {
							XL::BufferedReader bf(sheet.get());
							XL::VariantPtr currentVal(XL::GetVariant());

							addUserMessage("Starte �berpr�fung: " + QString(sheet->getName()) + " (" + QString(importBook->getName())+ ")",0);

							for(int k=0; k < fieldList.count(); ++k) {
								int column = XL::Util::getColumn(sheet.get(),fieldList[k].toStdString().c_str(),1);

								if(column < 1) {
									addUserMessage("Spalte " + fieldList[k] + " nicht gefunden.", 2);
								} else {
									for(int row=2; row <= bf.rows(); ++row) {
										bf.getValue(row,column,currentVal.get());

										char* tempVal = currentVal->getChar();

										if(currentVal->isEmpty() || currentVal->isNull() || QString(tempVal).length() < 1) {
											delete[] tempVal;
											addUserMessage("Nicht alle Pflichtfelder gef�llt in Spalte " + fieldList[k] + " ab Zeile " + QString::number(row),2);
											break;
										}

										delete[] tempVal;

										if(row%500==0) {
											int rowpercentage = (k/(double)fieldList.count()/(double)importSheets->count()/(double)files.count())*100;
											QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, filepercentage + sheetpercentage + rowpercentage));
										}
									}
								}
							}
						}
					}
				} else {
					addUserMessage("Problem beim �ffnen der Datei: " + files[i], 2);
					lastError = "Problem beim �ffnen der Datei: " + files[i];
					return false;
				}
			}

			QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 100));
			addUserMessage("Vorgang abgeschlossen. " + QString::number(files.count()) + " Dateien �berpr�ft.", 0);
			return true;
		} else {
			addUserMessage("Problem mit Excel Schnittstelle: " + QString(app->getLastError()->getErrorMessage()), 2);
			lastError = "Problem mit Excel Schnittstelle:\nFehler: " + QString(app->getLastError()->getErrorMessage())+ "\nBeschreibung: " + QString(app->getLastError()->getErrorDescription());
			return false;
		}
	} else {
		return false;
	}
}

QString MandatoryFieldsModul::loadFields()
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Konfiguration"].first));

	if(connection->isOpen()) {
		QSqlQuery retrieveFields(*connection);
		retrieveFields.setForwardOnly(true);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		retrieveFields.prepare("select configvalue from "+ syntax->delimitTablename(parameterMap["Konfiguration"].second) + " where context=?");
		retrieveFields.bindValue(0, context);

		if(retrieveFields.exec() && retrieveFields.next()) {
			return retrieveFields.record().value(0).toString();
		} else {
			addUserMessage("Pflichtfelder konnten nicht geladen werden. Abfrage fehlgeschlagen.",2);
			lastError="Abfrage fehlgeschlagen:\n"+retrieveFields.lastError().text();
			return QString();
		}
	} else {
		lastError="Problem mit Datenbankverbindung:\n"+connection->lastError().text();
		return QString();
	}
}

