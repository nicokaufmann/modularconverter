#include "modulslotsetup.h"

ModulSlotSetup::ModulSlotSetup( ConnectionManager* connectionManager, QString alias,  QWidget *parent, Qt::WFlags flags )
	: QWidget(parent,flags), connectionManager(connectionManager), alias(alias)
{
	ui.setupUi(this);
	ui.lSlotAlias->setText(alias);


	ui.cbDatabase->addItems(connectionManager->connectionList());
	
	selectedDatabase(0);

	QObject::connect(connectionManager->getDatabaseElementsUpdater(), SIGNAL(databaseElementsUpdated(QString)), this, SLOT(tablesUpdated(QString)));

	QObject::connect(ui.cbDatabase, SIGNAL(activated(int)), this, SLOT(selectedDatabase(int)));
	QObject::connect(ui.cbTable, SIGNAL(activated(int)), this, SLOT(selectedTable(int)));
}

ModulSlotSetup::~ModulSlotSetup()
{

}

void ModulSlotSetup::selectedDatabase( int index )
{
	if(connectionManager->containsConnection(ui.cbDatabase->itemText(index))) {
		ui.cbTable->clear();
		ui.cbTable->addItems(connectionManager->getConnection(ui.cbDatabase->itemText(index)).databaseElements()->getTables());
	}
}

void ModulSlotSetup::selectedTable( int index )
{
	emit tableSelected(this);
}

QPair<QString, QString> ModulSlotSetup::getParameter()
{
	return QPair<QString, QString>(ui.cbDatabase->currentText(),ui.cbTable->currentText());
}

QString ModulSlotSetup::getAlias()
{
	return alias;
}

void ModulSlotSetup::setParameter( QPair<QString, QString> param )
{
	int dbIdx = ui.cbDatabase->findText(param.first);
	if( dbIdx >= 0) {
		ui.cbDatabase->setCurrentIndex(dbIdx);
		selectedDatabase(dbIdx);

		int tableIdx = ui.cbTable->findText(param.second);
		if( tableIdx >= 0) {
			ui.cbTable->setCurrentIndex(tableIdx);
			selectedTable(tableIdx);
		}
	}
}

QStringList ModulSlotSetup::getContextList()
{
	QStringList contextList;

	if(connectionManager->containsConnection(ui.cbDatabase->currentText())) {
		AcquiredConnection connection(connectionManager->acquireConnection(ui.cbDatabase->currentText()));
		QString table = ui.cbTable->currentText();

		if(connection->isOpen()) {
			QSqlQuery data(*connection);
			
			SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

			if(data.exec("select distinct context from "+ syntax->delimitTablename(table))) {
				while(data.next()) {
					contextList<<data.record().value("context").toString();
				} 
			} 
		}
	}
	
	return contextList;
}

void ModulSlotSetup::tablesUpdated( QString connectionname )
{
	if(ui.cbDatabase->currentText() == connectionname) {
		QString temp = ui.cbTable->currentText();
		ui.cbTable->clear();
		ui.cbTable->addItems(connectionManager->getConnection(ui.cbDatabase->currentText()).databaseElements()->getTables());
		
		int tableIdx = ui.cbTable->findText(temp);
		if( tableIdx >= 0) {
			ui.cbTable->setCurrentIndex(tableIdx);
			selectedTable(tableIdx);
		}
	}
}


