#ifndef retrievedatasourceversion_h__
#define retrievedatasourceversion_h__

#include "retrievedatamodul.h"

class RetrieveDataSourceVersionModul : public RetrieveDataModul {
public:
	RetrieveDataSourceVersionModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
};
#endif // retrievedatasourceversion_h__