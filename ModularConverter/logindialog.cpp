#include "logindialog.h"

#include <QSettings>
#include <QCryptographicHash>
#include <QMessageBox>
#include "databaselogin.h"

LoginDialog::LoginDialog( QWidget *parent, Qt::WFlags flags )
	: QDialog(parent, flags), modConv(0), applicationDatabase(new ApplicationDatabase())
{
	ui.setupUi(this);

	QSettings settings(QSettings::IniFormat, QSettings::UserScope,"NK", "ModularConverter");
	ui.leUser->setText(settings.value("username").toString());

	QObject::connect(ui.bApplication, SIGNAL(clicked()), this, SLOT(selectApplication()));
	QObject::connect(ui.bLogin, SIGNAL(clicked()), this, SLOT(login()));
}

LoginDialog::~LoginDialog()
{
	QSettings settings(QSettings::IniFormat, QSettings::UserScope,"NK", "ModularConverter");
	settings.setValue("username", ui.leUser->text());

	delete modConv;
	delete applicationDatabase;
}

void LoginDialog::login()
{
	ConnectionInformation appConnection(getAppConnection());
	
	if(checkConnection(appConnection)) {
		if(applicationDatabase->checkApplication(appConnection)) {
			if(applicationDatabase->connect(ui.leUser->text(), QCryptographicHash::hash(ui.lePassword->text().toUtf8(),QCryptographicHash::Sha1).toHex(), appConnection)) {
				if(!modConv) {
					modConv = new ModularConverter(this, applicationDatabase);
				}
				modConv->display();
				hide();
			} else {
				//login failed
				QMessageBox msg;
				msg.setWindowTitle("Verbindung zu Anwendung fehlgeschlagen");
				msg.setIcon(QMessageBox::Critical);
				msg.setText("Der Benutzer " + ui.leUser->text() + " konnte nicht angemeldet werden.");
				msg.setDetailedText(applicationDatabase->getLastError());
				msg.exec();
			}
		} else {
			//create application?
			QMessageBox msg;
			msg.setWindowTitle("Verbindung zu Anwendung fehlgeschlagen");
			msg.setIcon(QMessageBox::Critical);
			msg.setText("Die Anwendung konnte nicht gefunden werden oder ist beschädigt.\nWollen sie eine neue Anwendung erstellen?");
			msg.setDetailedText(applicationDatabase->getLastError());

			msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
			msg.setDefaultButton(QMessageBox::No);
			if(msg.exec() == QMessageBox::Yes) {
				msg.setStandardButtons(QMessageBox::Ok);
				msg.setDefaultButton(QMessageBox::Ok);
				if(applicationDatabase->createApplication(appConnection)) {
					msg.setWindowTitle("Anwendung erstellt");
					msg.setIcon(QMessageBox::Information);
					msg.setText("Die Anwendung wurde erstellt.");
					msg.setDetailedText("");
					msg.exec();
				} else {
					msg.setWindowTitle("Erstellen der Anwendung fehlgeschlagen");
					msg.setText("Die Anwendung konnte nicht erstellt werden.");
					msg.setDetailedText(applicationDatabase->getLastError());
					msg.exec();
				}
			}
		}
	}
}

void LoginDialog::selectApplication()
{
	DatabaseLoginInformation dblogin;
	dblogin.exec();
}

ConnectionInformation LoginDialog::getAppConnection()
{
	SimpleCrypt crypto(Q_UINT64_C(0x0c3ed6a8fabcf012));
	QSettings settings(QSettings::IniFormat, QSettings::UserScope,"NK", "ModularConverter");
	
	if(settings.value("databasepassword").toString().length() > 0) {
		return ConnectionInformation(ConnectionManager::getDriver(settings.value("drivername").toString()),
			settings.value("syntaxname").toString(),
			settings.value("connectionname").toString(),
			settings.value("host").toString(),
			settings.value("databasename").toString(),
			settings.value("options").toString(),
			settings.value("databaseusername").toString(),
			crypto.decryptToString(settings.value("databasepassword").toString()));
	} else {
		return ConnectionInformation(ConnectionManager::getDriver(settings.value("drivername").toString()),
			settings.value("syntaxname").toString(),
			settings.value("connectionname").toString(),
			settings.value("host").toString(),
			settings.value("databasename").toString(),
			settings.value("options").toString(),
			settings.value("databaseusername").toString(),
			"");
	}
}

bool LoginDialog::checkConnection( ConnectionInformation connection )
{
	QSqlDatabase db = connection.database();

	if(db.open()) {
		db.close();
		return true;
	} else {
		QMessageBox msg;
		msg.setWindowTitle("Verbindungsfehler");
		msg.setIcon(QMessageBox::Critical);
		msg.setText("Verbindung zu Datenbank konnte nicht hergestellt werden.");
		msg.setDetailedText(db.lastError().text());
		msg.setStandardButtons(QMessageBox::Ok);
		msg.setDefaultButton(QMessageBox::Ok);
		msg.exec();
		return false;
	}
}
