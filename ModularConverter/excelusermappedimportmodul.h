#ifndef excelusermappedimportmodul_h__
#define excelusermappedimportmodul_h__

#include <QString>
#include <QVariant>
#include <QDate>
#include <QMap>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QSqlError>

#include "excelimportmappingmodulinterface.h"
#include "baseusermappedimportmodul.h"

#include "xlutil.h"
#include "xl_buffered_reader.h"

class ExcelUserMappedImportModul : public BaseUserMappedImportModul {
public:
	ExcelUserMappedImportModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~ExcelUserMappedImportModul();

	bool execute(int cmd=0);
	QWidget* createWidget();

	static QStringList getRequestedParameter();
protected:
	bool prepareImportMapping(XL::WorksheetPtr sheet);
	bool createImportInstructions(QString& statement, QVector<int>& indexes, QVector<int>& types);

	bool loadInstructions(XL::WorksheetPtr sheet, QVector<int>& mappedColumns, QVector<int>& mappedTypes, QString& statement);
	bool importSheet( XL::WorksheetPtr sheet, int versionId, QString& statement, QVector<int>& indexes, QVector<int>& types );

	QString formatValueODBC(const QVariant &field, bool trimStrings);
	QString formatValue(const QVariant &field, bool trimStrings);

	int dataRow;
	int titleRow;

	QMap<QString, int> fieldIndexes;
	QMap<QString, int> fieldTypes;
};
#endif // excelusermappedimportmodul_h__