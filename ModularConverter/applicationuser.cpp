#include "applicationuser.h"

ApplicationUser::ApplicationUser()
	: sharedValid(new bool), sharedAdmin(new bool), sharedUsername(new QString("")), workflowPermissions(new QMap<int, bool>()), featurePermissions(new QMap<QString, bool>())
{
	*sharedValid = false;
}

ApplicationUser::ApplicationUser( QString username, bool admin )
	: sharedValid(new bool), sharedAdmin(new bool), sharedUsername(new QString(username)), workflowPermissions(new QMap<int, bool>()), featurePermissions(new QMap<QString, bool>())
{
	*sharedValid = true;
	*sharedAdmin = admin;
}

ApplicationUser::ApplicationUser( const ApplicationUser& obj )
{
	sharedValid = obj.sharedValid;
	sharedUsername = obj.sharedUsername;
	sharedAdmin = obj.sharedAdmin;
	workflowPermissions = obj.workflowPermissions;
	featurePermissions = obj.featurePermissions;
}

ApplicationUser& ApplicationUser::operator=( const ApplicationUser& obj )
{
	sharedValid = obj.sharedValid;
	sharedUsername = obj.sharedUsername;
	sharedAdmin = obj.sharedAdmin;
	workflowPermissions = obj.workflowPermissions;
	featurePermissions = obj.featurePermissions;

	return *this;
}

bool ApplicationUser::isValid() const
{
	return *sharedValid;
}

QString ApplicationUser::username() const
{
	return *sharedUsername;
}

bool ApplicationUser::isAdmin() const
{
	return *sharedAdmin;
}

bool ApplicationUser::hasFeaturePermission( const QString& feature ) const
{
	return (featurePermissions->contains(feature) && featurePermissions->value(feature)) || (*sharedAdmin && (!featurePermissions->contains(feature) || featurePermissions->value(feature)));
}

bool ApplicationUser::hasWorkflowPermission( const int& workflowId ) const
{
	return (workflowPermissions->contains(workflowId) && workflowPermissions->value(workflowId)) || (*sharedAdmin && (!workflowPermissions->contains(workflowId) || workflowPermissions->value(workflowId)));
}

void ApplicationUser::setWorkflowPermission( const int& workflowId, const bool& permission )
{
	workflowPermissions->insert(workflowId,permission);
}

void ApplicationUser::setFeaturePermission( const QString& feature, const bool& permission )
{
	featurePermissions->insert(feature, permission);
}

