#include "excelimportmappingmodulinterface.h"

#include "baseusermappedimportmodul.h"
#include "datamatchingmodel.h"

ExcelImportMappingModulInterface::ExcelImportMappingModulInterface( BaseUserMappedImportModul* modul, QWidget *parent, Qt::WFlags flags )
	: StandardModulInterface(modul, parent, flags), importInterface(new ImportFileModulInterface(this)), tableView(new QTableView()), comboDelegate(QStringList()), interactionState(0)
{
	addModulWidget(tableView);
	addModulWidget(importInterface);
	

	tableView->hide();

	DataMatchingModel* dmm = new DataMatchingModel(QStringList());
	dmm->addMatchingRow("Quelldatei", QMap<QString,QString>());
	tableView->setModel(dmm);
	tableView->setItemDelegateForRow(0,&comboDelegate);

	QObject::connect(importInterface->importButton(), SIGNAL(clicked()), this, SLOT(import()));
}

ExcelImportMappingModulInterface::~ExcelImportMappingModulInterface()
{

}

void ExcelImportMappingModulInterface::finished( bool status )
{
	userMessageWidget->updateMessages();
	importInterface->importButton()->setEnabled(true);

	BaseUserMappedImportModul* importModul = (BaseUserMappedImportModul*)modul;
	if(importModul->needsCustomMapping()) {
		comboDelegate.setOptions(importModul->getSourceFields());

		DataMatchingModel* dmm = (DataMatchingModel*)tableView->model();
		dmm->setOriginalData(importModul->getTargetFields());
		dmm->setMatchingRow("Quelldatei", importModul->getMapping());

		tableView->horizontalHeader()->setMinimumSectionSize(100);
		tableView->resizeColumnsToContents();
		
		tableView->show();
	}
}

void ExcelImportMappingModulInterface::import()
{
	QMessageBox msgBox;
	msgBox.setStandardButtons(QMessageBox::Ok);
	msgBox.setDefaultButton(QMessageBox::Ok);

	BaseUserMappedImportModul* importModul = (BaseUserMappedImportModul*)modul;

	if(!importModul->isMappingPrepared() || interactionState == 0 ) {
		QString path = QFileDialog::getOpenFileName(this,"Arbeitsmappe �ffnen", "", "Excel Arbeitsmappe (*.xls *.xlsx *.csv)");
		if(path.length() > 0) {

			//select sheets
			QStringList sheetNames;

			XL::ApplicationPtr xlapp(XL::GetApplication());
			if(xlapp->good()) {
				XL::WorkbooksPtr books(xlapp->getWorkbooks());
				XL::IWorkbook* tempBook = books->open(path.toStdString().c_str());

				if(tempBook) {
					XL::WorkbookPtr importBook(tempBook);
					XL::WorksheetsPtr importSheets(importBook->sheets());

					for(int sheetIdx=0; sheetIdx < importSheets->count(); ++sheetIdx) {
						XL::WorksheetPtr sheet(importSheets->getItem(sheetIdx+1));
						sheetNames<<QString(sheet->getName());
					}
				} else {
					msgBox.setWindowTitle("Fehler");
					msgBox.setText("Excel Arbeitsmappe konnte nicht ge�ffnet werden.");
					msgBox.setDetailedText(books->getLastError()->getErrorMessage());
					msgBox.setIcon(QMessageBox::Critical);
					msgBox.exec();
					return;
				}
			} else {
				msgBox.setWindowTitle("Fehler");
				msgBox.setText("Problem mit Excel Schnittstelle");
				msgBox.setDetailedText(xlapp->getLastError()->getErrorMessage());
				msgBox.setIcon(QMessageBox::Warning);

				msgBox.exec();
				return;
			}

			//popup chose sheets
			ChoseElementsDialog choseSheets(sheetNames,true,this);

			if(choseSheets.exec() == QDialog::Accepted) {
				// go
				importModul->clearUserMessages();
				importModul->setSheetnames(choseSheets.getSelectedElements());
				importModul->setPath(path);
				importModul->setVersionDescription(importInterface->getVersionDescription());
				

				emit execute(0);
				importInterface->importButton()->setEnabled(false);
				interactionState = 1;
				return;
			} else {
				msgBox.setWindowTitle("Fehler");
				msgBox.setText("Keine Auswahl getroffen, vorgang wird abgebrochen.");
				msgBox.setIcon(QMessageBox::Warning);
			}

		} else {
			msgBox.setWindowTitle("Fehler");
			msgBox.setText("Bitte g�ltigen Pfad angeben!");
			msgBox.setIcon(QMessageBox::Warning);
		}
		msgBox.exec();
	} else if( importModul->needsCustomMapping() && interactionState == 1){
		DataMatchingModel* dmm = (DataMatchingModel*)tableView->model();

		importModul->setMapping(dmm->matchingRow("Quelldatei"));

		emit execute(1);
		importInterface->importButton()->setEnabled(false);
		interactionState = 0;
		return;
	}
}
