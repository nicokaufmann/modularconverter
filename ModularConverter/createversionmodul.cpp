#include "CreateVersionModul.h"

#include <QStringBuilder>

CreateVersionModul::CreateVersionModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion)
	: ConverterModul(parameterMap, versionMap, connectionManager, context, inheritVersion)
{
}

CreateVersionModul::~CreateVersionModul()
{
}

QWidget* CreateVersionModul::createWidget()
{
	return new CreateVersionModulInterface(this);
}

QStringList CreateVersionModul::getRequestedParameter()
{
	return (QStringList()<<"Zieltabelle"<<"Versionierung");
}


bool CreateVersionModul::execute(int cmd)
{
	QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, (1)));

	int versionId = generateVersion(parameterMap["Versionierung"].first,parameterMap["Versionierung"].second,versionDesc);

	if(versionId > 0) {
		versionMap[parameterMap["Zieltabelle"].first][parameterMap["Zieltabelle"].second] = versionId;
		QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, (100)));
		return true;
	} else {
		addUserMessage("Problem beim erstellen der Versionsnummer.",2);
		return false;
	}
}

void CreateVersionModul::setVersionDescription( QString versionDescription )
{
	this->versionDesc = versionDescription;
}