#ifndef DATABASEINFORMATION_H
#define DATABASEINFORMATION_H

#include <QDialog>
#include <QMessageBox>

#include "stringeditdialog.h"
#include "ui_databaseinformation.h"

#include "applicationdatabase.h"

class DatabaseInformation : public QDialog
{
	Q_OBJECT

public:
	DatabaseInformation(ApplicationDatabase* applicationDatabase, QString okText,QWidget *parent = 0, Qt::WFlags flags = 0);
	~DatabaseInformation();

	ConnectionInformation getConnection();
	void setInformation(ConnectionInformation connection);
	void setConnectionName(QString connectionName);

public slots:
	void showStringEdit();
	void showTables();
	void tableStateChanged(int state);
	void testConnection();

private:
	ApplicationDatabase* applicationDatabase;

	Ui::DatabaseInformation ui;
};

#endif // DATABASEINFORMATION_H
