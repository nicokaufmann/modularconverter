#include "excelimportmodul.h"

#include <QStringBuilder>

ExcelImportModul::ExcelImportModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion)
	: ConverterModul(parameterMap, versionMap, connectionManager, context, inheritVersion), dataRow(2), titleRow(1)
{
}

ExcelImportModul::~ExcelImportModul()
{
}

QWidget* ExcelImportModul::createWidget()
{
	return new ExcelImportModulInterface(this);
}

QStringList ExcelImportModul::getRequestedParameter()
{
	return (QStringList()<<"Zieltabelle"<<"Versionierung");
}


bool ExcelImportModul::execute(int cmd)
{
	if(sheetNames.size() > 0) {
		//prepare version
		int versionId = generateVersion(parameterMap["Versionierung"].first,parameterMap["Versionierung"].second,versionDesc);

		if(versionId > 0) {
			//get sheets
			XL::ApplicationPtr xlapp(XL::GetApplication());
			if(xlapp->good()) {
				XL::WorkbooksPtr books(xlapp->getWorkbooks());
				XL::IWorkbook* tempBook = books->open(path.toStdString().c_str());
				if(tempBook) {
					XL::WorkbookPtr importBook(tempBook);
					XL::WorksheetsPtr importSheets(importBook->sheets());

					//loop sheets
					for(int sheetIdx=0; sheetIdx < importSheets->count(); ++sheetIdx) {
						XL::WorksheetPtr sheet(importSheets->getItem(sheetIdx+1));

						QString tempName = sheet->getName();
						if(sheetNames.contains(tempName)) {
							//import sheet
							QVector<int> mappedColumns;
							QVector<int> mappedTypes; 
							QString statement;

							if(!loadInstructions(sheet,mappedColumns,mappedTypes,statement)) {
								return false;
							}

							if(!importSheet(sheet,versionId,mappedColumns,mappedTypes,statement)) {
								return false;
							}
						}
					}

					QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 100));
					return true;
				} else {
					addUserMessage("Excel Arbeitsmappe konnte nicht ge�ffnet werden",2);
					lastError="Excel Arbeitsmappe konnte nicht ge�ffnet werden:\n" +QString(books->getLastError()->getErrorMessage());
					return false;
				}
			} else {
				addUserMessage("Problem mit Excel Schnittstelle",2);
				lastError="Problem mit Excel Schnittstelle:\n" +QString(xlapp->getLastError()->getErrorDescription());
				return false;
			}
		} else {
			addUserMessage("Problem mit Versionserstellung",2);
			return false;
		}
	} else {
		addUserMessage("Kein Arbeitsblatt ausgew�hlt",2);
		return false;
	}
}

bool ExcelImportModul::loadInstructions(XL::WorksheetPtr sheet, QVector<int>& mappedColumns, QVector<int>& mappedTypes, QString& statement)
{
	QString connectionname = parameterMap["Zieltabelle"].first;
	QString table = parameterMap["Zieltabelle"].second;

	AcquiredConnection connection(connectionManager->acquireConnection(connectionname));

	if(connection->isOpen()) {
		QSqlRecord record = connection->record(table);

		QStringList fields;
		QStringList mappedFields;
		QMap<QString, int> typeMap;

		//get fields
		for(int fieldIdx=0; fieldIdx < record.count(); ++fieldIdx) {
			fields<<record.fieldName(fieldIdx);
			typeMap[record.fieldName(fieldIdx)] = record.field(fieldIdx).type();
		}

		//map fields
		XL::RangePtr usedRange(sheet->getUsedRange());
		XL::RangePtr columns(usedRange->columns());

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		for(int columnIdx=0; columnIdx < columns->count(); ++columnIdx) {
			XL::VariantPtr val(usedRange->getItem(1,columnIdx+1));
			char* tempVal = val->getChar();
			QString fieldname = syntax->filterSpecialChars(QString(tempVal));
			delete[] tempVal;

			if(!mappedFields.contains(fieldname) && fields.contains(fieldname, Qt::CaseInsensitive)) {
				mappedColumns.append(columnIdx+1);
				mappedTypes.append(typeMap[fieldname]);
				mappedFields<<fieldname;
			}
		}

		//build statement
		statement = "insert into " + syntax->delimitTablename(table) + " (";
		for(int fieldIdx=0; fieldIdx < mappedFields.count(); ++fieldIdx) {
			statement+=syntax->delimitFieldname(mappedFields[fieldIdx])+",";
		}
		statement+="version_id) values (";

		addUserMessage(QString::number(mappedFields.count()) + " von " + QString::number(fields.count()-1) + " Feldern gefunden", (mappedFields.count() != fields.count()-1)?1:0);
		return true;
	} else {
		addUserMessage("Problem mit Datenbankverbindung",2);
		lastError="Problem mit Datenbankverbindung:\n"+connection->lastError().text();
		return false;
	}
}

bool ExcelImportModul::importSheet( XL::WorksheetPtr sheet, int versionId, QVector<int>& mappedColumns, QVector<int>& mappedTypes, QString& statement )
{
	QString connectionname = parameterMap["Zieltabelle"].first;
	QString table = parameterMap["Zieltabelle"].second;

	AcquiredConnection connection(connectionManager->acquireConnection(connectionname));

	if(connection->isOpen()) {
		connection->transaction();

		QSqlQuery insert(*connection);
		insert.setForwardOnly(true);

		XL::BufferedReader reader(sheet.get());

		const QString emptyField("NULL,");
		const QString comma(",");
		const QString closeBracket(")");
		const QString versionField(formatValueODBC(versionId,false));
		const QRegExp matchInt("[0-9]+");

		const int columnCount = mappedColumns.count();
		const int rowCount = reader.rows()+1;
		int countEmptyCellsPerRow;
		int countEmptyRows = 0;
		int countInsertedRows = 0;

		XL::VariantPtr cellValue(XL::GetVariant());

		for(int row=2; row < rowCount; ++row) {
			QString statementValues;
			countEmptyCellsPerRow = 0;

			for(int column=0; column < columnCount; ++column) {
				const int currentType = mappedTypes.at(column);

				if(reader.getValue(row,mappedColumns[column],cellValue.get()) && !cellValue->isEmpty()) {
					if(currentType == QVariant::Double) {
						statementValues+=formatValueODBC(cellValue->getDouble(),false) % comma;
					} else if(currentType == QVariant::Int) {
						statementValues+=formatValueODBC(cellValue->getInt(),false) % comma;
					} else if(currentType == QVariant::LongLong) {
						statementValues+=formatValueODBC(cellValue->getInt(),false) % comma;
					} else if(currentType == QVariant::UInt) {
						statementValues+=formatValueODBC(cellValue->getUInt(),false) % comma;
					} else if(currentType == QVariant::Bool) {
						statementValues+=formatValueODBC(cellValue->getBool(),false) % comma;
					} else {
						char* tempValue = cellValue->getChar();
						const QString temp(tempValue);
						delete[] tempValue;

						if(currentType == QVariant::Date ||currentType == QVariant::DateTime) {
							QStringList parts = temp.split(".");
							if(parts.size()	== 3) {
								statementValues+=formatValueODBC(parts[2]+"-"+parts[1]+"-"+parts[0],false) % comma;
							} else {
								if(matchInt.exactMatch(temp)) {
									statementValues+=temp % comma;
								} else {
									statementValues+=formatValueODBC(temp,false) % comma;
								}
							}
						} else {
							statementValues+=formatValueODBC(temp,false) % comma;
						}
					}
				} else {
					countEmptyCellsPerRow++;
					statementValues+=emptyField;
				}
			}


			if(countEmptyCellsPerRow != columnCount) {
				statementValues+=versionField;

				if(!insert.exec(statement % statementValues % closeBracket)) {
					addUserMessage("Importfehler in Zeile " + QString::number(row),2);
					lastError="Problem beim Import, Zeile " + QString::number(row) + " konnte nicht importiert werden:\n" + insert.lastError().text() + "\n\nStatement:\n" + insert.lastQuery();
					
					connection->rollback();
					return false;
				} else {
					countInsertedRows++;
				}
			} else {
				countEmptyRows ++;
			}

			if(row%100==0) {
				QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, (row*100)/(rowCount)));
			}
		}

		addUserMessage(QString::number(countInsertedRows) + " Zeilen importiert",0);
		addUserMessage(QString::number(countEmptyRows) + " Leerzeilen ignoriert",0);

		versionMap[connectionname][table] = versionId;
		connection->commit();
		return true;
	} else {
		addUserMessage("Problem mit Datenbankverbindung",2);
		lastError="Problem mit Datenbankverbindung:\n"+connection->lastError().text();
		return false;
	}
}

void ExcelImportModul::setPath( QString path )
{
	addUserMessage("Datei ausgew�hlt: " + path.right(path.length()-path.lastIndexOf("/")-1),0);
	this->path = path;
}

void ExcelImportModul::setSheetnames( QStringList sheetNames )
{
	this->sheetNames = sheetNames;
}

void ExcelImportModul::setVersionDescription( QString versionDescription )
{
	this->versionDesc = versionDescription;
}

QString ExcelImportModul::formatValueODBC( const QVariant &field, bool trimStrings )
{
	QString r;
	if (field.isNull()) {
		r = QLatin1String("NULL");
	} else if (field.type() == QVariant::DateTime) {
		// Use an escape sequence for the datetime fields
		if (field.toDateTime().isValid()){
			QDate dt = field.toDateTime().date();
			QTime tm = field.toDateTime().time();
			// Dateformat has to be "yyyy-MM-dd hh:mm:ss", with leading zeroes if month or day < 10
			r = QLatin1String("{ ts '") +
				QString::number(dt.year()) + QLatin1Char('-') +
				QString::number(dt.month()).rightJustified(2, QLatin1Char('0'), true) +
				QLatin1Char('-') +
				QString::number(dt.day()).rightJustified(2, QLatin1Char('0'), true) +
				QLatin1Char(' ') +
				tm.toString() +
				QLatin1String("' }");
		} else
			r = QLatin1String("NULL");
	} else if(field.type() == QVariant::Date) {
		if (field.toDate().isValid()){
			QDate dt = field.toDateTime().date();
			// Dateformat has to be "yyyy-MM-dd", with leading zeroes if month or day < 10
			r = QLatin1String("{ d '") +
				QString::number(dt.year()) + QLatin1Char('-') +
				QString::number(dt.month()).rightJustified(2, QLatin1Char('0'), true) +
				QLatin1Char('-') +
				QString::number(dt.day()).rightJustified(2, QLatin1Char('0'), true) +
				QLatin1String("' }");
		} else
			r = QLatin1String("NULL");
	} else if (field.type() == QVariant::ByteArray) {
		QByteArray ba = field.toByteArray();
		QString res;
		static const char hexchars[] = "0123456789abcdef";
		for (int i = 0; i < ba.size(); ++i) {
			uchar s = (uchar) ba[i];
			res += QLatin1Char(hexchars[s >> 4]);
			res += QLatin1Char(hexchars[s & 0x0f]);
		}
		r = QLatin1String("0x") + res;
	} else {
		r = formatValue(field, trimStrings);
	}
	return r;
}

QString ExcelImportModul::formatValue( const QVariant &field, bool trimStrings )
{
	const QLatin1String nullTxt("NULL");

	QString r;
	if (field.isNull())
		r = nullTxt;
	else {
		switch (field.type()) {
		case QVariant::Int:
		case QVariant::UInt:
			if (field.type() == QVariant::Bool)
				r = field.toBool() ? QLatin1String("1") : QLatin1String("0");
			else
				r = field.toString();
			break;
		case QVariant::String:
		case QVariant::Char:
			{
				QString result = field.toString();
				if (trimStrings) {
					int end = result.length();
					while (end && result.at(end-1).isSpace()) /* skip white space from end */
						end--;
					result.truncate(end);
				}
				/* escape the "'" character */
				result.replace(QLatin1Char('\''), QLatin1String("''"));
				r = QLatin1Char('\'') + result + QLatin1Char('\'');
				break;
			}
		case QVariant::Bool:
			r = QString::number(field.toBool());
			break;
		case QVariant::ByteArray : {
			QByteArray ba = field.toByteArray();
			QString res;
			static const char hexchars[] = "0123456789abcdef";
			for (int i = 0; i < ba.size(); ++i) {
				uchar s = (uchar) ba[i];
				res += QLatin1Char(hexchars[s >> 4]);
				res += QLatin1Char(hexchars[s & 0x0f]);
			}
			r = QLatin1Char('\'') + res +  QLatin1Char('\'');
			break;
								   }
		default:
			r = field.toString();
			break;
		}
	}
	return r;
}
