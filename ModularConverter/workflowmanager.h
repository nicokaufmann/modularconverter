#ifndef workflowmanager_h__
#define workflowmanager_h__

#include <QVector>

#include "workflow.h"

class ApplicationDatabase;

class WorkflowManager {
public:
	WorkflowManager(ApplicationDatabase* applicationDatabase);
	~WorkflowManager();

	int workflowCount();
	QVector<Workflow*>& getWorkflowList();

	void addWorkflow(Workflow* flow);
	void addWorkflow(Workflow* flow, int at);
	void removeWorkflow(Workflow* flow);
	void removeWorkflow(int index);

	Workflow* getWorkflow(const int& index) const;
	Workflow* getWorkflow(const QString& workflowName) const;

	void clearWorkflowList();

	ApplicationDatabase* getApplicationDatabase();
private:
	QVector<Workflow*> workflowList;
	
	ApplicationDatabase* applicationDatabase;
};
#endif // workflowmanager_h__
