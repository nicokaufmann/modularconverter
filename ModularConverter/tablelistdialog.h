#ifndef tablelistdialog_h__
#define tablelistdialog_h__

#include <QDialog>

#include "ui_stringlistdialog.h"

class TablelistDialog : public QDialog
{
	Q_OBJECT

public:
	TablelistDialog(const QString& title, const QStringList& list,QWidget *parent = 0, Qt::WFlags flags = 0);
	~TablelistDialog();

	QStringList getList();
public slots:
	void addTable();
	void removeTable();

private:
	Ui::StringlistDialog ui;
};
#endif // tablelistdialog_h__