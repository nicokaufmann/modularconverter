

#ifndef datamatchingmodel_h__
#define datamatchingmodel_h__

#include <QAbstractItemModel>
#include <QVariant>
#include <QVector>
#include <QStringList>
#include <QMimeData>

class DataMatchingModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	DataMatchingModel(QStringList originalData, QObject *parent = 0);
	~DataMatchingModel();

	Qt::ItemFlags flags( const QModelIndex & index ) const;

	bool setData( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );
	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole ) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
	QMimeData* mimeData( const QModelIndexList & indexes ) const;

	int rowCount( const QModelIndex & parent = QModelIndex() ) const;
	int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

	QModelIndex index(int row, int column, const QModelIndex &parent) const;
	QModelIndex parent(const QModelIndex &index) const;

	Qt::DropActions supportedDropActions() const;

	void setOriginalData(QStringList originalData);
	void addMatchingRow(QString alias, const QMap<QString, QString>& currentMatching);
	void setMatchingRow(QString alias, const QMap<QString, QString>& currentMatching);
	QMap<QString, QString> matchingRow(QString alias);


public slots:

protected:

private:
	int columns;
	int rows;

	QModelIndex invalidIndex;
	QVariant invalidVariant;

	QStringList originalData;
	QVector<QVariant> columnHeaders;

	QVector<QVector<QVariant>> rowData;
	QVector<QVariant> rowAlias;
};
#endif // datamatchingmodel_h__