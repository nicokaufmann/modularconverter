#include "datatab.h"

#include <QSqlDriver>
#include <QSqlError>

DataTab::DataTab( ApplicationDatabase* applicationDatabase, SimpleSqlEditor* simpleSqlEditor, QWidget *parent, Qt::WFlags flags)
		: QWidget(parent,flags), applicationDatabase(applicationDatabase), connectionManager(applicationDatabase->getConnectionManager()), simpleSqlEditor(simpleSqlEditor), sqlTextEdit(new TextEdit())
{
	ui.setupUi(this);

	ui.cbDatabases->addItems(connectionManager->connectionList());
	ui.tableListView->setModel(new QStringListModel(this));
	
	QFont font;
	font.setFamily("Consolas");
	font.setFixedPitch(true);
	font.setPointSize(10);
	font.setBold(true);
	font.setStyleStrategy(QFont::PreferAntialias);

	sqlTextEdit->setFont(font);
	sqlTextEdit->setTabStopWidth(sqlTextEdit->fontMetrics().width(' ')*4);
	ui.sqlSourceLayout->addWidget(sqlTextEdit);

	sqlHighlighter = new SqlHighlighter(sqlTextEdit->document());

	QObject::connect(ui.cbDatabases, SIGNAL(activated(int)), this, SLOT(selectDatabase(int)));
	QObject::connect(ui.bShowTableData, SIGNAL(clicked()), this, SLOT(showTableData()));
	QObject::connect(ui.bShowQueryData, SIGNAL(clicked()), this, SLOT(showQueryData()));
	QObject::connect(ui.bExecuteQuery, SIGNAL(clicked()), this, SLOT(executeStatement()));
	QObject::connect(ui.bSqlEditor, SIGNAL(clicked()), this, SLOT(openSqlEditor()));
	QObject::connect(ui.bInsertData, SIGNAL(clicked()), this, SLOT(insertData()));
	QObject::connect(connectionManager->getDatabaseElementsUpdater(), SIGNAL(databaseElementsUpdated(QString)), this, SLOT(tableDataUpdated(QString)));

	QObject::connect(&watchExecution, SIGNAL(finished()),this, SLOT(statementExecutionFinished()));
}

DataTab::~DataTab()
{

}

void DataTab::setPermissions( ApplicationUser user )
{
	ui.bExecuteQuery->setEnabled(user.hasFeaturePermission("executequery"));
	ui.bShowQueryData->setEnabled(user.hasFeaturePermission("showquery"));
	ui.bSqlEditor->setEnabled(user.hasFeaturePermission("sqleditor"));
}

void DataTab::selectDatabase( int index )
{
	if(connectionManager->containsConnection(ui.cbDatabases->currentText())) {
		QStringListModel* model = (QStringListModel*)ui.tableListView->model();
		ConnectionInformation currentConnection(connectionManager->getConnection(ui.cbDatabases->currentText()));
		const DatabaseElements databaseElements(*currentConnection.databaseElements());
		const SyntaxElements syntaxElements(*currentConnection.syntaxElements());

		model->setStringList(databaseElements.getTables());
		sqlHighlighter->setHighlightingRules(syntaxElements, databaseElements);
	}
}

void DataTab::databasesUpdated()
{
	ui.cbDatabases->clear();
	ui.cbDatabases->addItems(connectionManager->connectionList());
}


void DataTab::tableDataUpdated( QString connectionname )
{
	QStringListModel* model = (QStringListModel*)ui.tableListView->model();
	model->setStringList(connectionManager->getConnection(ui.cbDatabases->currentText()).databaseElements()->getTables());
}

void DataTab::showTableData()
{
	if(connectionManager->containsConnection(ui.cbDatabases->currentText())) {	
		QModelIndex selectedIndex = ui.tableListView->currentIndex();
	
		if(selectedIndex.isValid()) {
			//build query
			
			windowList.append(new DataWindow(connectionManager, ui.cbDatabases->currentText(), "Select * From " + connectionManager->getConnection(ui.cbDatabases->currentText()).syntaxElements()->delimitTablename(selectedIndex.data().toString())));
			QObject::connect(windowList.last(), SIGNAL(closing(DataWindow*)), this, SLOT(closing(DataWindow*)));
			windowList.last()->show();
		} else {
			QMessageBox msgBox;
			msgBox.setWindowTitle("Keine Tabelle ausgewählt");
			msgBox.setText("Es muss eine Tabelle ausgewählt werden.");
			msgBox.setIcon(QMessageBox::Warning);
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::Ok);
			msgBox.exec();
		}
	}
}

void DataTab::showQueryData()
{
	if(connectionManager->containsConnection(ui.cbDatabases->currentText())) {	
		//build query
		windowList.append(new DataWindow(connectionManager, ui.cbDatabases->currentText(), sqlTextEdit->toPlainText()));
		QObject::connect(windowList.last(), SIGNAL(closing(DataWindow*)), this, SLOT(closing(DataWindow*)));
		windowList.last()->show();
	}
}

void DataTab::closing( DataWindow* window )
{
	if(windowList.contains(window)) {
		QObject::disconnect(window, SIGNAL(closing(DataWindow*)), this, SLOT(closing(DataWindow*)));
		windowList.removeOne(window);
		window->deleteLater();
	}
}

void DataTab::insertData()
{
	if(connectionManager->containsConnection(ui.cbDatabases->currentText())) {	
		QModelIndex selectedIndex = ui.tableListView->currentIndex();

		if(selectedIndex.isValid()) {
			DataManipulationDialog dmd(connectionManager,ui.cbDatabases->currentText(),selectedIndex.data().toString());
			dmd.exec();
		} else {
			QMessageBox msgBox;
			msgBox.setWindowTitle("Keine Tabelle ausgewählt");
			msgBox.setText("Es muss eine Tabelle ausgewählt werden.");
			msgBox.setIcon(QMessageBox::Warning);
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::Ok);
			msgBox.exec();
		}
	}
}

void DataTab::openSqlEditor()
{
	if(connectionManager->containsConnection(ui.cbDatabases->currentText())) {	
		simpleSqlEditor->addStatement(ui.cbDatabases->currentText(),sqlTextEdit->toPlainText());
		simpleSqlEditor->show();
		QApplication::setActiveWindow(simpleSqlEditor);
	}
}

void DataTab::executeStatement()
{
	QFuture<bool> future = QtConcurrent::run(this, &DataTab::statementExecution , sqlTextEdit->toPlainText());
	watchExecution.setFuture(future);

	executionError.clear();
	ui.bExecuteQuery->setEnabled(false);
}

void DataTab::statementExecutionFinished()
{
	ui.bExecuteQuery->setEnabled(true);

	if(watchExecution.future().result()) {
		QMessageBox msgBox;
		msgBox.setWindowTitle("Statement ausgeführt");
		msgBox.setText("Das Statement wurde erfolgreich ausgeführt");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.exec();
	} else {
		QMessageBox msgBox;
		msgBox.setWindowTitle("Fehler");
		msgBox.setText("Problem beim Ausführen des Statements.");
		msgBox.setDetailedText(executionError);
		msgBox.setIcon(QMessageBox::Critical);
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.exec();
	}

}

bool DataTab::statementExecution(QString statement)
{
	AcquiredConnection connection(connectionManager->acquireConnection(ui.cbDatabases->currentText()));
	
	if(connection->isOpen()) {
		QSqlQuery query(*connection);

		if(query.exec(statement)) {
			return true;
		} else {
			executionError = query.lastError().text() + "\n\nQuery:\n" + query.lastQuery();
		}
	} else {
		executionError = "Verbindung konnte nicht hergestellt werden.\n" + connection->lastError().text();
	}
	return false;
}
