#include "bufferedexcelwriter.h"

#include <iostream>

BufferedExcelWriter::BufferedExcelWriter( XL::IWorksheet* sheet, const int& rows, const int& columns, const int& chunkSize )
	: sheet(sheet), valueBuffer(rows*columns,0), rows(rows), columns(columns), chunkSize(chunkSize), writtenData(0)
{
}

BufferedExcelWriter::~BufferedExcelWriter()
{

}

void BufferedExcelWriter::setChunkSize( const int& chunkSize )
{
	this->chunkSize = chunkSize;
}

void BufferedExcelWriter::setValue( const int& row, const int& column, QVariant* value )
{
	unsigned int pos = row*columns+column;
	if(pos < valueBuffer.size()) {
		if(valueBuffer[pos] == 0) {
			writtenData++;
		}
		valueBuffer[pos] = value;
	}
}

void BufferedExcelWriter::writeBuffer()
{
	
	if(writtenData*3 > rows*columns) {
		//more than 33% filled
		writeChunks();
	} else {
		//might be faster to write small segments 
		unsigned int pos = 0;

		for(unsigned int row = 0; row < rows; ++row) {
			for(unsigned int column=0; column < columns; ++column) {
				if(valueBuffer[pos] != 0) {
					if(row == 848 && column==14) {
						QString();
					}

					writeSegment(getSegment(row, column));
					--column;
				} else {
					++pos;
				}
			}
		}
	}
}

BufferSegment BufferedExcelWriter::getLineSegment( const int& row, const int& column )
{
	int i=row*columns+column;
	int length = (row+1)*columns;
	while(i < length-1 && valueBuffer[i] == 0) {
		++i;
	}
	
	if(valueBuffer[i] != 0) {
		//determine length
		int j = i;
		while(j < length && valueBuffer[j] != 0) {
			++j;
		}
		return BufferSegment(row, i-row*columns, j-i);
	} else {
		return BufferSegment();
	}
}

BufferSegment BufferedExcelWriter::getSegment( const int& row, const int& column )
{
	QList<BufferSegment> segments;
	int currentRow = row;
	bool matched;

	do {
		matched = false;
		BufferSegment segment(getLineSegment(currentRow, column));
		
		if(segments.size() > 0) {
			for(int i = 0; i < segments.size(); ++i) {
				if(segments.at(i).fullmatch(segment)) {
					segments[i].combine(segment);
					matched = true;
					break;
				} else if(segments.at(i).match(segment)) {
					segment.combine(segments.at(i));
					segments.append(segment);
					matched = true;
					break;
				}
			}
		} else {
			segments.append(segment);
			matched = true;
		}

		currentRow++;
	} while(matched && currentRow < rows);

	int highest = 0;

	for(int i=1; i < segments.size(); ++i) {
		if(segments.at(i).count() > segments.at(highest).count()) {
			highest = i;
		}
	}

	return segments[highest];
}

void BufferedExcelWriter::writeSegment( const BufferSegment& segment )
{
	XL::VariantPtr buffer(XL::GetSafeArray(segment.height(),segment.length()));
	XL::ISafeArray* safeArray(buffer->getSafeArray());
	XL::VariantPtr tempVal(XL::GetVariant());

	for(int i=0; i < segment.length(); ++i) {
		for(int j=0; j < segment.height(); ++j) {
			const int pos = (segment.row()+j)*columns+segment.column()+i;

			tempVal->setChar(valueBuffer[pos]->toString().toStdString().c_str());

			safeArray->setValueAt(j+1,i+1,tempVal.get());

			//zero
			valueBuffer[pos] = 0;
		}
	}

	XL::RangePtr pCells(sheet->cells());
	XL::RangePtr pTargetRange(pCells->getRangeFrom(segment.column(), segment.row(),segment.height(),segment.length()));

	pTargetRange->setValue(buffer.get());
}

void BufferedExcelWriter::writeChunks()
{
	XL::RangePtr pCells(sheet->cells());
	XL::VariantPtr tempVal(XL::GetVariant());

	for(int chunk=0; chunk < rows; chunk+=chunkSize) {
		int bufferRows = (chunkSize > rows-chunk)? rows-chunk : chunkSize;
		
		XL::VariantPtr buffer(XL::GetSafeArray(bufferRows,columns));
		XL::ISafeArray* safeArray(buffer->getSafeArray());

		for(int row=chunk; row < rows; ++row) {
			for(int column=0; column < columns; ++column) {
				const int pos = row*columns+column;

				if(valueBuffer[pos] != 0) {
					tempVal->setChar(valueBuffer[pos]->toString().toStdString().c_str());
					safeArray->setValueAt(row-chunk+1,column+1,tempVal.get());
				}
			}
		}

		XL::RangePtr pTargetRange(pCells->getRangeFrom(0,chunk,bufferRows,columns));
		pTargetRange->setValue(buffer.get());
	}
}

