#include "databasemanager.h"

#include <QMutexLocker>

DatabaseManager::DatabaseManager( )
{
	driverMap["QSQLITE"] = DriverInformation("QSQLITE",true);
	driverMap["QODBC"] = DriverInformation("QODBC",true);
	driverMap["QODBC3"] = DriverInformation("QODBC3",true);
}

DatabaseManager::~DatabaseManager()
{
}

bool DatabaseManager::connect( QString username, QString password, ConnectionInformation connection )
{
	coreConnection = connection;
	currentUser = loadUser(username,password);

	return currentUser.isValid() && loadConnections();
}

bool DatabaseManager::loadConnections()
{
	ConnectionLocker locker(&coreConnection);
	QSqlDatabase coreConn = coreConnection.database();

	if(coreConn.open()) {
		databaseMap.clear();

		QSqlQuery query(coreConn);
		if(query.exec("select * from core_connections")) {
			SimpleCrypt crypto(Q_UINT64_C(0x0c4ad2a3aab8f012));

			while(query.next()) {
				ConnectionInformation temp(driverMap[query.record().value("drivername").toString()],
										   query.record().value("connectionname").toString(),
										   query.record().value("hostname").toString(),
										   query.record().value("databasename").toString(),
										   query.record().value("options").toString(),
										   query.record().value("username").toString(),
										   crypto.decryptToString(query.record().value("password").toString()));
				databaseMap[temp.connectionname()] = temp;
			}

			emit databasesUpdated();

			return true;
		} else {
			lastError = query.lastError().text();
		}
	} else {
		lastError = coreConn.lastError().text();
	}

	return false;
}

ApplicationUser DatabaseManager::loadUser( QString username, QString password )
{
	ConnectionLocker locker(&coreConnection);
	QSqlDatabase coreConn = coreConnection.database();

	if(coreConn.open()) {
		databaseMap.clear();

		QSqlQuery queryUser(coreConn);
		queryUser.prepare("select * from core_user where username=? and password=?");
		queryUser.bindValue(0,username);
		queryUser.bindValue(1,password);

		if(queryUser.exec()) {
			if(queryUser.next()) {
				ApplicationUser user(queryUser.record().value("username").toString(), queryUser.record().value("admin").toBool());

				QSqlQuery queryWorkflowPermissions(coreConn);
				queryWorkflowPermissions.prepare("select * from core_workflow_permissions where user_id=?");
				queryWorkflowPermissions.bindValue(0,queryUser.record().value("id").toInt());

				if(queryWorkflowPermissions.exec()) {
					while(queryWorkflowPermissions.next()) {
						user.setWorkflowPermission(queryWorkflowPermissions.record().value("workflow_id").toInt(),queryWorkflowPermissions.record().value("allowed").toBool());
					}
				}

				QSqlQuery queryFeaturePermissions(coreConn);
				queryFeaturePermissions.prepare("select * from core_feature_permissions where user_id=?");
				queryFeaturePermissions.bindValue(0,queryUser.record().value("id").toInt());

				if(queryFeaturePermissions.exec()) {
					while(queryFeaturePermissions.next()) {
						user.setFeaturePermission(queryFeaturePermissions.record().value("feature").toString(),queryFeaturePermissions.record().value("allowed").toBool());
					}
				}

				return user;
			} else {
				lastError = "Benutzerdaten nicht korrekt.";
			}
		} else {
			lastError = queryUser.lastError().text();
		}
	} else {
		lastError = coreConn.lastError().text();
	}

	return ApplicationUser();
}


bool DatabaseManager::addDatabase(ConnectionInformation connection)
{
	ConnectionLocker locker(&coreConnection);
	QSqlDatabase coreConn = coreConnection.database();

	if(coreConn.open()) {
		//name taken?
		QSqlQuery query(coreConn);
		query.prepare("select connectionname from core_connections where connectionname=:value");
		query.bindValue(":value",connection.connectionname());
		if(query.exec() && !query.next()) {
			//store new
			SimpleCrypt crypto(Q_UINT64_C(0x0c4ad2a3aab8f012));

			QSqlQuery insert(coreConn);
			insert.prepare("INSERT INTO core_connections (drivername, connectionname, hostname, databasename, username, password, options) "
				"VALUES (:drivername, :connectionname, :hostname, :databasename, :username, :password, :options)");
			insert.bindValue(0, connection.driver().alias());
			insert.bindValue(1, connection.connectionname());
			insert.bindValue(2, connection.hostname());
			insert.bindValue(3, connection.databasename());
			insert.bindValue(4, connection.username());
			insert.bindValue(5, crypto.encryptToString(connection.password()));
			insert.bindValue(6, connection.options());
			if(insert.exec()) {
				//put in map
				databaseMap[connection.connectionname()] = connection;

				emit databasesUpdated();

				return true;
			} else {
				lastError = insert.lastError().text();
			}
		} else {
			lastError = query.lastError().text();
		}
	} else {
		lastError = coreConn.lastError().text();
	}

	return false;
}

bool DatabaseManager::removeDatabase(ConnectionInformation& connection )
{
	ConnectionLocker locker(&coreConnection);
	QSqlDatabase coreConn = coreConnection.database();

	if(coreConn.open()) {
		QSqlQuery query(coreConn);
		query.prepare("delete from core_connections where connectionname=:value");
		query.bindValue(":value",connection.connectionname());
		if(query.exec()) {
			connection.lock()->lockForWrite();
			databaseMap.remove(connection.connectionname());

			emit databasesUpdated();

			return true;
		} else {
			lastError = query.lastError().text();
		}
	} else {
		lastError = coreConn.lastError().text();
	}

	return false;
}

bool DatabaseManager::updateDatabase(ConnectionInformation connection )
{
	ConnectionLocker locker(&coreConnection);
	QSqlDatabase coreConn = coreConnection.database();

	if(coreConn.open()) {
		SimpleCrypt crypto(Q_UINT64_C(0x0c4ad2a3aab8f012));

		QSqlQuery update(coreConn);
		update.prepare("update core_connections set drivername=:drivername, hostname=:hostname, databasename=:databasename, "
					   "username=:username, password=:password, options=:options "
					   "where connectionname=:connectionname");
		update.bindValue(0,connection.driver().alias());
		update.bindValue(1,connection.hostname());
		update.bindValue(2,connection.databasename());
		update.bindValue(3,connection.username());
		update.bindValue(4,crypto.encryptToString(connection.password()));
		update.bindValue(5,connection.options());
		update.bindValue(6,connection.connectionname());

		if(update.exec()) {
			if(databaseMap.contains(connection.connectionname())) {
				ConnectionLocker updateLocker(&databaseMap[connection.connectionname()]);
				databaseMap[connection.connectionname()].setDriver(connection.driver());
				databaseMap[connection.connectionname()].setHostname(connection.hostname());
				databaseMap[connection.connectionname()].setDatabasename(connection.databasename());
				databaseMap[connection.connectionname()].setOptions(connection.options());
				databaseMap[connection.connectionname()].setUsername(connection.username());
				databaseMap[connection.connectionname()].setPassword(connection.password());

				emit databasesUpdated();
			}
			
			return true;
		} else {
			lastError = update.lastError().text();
		}
	} else {
		lastError = coreConn.lastError().text();
	}

	return false;
}

QString DatabaseManager::getLastError()
{
	return lastError;
}


ConnectionInformation& DatabaseManager::getCoreConnection()
{
	return coreConnection;
}

ConnectionInformation& DatabaseManager::getConnection( QString connectionname )
{
	return databaseMap[connectionname];
}

bool DatabaseManager::containsConnection( QString connectionname )
{
	return databaseMap.contains(connectionname);
}

QStringList DatabaseManager::connectionList()
{
	return databaseMap.keys();
}

DriverInformation DatabaseManager::getDriver( QString alias )
{
	return driverMap[alias];
}

bool DatabaseManager::checkApplication( ConnectionInformation connection )
{
	ConnectionLocker locker(&connection);
	QSqlDatabase coreConn = connection.database();

	if(coreConn.open()) {
		QStringList tables(coreConn.tables());

		if(!tables.contains("core_connections")) {
			lastError = "core_connections Tabelle nicht gefunden.";
		} else if(!tables.contains("core_workflows")) {
			lastError = "core_workflows Tabelle nicht gefunden.";
		} else if(!tables.contains("core_workflow_item")) {
			lastError = "core_workflow_item Tabelle nicht gefunden.";
		} else if(!tables.contains("core_workflow_item_params")) {
			lastError = "core_workflow_item_params Tabelle nicht gefunden.";
		} else if(!tables.contains("core_user")) {
			lastError = "core_user Tabelle nicht gefunden.";
		} else if(!tables.contains("core_workflow_permissions")) {
			lastError = "core_workflow_permissions Tabelle nicht gefunden.";
		} else if(!tables.contains("core_feature_permissions")) {
			lastError = "core_feature_permissions Tabelle nicht gefunden.";
		} else {
			return true;
		}
	} else {
		lastError = "Verbindung fehlgeschlagen.\n" + coreConn.lastError().text();
	}
	return false;
}

bool DatabaseManager::createApplication( ConnectionInformation connection )
{
	ConnectionLocker locker(&connection);
	QSqlDatabase coreConn = connection.database();

	if(coreConn.open()) {
		QStringList tables(coreConn.tables());
		QSqlQuery createTable(coreConn);
		
		if(coreConn.databaseName().contains("Microsoft Access Driver")) {
			if(!tables.contains("core_connections")) {	
				if(!createTable.exec("create table core_connections(drivername VARCHAR(255) NOT NULL, connectionname VARCHAR(255) NOT NULL,"
					"hostname VARCHAR(255),databasename VARCHAR(255),username VARCHAR(255),password VARCHAR(255), options VARCHAR(255),"
					"PRIMARY KEY (connectionname))")) {
						lastError = createTable.lastError().text();
						return false;
				}
			}
			if(!tables.contains("core_workflows")) {	
				if(!createTable.exec("create table core_workflows(id COUNTER NOT NULL, name VARCHAR(255)  NOT NULL, "
					"description VARCHAR(255), forceorder YESNO, PRIMARY KEY (id));")) {
						lastError = createTable.lastError().text();
						return false;
				}
			}
			if(!tables.contains("core_workflow_item")) {	
				if(!createTable.exec("create table core_workflow_item(id COUNTER NOT NULL, workflow_id INT NOT NULL, order_index INT NOT NULL, "
					"description VARCHAR(255), modulname VARCHAR(255) NOT NULL, modulcontext VARCHAR(255) , inheritversion YESNO, PRIMARY KEY (id));")) {
						lastError = createTable.lastError().text();
						return false;
				}
			}
			if(!tables.contains("core_workflow_item_params")) {	
				if(!createTable.exec("create table core_workflow_item_params(id COUNTER NOT NULL, workflow_item_id INT NOT NULL, param_alias VARCHAR(255) , "
					"param_table VARCHAR(255), param_connectionname VARCHAR(255), PRIMARY KEY (id));")) {
						lastError = createTable.lastError().text();
						return false;
				}
			}
			if(!tables.contains("core_user")) {	
				if(!createTable.exec("create table core_user(id COUNTER NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, "
					"admin YESNO, PRIMARY KEY (id));")) {
						lastError = createTable.lastError().text();
						return false;
				}
				QSqlQuery insertUser(coreConn);
				if(!insertUser.exec("insert into core_user(username, password, admin) values('kaufmann','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',1)")){
					lastError = insertUser.lastError().text();
					return false;
				}
			}
			if(!tables.contains("core_workflow_permissions")) {	
				if(!createTable.exec("create table core_workflow_permissions(user_id INT NOT NULL, workflow_id INT NOT NULL, allowed YESNO NOT NULL);")) {
					lastError = createTable.lastError().text();
					return false;
				}
			}
			if(!tables.contains("core_feature_permissions")) {	
				if(!createTable.exec("create table core_feature_permissions(user_id INT NOT NULL, feature VARCHAR(255) NOT NULL, allowed YESNO NOT NULL);")) {
					lastError = createTable.lastError().text();
					return false;
				}
			}
			return true;
		} else {
			if(!tables.contains("core_connections")) {	
				if(!createTable.exec("create table core_connections(drivername VARCHAR(255) NOT NULL, connectionname VARCHAR(255) NOT NULL,"
									"hostname VARCHAR(255),databasename VARCHAR(255),username VARCHAR(255),password VARCHAR(255), options VARCHAR(255),"
									"PRIMARY KEY (connectionname))")) {
					lastError = createTable.lastError().text();
					return false;
				}
			}
			if(!tables.contains("core_workflows")) {	
				if(!createTable.exec("create table core_workflows(id INT NOT NULL AUTO_INCREMENT, name VARCHAR(255)  NOT NULL, "
									 "description VARCHAR(255), forceorder BOOLEAN, PRIMARY KEY (id));")) {
					lastError = createTable.lastError().text();
					return false;
				}
			}
			if(!tables.contains("core_workflow_item")) {	
				if(!createTable.exec("create table core_workflow_item(id INT NOT NULL AUTO_INCREMENT, workflow_id INT NOT NULL, order_index INT NOT NULL, "
									 "description VARCHAR(255), modulname VARCHAR(255) NOT NULL, modulcontext VARCHAR(255) , inheritversion BOOLEAN, PRIMARY KEY (id));")) {
					lastError = createTable.lastError().text();
					return false;
				}
			}
			if(!tables.contains("core_workflow_item_params")) {	
				if(!createTable.exec("create table core_workflow_item_params(id INT NOT NULL AUTO_INCREMENT, workflow_item_id INT NOT NULL, param_alias VARCHAR(255) , "
									 "param_table VARCHAR(255), param_connectionname VARCHAR(255), PRIMARY KEY (id));")) {
					lastError = createTable.lastError().text();
					return false;
				}
			}
			if(!tables.contains("core_user")) {	
				if(!createTable.exec("create table core_user(id INT NOT NULL AUTO_INCREMENT, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, "
									 "admin BOOLEAN, PRIMARY KEY (id));")) {
					lastError = createTable.lastError().text();
					return false;
				}
				QSqlQuery insertUser(coreConn);
				if(!insertUser.exec("insert into core_user(username, password, admin) values('kaufmann','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',1)")){
					lastError = insertUser.lastError().text();
					return false;
				}
			}
			if(!tables.contains("core_workflow_permissions")) {	
				if(!createTable.exec("create table core_workflow_permissions(user_id INT NOT NULL, workflow_id INT NOT NULL, allowed BOOLEAN NOT NULL);")) {
					lastError = createTable.lastError().text();
					return false;
				}
			}
			if(!tables.contains("core_feature_permissions")) {	
				if(!createTable.exec("create table core_feature_permissions(user_id INT NOT NULL, feature VARCHAR(255) NOT NULL, allowed BOOLEAN NOT NULL);")) {
					lastError = createTable.lastError().text();
					return false;
				}
			}
			return true;
		}
	} else {
		lastError = "Verbindung fehlgeschlagen.\n" + coreConn.lastError().text();
	}
	return false;
}

ApplicationUser DatabaseManager::getCurrentUser()
{
	return currentUser;
}
