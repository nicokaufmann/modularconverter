#include "executemodulinterface.h"

ExecuteModulInterface::ExecuteModulInterface( QWidget *parent , Qt::WFlags flags )
	: QWidget(parent , flags)
{
	ui.setupUi(this);
	ui.bExecute->setText("Ausf�hren");
}

ExecuteModulInterface::~ExecuteModulInterface()
{
}

QWidget* ExecuteModulInterface::executeButton()
{
	return ui.bExecute;
}
