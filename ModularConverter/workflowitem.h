#ifndef workflowitem_h__
#define workflowitem_h__

#include <QtSql/QSqlDatabase>
#include <QPair>
#include <QString>
#include <QMap>

#include "convertermodul.h"

class WorkflowItem {
public:
	WorkflowItem();
	WorkflowItem(int uniqueId, QString modulName);
	~WorkflowItem();

	int getUniqueId();
	void setUniqueId(int id);
	QString getDescription();
	void setDescription(QString description);
	QString getModulName();
	void setModulName(QString modulName);
	QString getModulContext();
	void setModulContext(QString modulContext);
	bool getInheritVersion();
	void setInheritVersion(bool inheritVersion);

	ParameterMap& getParameterMap();
	void setParameterMap(ParameterMap params);
private:
	int uniqueId;
	QString description;
	QString modulName;
	QString modulContext;
	bool inheritVersion;
	
	ParameterMap params;
};

#endif // workflowitem_h__