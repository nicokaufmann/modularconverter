#ifndef dataversion_h__
#define dataversion_h__

#include <QString>
#include <QDateTime>

class DataVersion {
public:
	DataVersion()
		: version(0), username(""), description(""), time(QDateTime()) {};
	DataVersion(unsigned int version, QString username, QString description, QDateTime time) 
		: version(version), username(username), description(description), time(time) {};

	unsigned int version;
	QString username;
	QString description;
	QDateTime time;
};
#endif // dataversion_h__