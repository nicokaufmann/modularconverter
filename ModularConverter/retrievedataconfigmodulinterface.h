#ifndef retrievedataconfigmodulinterface_h__
#define retrievedataconfigmodulinterface_h__


#include <QInputDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QMap>
#include <QStringList>
#include <QVariant>

#include "comboboxdelegate.h"
#include "retrievedatastageconfigmodulinterface.h"

#include "ui_retrievedataconfigmodulinterface.h"

class RetrieveDataConfigModul;

class RetrieveDataConfigModulInterface : public QWidget
{
	Q_OBJECT

public:
	RetrieveDataConfigModulInterface(RetrieveDataConfigModul* rcm, QWidget *parent = 0, Qt::WFlags flags = 0);
	~RetrieveDataConfigModulInterface();

	void init();

public slots:
	void selectContext(int index);
	void saveContext();
	void deleteContext();
	void createContext();
	void addStage();
	void removeStage(int index);
signals:

private:
	void clearStages();

	QList<RetrieveDataStageConfigModulInterface*> stageList;

	RetrieveDataConfigModul* rdcm;
	
	ComboBoxDelegate comboDelegate;
	Ui::RetrieveDataConfigModulInterface ui;
};
#endif // retrievedataconfigmodulinterface_h__
