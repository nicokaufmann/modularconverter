#ifndef excelexportmodulinterface_h__
#define excelexportmodulinterface_h__

#include <QFileDialog>

#include "standardmodulinterface.h"
#include "selectversionmodulinterface.h"
#include "exportfilemodulinterface.h"

class BasePathModul;

class ExcelExportModulInterface : public StandardModulInterface 
{
	Q_OBJECT

public:
	ExcelExportModulInterface(BasePathModul* modul, QWidget *parent = 0, Qt::WFlags flags = 0);
	~ExcelExportModulInterface();

	void finished(bool status);

public slots:
	void exportFile();

signals:
	void execute(int cmd=0);

private:
	ExportFileModulInterface* exportInterface;
};
#endif // excelexportmodulinterface_h__
