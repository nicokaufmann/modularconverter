#include "retrievedataconfigmodul.h"

#include "retrievedataconfigmodulinterface.h"
#include <QSqlQuery>
#include <QSqlError>

RetrieveDataConfigModul::RetrieveDataConfigModul( ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager , QString context, bool inheritVersion)
	: ConverterModul(parameterMap, versionMap, connectionManager, context, inheritVersion)
{
}

RetrieveDataConfigModul::~RetrieveDataConfigModul()
{
}

QWidget* RetrieveDataConfigModul::createWidget()
{
	return new RetrieveDataConfigModulInterface(this);
}

QStringList RetrieveDataConfigModul::getRequestedParameter()
{
	return (QStringList()<<"Stageconfig"<<"Feldconfig"<<"Zieltabelle"<<"Quelltabelle");
}

bool RetrieveDataConfigModul::execute(int cmd)
{
	return true;
}

QStringList RetrieveDataConfigModul::getContextList()
{
	QStringList results;

	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Feldconfig"].first));
	QString table = parameterMap["Feldconfig"].second;

	if(connection->isOpen()) {
		QSqlQuery data(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		if(data.exec("select distinct context from "+ syntax->delimitTablename(table))) {
			while(data.next()) {
				results<<data.record().value("context").toString();
			}
		}
		data.finish();
	}
	return results;
}



bool RetrieveDataConfigModul::deleteContext( QString context )
{
	{
		AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Feldconfig"].first));
		QString table = parameterMap["Feldconfig"].second;


		if(connection->isOpen()) {
			QSqlQuery deleteStatement(*connection);

			SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

			deleteStatement.prepare("delete from " + syntax->delimitTablename(table)+ " where context=:context");
			deleteStatement.bindValue(0, context);
			if(!deleteStatement.exec()) {
				lastError = deleteStatement.lastError().text();
				return false;
			}
			deleteStatement.finish();
		} else {
			lastError = "Verbindung zur Datenbank konnte nicht hergestellt werden.\n\n" + connection->lastError().text();
			return false;
		}
	}

	{
		AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Stageconfig"].first));
		QString table = parameterMap["Stageconfig"].second;


		if(connection->isOpen()) {
			QSqlQuery deleteStatement(*connection);

			SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

			deleteStatement.prepare("delete from " + syntax->delimitTablename(table)+ " where context=:context");
			deleteStatement.bindValue(0, context);
			if(!deleteStatement.exec()) {
				lastError = deleteStatement.lastError().text();
				return false;
			}
			deleteStatement.finish();
		} else {
			lastError = "Verbindung zur Datenbank konnte nicht hergestellt werden.\n\n" + connection->lastError().text();
			return false;
		}
	}
	return true;
}

bool RetrieveDataConfigModul::saveFieldMap(QString context, QMap<QString, QString>& fieldMap )
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Feldconfig"].first));
	QString table = parameterMap["Feldconfig"].second;


	if(connection->isOpen()) {
		QSqlQuery insert(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		insert.prepare("insert into "+ syntax->delimitTablename(table) + " (context, targetfield, sourcefield) VALUES (?, ?, ?)");
		insert.bindValue(0,context);

		const QStringList keys(fieldMap.keys());

		for(int i=0; i < keys.count(); ++i) {
			insert.bindValue(1,keys[i]);
			insert.bindValue(2,fieldMap.value(keys[i]));

			if(!insert.exec()) {
				lastError = insert.lastError().text();
				return false;
			}
		}

		return true;
	} else {
		lastError = "Feldmapping konnte nicht gespeichert werden. Problem mit Datenbankverbindung.\n\n" + connection->lastError().text();
		return false;
	}
}

bool RetrieveDataConfigModul::loadFieldMap(QString context, QMap<QString, QString>& fieldMap )
{
	QString connectionname = parameterMap["Feldconfig"].first;
	QString table = parameterMap["Feldconfig"].second;

	AcquiredConnection connection(connectionManager->acquireConnection(connectionname));

	if(connection->isOpen()) {
		QSqlQuery fields(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		fields.prepare("select * from " + syntax->delimitTablename(table) + " where context=?");
		fields.bindValue(0, context);

		if(fields.exec()) {
			while(fields.next()) {
				const QSqlRecord record = fields.record();
				fieldMap.insert(record.value("targetfield").toString(), record.value("sourcefield").toString());
			}

			return true;
		} else {
			addUserMessage("Feldinformationen konnten nicht geladen werden",2);
			lastError = "Feldinformationen konnten nicht geladen werden." + fields.lastError().text();
			return false;
		}
	} else {
		addUserMessage("Problem mit Datenbankverbindung",2);
		lastError = "Problem mit Datenbankverbindung." + connection->lastError().text();
		return false;
	}
}

bool RetrieveDataConfigModul::saveStageMap(QString context, QMap<int, QMap<QString, QPair<QString, QString>>>& stageMap )
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Stageconfig"].first));
	QString table = parameterMap["Stageconfig"].second;


	if(connection->isOpen()) {
		QSqlQuery insert(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		insert.prepare("insert into "+ syntax->delimitTablename(table) + " (context, stage, targetfield, sourcefield, matchmethod) VALUES (?, ?, ?, ?, ?)");
		insert.bindValue(0,context);

		const QList<int> stages(stageMap.keys());

		for(int i=0; i < stages.count(); ++i) {
			insert.bindValue(1, stages[i]);

			const QMap<QString, QPair<QString, QString>> currentStage(stageMap[stages[i]]);
			const QStringList targetFields(currentStage.keys());

			for(int j=0; j < targetFields.count(); ++j) {
				insert.bindValue(2,targetFields[j]);
				insert.bindValue(3,currentStage[targetFields[j]].first);
				insert.bindValue(4,currentStage[targetFields[j]].second);
				
				if(!insert.exec()) {
					lastError = insert.lastError().text();
					return false;
				}
			}
		}

		return true;
	} else {
		lastError = "Stagemapping konnte nicht gespeichert werden. Problem mit Datenbankverbindung.\n\n" + connection->lastError().text();
		return false;
	}
}


bool RetrieveDataConfigModul::loadStageMap(QString context, QMap<int, QMap<QString, QPair<QString, QString>>>& stageMap )
{
	QString connectionname = parameterMap["Stageconfig"].first;
	QString table = parameterMap["Stageconfig"].second;

	AcquiredConnection connection(connectionManager->acquireConnection(connectionname));

	if(connection->isOpen()) {
		QSqlQuery fields(*connection);

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		fields.prepare("select * from " + syntax->delimitTablename(table) + " where context=?");
		fields.bindValue(0, context);

		if(fields.exec()) {
			QStringList inputfieldList;

			while(fields.next()) {
				const QSqlRecord record = fields.record();
				const int stage = record.value("stage").toInt();

				stageMap[stage].insert(record.value("targetfield").toString(), QPair<QString,QString>(record.value("sourcefield").toString(),record.value("matchmethod").toString()));
			}

			return true;
		} else {
			addUserMessage("Stageinformationen konnten nicht geladen werden",2);
			lastError = "Stageinformationen konnten nicht geladen werden." + fields.lastError().text();
			return false;
		}
	} else {
		addUserMessage("Problem mit Datenbankverbindung",2);
		lastError = "Problem mit Datenbankverbindung." + connection->lastError().text();
		return false;
	}
}

QStringList RetrieveDataConfigModul::getSourceFields()
{
	return getFields(parameterMap["Quelltabelle"].first, parameterMap["Quelltabelle"].second);
}

QStringList RetrieveDataConfigModul::getTargetFields()
{
	return getFields(parameterMap["Zieltabelle"].first, parameterMap["Zieltabelle"].second);
}

QStringList RetrieveDataConfigModul::getFields( QString connectionname, QString tablename )
{
	AcquiredConnection connection(connectionManager->acquireConnection(connectionname));
	QStringList result;

	if(connection->isOpen()) {
		const QSqlRecord record(connection->record(tablename));

		for(int i=0; i < record.count(); ++i) {
			result<<record.fieldName(i);
		}
	} else {
		addUserMessage("Problem mit Datenbankverbindung",2);
		lastError = "Problem mit Datenbankverbindung." + connection->lastError().text();
	}

	return result;
}
