#ifndef syntaxanalyzer_h__
#define syntaxanalyzer_h__

#include <QVector>

#include "syntaxnodes.h"

class SyntaxAnalyzer {
public:
	SyntaxAnalyzer(SyntaxNode* rootNode);
	~SyntaxAnalyzer();

	SyntaxNode* getFirstBreadth(const QString& nodeType);
	SyntaxNode* getFirstDepth(const QString& nodeType);

	SyntaxNode* getCorresponding(const QString& nodeType);
	QVector<SyntaxNode*> getAllDepth(const QString& nodeType);
	QVector<SyntaxNode*> getAllBreadth(const QString& nodeType);

	QVector<SyntaxNode*> getNotContained(const QString& nodeType, const QStringList& elements);

	void reset();
private:
	void getAllDepth(SyntaxNode* node, const QString& nodeType, QVector<SyntaxNode*>& nodes);
	void getAllBreadth(QList<SyntaxNode*>& checkList, QVector<SyntaxNode*>& nodes, const QString& nodeType);

	void getNotContained(SyntaxNode* node, QVector<SyntaxNode*>& nodes, const QString& nodeType, const QStringList& elements);

	SyntaxNode* getFirstBreadth(QList<SyntaxNode*>& checkList, const QString& nodeType);
	SyntaxNode* getFirstDepth(SyntaxNode* node, const QString& nodeType);

	SyntaxNode* rootNode;
};
#endif // syntaxanalyzer_h__