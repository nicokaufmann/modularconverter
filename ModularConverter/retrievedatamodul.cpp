#include "retrievedatamodul.h"
#include <QStringBuilder>

RetrieveDataModul::RetrieveDataModul( ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion )
	: ConverterModul(parameterMap, versionMap, connectionManager, context, inheritVersion)
{
	sourceVersion = false;
}

RetrieveDataModul::~RetrieveDataModul()
{
}

QWidget* RetrieveDataModul::createWidget()
{
	return new RetrieveDataModulInterface(this);
}

QStringList RetrieveDataModul::getRequestedParameter()
{
	return (QStringList()<<"Zieltabelle"<<"Quelltabelle"<<"Stageconfig"<<"Feldconfig"<<"Versionierung");
}

bool RetrieveDataModul::useContext()
{
	return true;
}

bool RetrieveDataModul::initialize()
{
	
	targetTable = VersionRequest(parameterMap["Zieltabelle"].first,parameterMap["Zieltabelle"].second, "Zieltabelle");
	addVersionRequestInherited(targetTable);

	if(sourceVersion) {
		sourceTable = VersionRequest(parameterMap["Quelltabelle"].first,parameterMap["Quelltabelle"].second, "Quelltabelle");
		addVersionRequestInherited(sourceTable);
	}

	processInheritedRequests();
	return true;
}

bool RetrieveDataModul::execute(int cmd)
{
	QString sourcefields("");
	QString targetfields("");
	QString inputfields("");
	QMap<int, QString> matchConditions;
	QMap<int, QVector<int>> inputFieldPositions;

	queriesWithResults.clear();
	queriesWithoutResults.clear();

	if(loadFields(sourcefields, targetfields) && loadStages(matchConditions, inputFieldPositions, inputfields)) {
		QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 1));
		
		const QString versionStr(QString::number(generateVersion(parameterMap["Versionierung"].first,parameterMap["Versionierung"].second,"Retrieve Data (" + context + ") basierend auf v. " + QString::number(targetTable.versionId()))));

		//open connections
		AcquiredConnection sourceConnection(connectionManager->acquireConnection(parameterMap["Quelltabelle"].first));
		AcquiredConnection targetConnection(connectionManager->acquireConnection(parameterMap["Zieltabelle"].first));

		SyntaxElements* targetSyntax = targetConnection.getConnectionInformation().syntaxElements();
		SyntaxElements* sourceSyntax = sourceConnection.getConnectionInformation().syntaxElements();

		if(targetConnection->isOpen() && sourceConnection->isOpen()) {
			//get source count
			int sourceCount = 0;
			int progress = 0;
			int successfulInserts = 0;
			int failedInserts = 0;
			int failedMatchquerys = 0;


			QSqlQuery queryTargetCount(*targetConnection);
			queryTargetCount.setForwardOnly(true);

			if(queryTargetCount.exec("SELECT count(*) FROM " + targetSyntax->delimitTablename(parameterMap["Zieltabelle"].second) + "WHERE version_id=" + QString::number(targetTable.versionId())) && queryTargetCount.next()) {	
				sourceCount = queryTargetCount.record().value(0).toInt();
			}
			queryTargetCount.finish();
			queryTargetCount.clear();

			//retrieve
			QList<int> stages = matchConditions.keys();
			qSort(stages.begin(),stages.end());

			const QString matchStatement((sourceVersion)?QString("SELECT " % sourcefields % " FROM " % sourceSyntax->delimitTablename(parameterMap["Quelltabelle"].second) % " WHERE version_id=" % QString::number(sourceTable.versionId()) % " AND ")
													    :QString("SELECT " % sourcefields % " FROM " % sourceSyntax->delimitTablename(parameterMap["Quelltabelle"].second) % " WHERE "));
			const QString insertStatement("INSERT INTO " %  targetSyntax->delimitTablename(parameterMap["Zieltabelle"].second) % " (" % inputfields % ", unique_id, " % targetfields % ",version_id) VALUES (");
			const QString comma(",");

			QSqlQuery queryInsertData(*targetConnection);
			queryInsertData.setForwardOnly(true);

			QSqlQuery queryInputData(*targetConnection);
			queryInputData.setForwardOnly(true);

			if(queryInputData.exec("SELECT " % inputfields % ",unique_id FROM " % targetSyntax->delimitTablename(parameterMap["Zieltabelle"].second) % " WHERE version_id=" % QString::number(targetTable.versionId()))) {	
				while(queryInputData.next()) {
					const QSqlRecord inputRecord(queryInputData.record());

					//loop stages
					for(int i=0; i < stages.count(); ++i) {
						QSqlQuery matchQuery(*sourceConnection);
						matchQuery.setForwardOnly(true);
						matchQuery.prepare(matchStatement + matchConditions[stages[i]]);

						const QVector<int> inputFields(inputFieldPositions[stages[i]]);
						for(int j=0; j < inputFields.count(); ++j) {
							const QVariant tempVal(inputRecord.value(inputFields[j]));
							if(tempVal.isValid() && !tempVal.isNull() && tempVal.toString().length() > 0) {
								matchQuery.bindValue(j,tempVal);
							} else {
								//exec should fail if not all fields are filled
								break;
							}
						}

						if(matchQuery.exec()) {
							//insert found data
							bool done = false;
							while(matchQuery.next()) {
								done = true;
								const QSqlRecord matchRecord(matchQuery.record());

								QString values("");
								for(int j=0; j < inputRecord.count(); ++j) {
									values+=formatValueODBC(inputRecord.value(j),false) % comma;
								}

								for(int j=0; j < matchRecord.count(); ++j) {
									values+=formatValueODBC(matchRecord.value(j),false) % comma;
								}

								values+=versionStr;

								if(queryInsertData.exec(insertStatement % values % ")")) {
									successfulInserts++;
								} else {
									failedInserts++;
								}
							}

							if(done) {
								break;
							}
						} else {
							failedMatchquerys++;
						}
					}
					progress++;

					if(sourceCount > 0) {
						QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, (progress*100/(double)sourceCount)));
					}
				}

				addUserMessage(QString::number(progress) % " Datensätze verarbeitet",0);

// 				if(failedMatchquerys > 0) {
// 					addUserMessage(QString::number(failedMatchquerys)+ " Match-Abfragen fehlgeschlagen", 1);
// 				}
				if(failedInserts > 0) {
					addUserMessage(QString::number(failedInserts)+ " Ergebnisse wurden nicht übernommen (Insert failed)", 1);
				}
				addUserMessage(QString::number(successfulInserts)+ " Ergebnisse hinzugefügt", 0);

				QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 100));

				//versionmap hack
				versionMap[parameterMap["Zieltabelle"].first][parameterMap["Zieltabelle"].second + "#R" + versionStr] = versionStr.toInt();

				return true;
			} else {
				addUserMessage("Problem bei Abfrage der Zieltabelle",2);
				lastError = "Problem bei Abfrage der Zieltabelle.\n" + queryInputData.lastError().text() + "\n\n" + queryInputData.lastQuery();
				return false;
			}
		} else {
			addUserMessage("Problem mit Datenbankverbindung",2);
			lastError = "Problem mit Datenbankverbindung.\n" + targetConnection->lastError().text() + "\n" + sourceConnection->lastError().text();
			return false;
		}
	} else {
		return false;
	}	
}


bool RetrieveDataModul::loadFields( QString& sourcefields, QString& targetfields )
{
	QString connectionname = parameterMap["Feldconfig"].first;
	QString table = parameterMap["Feldconfig"].second;

	AcquiredConnection connection(connectionManager->acquireConnection(connectionname));
	SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

	if(connection->isOpen()) {
		QSqlQuery fields(*connection);

		SyntaxElements* sourceSyntax = connectionManager->getConnection(parameterMap["Quelltabelle"].first).syntaxElements();
		SyntaxElements* targetSyntax = connectionManager->getConnection(parameterMap["Zieltabelle"].first).syntaxElements();

		fields.prepare("select * from " + syntax->delimitTablename(table) + " where context=?");
		fields.bindValue(0, context);

		if(fields.exec()) {
			while(fields.next()) {
				const QSqlRecord record = fields.record();
				sourcefields+=sourceSyntax->delimitFieldname(record.value("sourcefield").toString()) + ",";
				targetfields+=targetSyntax->delimitFieldname(record.value("targetfield").toString()) + ",";
			}
			sourcefields.chop(1);
			targetfields.chop(1);

			return true;
		} else {
			addUserMessage("Feldinformationen konnten nicht geladen werden",2);
			lastError = "Feldinformationen konnten nicht geladen werden." + fields.lastError().text();
			return false;
		}
	} else {
		addUserMessage("Problem mit Datenbankverbindung",2);
		lastError = "Problem mit Datenbankverbindung." + connection->lastError().text();
		return false;
	}
}

bool RetrieveDataModul::loadStages(QMap<int, QString>& matchConditions, QMap<int, QVector<int>>& inputFieldPositions, QString& inputfields )
{
	QString connectionname = parameterMap["Stageconfig"].first;
	QString table = parameterMap["Stageconfig"].second;

	AcquiredConnection connection(connectionManager->acquireConnection(connectionname));
	SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

	if(connection->isOpen()) {
		QSqlQuery fields(*connection);
		fields.prepare("select * from " + syntax->delimitTablename(table) + " where context=?");
		fields.bindValue(0, context);

		if(fields.exec()) {
			QStringList inputfieldList;

			SyntaxElements* sourceSyntax = connectionManager->getConnection(parameterMap["Quelltabelle"].first).syntaxElements();
			SyntaxElements* targetSyntax = connectionManager->getConnection(parameterMap["Zieltabelle"].first).syntaxElements();

			while(fields.next()) {
				const QSqlRecord record = fields.record();
				const int stage = record.value("stage").toInt();

				if(matchConditions.contains(stage) && matchConditions[stage].length() > 0) {
					matchConditions[stage]+=" AND ";
				}

				matchConditions[stage]+=sourceSyntax->delimitFieldname(record.value("sourcefield").toString()) + record.value("matchmethod").toString() + "?";

				const QString inputfield(targetSyntax->delimitFieldname(record.value("targetfield").toString()));
				if(!inputfieldList.contains(inputfield)) {
					inputfieldList<<inputfield;
					if(inputfields.length() > 0) {
						inputfields+=",";
					}
					inputfields+=inputfield;
				}

				inputFieldPositions[stage].append(inputfieldList.indexOf(inputfield));
			}
			return true;
		} else {
			addUserMessage("Stageinformationen konnten nicht geladen werden",2);
			lastError = "Stageinformationen konnten nicht geladen werden." + fields.lastError().text();
			return false;
		}
	} else {
		addUserMessage("Problem mit Datenbankverbindung",2);
		lastError = "Problem mit Datenbankverbindung." + connection->lastError().text();
		return false;
	}
}

QString RetrieveDataModul::formatValueODBC( const QVariant &field, bool trimStrings )
{
	QString r;
	if (field.isNull()) {
		r = QLatin1String("NULL");
	} else if (field.type() == QVariant::DateTime) {
		// Use an escape sequence for the datetime fields
		if (field.toDateTime().isValid()){
			QDate dt = field.toDateTime().date();
			QTime tm = field.toDateTime().time();
			// Dateformat has to be "yyyy-MM-dd hh:mm:ss", with leading zeroes if month or day < 10
			r = QLatin1String("{ ts '") +
				QString::number(dt.year()) + QLatin1Char('-') +
				QString::number(dt.month()).rightJustified(2, QLatin1Char('0'), true) +
				QLatin1Char('-') +
				QString::number(dt.day()).rightJustified(2, QLatin1Char('0'), true) +
				QLatin1Char(' ') +
				tm.toString() +
				QLatin1String("' }");
		} else
			r = QLatin1String("NULL");
	} else if(field.type() == QVariant::Date) {
		if (field.toDate().isValid()){
			QDate dt = field.toDateTime().date();
			// Dateformat has to be "yyyy-MM-dd", with leading zeroes if month or day < 10
			r = QLatin1String("{ d '") +
				QString::number(dt.year()) + QLatin1Char('-') +
				QString::number(dt.month()).rightJustified(2, QLatin1Char('0'), true) +
				QLatin1Char('-') +
				QString::number(dt.day()).rightJustified(2, QLatin1Char('0'), true) +
				QLatin1String("' }");
		} else
			r = QLatin1String("NULL");
	} else if (field.type() == QVariant::ByteArray) {
		QByteArray ba = field.toByteArray();
		QString res;
		static const char hexchars[] = "0123456789abcdef";
		for (int i = 0; i < ba.size(); ++i) {
			uchar s = (uchar) ba[i];
			res += QLatin1Char(hexchars[s >> 4]);
			res += QLatin1Char(hexchars[s & 0x0f]);
		}
		r = QLatin1String("0x") + res;
	} else {
		r = formatValue(field, trimStrings);
	}
	return r;
}

QString RetrieveDataModul::formatValue( const QVariant &field, bool trimStrings )
{
	const QLatin1String nullTxt("NULL");

	QString r;
	if (field.isNull())
		r = nullTxt;
	else {
		switch (field.type()) {
		case QVariant::Int:
		case QVariant::UInt:
			if (field.type() == QVariant::Bool)
				r = field.toBool() ? QLatin1String("1") : QLatin1String("0");
			else
				r = field.toString();
			break;
		case QVariant::String:
		case QVariant::Char:
			{
				QString result = field.toString();
				if (trimStrings) {
					int end = result.length();
					while (end && result.at(end-1).isSpace()) /* skip white space from end */
						end--;
					result.truncate(end);
				}
				/* escape the "'" character */
				result.replace(QLatin1Char('\''), QLatin1String("''"));
				r = QLatin1Char('\'') + result + QLatin1Char('\'');
				break;
			}
		case QVariant::Bool:
			r = QString::number(field.toBool());
			break;
		case QVariant::ByteArray : {
			QByteArray ba = field.toByteArray();
			QString res;
			static const char hexchars[] = "0123456789abcdef";
			for (int i = 0; i < ba.size(); ++i) {
				uchar s = (uchar) ba[i];
				res += QLatin1Char(hexchars[s >> 4]);
				res += QLatin1Char(hexchars[s & 0x0f]);
			}
			r = QLatin1Char('\'') + res +  QLatin1Char('\'');
			break;
								   }
		default:
			r = field.toString();
			break;
		}
	}
	return r;
}

QStringList RetrieveDataModul::getQueriesWithResults()
{
	return queriesWithResults;
}

QStringList RetrieveDataModul::getQueriesWithoutResults()
{
	return queriesWithoutResults;
}

int RetrieveDataModul::getResultCount()
{
	return queriesWithResults.count() + queriesWithoutResults.count();
}
