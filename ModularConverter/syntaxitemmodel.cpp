#include "SyntaxItemModel.h"

SyntaxItemModel::SyntaxItemModel(QStringList itemList, QObject *parent)
	: QAbstractItemModel(parent), itemList(itemList), iconPath(""), itemType("")
{
}

SyntaxItemModel::SyntaxItemModel( QStringList itemList, QString iconPath, QObject *parent)
	: QAbstractItemModel(parent), itemList(itemList), iconPath(iconPath), icon(QIcon(iconPath)), itemType("")
{

}

SyntaxItemModel::SyntaxItemModel( QStringList itemList, QString iconPath, QString itemType, QObject *parent)
	: QAbstractItemModel(parent), itemList(itemList), iconPath(iconPath), icon(QIcon(iconPath)), itemType(itemType)
{

}

Qt::ItemFlags SyntaxItemModel::flags( const QModelIndex & index ) const
{
	return Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsSelectable;
}


QVariant SyntaxItemModel::data( const QModelIndex & index, int role) const
{
	if(role == Qt::DecorationRole) {
		return icon;
	} else if(role == Qt::DisplayRole) {
		if(index.isValid()) {
			if(index.row() < itemList.size()) {
				return QVariant(itemList[index.row()]);
			} 
		}
	}

	return invalidVariant;
}

QVariant SyntaxItemModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	return invalidVariant;
}

int SyntaxItemModel::rowCount( const QModelIndex & parent /*= QModelIndex() */ ) const
{
	return itemList.count();
}

int SyntaxItemModel::columnCount( const QModelIndex & parent /*= QModelIndex() */ ) const
{
	return 1;
}

QModelIndex SyntaxItemModel::parent( const QModelIndex &index ) const
{
	return invalidModelIndex;
}

QModelIndex SyntaxItemModel::index( int row, int column, const QModelIndex &parent ) const
{
	if (column == 0 && !parent.isValid()) {
		if(row < itemList.size()) {
			return createIndex(row, column);
		}
	}

	return invalidModelIndex;
}

void SyntaxItemModel::setItemList( QStringList itemList )
{
	beginRemoveRows(invalidModelIndex,0, itemList.count());
	endRemoveRows();

	this->itemList = itemList;

	beginInsertRows(invalidModelIndex,0, itemList.count());
	endInsertRows();
}

void SyntaxItemModel::setIconPath( QString iconPath )
{
	this->iconPath = iconPath;
	this->icon = QIcon(iconPath);
}

QMimeData* SyntaxItemModel::mimeData( const QModelIndexList & indexes ) const
{
	if(indexes.size() == 0) {
		return 0;
	} else {
		QMimeData* data = new QMimeData;
		QString textData("");
		int itemCount = 0;
		for(int i=0; i < indexes.size(); ++i) {
			if (indexes[i].isValid()) {
				textData += indexes[i].data(Qt::DisplayRole).toString()+";";
				itemCount++;
			}
		}

		if(itemCount > 0) {
			textData.chop(1);
		}

		data->setText(textData);
		data->setData("application/typeid", itemType.toAscii());
		return data;
	}
}

QStringList SyntaxItemModel::mimeTypes() const
{
	return (QStringList() << "text/plain" << "application/typeid");
}