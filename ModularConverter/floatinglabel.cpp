#include "floatinglabel.h"

FloatingLabel::FloatingLabel( int id, QString labelText, QWidget *parent , Qt::WFlags flags )
	: QWidget(parent,flags), id(id), labelText(labelText)
{
	ui.setupUi(this);

	ui.label->setText(labelText);
	QObject::connect(ui.bRemove, SIGNAL(clicked()), this, SLOT(removeLabel()));
}

FloatingLabel::~FloatingLabel()
{

}


int FloatingLabel::getId()
{
	return id;
}

QString FloatingLabel::getLabelText()
{
	return labelText;
}

void FloatingLabel::removeLabel()
{
	emit removed(id);
	emit removed(this);
}
