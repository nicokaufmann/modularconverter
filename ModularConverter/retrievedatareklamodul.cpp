#include "retrievedatareklamodul.h"

#include <QStringBuilder>

RetrieveDataReklaModul::RetrieveDataReklaModul( ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion )
	: RetrieveDataModul(parameterMap, versionMap, connectionManager, context, inheritVersion)
{

}

RetrieveDataReklaModul::~RetrieveDataReklaModul()
{

}

bool RetrieveDataReklaModul::execute( int cmd )
{
	QString sourcefields("");
	QString targetfields("");
	QString inputfields("");
	QMap<int, QString> matchConditions;
	QMap<int, QVector<int>> inputFieldPositions;

	queriesWithResults.clear();
	queriesWithoutResults.clear();

	if(loadFields(sourcefields, targetfields) && loadStages(matchConditions, inputFieldPositions, inputfields)) {
		QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 1));

		const QString versionStr(QString::number(generateVersion(parameterMap["Versionierung"].first,parameterMap["Versionierung"].second,"Retrieve Data (" + context + ") basierend auf v. " + QString::number(targetTable.versionId()))));

		//open connections
		AcquiredConnection sourceConnection(connectionManager->acquireConnection(parameterMap["Quelltabelle"].first));
		AcquiredConnection targetConnection(connectionManager->acquireConnection(parameterMap["Zieltabelle"].first));

		SyntaxElements* targetSyntax = targetConnection.getConnectionInformation().syntaxElements();
		SyntaxElements* sourceSyntax = sourceConnection.getConnectionInformation().syntaxElements();

		if(targetConnection->isOpen() && sourceConnection->isOpen()) {
			//get source count
			int sourceCount = 0;
			int progress = 0;
			int successfulInserts = 0;
			int failedInserts = 0;
			int failedMatchquerys = 0;


			QSqlQuery queryTargetCount(*targetConnection);
			queryTargetCount.setForwardOnly(true);

			if(queryTargetCount.exec("SELECT count(*) FROM " + targetSyntax->delimitTablename(parameterMap["Zieltabelle"].second) + "WHERE version_id=" + QString::number(targetTable.versionId())) && queryTargetCount.next()) {	
				sourceCount = queryTargetCount.record().value(0).toInt();
			}
			queryTargetCount.finish();
			queryTargetCount.clear();

			//retrieve
			QList<int> stages = matchConditions.keys();
			qSort(stages.begin(),stages.end());

			const QString matchStatement((sourceVersion)?QString("SELECT " % sourcefields % " FROM " % sourceSyntax->delimitTablename(parameterMap["Quelltabelle"].second) % " WHERE version_id=" % QString::number(sourceTable.versionId()) % " AND ")
				:QString("SELECT " % sourcefields % " FROM " % sourceSyntax->delimitTablename(parameterMap["Quelltabelle"].second) % " WHERE "));
			const QString insertStatement("INSERT INTO " %  targetSyntax->delimitTablename(parameterMap["Zieltabelle"].second) % " (unique_id, " % targetfields % ",version_id) VALUES (");
			const QString comma(",");

			QSqlQuery queryInsertData(*targetConnection);
			queryInsertData.setForwardOnly(true);

			QSqlQuery queryInputData(*targetConnection);
			queryInputData.setForwardOnly(true);

			if(queryInputData.exec("SELECT " % inputfields % ",unique_id FROM " % targetSyntax->delimitTablename(parameterMap["Zieltabelle"].second) % " WHERE version_id=" % QString::number(targetTable.versionId()))) {	
				while(queryInputData.next()) {
					const QSqlRecord inputRecord(queryInputData.record());

					//loop stages
					for(int i=0; i < stages.count(); ++i) {
						QSqlQuery matchQuery(*sourceConnection);
						matchQuery.setForwardOnly(true);
						matchQuery.prepare(matchStatement + matchConditions[stages[i]]);

						const QVector<int> inputFields(inputFieldPositions[stages[i]]);
						for(int j=0; j < inputFields.count(); ++j) {
							const QVariant tempVal(inputRecord.value(inputFields[j]));
							if(tempVal.isValid() && !tempVal.isNull() && tempVal.toString().length() > 0) {
								matchQuery.bindValue(j,tempVal);
							} else {
								//exec should fail if not all fields are filled
								break;
							}
						}

						if(matchQuery.exec()) {
							//insert found data
							bool done = false;
							while(matchQuery.next()) {
								done = true;
								const QSqlRecord matchRecord(matchQuery.record());

								QString values(formatValueODBC(inputRecord.value("unique_id"),false) % comma);

								for(int j=0; j < matchRecord.count(); ++j) {
									values+=formatValueODBC(matchRecord.value(j),false) % comma;
								}

								values+=versionStr;

								if(queryInsertData.exec(insertStatement % values % ")")) {
									successfulInserts++;
								} else {
									failedInserts++;
								}
							}

							if(done) {
								break;
							}
						} else {
							failedMatchquerys++;
						}
					}
					progress++;

					if(sourceCount > 0) {
						QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, (progress*100/(double)sourceCount)));
					}
				}

				addUserMessage(QString::number(progress) % " Datensätze verarbeitet",0);

				// 				if(failedMatchquerys > 0) {
				// 					addUserMessage(QString::number(failedMatchquerys)+ " Match-Abfragen fehlgeschlagen", 1);
				// 				}
				if(failedInserts > 0) {
					addUserMessage(QString::number(failedInserts)+ " Ergebnisse wurden nicht übernommen (Insert failed)", 1);
				}
				addUserMessage(QString::number(successfulInserts)+ " Ergebnisse hinzugefügt", 0);

				QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 100));

				//versionmap hack
				versionMap[parameterMap["Zieltabelle"].first][parameterMap["Zieltabelle"].second + "#R" + versionStr] = versionStr.toInt();

				return true;
			} else {
				addUserMessage("Problem bei Abfrage der Zieltabelle",2);
				lastError = "Problem bei Abfrage der Zieltabelle.\n" + queryInputData.lastError().text() + "\n\n" + queryInputData.lastQuery();
				return false;
			}
		} else {
			addUserMessage("Problem mit Datenbankverbindung",2);
			lastError = "Problem mit Datenbankverbindung.\n" + targetConnection->lastError().text() + "\n" + sourceConnection->lastError().text();
			return false;
		}
	} else {
		return false;
	}	
}

