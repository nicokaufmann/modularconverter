#ifndef modulslotsetup_h__
#define modulslotsetup_h__

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QPair>
#include <QString>
#include <QStringList>
#include "ui_ModulSlotSetup.h"

#include "connectionmanager.h"

class ModulSlotSetup : public QWidget
{
	Q_OBJECT

public:
	ModulSlotSetup(ConnectionManager* connectionManager, QString alias, QWidget *parent = 0, Qt::WFlags flags = 0);
	~ModulSlotSetup();

	QPair<QString, QString> getParameter();
	void setParameter(QPair<QString, QString> param);
	
	QString getAlias();

	QStringList getContextList();

public slots:
	void selectedDatabase(int index);
	void selectedTable(int index);

	void tablesUpdated(QString connectionname);

signals:
	void tableSelected(ModulSlotSetup* modulSlot);

private:
	QString alias;
	ConnectionManager* connectionManager;

	Ui::ModulSlotSetup ui;
};
#endif // modulslotsetup_h__