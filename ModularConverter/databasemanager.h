#ifndef DatabaseManager_h__
#define DatabaseManager_h__

#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QStringList>
#include <QVariant>
#include <QList>
#include <QMap>
#include <QComboBox>
#include <applicationuser.h>

#include "connectioninformation.h"
#include "connectionlocker.h"
#include "simplecrypt.h"

class DatabaseManager : public QObject {
	Q_OBJECT
public:
	DatabaseManager();
	~DatabaseManager();

	bool checkApplication(ConnectionInformation connection);
	bool createApplication(ConnectionInformation connection);
	bool connect(QString username, QString password, ConnectionInformation connection);

	bool addDatabase(ConnectionInformation connection);
	bool removeDatabase(ConnectionInformation& connection);
	bool updateDatabase(ConnectionInformation connection);

	ConnectionInformation& getCoreConnection();
	ConnectionInformation& getConnection(QString connectionname);
	QStringList connectionList();
	bool containsConnection(QString connectionname);

	ApplicationUser getCurrentUser();

	DriverInformation getDriver(QString alias);

	QString getLastError();
signals:
	void databasesUpdated();

private:
	bool loadConnections();
	ApplicationUser loadUser(QString username, QString password);

	ApplicationUser currentUser;
	ConnectionInformation coreConnection;
	QMap<QString, ConnectionInformation> databaseMap;
	QMap<QString, DriverInformation> driverMap;

	QString lastError;
};

#endif // DatabaseManager_h__