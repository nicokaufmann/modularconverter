#ifndef choseelementsdialog_h__
#define choseelementsdialog_h__

#include <QtGui/QDialog>
#include <QListWidgetItem>

#include "ui_choseelementsdialog.h"

class ChoseElementsDialog : public QDialog
{
	Q_OBJECT

public:
	ChoseElementsDialog(const QStringList& elements, bool multiSelect, QWidget *parent = 0, Qt::WFlags flags = 0);
	~ChoseElementsDialog();

	void setTilte(const QString& title);
	QStringList getSelectedElements();

public slots:

private:

	Ui::ChoseElementsDialog ui;
};
#endif // choseelementsdialog_h__