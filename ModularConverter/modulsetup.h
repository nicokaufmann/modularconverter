#ifndef modulsetup_h__
#define modulsetup_h__

#include <QtGui/QWidget>
#include "modulslotsetup.h"
#include "ui_ModulSetup.h"

#include "connectionmanager.h"
#include "modulmanager.h"

class ModulSetup : public QWidget
{
	Q_OBJECT

public:
	ModulSetup(ConnectionManager* connectionManager, ModulManager* modulManager, int index, QWidget *parent = 0, Qt::WFlags flags = 0);
	~ModulSetup();

	void setIndex(int index);

	QString getModulDescription();
	void setModulDescription(QString description);

	QString getModulName();
	void setModulName(QString modulName);

	QString getModulContext();
	void setModulContext(QString modulContext);

	bool getModulInheritVersion();
	void setModulInheritVersion(bool inheritVersion);

	ParameterMap getParameterMap();
	void setParameterMap(ParameterMap& params);
	
public slots:
	void selected(int index);
	void addAfter();
	void removeCurrent();
	void tableSelected(ModulSlotSetup* modulSlot);

signals:
	void addAfter(int index);
	void removeCurrent(int index);

private:
	void clearItemList();
	void buildItemListLayout();

	int index;
	ConnectionManager* connectionManager;
	ModulManager* modulManager;

	QList<ModulSlotSetup*> itemList;

	Ui::ModulSetup ui;
};
#endif // modulsetup_h__