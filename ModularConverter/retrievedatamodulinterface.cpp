#include "retrievedatamodulinterface.h"

#include "listdialog.h"
#include "retrievedatamodul.h"


RetrieveDataModulInterface::RetrieveDataModulInterface( RetrieveDataModul* modul, QWidget *parent, Qt::WFlags flags )
	: StandardModulInterface(modul, parent, flags), executeInterface(new ExecuteModulInterface(this)), detailsInterface(new RetrieveDetailsModulInterface(this))
{
	addModulWidget(new SelectVersionModulInterface(modul->getConnectionManager(), modul->getGuiVersionRequests(), modul->getParameterMap()["Versionierung"],this));
	addModulWidget(executeInterface);
	addModulWidget(detailsInterface);

	detailsInterface->setVisible(false);

	QObject::connect(executeInterface->executeButton(), SIGNAL(clicked()), this, SLOT(executeModul()));
	QObject::connect(detailsInterface->withResultsButton(), SIGNAL(clicked()), this, SLOT(showWithResult()));
	QObject::connect(detailsInterface->withoutResultsButton(), SIGNAL(clicked()), this, SLOT(showWithoutResult()));

}

RetrieveDataModulInterface::~RetrieveDataModulInterface()
{

}

void RetrieveDataModulInterface::finished( bool status )
{
	userMessageWidget->updateMessages();

	if(status) {
		RetrieveDataModul* retrieveModul = (RetrieveDataModul*)modul;
		detailsInterface->setVisible(retrieveModul->getResultCount() > 0);
	}
}

void RetrieveDataModulInterface::executeModul()
{
	modul->clearUserMessages();
	userMessageWidget->updateMessages();

	detailsInterface->setVisible(false);

	emit execute();
}

void RetrieveDataModulInterface::showWithResult()
{
	RetrieveDataModul* retrieveModul = (RetrieveDataModul*)modul;

	ListDialog dlg("Statements die ein Ergebnis zur�ckgeliefert haben", retrieveModul->getQueriesWithResults());
	dlg.exec();
}

void RetrieveDataModulInterface::showWithoutResult()
{
	RetrieveDataModul* retrieveModul = (RetrieveDataModul*)modul;

	ListDialog dlg("Statements die kein Ergebnis zur�ckgeliefert haben", retrieveModul->getQueriesWithoutResults());
	dlg.exec();
}

