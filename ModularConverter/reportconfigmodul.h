#ifndef reportconfigmodul_h__
#define reportconfigmodul_h__

#include "convertermodul.h"

#include <QStringList>
#include <QMap>
#include <QVariant>

#include "databasemanager.h"

#include "reportconfigurationmodul.h"

class ReportConfigModul : public ConverterModul {
public:

	ReportConfigModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~ReportConfigModul();

	QStringList getContextList();
	QStringList getDatabaseNames();
	QStringList getFieldNames( QString connectionName, QString statement );

	bool execute(int cmd=0);
	QWidget* createWidget();

	bool loadReportConfig( QString context, QMap<QString, QVariant>& cfg );
	bool getStatementIds( QString context, QStringList& statementIds );
	bool loadStatementConfig( QString context, QString statementId, QMap<QString, QVariant>& statementCfg );

	bool deleteContext( QString context );
	bool saveReportConfig( QString context, QMap<QString, QVariant>& cfg);
	bool saveStatementConfig( QString context, QString statementId, QMap<QString, QVariant>& statementCfg );

	static QStringList getRequestedParameter();
private:
};
#endif // reportconfigmodul_h__