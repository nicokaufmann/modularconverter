#ifndef datatab_h__
#define datatab_h__

#include <QWidget>
#include <QSqlDatabase>
#include <QString>
#include <QMessageBox>
#include <QStringListModel>

#include <QtConcurrentRun>
#include <QFuture>
#include <QFutureWatcher>

#include "dataentrydialog.h"
#include "datamanipulationdialog.h"
#include "datawindow.h"
#include "sqlhighlighter.h"
#include "textedit.h"
#include "simplesqleditor.h"
#include "ui_datatab.h"

#include "applicationdatabase.h"

class DataTab : public QWidget
{
	Q_OBJECT

public:
	DataTab(ApplicationDatabase* applicationDatabase, SimpleSqlEditor* simpleSqlEditor, QWidget *parent = 0, Qt::WFlags flags = 0);
	~DataTab();

	void setPermissions(ApplicationUser user);
public slots:
	void selectDatabase(int index);
	void databasesUpdated();
	void tableDataUpdated(QString connectionname);
	
	void openSqlEditor();
	void showTableData();
	void showQueryData();
	void executeStatement();
	void statementExecutionFinished();
	void insertData();
	void closing(DataWindow* window);
private:
	bool statementExecution(QString statement);

	ApplicationDatabase* applicationDatabase;
	ConnectionManager* connectionManager;

	QFutureWatcher<bool> watchExecution;
	QString executionError;

	QList<DataWindow*> windowList;

	TextEdit* sqlTextEdit;
	SqlHighlighter* sqlHighlighter;
	SimpleSqlEditor* simpleSqlEditor;
	Ui::DataTab ui;
};
#endif datatab_h__