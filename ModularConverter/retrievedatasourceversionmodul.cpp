#include "retrievedatasourceversionmodul.h"

RetrieveDataSourceVersionModul::RetrieveDataSourceVersionModul( ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion )
	: RetrieveDataModul(parameterMap, versionMap, connectionManager, context, inheritVersion)
{
	sourceVersion = true;
}

