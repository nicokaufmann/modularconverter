#ifndef databaseelementsupdater_h__
#define databaseelementsupdater_h__

#include "databaseelements.h"

class ConnectionManager;

class DatabaseElementsUpdater  : public QObject {
	Q_OBJECT
public:
	DatabaseElementsUpdater(ConnectionManager* connectionManager);
	~DatabaseElementsUpdater();

	void updateElements();
	void updateElementsBlocking();
	void updateElements(QString connectionname);
	void updateElementsBlocking(QString connectionname);

	void addElements(DatabaseElements* elements);
	void removeElements(DatabaseElements* elements);

public slots:
	void updatedDatabaseElementsTables(QString connectionname);
	void updatedDatabaseElementsAttributes(QString connectionname);

	void databasesUpdated();
signals:
	void elementsUpdated();
	void databaseElementsUpdated(QString connectionname);

private:
	ConnectionManager* connectionManager;


};
#endif // databaseelementsupdater_h__
