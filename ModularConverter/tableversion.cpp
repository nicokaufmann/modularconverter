#include "tableversion.h"

TableVersion::TableVersion( const QMap<QString,DataVersion>& versionMap, QStringList relevantVersions, VersionRequest requestedVersion, QWidget *parent, Qt::WFlags flags)
	: QWidget(parent, flags), versionMap(versionMap), requestedVersion(requestedVersion)
{
	ui.setupUi(this);

	if(requestedVersion.context() != "") {
		ui.lTableContext->setText(requestedVersion.context());
	} else {
		ui.lTableContext->setText(requestedVersion.connectionname() + ":" + requestedVersion.tablename());
	}

	ui.cbVersions->clear();
	ui.cbVersions->addItems(relevantVersions);

	selectVersion(0);

	QObject::connect(ui.cbVersions, SIGNAL(activated(int)), this, SLOT(selectVersion(int)));
}

TableVersion::~TableVersion()
{
}

void TableVersion::selectVersion( int index )
{
	if(versionMap.contains(ui.cbVersions->currentText())) {
		ui.lUsername->setText(versionMap[ui.cbVersions->currentText()].username);
		ui.lDescription->setText(versionMap[ui.cbVersions->currentText()].description);
		ui.lDateTime->setText(versionMap[ui.cbVersions->currentText()].time.toString("dd.MM.yyyy hh:mm:ss"));

		requestedVersion.setVersionId(ui.cbVersions->currentText().toUInt());
	}
}

