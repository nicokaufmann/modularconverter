#ifndef applicationuser_h__
#define applicationuser_h__

#include <QString>
#include <QMap>
#include <QSharedPointer>

class ApplicationUser {
public:
	ApplicationUser();
	ApplicationUser(QString username, bool admin);
	ApplicationUser(const ApplicationUser& obj);
	ApplicationUser& operator= (const ApplicationUser& obj);

	bool isValid() const;

	QString username() const;
	bool isAdmin() const;

	bool hasFeaturePermission(const QString& feature) const;
	bool hasWorkflowPermission(const int& workflowId) const;

	void setWorkflowPermission(const int& workflowId, const bool& permission);
	void setFeaturePermission(const QString& feature, const bool& permission);
private:
	QSharedPointer<bool> sharedValid;
	QSharedPointer<QString> sharedUsername;
	QSharedPointer<bool> sharedAdmin;
	QSharedPointer<QMap<int,bool>> workflowPermissions;
	QSharedPointer<QMap<QString, bool>> featurePermissions;
};
#endif // applicationuser_h__