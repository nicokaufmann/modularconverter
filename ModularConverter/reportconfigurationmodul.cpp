#include "reportconfigurationmodul.h"

#include "reportconfigmodul.h"

ReportConfigurationModul::ReportConfigurationModul(ReportConfigModul* rcm, QWidget *parent, Qt::WFlags flags )
	: QWidget(parent,flags), rcm(rcm)
{
	ui.setupUi(this);

	init();

	QObject::connect(ui.cbContext, SIGNAL(activated(int)), this, SLOT(selectContext(int)));
	QObject::connect(ui.bSave, SIGNAL(clicked()), this, SLOT(saveContext()));
	QObject::connect(ui.bDelete, SIGNAL(clicked()), this, SLOT(deleteContext()));
	QObject::connect(ui.bNewContext, SIGNAL(clicked()), this, SLOT(createContext()));
	QObject::connect(ui.bTemplatepath, SIGNAL(clicked()), this, SLOT(changeTemplatepath()));
	QObject::connect(ui.bAddStatement, SIGNAL(clicked()), this, SLOT(addStatement()));
}

ReportConfigurationModul::~ReportConfigurationModul()
{

}

void ReportConfigurationModul::init()
{
	ui.cbContext->clear();
	ui.cbContext->addItems(rcm->getContextList());
	selectContext(0);
}

void ReportConfigurationModul::selectContext( int index )
{
	QMessageBox msgBox;
	msgBox.setStandardButtons(QMessageBox::Ok);
	msgBox.setDefaultButton(QMessageBox::Ok);
	msgBox.setWindowTitle("Fehler");
	msgBox.setText("Fehler bei der Auswahl des Kontext");
	msgBox.setIcon(QMessageBox::Critical);
	

	QMap<QString, QVariant> cfg;
	if(!rcm->loadReportConfig(ui.cbContext->itemText(index), cfg)) {
		msgBox.setDetailedText(rcm->getLastError());
		msgBox.exec();
		return;
	}
	setReportValues(cfg);
	clearStatements();

	QStringList statementIds;
	if(!rcm->getStatementIds(ui.cbContext->itemText(index),statementIds)) {
		msgBox.setDetailedText(rcm->getLastError());
		msgBox.exec();
		return;
	}

	for(int i=0; i < statementIds.size();++i) {
		QMap<QString, QVariant> statementCfg;
		if(rcm->loadStatementConfig(ui.cbContext->itemText(index),statementIds[i],statementCfg)) {
			StatementConfigurationModul* scm = new StatementConfigurationModul(statementIds[i].toInt(),rcm,this);
			scm->setStatementValues(statementCfg);

			statementList.push_back(scm);
			ui.statementList->addWidget(scm);

			QObject::connect(statementList[i], SIGNAL(removeStatement(int)), this, SLOT(removeStatement(int)));
		}
	}
}

void ReportConfigurationModul::createContext()
{
	QString name=QInputDialog::getText(this,"Eingabe","Kontextname:");

	if(!name.isEmpty() && ui.cbContext->findText(name) == -1) {
		ui.cbContext->addItem(name);
		ui.cbContext->setCurrentIndex(ui.cbContext->findText(name));

		resetReportValues();
		clearStatements();
	}
}

void ReportConfigurationModul::changeTemplatepath()
{
	ui.lTemplatePath->setText(QFileDialog::getOpenFileName(this,"Template ausw�hlen","","Excel Arbeitsmappe (*.xls *.xlsx)"));
}

void ReportConfigurationModul::addStatement()
{
	StatementConfigurationModul* scm;
	if(statementList.size() > 0) {
		scm = new StatementConfigurationModul(statementList.last()->getId()+1,rcm,this);
	} else {
		scm = new StatementConfigurationModul(1,rcm,this);
	}

	statementList.push_back(scm);
	ui.statementList->addWidget(scm);

	QObject::connect(scm, SIGNAL(removeStatement(int)), this, SLOT(removeStatement(int)));
}

void ReportConfigurationModul::setReportValues( QMap<QString, QVariant>& cfg )
{
	ui.cbTemplate->setChecked(cfg["template.enabled"].toBool());
	ui.lTemplatePath->setText(cfg["template.path"].toString());
}

void ReportConfigurationModul::resetReportValues()
{
	ui.cbTemplate->setChecked(false);
	ui.lTemplatePath->setText("");
}

void ReportConfigurationModul::clearStatements()
{
	// clear widgets
	for(int i=0; i < statementList.size(); ++i) {
		QObject::disconnect(statementList[i], SIGNAL(removeStatement(int)), this, SLOT(removeStatement(int)));

		delete statementList[i];
	}
	statementList.clear();

	// clear layout
	QLayoutItem *child;
	while ((child = ui.statementList->takeAt(0)) != 0) {
		delete child;
	}
}

void ReportConfigurationModul::saveContext()
{
	QMessageBox msgBox;
	msgBox.setStandardButtons(QMessageBox::Ok);
	msgBox.setDefaultButton(QMessageBox::Ok);
	msgBox.setWindowTitle("Fehler");
	msgBox.setText("Fehler beim speichern des Kontexts");
	msgBox.setIcon(QMessageBox::Critical);

	if(rcm->deleteContext(ui.cbContext->currentText())) {
		QMap<QString, QVariant> mainCfg;
		mainCfg["template.enabled"] = ui.cbTemplate->isChecked();
		mainCfg["template.path"] = ui.lTemplatePath->text();

		if(rcm->saveReportConfig(ui.cbContext->currentText(),mainCfg)) {
			QMap<QString, QVariant> statementCfg;
			for(int i=0; i < statementList.size(); ++i) {
				statementCfg.clear();
				statementList[i]->getStatementValues(statementCfg);

				if(!rcm->saveStatementConfig(ui.cbContext->currentText(),QString::number(statementList[i]->getId()),statementCfg)) {
					msgBox.setDetailedText(rcm->getLastError());
					msgBox.exec();
				}
			}
		} else {
			msgBox.setDetailedText(rcm->getLastError());
			msgBox.exec();
		}
	} else {
		msgBox.setDetailedText(rcm->getLastError());
		msgBox.exec();
	}
}

void ReportConfigurationModul::removeStatement( int id )
{
	for(int i=0; i < statementList.size(); ++i) {
		if(statementList[i]->getId()==id) {
			QObject::disconnect(statementList[i], SIGNAL(removeStatement(int)), this, SLOT(removeStatement(int)));
			delete statementList[i];
			statementList.removeAt(i);
			return;
		}
	}
}

void ReportConfigurationModul::deleteContext()
{
	if(ui.cbContext->currentText().length() > 0) {
		QMessageBox msgBox;
		msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgBox.setDefaultButton(QMessageBox::No);
		msgBox.setWindowTitle("Achtung");
		msgBox.setText("Soll der Kontext wirklich gel�scht werden?");
		msgBox.setIcon(QMessageBox::Warning);

		if(msgBox.exec() == QMessageBox::Yes) {
			if(rcm->deleteContext(ui.cbContext->currentText())) {
				init();
			} else {
				QMessageBox errorBox;
				errorBox.setStandardButtons(QMessageBox::Ok);
				errorBox.setDefaultButton(QMessageBox::Ok);
				errorBox.setWindowTitle("Fehler");
				errorBox.setText("Der Kontext konnte nicht gel�scht werden.");
				errorBox.setIcon(QMessageBox::Critical);
				errorBox.setDetailedText(rcm->getLastError());
				errorBox.exec();
			}
		}
	}
}


