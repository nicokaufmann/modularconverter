#include "databaseinformation.h"

#include "tablelistdialog.h"
#include <QSqlError>

DatabaseInformation::DatabaseInformation(ApplicationDatabase* applicationDatabase, QString okText,QWidget *parent, Qt::WFlags flags)
	: QDialog(parent, flags), applicationDatabase(applicationDatabase)
{
	ui.setupUi(this);

	ui.bOk->setText(okText);
	ui.cbDriver->addItems(ConnectionManager::getDriverList());
	ui.cbSyntax->addItems(ConnectionManager::getSyntaxList());
	ui.cbParallelExecution->setChecked(true);
	ui.cbUpdateTables->setChecked(true);

	QObject::connect(ui.bOk, SIGNAL(clicked()), this, SLOT(accept()));
	QObject::connect(ui.bAbort, SIGNAL(clicked()), this, SLOT(reject()));
	QObject::connect(ui.bTest, SIGNAL(clicked()), this, SLOT(testConnection()));
	QObject::connect(ui.bODBC, SIGNAL(clicked()), this, SLOT(showStringEdit()));
	QObject::connect(ui.bTables, SIGNAL(clicked()), this, SLOT(showTables()));
	QObject::connect(ui.cbUpdateTables, SIGNAL(stateChanged(int)), this, SLOT(tableStateChanged(int)));
}

DatabaseInformation::~DatabaseInformation()
{

}

ConnectionInformation DatabaseInformation::getConnection()
{
	ConnectionInformation connection(ConnectionManager::getDriver(ui.cbDriver->currentText()),
		ui.cbSyntax->currentText(),
		ui.leConnectionName->text(),
		ui.leHost->text(),
		ui.leDatabaseName->text(),
		ui.leOptions->text(),
		ui.leUser->text(),
		ui.lePassword->text(),
		ui.cbParallelExecution->isChecked(),
		ui.cbUpdateTables->isChecked());
	return connection;
}

void DatabaseInformation::setInformation( ConnectionInformation connection )
{
	int idx = ui.cbDriver->findText(connection.driver().alias());
	if(idx >= 0) {
		ui.cbDriver->setCurrentIndex(idx);
	}
	idx = ui.cbSyntax->findText(connection.syntaxname());
	if(idx >= 0) {
		ui.cbSyntax->setCurrentIndex(idx);
	}

	ui.leHost->setText(connection.hostname());
	ui.leConnectionName->setText(connection.connectionname());
	ui.leDatabaseName->setText(connection.databasename());
	ui.leUser->setText(connection.username());
	ui.lePassword->setText(connection.password());
	ui.leOptions->setText(connection.options());
	ui.cbParallelExecution->setChecked(connection.synchronousUse());
	ui.cbUpdateTables->setChecked(connection.updateTables());
}

void DatabaseInformation::setConnectionName( QString connectionName )
{
	ui.leConnectionName->setText(connectionName);
}

void DatabaseInformation::showStringEdit()
{
	QStringList parts;
	parts<<"Driver={};"<<"Server=;"<<"Srvr=;"<<"Uid=;"<<"User=;"<<"Pwd=;"<<"Password=;"<<"Database=;"<<"Db=;"<<"Dbq=;"<<"Fil={MS Access};"<<"App=;"<<"Port=;"<<":memory:"
		<<"SQLite3 ODBC Driver"<<"Microsoft Access Driver (*.mdb, *.accdb)"<<"MySQL ODBC 5.1 Driver"
		<<"Driver=SQLite3 ODBC Driver;Database=;"
		<<"Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=;"
		<<"Driver={Microsoft Access Driver (*.mdb)};Dbq=;"
		<<"Driver={Adaptive Server Enterprise};App=;Server=;Port=;Db=;Uid=;Pwd=;"
		<<"Driver={SYBASE ASE ODBC Driver};Srvr=;Uid=;Pwd=;"
		<<"Driver={MySQL ODBC 5.1 Driver};Server=;Database=;User=;Password=;Option=3;"
		<<"Driver={MySQL ODBC 5.1 Driver};Server=;Port=;Database=;User=;Password=;Option=3;";
	StringEditDialog sed(ui.leDatabaseName->text(),"Connection String:",parts,this);
	if(sed.exec() == QDialog::Accepted) {
		ui.leDatabaseName->setText(sed.getEditedText());
	}
}

void DatabaseInformation::testConnection()
{
	ConnectionInformation testConn(ConnectionManager::getDriver(ui.cbDriver->currentText()),
		ui.cbSyntax->currentText(),
		ui.leConnectionName->text(),
		ui.leHost->text(),
		ui.leDatabaseName->text(),
		ui.leOptions->text(),
		ui.leUser->text(),
		ui.lePassword->text(),
		ui.cbParallelExecution->isChecked(),
		ui.cbUpdateTables->isChecked());

	QSqlDatabase testDb = testConn.database();

	if(testDb.open()) {
		testDb.close();
		QMessageBox msg;
		msg.setWindowTitle("Verbindungstest erfolgreich");
		msg.setIcon(QMessageBox::Information);
		msg.setText("Verbindung zur Datenbank konnte hergestellt werden.");
		msg.setStandardButtons(QMessageBox::Ok);
		msg.setDefaultButton(QMessageBox::Ok);
		msg.exec();
	} else {
		QMessageBox msg;
		msg.setWindowTitle("Verbindungstest fehlgeschlagen");
		msg.setIcon(QMessageBox::Critical);
		msg.setText("Verbindung zu Datenbank konnte nicht hergestellt werden.");
		msg.setDetailedText(testDb.lastError().text());
		msg.setStandardButtons(QMessageBox::Ok);
		msg.setDefaultButton(QMessageBox::Ok);
		msg.exec();
	}
}

void DatabaseInformation::showTables()
{
	const QStringList tables(applicationDatabase->getTables(ui.leConnectionName->text()));
	TablelistDialog tableDlg("Tabellen - " + ui.leConnectionName->text(), tables);
	tableDlg.exec();

	if(tables!=tableDlg.getList()) {
		applicationDatabase->updateTables(ui.leConnectionName->text(), tableDlg.getList());
	}
}

void DatabaseInformation::tableStateChanged( int state )
{
}