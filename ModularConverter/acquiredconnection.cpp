#include "acquiredconnection.h"

#include "connectionmanager.h"

AcquiredConnection::AcquiredConnection()
	: connectionManager(NULL), lock(NULL), referenceCounter(new int), valid(false)
{

}

AcquiredConnection::AcquiredConnection( ConnectionInformation connectionInformation )
	: connectionInformation(connectionInformation), connectionManager(NULL), lock(new ConnectionLocker(connectionInformation)), referenceCounter(new int), valid(true)
{
	connection = connectionInformation.database();
	connection.open();
	*referenceCounter = 1;
}

AcquiredConnection::AcquiredConnection( ConnectionInformation connectionInformation, ConnectionManager* connectionManager )
	: connectionInformation(connectionInformation), connectionManager(connectionManager), lock(new ConnectionLocker(connectionInformation)), referenceCounter(new int), valid(true)
{
	connection = connectionInformation.database();
	connection.open();
	*referenceCounter = 1;
}

AcquiredConnection::AcquiredConnection( const AcquiredConnection& obj )
{
	connection = obj.connection;
	connectionInformation = obj.connectionInformation;
	lock = obj.lock;
	referenceCounter = obj.referenceCounter;
	connectionManager = obj.connectionManager;
	valid = obj.valid;
	
	(*referenceCounter)++;
}

AcquiredConnection& AcquiredConnection::operator=( const AcquiredConnection& obj )
{
	connection = obj.connection;
	connectionInformation = obj.connectionInformation;
	lock = obj.lock;
	referenceCounter = obj.referenceCounter;
	connectionManager = obj.connectionManager;
	valid = obj.valid;

	(*referenceCounter)++;

	return *this;
}

AcquiredConnection::~AcquiredConnection()
{
	(*referenceCounter)--;

	if((*referenceCounter) == 1 && connectionManager) {
		connectionManager->checkRequests(connectionInformation.connectionname(), connectionInformation.driver().alias());
	} else if((*referenceCounter) < 1 && connection.isOpen()) {
		connection.close();
	}
}

QSqlDatabase* AcquiredConnection::operator->()
{
	return &connection;
}

QSqlDatabase& AcquiredConnection::operator*()
{
	return connection;
}

ConnectionInformation AcquiredConnection::getConnectionInformation()
{
	return connectionInformation;
}

QSqlDatabase* AcquiredConnection::getConnection()
{
	return &connection;
}

bool AcquiredConnection::isSafe()
{
	return connectionManager != NULL;
}

bool AcquiredConnection::isValid()
{
	return valid;
}

int AcquiredConnection::referenceCount() const
{
	return *referenceCounter;
}
