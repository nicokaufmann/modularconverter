#ifndef syntaxbuilder_h__
#define syntaxbuilder_h__

#include "syntaxnodes.h"

class SyntaxBuilder {
public:
	static void addConjunction( SyntaxNode* whereNode, SyntaxNode* conditionNode );
};
#endif // syntaxbuilder_h__