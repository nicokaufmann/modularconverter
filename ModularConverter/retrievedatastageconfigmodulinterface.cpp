#include "retrievedatastageconfigmodulinterface.h"

#include "datamatchingmodel.h"
#include "retrievedataconfigmodul.h"

RetrieveDataStageConfigModulInterface::RetrieveDataStageConfigModulInterface( int index, int stage, RetrieveDataConfigModul* rdcm, const QMap<QString, QPair<QString, QString>>& stageMap,  QWidget *parent, Qt::WFlags flags )
	: QWidget(parent, flags), index(index), comboDelegate(rdcm->getSourceFields())
{
	ui.setupUi(this);
	ui.leStage->setText(QString::number(stage));
	ui.leCustomSql->hide();
	ui.label_2->hide();

	DataMatchingModel* dmm = new DataMatchingModel(rdcm->getTargetFields());
	dmm->addMatchingRow("Quellmapping", QMap<QString,QString>());
	dmm->addMatchingRow("Matching", QMap<QString,QString>());
	ui.stageMappingTable->setModel(dmm);
	ui.stageMappingTable->setItemDelegateForRow(0, &comboDelegate);
	ui.stageMappingTable->setItemDelegateForRow(1, &lineDelegate);

	setStageMapping(stageMap);

	QObject::connect(ui.bDelete, SIGNAL(clicked()), this, SLOT(removeStage()));
}

RetrieveDataStageConfigModulInterface::~RetrieveDataStageConfigModulInterface()
{
}


void RetrieveDataStageConfigModulInterface::removeStage()
{
	emit removeStage(index);
}

int RetrieveDataStageConfigModulInterface::getIndex()
{
	return index;
}

int RetrieveDataStageConfigModulInterface::getStage()
{
	return ui.leStage->text().toInt();
}

void RetrieveDataStageConfigModulInterface::getStageMapping( QMap<QString, QPair<QString, QString>>& stageMap )
{
	DataMatchingModel* dmm = (DataMatchingModel*)ui.stageMappingTable->model();
	const QMap<QString, QString> fieldMap(dmm->matchingRow("Quellmapping"));
	const QMap<QString, QString> methodMap(dmm->matchingRow("Matching"));

	const QStringList keys(fieldMap.keys());

	for(int i=0; i < keys.count(); ++i) {
		stageMap.insert(keys[i], QPair<QString,QString>(fieldMap[keys[i]], methodMap[keys[i]]));
	}

	//introducing hack - custom sql is stored as "customsql" in stagemap...
// 	if(ui.leCustomSql->text().length() > 0) {
// 		stageMap.insert(QString("customsql"), QPair<QString,QString>(ui.leCustomSql->text(), QString("")));
// 	}
}

void RetrieveDataStageConfigModulInterface::setStageMapping( const QMap<QString, QPair<QString, QString>>& stageMap )
{
	const QStringList keys(stageMap.keys());

	QMap<QString, QString> methodMap;
	QMap<QString, QString> fieldMap;

	for(int i=0; i < keys.count(); ++i) {
		//introducing hack - custom sql is stored as "customsql" in stagemap...
// 		if(keys[i]=="customsql") {
// 			ui.leCustomSql->setText(stageMap[keys[i]].first);
// 		} else {
			fieldMap.insert(keys[i], stageMap[keys[i]].first);
			methodMap.insert(keys[i], stageMap[keys[i]].second);
		//}
	}

	DataMatchingModel* dmm = (DataMatchingModel*)ui.stageMappingTable->model();
	dmm->setMatchingRow("Quellmapping",fieldMap);
	dmm->setMatchingRow("Matching",methodMap);
}
