#include "floatingfilter.h"

FloatingFilter::FloatingFilter( int id, const QString& fieldname, QWidget *parent, Qt::WFlags flags)
	: QWidget(parent, flags), id(id), fieldname(fieldname)
{
	ui.setupUi(this);

	ui.lField->setText(fieldname);
	ui.cbMethod->addItem("=");
	ui.cbMethod->addItem("<>");
	ui.cbMethod->addItem(">");
	ui.cbMethod->addItem(">=");
	ui.cbMethod->addItem("<");
	ui.cbMethod->addItem("<=");
	ui.cbMethod->addItem("like");
	ui.cbMethod->addItem("IN");
	
	QObject::connect(ui.bRemove, SIGNAL(clicked()), this, SLOT(removeFilter()));
}

FloatingFilter::~FloatingFilter()
{

}

void FloatingFilter::removeFilter()
{
	emit removed(id);
	emit removed(this);
}

int FloatingFilter::getId()
{
	return id;
}

QString FloatingFilter::getFieldname()
{
	return fieldname;
}

QString FloatingFilter::getMethod()
{
	return ui.cbMethod->currentText();
}

QString FloatingFilter::getCheckString()
{
	return ui.leCheck->text();
}

