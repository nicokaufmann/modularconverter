#include "dataentrydialog.h"

DataEntryDialog::DataEntryDialog(QStringList fields, QWidget *parent, Qt::WFlags flags)
	: QDialog(parent, flags)
{
	//setup ui
	ui.setupUi(this);

	for(int i=0; i < fields.size(); ++i) {
		ui.formLayout->addRow(fields[i], new QLineEdit(this));
	}

	QObject::connect(ui.bAdd, SIGNAL(clicked()), this, SLOT(accept()));
	QObject::connect(ui.bAbort, SIGNAL(clicked()), this, SLOT(reject()));
}

DataEntryDialog::~DataEntryDialog()
{

}

QMap<QString,QString> DataEntryDialog::getValueMap()
{
	QMap<QString,QString> result;

	for(int i=0; i < ui.formLayout->rowCount(); ++i) {
		QLabel* label = (QLabel*)ui.formLayout->itemAt(i,QFormLayout::LabelRole)->widget();
		QLineEdit* lineEdit = (QLineEdit*)ui.formLayout->itemAt(i,QFormLayout::FieldRole)->widget();

		result[label->text()] = lineEdit->text();
	}
	
	return result;
}
