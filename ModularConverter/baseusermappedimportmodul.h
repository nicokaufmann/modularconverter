#ifndef baseusermappedimportmodul_h__
#define baseusermappedimportmodul_h__

#include "convertermodul.h"

class BaseUserMappedImportModul : public ConverterModul {
public:
	BaseUserMappedImportModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);

	virtual void setPath(QString path);
	virtual void setSheetnames(QStringList sheetNames);
	virtual void setVersionDescription(QString versionDescription);

	virtual bool isMappingPrepared();
	virtual bool needsCustomMapping();
	virtual QStringList& getTargetFields();
	virtual QStringList& getSourceFields();
	virtual QMap<QString, QString>& getMapping();
	virtual void setMapping(const QMap<QString, QString>& mapping);

protected:
	QStringList sheetNames;
	QString path;
	QString versionDesc;

	bool mappingPrepared;
	QStringList targetFields;
	QStringList sourceFields;
	QMap<QString, QString> fieldMapping;
};
#endif // baseusermappedimportmodul_h__