#include "eventtestmodul.h"

EventTestModul::EventTestModul( ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion )
	: ConverterModul(parameterMap, versionMap, connectionManager, context, inheritVersion)
{
	immediateExecution = true;
}

EventTestModul::~EventTestModul()
{

}

QWidget* EventTestModul::createWidget()
{
	return new StandardModulInterface(this);
}

bool EventTestModul::execute(int cmd)
{
	unsigned int progress = 0;
	unsigned int randomCounter = 0;

	while(!abortExecution) {
		randomCounter++;

		if(randomCounter%100000 == 0) {
			progress++;
			QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, progress%100));
		}
	}

	return true;
}
