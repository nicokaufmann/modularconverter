#ifndef sqlserversyntaxelements_h__
#define sqlserversyntaxelements_h__

#include "syntaxelements.h"

class SqlServerSyntaxElements : public SyntaxElements {
public:
	SqlServerSyntaxElements();
	SqlServerSyntaxElements(const SqlServerSyntaxElements& obj);
	SqlServerSyntaxElements& operator= (const SqlServerSyntaxElements& obj);

	virtual void initializeSyntax();
};
#endif // sqlserversyntaxelements_h__