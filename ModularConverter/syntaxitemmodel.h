#ifndef SyntaxItemModel_h__
#define SyntaxItemModel_h__

#include <QAbstractItemModel>
#include <QVariant>
#include <QStringList>
#include <QIcon>
#include <QMimeData>

class SyntaxItemModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	SyntaxItemModel(QStringList itemList, QObject *parent = 0);
	SyntaxItemModel(QStringList itemList, QString iconPath, QObject *parent = 0);
	SyntaxItemModel(QStringList itemList, QString iconPath, QString itemType, QObject *parent = 0);

	void setItemList(QStringList itemList);
	void setIconPath(QString iconPath);

	QMimeData* mimeData(const QModelIndexList & indexes) const;
	QStringList mimeTypes() const;

	Qt::ItemFlags flags(const QModelIndex & index) const;
	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole ) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

	int rowCount( const QModelIndex & parent = QModelIndex() ) const;
	int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

	QModelIndex index(int row, int column, const QModelIndex &parent) const;
	QModelIndex parent(const QModelIndex &index) const;

private:
	QString itemType;
	QString iconPath;
	QVariant icon;
	QStringList itemList;

	QVariant invalidVariant;
	QModelIndex invalidModelIndex;
};

#endif // SyntaxItemModel_h__