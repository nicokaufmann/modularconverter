#include "driverinformation.h"

DriverInformation::DriverInformation()
	: sharedAlias(new QString("")), sharedSynchronousUse(new bool), sharedMutex(new QMutex)
{
	*sharedSynchronousUse = true;
}

DriverInformation::DriverInformation( QString alias, bool synchronousUse )
	: sharedAlias(new QString(alias)), sharedSynchronousUse(new bool), sharedMutex(new QMutex)
{
	*sharedSynchronousUse = synchronousUse;
}

DriverInformation::DriverInformation( const DriverInformation& obj )
{
	sharedAlias = obj.sharedAlias;
	sharedMutex = obj.sharedMutex;
	sharedSynchronousUse = obj.sharedSynchronousUse;
}

DriverInformation& DriverInformation::operator=( const DriverInformation& obj )
{
	sharedAlias = obj.sharedAlias;
	sharedMutex = obj.sharedMutex;
	sharedSynchronousUse = obj.sharedSynchronousUse;

	return *this;
}

DriverInformation::~DriverInformation()
{
}

QMutex* DriverInformation::mutex()
{
	return sharedMutex.data();
}

void DriverInformation::setSynchronousUse( bool val )
{
	*sharedSynchronousUse = val;
}

bool DriverInformation::synchronousUse() const
{
	return *sharedSynchronousUse;
}

void DriverInformation::setAlias( QString val )
{
	sharedAlias->clear();
	sharedAlias->append(val);
}

QString DriverInformation::alias() const
{
	return *sharedAlias;
}
