#ifndef selectversionmodul_h__
#define selectversionmodul_h__

#include <QMessageBox>
#include <QFileDialog>
#include <QMap>
#include <QPair>
#include <QDateTime>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>

#include "connectionmanager.h"
#include "versionrequest.h"
#include "tableversion.h"
#include "dataversion.h"

#include "ui_selectversionmodulinterface.h"

class SelectVersionModulInterface : public QWidget
{
	Q_OBJECT

public:
	SelectVersionModulInterface(ConnectionManager* connectionManager, QVector<VersionRequest> versionRequests, QPair<QString, QString> versioningParameter, QWidget *parent = 0, Qt::WFlags flags = 0);
	~SelectVersionModulInterface();

protected:
	void updateVersionList(ConnectionManager* connectionManager, QPair<QString, QString> versioningParameter);
	void buildLayout(QVector<VersionRequest> versionRequests);
	QStringList getRelevantVersions(VersionRequest request);

	ConnectionManager* connectionManager;
	QMap<QString,DataVersion> versionMap;
	QString lastError;

	Ui::SelectVersionModulInterface ui;
};
#endif // selectversionmodul_h__
