#include "csvmergemodulinterface.h"

#include "csvmergemodul.h"
#include "xlcom.h"
#include "choseelementsdialog.h"
#include <QFileDialog>
#include <QMessageBox>

CsvMergeModulInterface::CsvMergeModulInterface( CsvMergeModul* modul, QWidget *parent, Qt::WFlags flags )
	: StandardModulInterface(modul, parent, flags), executeInterface(new ExecuteModulInterface(this))
{
	addModulWidget(executeInterface);

	QObject::connect(executeInterface->executeButton(), SIGNAL(clicked()), this, SLOT(executeModul()));
}

CsvMergeModulInterface::~CsvMergeModulInterface()
{

}

void CsvMergeModulInterface::finished( bool status )
{
	userMessageWidget->updateMessages();
}

void CsvMergeModulInterface::executeModul()
{
	modul->clearUserMessages();

	QMessageBox msgBox;
	msgBox.setStandardButtons(QMessageBox::Ok);
	msgBox.setDefaultButton(QMessageBox::Ok);

	const QStringList files(QFileDialog::getOpenFileNames(this,"Dateien angeben die zusammengef�hrt werden sollen.",QString(),"Excel Arbeitsmappe (*.xlsx);;Excel Arbeitsmappe 97-2003 (*.xls);;Zeichensepariert (*.csv)"));
	QMap<QString, QStringList> paths;

	if(files.count() > 0) {
		XL::ApplicationPtr xlapp(XL::GetApplication());
		if(xlapp->good()) {
			XL::WorkbooksPtr books(xlapp->getWorkbooks());

			for(int i=0; i < files.count(); ++i) {
				QStringList sheetNames;
				XL::IWorkbook* tempBook = books->open(files[i].toStdString().c_str());

				if(tempBook) {
					XL::WorkbookPtr importBook(tempBook);
					XL::WorksheetsPtr importSheets(importBook->sheets());

					for(int sheetIdx=0; sheetIdx < importSheets->count(); ++sheetIdx) {
						XL::WorksheetPtr sheet(importSheets->getItem(sheetIdx+1));
						sheetNames<<QString(sheet->getName());
					}
				} else {
					msgBox.setWindowTitle("Fehler");
					msgBox.setText("Excel Arbeitsmappe konnte nicht ge�ffnet werden.");
					msgBox.setDetailedText(books->getLastError()->getErrorMessage());
					msgBox.setIcon(QMessageBox::Critical);
					msgBox.exec();
					return;
				}
			
				//popup chose sheets
				ChoseElementsDialog choseSheets(sheetNames,true,this);
				choseSheets.setTilte(files[i].right(files[i].length()-files[i].lastIndexOf("\\")-1));

				if(choseSheets.exec() == QDialog::Accepted) {
					paths.insert(files[i], choseSheets.getSelectedElements());
				}
			}

			QString path = QFileDialog::getSaveFileName(this,"Exportdatei angeben",QString(),"Zeichensepariert (*.csv)");
			
			if(path.length() > 0) {
				CsvMergeModul* mergeModul = (CsvMergeModul*)modul;
				mergeModul->setPaths(paths);
				mergeModul->setPath(path);
				emit execute();
			}
		} else {
			msgBox.setWindowTitle("Fehler");
			msgBox.setText("Problem mit Excel Schnittstelle");
			msgBox.setDetailedText(xlapp->getLastError()->getErrorMessage());
			msgBox.setIcon(QMessageBox::Warning);

			msgBox.exec();
			return;
		}
	}
}
