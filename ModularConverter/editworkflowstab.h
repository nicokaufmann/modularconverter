#ifndef editworkflowstab_h__
#define editworkflowstab_h__

#include <QtGui/QWidget>
#include <QtSql/QSqlDatabase>
#include <QString>
#include <QMessageBox>

#include "modulsetup.h"

#include "ui_editworkflowstab.h"

#include "applicationdatabase.h"
#include "workflowmanager.h"

class EditWorkflowsTab : public QWidget
{
	Q_OBJECT

public:
	EditWorkflowsTab(ApplicationDatabase* applicationDatabase, ModulManager* modulManager, QWidget *parent = 0, Qt::WFlags flags = 0);
	~EditWorkflowsTab();

public slots:
	void newWorkflow();
	void removeWorkflow();
	void saveWorkflow();

	void selectWorkflow(int index);

	void addModulTop();
	void addModul(int index);
	void removeModul(int index);

	void databasesUpdated();
	void workflowsUpdated();
private:
	void clearModulList();
	void clearModulListLayout();
	void buildModulListLayout();

	ApplicationDatabase* applicationDatabase;
	WorkflowManager* workflowManager;
	ModulManager* modulManager;

	int currentWorkflowId;
	QList<ModulSetup*> modulList;

	Ui::EditWorkflowsTab ui;
};
#endif // editworkflowstab_h__