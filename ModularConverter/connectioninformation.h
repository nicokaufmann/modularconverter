#ifndef connectioninformation_h__
#define connectioninformation_h__

#include <QSqlDatabase>
#include <QMutex>
#include <QReadWriteLock>
#include <QSharedPointer>
#include <QString>

#include "driverinformation.h"

#include "databaseelements.h"
#include "syntaxelements.h"

class ConnectionInformation {
public:
	ConnectionInformation();
	ConnectionInformation(DriverInformation driver, QString syntaxname, QString connection, QString hostname, QString databasename, 
						  QString options, QString username, QString password, bool synchronousUse=true, bool updateTables=true);
	ConnectionInformation(const ConnectionInformation& obj);
	ConnectionInformation& operator= (const ConnectionInformation& obj);

	~ConnectionInformation();

	DriverInformation driver() const;
	QString syntaxname() const;
	QString connectionname() const;
	QString hostname() const;
	QString databasename() const;
	QString options() const;
	QString username() const;
	QString password() const;
	bool synchronousUse() const;
	bool updateTables() const;

	void setDriver(DriverInformation val);
	void setSyntaxname(QString val);
	void setConnectionName(QString val);
	void setHostname(QString val);
	void setDatabasename(QString val);
	void setOptions(QString val);
	void setUsername(QString val);
	void setPassword(QString val);
	void setSynchronousUse(bool val);
	void setUpdateTables(bool val);

	DatabaseElements* databaseElements() const;
	SyntaxElements* syntaxElements() const;

	QSqlDatabase database();
	QReadWriteLock* lock();
	
	void setLastError(QString lastError);
	QString getLastError();
private:
	DriverInformation driverInformation;
	
	QSharedPointer<DatabaseElements> sharedDatabaseElements;
	QSharedPointer<QString> sharedSyntaxname;
	QSharedPointer<QString> sharedConnectionname;
	QSharedPointer<QString> sharedHostname;
	QSharedPointer<QString> sharedDatabasename;
	QSharedPointer<QString> sharedOptions;
	QSharedPointer<QString> sharedUsername;
	QSharedPointer<QString> sharedPassword;
	QSharedPointer<bool> sharedUpdateTables;
	QSharedPointer<bool> sharedSynchronousUse;

	QSharedPointer<QString> lastError;

	QSharedPointer<QReadWriteLock> connectionLock;
};

#endif // connectioninformation_h__
