#include "sybasesyntaxelements.h"

SybaseSyntaxElements::SybaseSyntaxElements()
	: SyntaxElements()
{
}

SybaseSyntaxElements::SybaseSyntaxElements( const SybaseSyntaxElements& obj )
	: SyntaxElements(obj)
{

}

SybaseSyntaxElements& SybaseSyntaxElements::operator=( const SybaseSyntaxElements& obj )
{
	SyntaxElements::operator=(obj);

	return *this;
}


void SybaseSyntaxElements::initializeSyntax()
{
	*sharedFieldLeadDelimiter = "[";
	*sharedFieldTrailDelimiter = "]";
	*sharedTableLeadDelimiter = "";
	*sharedTableTrailDelimiter = "";
	*sharedTextDelimiter = "'";
	*sharedDateDelimiter = "'";

	*sharedSpecialCharsReplacement = "";
	sharedSpecialChars->setPattern("[,.'*+~�`?=})\\[\\(/\\\\&%$�""!:;#\\]]");

	keywords->clear();
	*keywords << "SELECT" << "FROM" << "DELETE" <<"WHERE" << "UPDATE" << "INSERT" << "INTO" << "VALUES"
		<< "INNER" << "TABLE" << "CREATE"<< "OUTER" << "JOIN" << "ON" << "AND" << "OR" << "LEFT" << "RIGHT"
		<< "SET" << "AS";

	functions->clear();
	functions->insert("left", (QStringList()<<"str"<<"n"));
	functions->insert("right", (QStringList()<<"str"<<"n"));
	functions->insert("instr", (QStringList()<<"str"<<"pos"));
	functions->insert("nz", (QStringList()<<"value"<<"replacement"));

	logicalFunctions->clear();
	*logicalFunctions << "AND" << "OR" << "=" << ">" << ">=" << "<" << "<=";

	typeMapping->clear();
	typeMapping->insert(QVariant::Bool, "TINYINT");
	typeMapping->insert(QVariant::Int, "INT");
	typeMapping->insert(QVariant::UInt,"INT UNSIGNED");
	typeMapping->insert(QVariant::LongLong, "BIGINT");
	typeMapping->insert(QVariant::ULongLong,"INT UNSIGNED");
	typeMapping->insert(QVariant::Double,"DOUBLE");
	typeMapping->insert(QVariant::Char,"VARCHAR(255)");
	typeMapping->insert(QVariant::Map,"BLOB");
	typeMapping->insert(QVariant::List,"BLOB");
	typeMapping->insert(QVariant::String,"VARCHAR(255)");
	typeMapping->insert(QVariant::StringList, "BLOB");
	typeMapping->insert(QVariant::ByteArray, "BLOB");
	typeMapping->insert(QVariant::BitArray, "BLOB");
	typeMapping->insert(QVariant::Date, "DATE");
	typeMapping->insert(QVariant::Time, "TIME");
	typeMapping->insert(QVariant::DateTime, "DATETIME");

	typeList->clear();
	*typeList<<"TINYINT"<<"INT"<<"INT UNSIGNED"<<"BIGINT"<<"DOUBLE"<<"VARCHAR(255)"<<"DATE"<<"TIME"<<"DATETIME"<<"TIMESTAMP"<<"BLOB";
}
