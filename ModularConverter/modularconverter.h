#ifndef MODULARCONVERTER_H
#define MODULARCONVERTER_H

//qt
#include <QtGui/QMainWindow>
#include <QtGui/QMessageBox>
#include <QSettings>
#include <QMap>

//manager
#include "applicationdatabase.h"
#include "modulmanager.h"

//tabs
#include "workflowtab.h"
#include "editworkflowstab.h"
#include "datatab.h"
#include "tabletab.h"
#include "modultab.h"

//widgets & dialogs
#include "databaselistdialog.h"
#include "simplesqleditor.h"

#include "ui_modularconverter.h"

class LoginDialog;

class ModularConverter : public QMainWindow
{
	Q_OBJECT

public:
	ModularConverter(LoginDialog* loginDialog, ApplicationDatabase* applicationDatabase, QWidget *parent = 0, Qt::WFlags flags = 0);
	~ModularConverter();

	void display();

public slots:
	void connectionLoss();
	void openDatabaseList();
	void openSqlEditor();

	void changeApplication();
	void exitApplication();
	void logout();
	
private:
	void setPermissions(ApplicationUser user);

	LoginDialog* loginDialog;
	ApplicationDatabase* applicationDatabase;
	ModulManager* modulManager;

	WorkflowTab* workflowTab;
	EditWorkflowsTab* editWorkflowsTab;
	DataTab* dataTab;
	TableTab* tableTab;
	ModulTab* modulTab;

	QList<WorkflowItem*> currentWorkflowItems;
	int currentWorkflowItemIdx;

	SimpleSqlEditor* simpleSqlEditor;

	Ui::ModularConverter ui;
};

#endif // MODULARCONVERTER_H
