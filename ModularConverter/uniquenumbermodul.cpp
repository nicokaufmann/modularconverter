#include "uniquenumbermodul.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QStringBuilder>


#include <QFile>
#include <QTextStream>

UniqueNumberModul::UniqueNumberModul( ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion )
	: ConverterModul(parameterMap, versionMap, connectionManager, context, inheritVersion),targetVersion(parameterMap["Zieltabelle"].first, parameterMap["Zieltabelle"].second,"Zielversion")
{
}

UniqueNumberModul::~UniqueNumberModul()
{

}

bool UniqueNumberModul::initialize()
{
	addVersionRequestInherited(targetVersion);
	processInheritedRequests();

	return true;
}

bool UniqueNumberModul::useContext()
{
	return true;
}

QStringList UniqueNumberModul::getRequestedParameter()
{
	return (QStringList()<<"Zieltabelle"<<"Konfiguration"<<"Versionierung");
}

QWidget* UniqueNumberModul::createWidget()
{
	return new ExecuteVersionModulInterface(this);
}

bool UniqueNumberModul::execute(int cmd)
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Zieltabelle"].first));
	QString targetTable = parameterMap["Zieltabelle"].second;

	if(connection->isOpen()) {
		//check if field exists

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		const QSqlRecord record(connection->record(targetTable));
		if(record.contains("version_id") && record.contains("unique_id")) {
			QSqlQuery countData(*connection);
			countData.setForwardOnly(true);
			countData.prepare("select count(version_id) from "+syntax->delimitTablename(targetTable)+ " where version_id=?");
			countData.bindValue(0,targetVersion.versionId());

			if(countData.exec() && countData.next()) {
				int count = countData.record().value(0).toInt();
				if(count > 0) {
					int startId = getUniqueId(count);

					if(startId > 0) {
						countData.finish();
						QSqlQuery update(*connection);
						update.setForwardOnly(true);
						update.prepare("UPDATE " + syntax->delimitTablename(parameterMap["Zieltabelle"].second) + " SET unique_id=0 WHERE version_id=?");
						update.bindValue(0, targetVersion.versionId());
						
						if(update.exec()) {
							connection->transaction();

							update.prepare("UPDATE (SELECT TOP 1 unique_id FROM " % syntax->delimitTablename(parameterMap["Zieltabelle"].second)  % " WHERE version_id=? AND unique_id=0) SET unique_id=?");
							update.bindValue(0,targetVersion.versionId());
							
							for(int i=0; i < count; ++i) {
								update.bindValue(1,startId + i);

								if(!update.exec()) {
									addUserMessage("Abfrage fehlgeschlagen. Datensätze konnten nicht aktualisiert werden.(Zeile " + QString::number(i) + ")",2);
									lastError="Abfrage fehlgeschlagen(Zeile " + QString::number(i) + "):\n"+update.lastError().text();
									
									connection->rollback();
									
									return false;
								}

								if(i%100==0) {
									QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, (i*100)/count));
								}
							}

							connection->commit();

							QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 100));
							addUserMessage(QString::number(count) + " Datensätze aktualisiert.",0);
							return true;
						} else {
							addUserMessage("Abfrage fehlgeschlagen. Datensätze konnten nicht aktualisiert werden.",2);
							lastError="Abfrage fehlgeschlagen:\n"+update.lastError().text();
							return false;
						}
					} else {
						return false;
					}
				} else {
					addUserMessage("Anzahl der Datensätze konnten nicht bestimmt werden.",2);
					lastError="Keine Datensätze gefunden.";
					return false;
				}
			} else {
				addUserMessage("Abfrage fehlgeschlagen. Anzahl der Datensätze konnten nicht bestimmt werden.",2);
				lastError="Abfrage fehlgeschlagen:\n"+countData.lastError().text();
				return false;
			}
		} else {
			addUserMessage("Die Pflichtfelder version_id & unique_id konnten nicht gefunden werden.",2);
			lastError="Die Pflichtfelder version_id & unique_id konnten nicht gefunden werden.";
			return false;
		}	
	} else {
		addUserMessage("Problem mit Datenbankverbindung.",2);
		lastError="Problem mit Datenbankverbindung:\n"+connection->lastError().text();
		return false;
	}
	return true;
}

int UniqueNumberModul::getUniqueId( int count )
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Konfiguration"].first));

	if(connection->isOpen()) {
		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		connection->transaction();
		QSqlQuery retrieveId(*connection);
		retrieveId.setForwardOnly(true);
		retrieveId.prepare("select configvalue from "+ syntax->delimitTablename(parameterMap["Konfiguration"].second) + " where context=?");
		retrieveId.bindValue(0, context);

		if(retrieveId.exec() && retrieveId.next()) {
			int startId = retrieveId.record().value(0).toInt();

			QSqlQuery updateId(*connection);
			updateId.prepare("update " + syntax->delimitTablename(parameterMap["Konfiguration"].second) + 
							" set configvalue=configvalue+? where context=?");
			updateId.bindValue(0,count);
			updateId.bindValue(1,context);

			if(updateId.exec()) {
				connection->commit();
				return startId;
			} else {
				connection->rollback();
				addUserMessage("IDs konnten nicht generiert werden. Aktualisierung fehlgeschlagen.",2);
				lastError="Aktualisierung fehlgeschlagen:\n"+updateId.lastError().text();
				return 0;
			}
		} else {
			connection->rollback();
			addUserMessage("ID konnte nicht geladen werden. Abfrage fehlgeschlagen.",2);
			lastError="Abfrage fehlgeschlagen:\n"+retrieveId.lastError().text();
			return 0;
		}
	} else {
		lastError="Problem mit Datenbankverbindung:\n"+connection->lastError().text();
		return 0;
	}
}