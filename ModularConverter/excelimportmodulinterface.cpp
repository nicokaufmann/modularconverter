#include "excelimportmodulinterface.h"

#include "excelimportmodul.h"

ExcelImportModulInterface::ExcelImportModulInterface( ExcelImportModul* modul, QWidget *parent, Qt::WFlags flags )
	: StandardModulInterface(modul, parent, flags), importInterface(new ImportFileModulInterface(this))
{
	addModulWidget(importInterface);

	QObject::connect(importInterface->importButton(), SIGNAL(clicked()), this, SLOT(import()));
}

ExcelImportModulInterface::~ExcelImportModulInterface()
{

}

void ExcelImportModulInterface::finished( bool status )
{
	userMessageWidget->updateMessages();
}

void ExcelImportModulInterface::import()
{
	QMessageBox msgBox;
	msgBox.setStandardButtons(QMessageBox::Ok);
	msgBox.setDefaultButton(QMessageBox::Ok);

	QString path = QFileDialog::getOpenFileName(this,"Arbeitsmappe �ffnen", "", "Excel Arbeitsmappe (*.xls *.xlsx *.csv)");
	if(path.length() > 0) {

		//select sheets
		QStringList sheetNames;

		XL::ApplicationPtr xlapp(XL::GetApplication());
		if(xlapp->good()) {
			XL::WorkbooksPtr books(xlapp->getWorkbooks());
			XL::IWorkbook* tempBook = books->open(path.toStdString().c_str());

			if(tempBook) {
				XL::WorkbookPtr importBook(tempBook);
				XL::WorksheetsPtr importSheets(importBook->sheets());

				for(int sheetIdx=0; sheetIdx < importSheets->count(); ++sheetIdx) {
					XL::WorksheetPtr sheet(importSheets->getItem(sheetIdx+1));
					sheetNames<<QString(sheet->getName());
				}
			} else {
				msgBox.setWindowTitle("Fehler");
				msgBox.setText("Excel Arbeitsmappe konnte nicht ge�ffnet werden.");
				msgBox.setDetailedText(books->getLastError()->getErrorMessage());
				msgBox.setIcon(QMessageBox::Critical);
				msgBox.exec();
				return;
			}
		} else {
			msgBox.setWindowTitle("Fehler");
			msgBox.setText("Problem mit Excel Schnittstelle");
			msgBox.setDetailedText(xlapp->getLastError()->getErrorMessage());
			msgBox.setIcon(QMessageBox::Warning);

			msgBox.exec();
			return;
		}

		//popup chose sheets
		ChoseElementsDialog choseSheets(sheetNames,true,this);

		if(choseSheets.exec() == QDialog::Accepted) {
			// go
			ExcelImportModul* importModul = (ExcelImportModul*)modul;
			importModul->clearUserMessages();
			importModul->setSheetnames(choseSheets.getSelectedElements());
			importModul->setPath(path);
			importModul->setVersionDescription(importInterface->getVersionDescription());
			
			
			emit execute();
			return;
		} else {
			msgBox.setWindowTitle("Fehler");
			msgBox.setText("Keine Auswahl getroffen, vorgang wird abgebrochen.");
			msgBox.setIcon(QMessageBox::Warning);
		}
	} else {
		msgBox.setWindowTitle("Fehler");
		msgBox.setText("Bitte g�ltigen Pfad angeben!");
		msgBox.setIcon(QMessageBox::Warning);
	}

	msgBox.exec();
}
