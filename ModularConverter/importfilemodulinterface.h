#ifndef importfilemodulinterface_h__
#define importfilemodulinterface_h__

#include <QWidget>

#include "ui_importfilemodulinterface.h"

class ImportFileModulInterface : public QWidget
{
	Q_OBJECT

public:
	ImportFileModulInterface( QWidget *parent = 0, Qt::WFlags flags = 0);
	~ImportFileModulInterface();

	QString getVersionDescription();
	QWidget* importButton();

protected:
	
private:
	Ui::ImportFileModulInterface ui;
};
#endif // importfilemodulinterface_h__

