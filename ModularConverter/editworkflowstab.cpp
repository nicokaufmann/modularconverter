#include "editworkflowstab.h"

EditWorkflowsTab::EditWorkflowsTab (ApplicationDatabase* applicationDatabase, ModulManager* modulManager,  QWidget *parent, Qt::WFlags flags)
	: QWidget(parent,flags), applicationDatabase(applicationDatabase), workflowManager(applicationDatabase->getWorkflowManager()), modulManager(modulManager), currentWorkflowId(0)
{
	ui.setupUi(this);

	workflowsUpdated();

	QObject::connect(ui.bSaveWorkflow, SIGNAL(clicked()), this, SLOT(saveWorkflow()));
	QObject::connect(ui.bCreateWorkflow, SIGNAL(clicked()), this, SLOT(newWorkflow()));
	QObject::connect(ui.bDeleteWorkflow, SIGNAL(clicked()), this, SLOT(removeWorkflow()));
	
	QObject::connect(ui.bAddModulTop, SIGNAL(clicked()), this, SLOT(addModulTop()));

	QObject::connect(ui.cbWorkflows, SIGNAL(activated(int)), this, SLOT(selectWorkflow(int)));
}

EditWorkflowsTab::~EditWorkflowsTab()
{

}


void EditWorkflowsTab::selectWorkflow( int index )
{
	QVector<Workflow*> flowList = workflowManager->getWorkflowList();

	if(index < flowList.count()) {
		currentWorkflowId = flowList[index]->getUniqueId();

		// clear current
		clearModulListLayout();
		clearModulList();
	
		//build new
		ui.leWorkflowName->setText(flowList[index]->getName());
		ui.teWorkflowDesc->setText(flowList[index]->getDescription());
		ui.cbForceOrder->setChecked(flowList[index]->getForceOrder());

		QList<WorkflowItem*> itemList = flowList[index]->getItems();
		for(int itemIdx=0; itemIdx < itemList.size();++itemIdx) {
			
			ModulSetup* temp = new ModulSetup(applicationDatabase->getConnectionManager(),modulManager,0,this);
			QObject::connect(temp, SIGNAL(addAfter(int)), this, SLOT(addModul(int)));
			QObject::connect(temp, SIGNAL(removeCurrent(int)), this, SLOT(removeModul(int)));
		
			temp->setModulDescription(itemList[itemIdx]->getDescription());
			temp->setModulName(itemList[itemIdx]->getModulName());
			temp->setParameterMap(itemList[itemIdx]->getParameterMap());
			temp->setModulContext(itemList[itemIdx]->getModulContext());
			temp->setModulInheritVersion(itemList[itemIdx]->getInheritVersion());

			modulList.push_back(temp);
		}

		buildModulListLayout();
	} else {
		currentWorkflowId = 0;
	}
}

void EditWorkflowsTab::newWorkflow()
{
	clearModulListLayout();
	clearModulList();
	currentWorkflowId = 0;
}

void EditWorkflowsTab::saveWorkflow()
{
	//name schon vorhanden
	if(currentWorkflowId != 0) {
		QMessageBox msgBox;
		msgBox.setWindowTitle("Achtung");
		msgBox.setText("\nSollen die Änderungen am Workflow wirklich übernommen werden?");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgBox.setDefaultButton(QMessageBox::Yes);
		if(msgBox.exec() == QMessageBox::No) {
			return;
		}
	}

	Workflow* flow = new Workflow(currentWorkflowId,ui.leWorkflowName->text(),ui.teWorkflowDesc->document()->toPlainText(), ui.cbForceOrder->isChecked());

	for(int i=0; i < modulList.size(); ++i) {
		WorkflowItem* temp = new WorkflowItem(0,modulList[i]->getModulName());
		temp->setDescription(modulList[i]->getModulDescription());
		temp->setParameterMap(modulList[i]->getParameterMap());
		temp->setModulContext(modulList[i]->getModulContext());
		temp->setInheritVersion(modulList[i]->getModulInheritVersion());
		flow->getItems().push_back(temp);
	}

	if(!applicationDatabase->saveWorkflow(flow)) {
		delete flow;
	}
}

void EditWorkflowsTab::clearModulList() 
{
	// clear widgets
	ModulSetup* item;
	for(int i=0; i < modulList.size(); ++i) {
		QObject::disconnect(modulList[i], SIGNAL(addAfter(int)), this, SLOT(addModul(int)));
		QObject::disconnect(modulList[i], SIGNAL(removeCurrent(int)), this, SLOT(removeModul(int)));

		delete modulList[i];
	}
	modulList.clear();
}

void EditWorkflowsTab::clearModulListLayout()
{
	// clear layout
	QLayoutItem *child;
	while ((child = ui.modulList->takeAt(0)) != 0) {
		delete child;
	}
}

void EditWorkflowsTab::buildModulListLayout()
{
	for(int i=0; i < modulList.size(); ++i) {
		modulList[i]->setIndex(i);
		ui.modulList->addWidget(modulList[i]);
	}
}

void EditWorkflowsTab::addModulTop()
{
	ModulSetup* temp = new ModulSetup(applicationDatabase->getConnectionManager(),modulManager,0,this);
	QObject::connect(temp, SIGNAL(addAfter(int)), this, SLOT(addModul(int)));
	QObject::connect(temp, SIGNAL(removeCurrent(int)), this, SLOT(removeModul(int)));

	modulList.push_front(temp);

	clearModulListLayout();
	buildModulListLayout();
}

void EditWorkflowsTab::addModul( int index )
{
	ModulSetup* temp = new ModulSetup(applicationDatabase->getConnectionManager(),modulManager,0,this);
	QObject::connect(temp, SIGNAL(addAfter(int)), this, SLOT(addModul(int)));
	QObject::connect(temp, SIGNAL(removeCurrent(int)), this, SLOT(removeModul(int)));

	modulList.insert(index, temp);

	clearModulListLayout();
	buildModulListLayout();
}

void EditWorkflowsTab::removeModul( int index )
{
	ModulSetup* temp = modulList.takeAt(index);
	QObject::disconnect(temp, SIGNAL(addAfter(int)), this, SLOT(addModul(int)));
	QObject::disconnect(temp, SIGNAL(removeCurrent(int)), this, SLOT(removeModul(int)));

	delete temp;

	clearModulListLayout();
	buildModulListLayout();
}

void EditWorkflowsTab::databasesUpdated()
{
	//modules?
}

void EditWorkflowsTab::workflowsUpdated()
{
	QVector<Workflow*> flowList = workflowManager->getWorkflowList();

	ui.cbWorkflows->clear();
	for(int i=0; i < flowList.size(); ++i) {
		ui.cbWorkflows->addItem(flowList[i]->getName());
	}

	selectWorkflow(0);
}

void EditWorkflowsTab::removeWorkflow()
{
	QVector<Workflow*> flowList = workflowManager->getWorkflowList();

	for(int i=0; i < flowList.size(); ++i) {
		if(flowList[i]->getUniqueId() == currentWorkflowId) {
			applicationDatabase->removeWorkflow(flowList[i]);
			currentWorkflowId = 0;
			return;
		}
	}
}
