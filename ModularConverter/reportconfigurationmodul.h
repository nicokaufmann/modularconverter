#ifndef reportconfigurationmodul_h__
#define reportconfigurationmodul_h__

#include <QInputDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QMap>
#include <QStringList>
#include <QVariant>

#include "statementconfigurationmodul.h"

#include "ui_reportconfiguration.h"

class ReportConfigModul;

class ReportConfigurationModul : public QWidget
{
	Q_OBJECT

public:
	ReportConfigurationModul(ReportConfigModul* rcm, QWidget *parent = 0, Qt::WFlags flags = 0);
	~ReportConfigurationModul();

	void init();

public slots:
	void selectContext(int index);
	void saveContext();
	void deleteContext();
	void createContext();
	void changeTemplatepath();
	void addStatement();
	void removeStatement(int id);
signals:

private:
	void resetReportValues();
	void clearStatements();
	
	void setReportValues(QMap<QString, QVariant>& cfg );
	
	QList<StatementConfigurationModul*> statementList;

	ReportConfigModul* rcm;
	Ui::ReportConfigurationModul ui;
};
#endif // reportconfigurationmodul_h__