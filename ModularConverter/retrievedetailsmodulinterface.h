#ifndef retrievedetailsmodulinterface_h__
#define retrievedetailsmodulinterface_h__

#include <QWidget>
#include "ui_retrievedetails.h"

class RetrieveDetailsModulInterface : public QWidget
{
	Q_OBJECT

public:
	RetrieveDetailsModulInterface(QWidget *parent = 0, Qt::WFlags flags = 0);
	~RetrieveDetailsModulInterface();

	QWidget* withResultsButton();
	QWidget* withoutResultsButton();

private:
	Ui::RetrieveDetailsModulInterface ui;
};
#endif // retrievedetailsmodulinterface_h__
