#include "workflowelement.h"

WorkflowElement::WorkflowElement( int index, QString modulText, bool startable, QWidget *parent, Qt::WFlags flags )
	: QWidget(parent, flags), index(index)
{
	ui.setupUi(this);

	ui.lModulname->setText(modulText);
	ui.bStart->setEnabled(startable);

	QObject::connect(ui.bStart, SIGNAL(clicked()), this, SLOT(execute()));
}

WorkflowElement::~WorkflowElement()
{

}

void WorkflowElement::setIndex( int index )
{
	this->index = index;
}

void WorkflowElement::setStatus( bool status )
{
	ui.lCheck->setEnabled(status);
}

void WorkflowElement::execute()
{
	emit execute(index);
}

void WorkflowElement::setModulText( QString modulText )
{
	ui.lModulname->setText(modulText);
}

void WorkflowElement::setStartable( bool startable )
{
	ui.bStart->setEnabled(startable);
}
