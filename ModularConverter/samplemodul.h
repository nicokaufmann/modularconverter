#ifndef samplemodul_h__
#define samplemodul_h__

#include "convertermodul.h"

#include "standardmodulinterface.h"

class SampleModul : public ConverterModul {
public:

	SampleModul();
	~SampleModul();
};
#endif // samplemodul_h__