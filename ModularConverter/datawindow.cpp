#include "datawindow.h"

#include <QtAlgorithms>
#include <QSqlError>
#include <QFileDialog>
#include <QDesktopServices>
#include <QUrl>

#include "xl_buffered_writer.h"
#include "bufferedexcelwriter.h"

DataWindow::DataWindow( ConnectionManager* connectionManager, QString connectionname, QString query, QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags), connectionManager(connectionManager) , connectionname(connectionname), query(query), connection(AcquiredConnection()), dbQuery(0)
{
	ui.setupUi(this);
	vScroll = ui.dataView->verticalScrollBar();

	copyAction = new QAction(this);
	copyAction->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_C));
	addAction(copyAction);

	QObject::connect(&watchQueryInitialization, SIGNAL(finished()),this, SLOT(initializationFinished()));
	QObject::connect(&watchExcelExport, SIGNAL(finished()),this, SLOT(excelExportFinished()));
	QObject::connect(ui.bApply, SIGNAL(clicked()),this, SLOT(changeSection()));
	QObject::connect(ui.bCount, SIGNAL(clicked()),this, SLOT(countRecords()));
	QObject::connect(ui.bJump, SIGNAL(clicked()),this, SLOT(prepareJump()));
	QObject::connect(vScroll, SIGNAL(valueChanged(int)),this, SLOT(valueChanged(int)));
	QObject::connect(copyAction, SIGNAL(triggered()), this, SLOT(copyData()));
	QObject::connect(ui.bExcelExport, SIGNAL(clicked()), this, SLOT(showInExcel()));
	init();
}

DataWindow::~DataWindow()
{
	if(watchQueryInitialization.future().isRunning()) {
		watchQueryInitialization.waitForFinished();
	}

	if(watchExcelExport.future().isRunning()) {
		watchExcelExport.waitForFinished();
	}

	delete dbQuery;
}

void DataWindow::init( )
{
	QFuture<bool> future = QtConcurrent::run(this, &DataWindow::queryInitialization );
	watchQueryInitialization.setFuture(future);

	this->setWindowTitle("Datenansicht - Initialisierung");
}

void DataWindow::initializationFinished()
{
	if(watchQueryInitialization.result()) {
		setWindowTitle("Datenansicht - " + connectionname);

		if(dbQuery->size() > 0) {
			ui.bCount->setText(QString::number(dbQuery->size()));
		}

		SqlQueryModel* model = new SqlQueryModel(dbQuery, this);
		ui.dataView->setModel(model);
		
		ui.leFrom->setText(QString::number(model->getStartRow()));
		ui.leTo->setText(QString::number(model->getEndRow()));
	} else {
		this->setWindowTitle("Datenansicht - Fehler");

		QMessageBox msgBox;
		msgBox.setWindowTitle("Fehler");
		msgBox.setText("Problem beim Ausf�hren des Statements.");
		msgBox.setDetailedText(initError);
		msgBox.setIcon(QMessageBox::Critical);
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.exec();
	}
}

bool DataWindow::queryInitialization()
{
	connection = connectionManager->acquireConnection(connectionname);

	if(connection->isOpen()) {
		dbQuery = new QSqlQuery(*connection);
		dbQuery->setForwardOnly(true);
		if(dbQuery->exec(query)) {
			return true;
		} else {
			initError = dbQuery->lastError().text();
		}
	} else {
		initError = connection->lastError().text();
	}
	return false;
}

void DataWindow::changeSection()
{
	SqlQueryModel* model = (SqlQueryModel*)ui.dataView->model();
	if(ui.leFrom->text().toInt() > 0 && ui.leTo->text().toInt() > 0 && ui.leFrom->text().toInt() <= ui.leTo->text().toInt()) {
		model->setBuffer(ui.leFrom->text().toInt(), ui.leTo->text().toInt());
		ui.leFrom->setText(QString::number(model->getStartRow()));
		ui.leTo->setText(QString::number(model->getEndRow()));
	}
}


void DataWindow::closeEvent( QCloseEvent *event )
{
	if(watchQueryInitialization.future().isRunning()) {
		watchQueryInitialization.waitForFinished();
	}

	emit closing(this);
}

void DataWindow::valueChanged( int val )
{
	if(val == vScroll->maximum()) {
		SqlQueryModel* model = (SqlQueryModel*)ui.dataView->model();
		model->setBuffer(model->getStartRow(), model->getEndRow() + 200);
		ui.leTo->setText(QString::number(model->getEndRow()));
	}
}

void DataWindow::countRecords()
{
	if(connection->isOpen()) {
		SqlInterpreter interpreter(*connection.getConnectionInformation().databaseElements(), *connection.getConnectionInformation().syntaxElements());
		StatementNode statement(0,"");
		interpreter.interpret(query,&statement);
		SyntaxAnalyzer analyzer(&statement);
		SyntaxNode* select = analyzer.getFirstBreadth("select");
		
		if(select) {
			//remove selected fields
			while(select->count() > 0) {
				select->remove(select->children()[0]);
			}

			//add count(*)
			select->add( new FunctionNode(0,"count"));
			select->children()[0]->add(new FieldNode(0,"*"));

			QSqlQuery countQuery(*connection);
			if(countQuery.exec(statement.toString()) && countQuery.next()) {
				ui.bCount->setText(QString::number(countQuery.value(0).toInt()));
			}
		}
	}
}

void DataWindow::prepareJump()
{
	if(ui.bCount->text() != "unbekannt") {
		ui.leTo->setText(ui.bCount->text());
	}
}

void DataWindow::copyData()
{
	SqlQueryModel* model = (SqlQueryModel*)ui.dataView->model();
	QItemSelectionModel * selection = ui.dataView->selectionModel();
	QModelIndexList indexes = selection->selectedIndexes();

	if(indexes.size() > 0) {
		QMap<int, QString> rows;

		for(int i=0; i < indexes.size(); ++i) {
			rows[indexes[i].row()].append(model->data(indexes[i]).toString()).append('\t');
		}

		QString selectedText;
		QList<int> keys(rows.keys());
		qSort(keys.begin(),keys.end());

		for(int i=0; i < keys.size(); ++i) {
			selectedText+=rows[keys[i]]+"\n";
		}
		QApplication::clipboard()->setText(selectedText);
	}
}

void DataWindow::showInExcel()
{
	if(!watchExcelExport.future().isRunning()) {
		exportPath = QFileDialog::getSaveFileName(this, "Excelexport speichern", "", "Excel Arbeitsmappe (*.xlsx);;Excel Arbeitsmappe 97-2003 (*.xls)");

		if(exportPath.length() > 0) {
			QFuture<bool> future = QtConcurrent::run(this, &DataWindow::excelExport, exportPath );
			watchExcelExport.setFuture(future);

			ui.leFrom->setEnabled(false);
			ui.dataView->setEnabled(false);
			ui.leTo->setEnabled(false);
			ui.bApply->setEnabled(false);
			ui.bCount->setEnabled(false);
			ui.bJump->setEnabled(false);
		}
	} else {
		QMessageBox msgBox;
		msgBox.setWindowTitle("Fehler");
		msgBox.setText("Export l�uft noch.");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.exec();
	}
}



bool DataWindow::excelExport(QString path)
{
	XL::ApplicationPtr xlapp(XL::GetApplication());
	if(xlapp->good()) {
		XL::WorkbooksPtr books(xlapp->getWorkbooks());
		XL::IWorkbook* tempBook = books->add();

		if(tempBook) {
			XL::WorkbookPtr exportBook(tempBook);
			XL::WorksheetsPtr importSheets(exportBook->sheets());
			XL::WorksheetPtr sheet(importSheets->getItem(1));

			SqlQueryModel* model = (SqlQueryModel*)ui.dataView->model();
			QVector<QVariant> valueBuffer(model->getValueBuffer());
			QVector<QVariant> columnHeader(model->getColumnHeader());

			int columns = model->columnCount();
			int rows = model->rowCount();

			BufferedExcelWriter writer(sheet.get(),rows+1,columns,10000);

			for(int column=0; column < columns; ++column) {
				writer.setValue(0,column,&columnHeader[column]);
			}

			for(int row=0; row < rows; ++row) {
				for(int column=0; column < columns; ++column) {
					const int pos=row*columns+column;

					if(valueBuffer[pos].isValid() && !valueBuffer[pos].isNull() && valueBuffer[pos].isDetached() && valueBuffer[pos].toString().length() > 0) {
						writer.setValue(row+1, column, &valueBuffer[pos] );
					}
				}
			}

			writer.writeBuffer();

			exportBook->saveAs(path.replace("/","\\").toStdString().c_str());
			return true;
		} else {
			exportError = books->getLastError()->getErrorMessage();
			return false;
		}
	} else {
		exportError = xlapp->getLastError()->getErrorMessage();
		return false;
	}
}

void DataWindow::excelExportFinished()
{
	ui.leFrom->setEnabled(true);
	ui.dataView->setEnabled(true);
	ui.leTo->setEnabled(true);
	ui.bApply->setEnabled(true);
	ui.bCount->setEnabled(true);
	ui.bJump->setEnabled(true);

	if(watchExcelExport.result()) {
		QDesktopServices::openUrl(QUrl("file:///"+ exportPath, QUrl::TolerantMode));
	} else {
		QMessageBox msgBox;
		msgBox.setWindowTitle("Fehler");
		msgBox.setText("Problem beim Excel Export");
		msgBox.setDetailedText(exportError);
		msgBox.setIcon(QMessageBox::Critical);
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.exec();
	}
}
