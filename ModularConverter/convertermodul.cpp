#include "convertermodul.h"

#include "applicationdatabase.h"
#include <QSqlQuery>
#include <QSqlError>

ConverterModul::ConverterModul(ParameterMap parameterMap, VersionMap versionMap,ConnectionManager* connectionManager, QString context, bool inheritVersion)
	: connectionManager(connectionManager), parameterMap(parameterMap), versionMap(versionMap), widget(0), context(context), inheritVersion(inheritVersion), immediateExecution(false), abortExecution(false), errorType(3)
{
}


ConverterModul::~ConverterModul()
{
	delete widget;
	widget = 0;
}

QStringList ConverterModul::getRequestedParameter()
{
	return QStringList();
}

bool ConverterModul::execute( int cmd )
{
	return true;
}

bool ConverterModul::initialize()
{
	return true;
}

QWidget* ConverterModul::createWidget()
{
	return 0;
}

QWidget* ConverterModul::getWidget()
{
	if(!widget) {
		widget = createWidget();
	}
	return widget;
}

void ConverterModul::setParameterMap( ParameterMap pm )
{
	parameterMap = pm;
}

ParameterMap ConverterModul::getParameterMap()
{
	return parameterMap;
}


void ConverterModul::setVersionMap( VersionMap vm )
{
	versionMap = vm;
}

VersionMap ConverterModul::getVersionMap()
{
	return versionMap;
}

QString ConverterModul::getContext()
{
	return context;
}

void ConverterModul::setContext( QString context )
{
	this->context = context;
}

bool ConverterModul::useContext()
{
	return false;
}

QString ConverterModul::getLastError()
{
	return lastError;
}

void ConverterModul::setInheritVersion( bool inheritVersion )
{
	this->inheritVersion;
}

bool ConverterModul::getInheritVersion()
{
	return inheritVersion;
}

unsigned int ConverterModul::generateVersion(QString connectionname, QString table , QString description)
{
	AcquiredConnection connection(connectionManager->acquireConnection(connectionname));

	if(connection->isOpen()) {
		connection->transaction();

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		QSqlQuery data(*connection);
		if(data.exec("select max(id) as maxid from "+ syntax->delimitTablename(table))) {
			int versionId = 0;
			if(data.next()) {
				versionId = data.record().value("maxid").toInt()+1;
			} 
			
			QSqlQuery insert(*connection);
			insert.prepare("insert into "+syntax->delimitTablename(table)+" (id, description, username) values (:id, :description, :username)");
			insert.bindValue(0,versionId);
			insert.bindValue(1,description);
			insert.bindValue(2,connectionManager->getApplicationDatabase()->getCurrentUser().username());
			if(insert.exec()) {
				connection->commit();
				return versionId;
			} else {
				connection->rollback();
				lastError="Es konnte keine neue Version erstellt werden:\n"+insert.lastError().text();
				return 0;
			}
		} else {
			lastError="Abfrage fehlgeschlagen:\n"+data.lastError().text();
			return 0;
		}
	} else {
		lastError="Problem mit Datenbankverbindung:\n"+connection->lastError().text();
		return 0;
	}
}

void ConverterModul::addVersionRequestInherited( VersionRequest request )
{
	if(inheritVersion) {
		inheritedVersionRequests.append(request);
	} else {
		guiVersionRequests.append(request);
	}
}

void ConverterModul::addVersionRequestGui( VersionRequest request )
{
	guiVersionRequests.append(request);
}

void ConverterModul::processInheritedRequests()
{
	for(int i=0; i < inheritedVersionRequests.count(); ++i) {
		if(versionMap.contains(inheritedVersionRequests[i].connectionname()) && 
			versionMap[inheritedVersionRequests[i].connectionname()].contains(inheritedVersionRequests[i].tablename())) {

			inheritedVersionRequests[i].setVersionId(versionMap[inheritedVersionRequests[i].connectionname()][inheritedVersionRequests[i].tablename()]);
		} else {
			guiVersionRequests.append(inheritedVersionRequests[i]);
		}
	}
}

QVector<VersionRequest> ConverterModul::getGuiVersionRequests()
{
	return guiVersionRequests;
}

ConnectionManager* ConverterModul::getConnectionManager()
{
	return connectionManager;
}

bool ConverterModul::executeImmediatly()
{
	return immediateExecution;
}

int ConverterModul::userMessageCount()
{
	return userMessages.count();
}

void ConverterModul::addUserMessage( QString message, int warningLevel )
{
	userMessages.append(QPair<QString, int>(message,warningLevel));
}

QString ConverterModul::getUserMessage( int index )
{
	return userMessages[index].first;
}

int ConverterModul::getWarningLevel( int index )
{
	return userMessages[index].second;
}

void ConverterModul::clearUserMessages()
{
	userMessages.clear();
}

void ConverterModul::abort()
{
	abortExecution = true;
}

int ConverterModul::getErrorType()
{
	return errorType;
}
