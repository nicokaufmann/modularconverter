#ifndef sybasesyntaxelements_h__
#define sybasesyntaxelements_h__

#include "syntaxelements.h"

class SybaseSyntaxElements : public SyntaxElements {
public:
	SybaseSyntaxElements();
	SybaseSyntaxElements(const SybaseSyntaxElements& obj);
	SybaseSyntaxElements& operator= (const SybaseSyntaxElements& obj);

	virtual void initializeSyntax();
};
#endif // sybasesyntaxelements_h__