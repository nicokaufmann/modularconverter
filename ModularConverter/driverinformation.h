
#ifndef driverinformation_h__
#define driverinformation_h__

#include <QSharedPointer>
#include <QString>
#include <QMutex>

class DriverInformation {
public:

	DriverInformation();
	DriverInformation(QString alias, bool synchronousUse);
	DriverInformation(const DriverInformation& obj);
	DriverInformation& operator= (const DriverInformation& obj);
	~DriverInformation();

	void setAlias(QString val);
	QString alias() const;

	void setSynchronousUse(bool val);
	bool synchronousUse() const;

	QMutex* mutex();
private:
	QSharedPointer<QString> sharedAlias;
	QSharedPointer<bool> sharedSynchronousUse;

	QSharedPointer<QMutex> sharedMutex;
};
#endif // driverinformation_h__