#include "choseelementsdialog.h"

ChoseElementsDialog::ChoseElementsDialog( const QStringList& elements, bool multiSelect, QWidget *parent, Qt::WFlags flags )
	:  QDialog(parent, flags)
{
	ui.setupUi(this);

	if(multiSelect) {
		ui.listElements->setSelectionMode(QListWidget::MultiSelection);
	} else {
		ui.listElements->setSelectionMode(QListWidget::SingleSelection);
	}
	
	ui.listElements->addItems(elements);

	QObject::connect(ui.bChose, SIGNAL(clicked()), this, SLOT(accept()));
	QObject::connect(ui.bAbort, SIGNAL(clicked()), this, SLOT(reject()));
}

ChoseElementsDialog::~ChoseElementsDialog()
{

}

QStringList ChoseElementsDialog::getSelectedElements()
{
	QStringList resultList;

	for(int i=0; i < ui.listElements->selectedItems().size();++i) {
		resultList<<ui.listElements->selectedItems()[i]->text();
	}

	return resultList;
}

void ChoseElementsDialog::setTilte( const QString& title )
{
	setWindowTitle(title);
}
