#ifndef usermodulwidget_h__
#define usermodulwidget_h__

#include "ui_usermessagewidget.h"

#include "usermessagemodel.h"

class UserMessageWidget : public QWidget {
	Q_OBJECT
public:
	UserMessageWidget(ConverterModul* modul, QWidget *parent = 0, Qt::WFlags flags = 0);
	~UserMessageWidget();

public slots:
	void updateMessages();

protected:
	Ui::UserMessageWidget ui;
};
#endif // usermodulwidget_h__