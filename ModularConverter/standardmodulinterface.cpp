#include "standardmodulinterface.h"

StandardModulInterface::StandardModulInterface(ConverterModul* modul, QWidget *parent, Qt::WFlags flags)
	: BaseModulWidget(parent,flags), modul(modul), userMessageWidget(new UserMessageWidget(modul,this))
{
	ui.setupUi(this);

	ui.userMessagesLayout->addWidget(userMessageWidget);
}

StandardModulInterface::~StandardModulInterface()
{

}

void StandardModulInterface::setProgress( int progress )
{
	ui.pProgress->setValue(progress);
}

void StandardModulInterface::addModulWidget( QWidget* modulWidget )
{
	ui.modulElementsLayout->addWidget(modulWidget);
}
