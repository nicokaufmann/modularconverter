#ifndef ConverterModul_h__
#define ConverterModul_h__

#include <QWidget>
#include <QSqlDatabase>
#include <QString>
#include <QMap>

#include "connectionmanager.h"

#include "versionrequest.h"

//alias, connection, table
typedef QMap<QString, QPair<QString, QString>> ParameterMap;
//connection, table, version
typedef QMap<QString, QMap<QString, unsigned int>> VersionMap;

class ConverterModul {
public:
	ConverterModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	virtual ~ConverterModul();
	
	virtual bool execute(int cmd=0);
	virtual bool initialize();
	virtual QWidget* createWidget();

	QWidget* getWidget();

	void setParameterMap(ParameterMap pm);
	ParameterMap getParameterMap();

	void setVersionMap(VersionMap vm);
	VersionMap getVersionMap();

	void setInheritVersion(bool inheritVersion);
	bool getInheritVersion();

	void setContext(QString context);
	QString getContext();

	bool executeImmediatly();

	ConnectionManager* getConnectionManager();

	static bool useContext();
	static QStringList getRequestedParameter();
	QVector<VersionRequest> getGuiVersionRequests();

	int userMessageCount();
	QString getUserMessage(int index);
	int getWarningLevel(int index);

	void clearUserMessages();

	void abort();

	int getErrorType();
	QString getLastError();
protected:
	void addUserMessage(QString message, int warningLevel);

	void addVersionRequestInherited(VersionRequest request);
	void addVersionRequestGui(VersionRequest request);
	void processInheritedRequests();
	unsigned int generateVersion(QString connectionname, QString table, QString description);

	ParameterMap parameterMap;
	VersionMap versionMap;
	bool inheritVersion;
	bool immediateExecution;
	QString context;
	QWidget* widget;
	bool abortExecution;
	ConnectionManager* connectionManager;

	int errorType;
	QString lastError;
private:
	QVector<VersionRequest> inheritedVersionRequests;
	QVector<VersionRequest> guiVersionRequests;
	QVector<QPair<QString, int>> userMessages;
};

#endif // ConverterModul_h__