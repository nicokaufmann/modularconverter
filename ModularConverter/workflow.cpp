#include "workflow.h"



Workflow::Workflow( int uniqueId, QString name, QString description, bool forceOrder ) 
	: uniqueId(uniqueId), name(name), description(description), forceOrder(forceOrder)
{

}

Workflow::~Workflow()
{
	for(int i=0; i < items.size(); ++i) {
		delete items[i];
	}
}

QString Workflow::getName()
{
	return name;
}

void Workflow::setName( QString name )
{
	this->name = name;
}

QString Workflow::getDescription()
{
	return description;
}

void Workflow::setDescription( QString desc )
{
	this->description = desc;
}

QList<WorkflowItem*>& Workflow::getItems()
{
	return items;
}

int Workflow::getUniqueId()
{
	return uniqueId;
}

void Workflow::setUniqueId( int id )
{
	uniqueId = id;
}

WorkflowItem* Workflow::getItem( int index )
{
	if(index > -1 && index < items.size()) {
		return items[index];
	} else {
		return 0;
	}
}

void Workflow::setForceOrder( bool forceOrder )
{
	this->forceOrder = forceOrder;
}

bool Workflow::getForceOrder()
{
	return forceOrder;
}

int Workflow::count()
{
	return items.count();
}


