#ifndef workflowwindow_h__
#define workflowwindow_h__

#include <QMainWindow>
#include <QMessageBox>
#include <QCloseEvent>
#include <QtConcurrentRun>
#include <QFuture>
#include <QFutureWatcher>

#include "workflowelement.h"

#include "ui_workflowwindow.h"

#include "basemodulwidget.h"
#include "modulmanager.h"
#include "workflow.h"

class WorkflowWindow : public QMainWindow
{
	Q_OBJECT

public:
	WorkflowWindow(ModulManager* modulManager, ConnectionManager* connectionManager, Workflow* workflow, QWidget *parent = 0, Qt::WFlags flags = 0);
	~WorkflowWindow();
	
	
	Workflow* getWorkflow();

	void init();
public slots:
	void startItem(int index);
	void initializationFinished();
	void execute(int cmd);
	void executionFinished();
	void nextItem();
	void abortExecution();

signals:
	void closing(WorkflowWindow* window);
	void statusChanged(Workflow* workflow, int activeIndex);

protected:
	void closeEvent(QCloseEvent *event);
	bool modulExecution(int cmd);
	bool modulInitialization();

private:
	ModulManager* modulManager;
	ConnectionManager* connectionManager;
	Workflow* workflow;

	ConverterModul* currentModul;
	int currentIndex;

	QFutureWatcher<bool> watchModulInitialization;
	QFutureWatcher<bool> watchModulExecution;

	Ui::WorkflowWindow ui;
};
#endif // workflowwindow_h__