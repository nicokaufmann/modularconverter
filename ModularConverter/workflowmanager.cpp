#include "workflowmanager.h"


WorkflowManager::WorkflowManager( ApplicationDatabase* applicationDatabase )
	: applicationDatabase(applicationDatabase)
{

}

WorkflowManager::~WorkflowManager()
{
	clearWorkflowList();
}


QVector<Workflow*>& WorkflowManager::getWorkflowList()
{
	return workflowList;
}

void WorkflowManager::clearWorkflowList()
{
	for(int i=0; i < workflowList.size();++i) {
		delete workflowList[i];
	}
	workflowList.clear();
}

Workflow* WorkflowManager::getWorkflow( const int& index ) const
{
	if(index > -1 && index < workflowList.count()) {
		return workflowList[index];
	} else {
		return 0;
	}
}

Workflow* WorkflowManager::getWorkflow( const QString& workflowName ) const
{
	for(int i=0; i < workflowList.count(); ++i) {
		if(workflowList[i]->getName() == workflowName) {
			return workflowList[i];
		}
	}

	return 0;			
}

int WorkflowManager::workflowCount()
{
	return workflowList.count();
}

ApplicationDatabase* WorkflowManager::getApplicationDatabase()
{
	return applicationDatabase;
}

void WorkflowManager::removeWorkflow( int index )
{
	if(index < workflowList.count()) {
		delete workflowList[index];
		workflowList.remove(index);
	}
}

void WorkflowManager::removeWorkflow( Workflow* flow )
{
	workflowList.remove(workflowList.indexOf(flow));
}

void WorkflowManager::addWorkflow( Workflow* flow )
{
	workflowList.append(flow);
}

void WorkflowManager::addWorkflow( Workflow* flow, int at )
{
	workflowList.insert(at,flow);
}
