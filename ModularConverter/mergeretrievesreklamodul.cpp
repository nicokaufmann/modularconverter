#include "mergeretrievesreklamodul.h"

#include <QStringBuilder>

MergeRetrievesReklaModul::MergeRetrievesReklaModul( ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion )
	: MergeRetrievesModul(parameterMap, versionMap, connectionManager, context, inheritVersion)
{

}

MergeRetrievesReklaModul::~MergeRetrievesReklaModul()
{

}

bool MergeRetrievesReklaModul::execute( int cmd )
{
	AcquiredConnection connection(connectionManager->acquireConnection(parameterMap["Zieltabelle"].first));
	QString targetTable = parameterMap["Zieltabelle"].second;

	if(connection->isOpen()) {
		//check if field exists
		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		QList<int> versions;
		getVersionList(versions);

		if(versions.count() > 0) {
			const QSqlRecord record(connection->record(targetTable));
			if(record.contains("version_id") && record.contains("unique_id")) {

				const QString versionStr(QString::number(generateVersion(parameterMap["Versionierung"].first,parameterMap["Versionierung"].second,"Merge Retrieves basierend auf v. " + QString::number(targetVersion.versionId()))));

				//build fieldsstring
				QString fields("");
				for(int i=0; i < record.count(); ++i) {
					if(record.fieldName(i) != "version_id") {
						fields+=syntax->delimitFieldname(record.fieldName(i)) + ",";
					}
				}
				fields.chop(1);

				QSqlQuery insertData(*connection);
				insertData.setForwardOnly(true);

				const QString insertStatement("insert into " % syntax->delimitTablename(targetTable) % " (" % fields % ", version_id) " );
				const QString selectStatement("select " % fields % ", "  % versionStr % " from " % syntax->delimitTablename(targetTable) % " where version_id=?");

				//insert not matched
				insertData.prepare(insertStatement % selectStatement);
				insertData.bindValue(0,targetVersion.versionId());

				if(!insertData.exec()) {
					addUserMessage("Abfrage fehlgeschlagen. Datens�tze konnten nicht zusammengefasst werden.",2);
					lastError="Abfrage fehlgeschlagen:\n"+insertData.lastError().text();
					return false;
				}

				//insert matched
				for(int i=0; i < versions.count(); ++i) {
					const QString selectMatchedStatement("select " % fields % ", "  % versionStr % " from " % syntax->delimitTablename(targetTable) % " where version_id=?");

					insertData.prepare(insertStatement % selectMatchedStatement);
					insertData.bindValue(0,versions[i]);

					if(!insertData.exec()) {
						addUserMessage("Abfrage fehlgeschlagen. Datens�tze konnten nicht zusammengefasst werden.",2);
						lastError="Abfrage fehlgeschlagen:\n"+insertData.lastError().text();
						return false;
					}

					QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, (100.0f/versions.count()*(i+1))));
				}

				QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, (100)));

				versionMap[parameterMap["Zieltabelle"].first][targetTable] = versionStr.toInt();

				return true;
			} else {
				addUserMessage("Pflichtfelder version_id + unique_id konnten nicht gefunden werden.",2);
				lastError="Pflichtfelder version_id + unique_id konnten nicht gefunden werden.";
				return false;
			}	
		} else {
			addUserMessage("Keine Versionen f�r Merge gefunden.",2);
			lastError="Keine Versionen f�r Merge gefunden.";
			return false;
		}
	} else {
		addUserMessage("Problem mit Datenbankverbindung.",2);
		lastError="Problem mit Datenbankverbindung:\n"+connection->lastError().text();
		return false;
	}
	return true;
}

