#ifndef retrievedataconfigmodul_h__
#define retrievedataconfigmodul_h__

#include "convertermodul.h"

#include <QMap>
#include <QVariant>

class RetrieveDataConfigModul : public ConverterModul {
public:

	RetrieveDataConfigModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~RetrieveDataConfigModul();

	bool execute(int cmd=0);
	QWidget* createWidget();

	QStringList getContextList();
	QStringList getSourceFields();
	QStringList getTargetFields();

	bool deleteContext(QString context );
	bool loadFieldMap(QString context, QMap<QString, QString>& fieldMap );
	bool loadStageMap(QString context, QMap<int, QMap<QString, QPair<QString, QString>>>& stageMap );

	bool saveFieldMap(QString context, QMap<QString, QString>& fieldMap );
	bool saveStageMap(QString context, QMap<int, QMap<QString, QPair<QString, QString>>>& stageMap );

	static QStringList getRequestedParameter();
private:
	QStringList getFields(QString connectionname, QString tablename);
};
#endif // retrievedataconfigmodul_h__