#ifndef retrievedatamodul_h__
#define retrievedatamodul_h__

#include <QString>
#include <QVariant>
#include <QDate>
#include <QMap>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QSqlError>

#include "retrievedatamodulinterface.h"
#include "convertermodul.h"

#include "xlutil.h"
#include "xl_buffered_reader.h"

class RetrieveDataModul : public ConverterModul {
public:
	RetrieveDataModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~RetrieveDataModul();

	bool execute(int cmd=0);
	bool initialize();
	QWidget* createWidget();

	static bool useContext();
	static QStringList getRequestedParameter();


	int getResultCount();
	QStringList getQueriesWithResults();
	QStringList getQueriesWithoutResults();

protected:
	bool loadFields(QString& selectfields, QString& insertfields);
	bool loadStages(QMap<int, QString>& matchConditions, QMap<int, QVector<int>>& inputFieldPositions, QString& inputfields );

	QString formatValueODBC(const QVariant &field, bool trimStrings);
	QString formatValue(const QVariant &field, bool trimStrings);

	bool sourceVersion;
	VersionRequest sourceTable;
	VersionRequest targetTable;

	QStringList queriesWithResults;
	QStringList queriesWithoutResults;
};
#endif // retrievedatamodul_h__
