#ifndef excelreportmodul_h__
#define excelreportmodul_h__

#include <QString>

#include "excelexportmodulinterface.h"
#include "basepathmodul.h"

#include "sqlinterpreter.h"
#include "syntaxanalyzer.h"
#include "syntaxbuilder.h"

#include "xlutil.h"
#include "xl_buffered_writer.h"

class ExcelReportModul : public BasePathModul {
public:
	ExcelReportModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~ExcelReportModul();

	bool execute(int cmd=0);
	bool initialize();
	QWidget* createWidget();

	static bool useContext();
	static QStringList getRequestedParameter();
private:
	bool exportSheet(XL::WorksheetsPtr sheets);
	bool writeStatement( XL::WorksheetsPtr sheets, int statementId, QSqlQuery& query);

	bool loadContext(QString context);
	bool loadReportConfig( QString context, QMap<QString, QVariant>& cfg );
	bool getStatementIds( QString context, QStringList& statementIds );
	bool loadStatementConfig( QString context, QString statementId, QMap<QString, QVariant>& statementCfg );

	void generateVersionRequests();

	QMap<QString, QVariant> reportCfg;
	QVector<QMap<QString, QVariant>> statementCfgs;
	QMap<int, QVector<VersionRequest>> statementVersions;
};
#endif // excelreportmodul_h__
