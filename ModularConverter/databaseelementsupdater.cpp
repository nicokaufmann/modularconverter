#include "databaseelementsupdater.h"

#include "connectionmanager.h"
#include "databaseelements.h"

DatabaseElementsUpdater::DatabaseElementsUpdater(ConnectionManager* connectionManager)
	: connectionManager(connectionManager)
{
} 

DatabaseElementsUpdater::~DatabaseElementsUpdater()
{

}

void DatabaseElementsUpdater::updateElements()
{
	const QStringList connections(connectionManager->connectionList());

	for(int i=0; i < connections.size(); ++i) {
		connectionManager->getConnection(connections.at(i)).databaseElements()->update();
	}
}

void DatabaseElementsUpdater::updateElementsBlocking()
{
	const QStringList connections(connectionManager->connectionList());

	for(int i=0; i < connections.size(); ++i) {
		connectionManager->getConnection(connections.at(i)).databaseElements()->updateBlocking();
	}
}

void DatabaseElementsUpdater::updateElements( QString connectionname )
{
	if(connectionManager->containsConnection(connectionname)) {
		connectionManager->getConnection(connectionname).databaseElements()->update();
	}
}

void DatabaseElementsUpdater::updateElementsBlocking( QString connectionname )
{
	if(connectionManager->containsConnection(connectionname)) {
		connectionManager->getConnection(connectionname).databaseElements()->updateBlocking();
	}
}

void DatabaseElementsUpdater::updatedDatabaseElementsTables( QString connectionname )
{
	emit databaseElementsUpdated(connectionname);
	emit elementsUpdated();
}

void DatabaseElementsUpdater::updatedDatabaseElementsAttributes( QString connectionname )
{
	emit databaseElementsUpdated(connectionname);
	emit elementsUpdated();
}

void DatabaseElementsUpdater::databasesUpdated()
{
	updateElements();
}

void DatabaseElementsUpdater::addElements( DatabaseElements* elements )
{
	QObject::connect(elements, SIGNAL(tablesUpdated(QString)),this, SLOT(updatedDatabaseElementsTables(QString)));
	QObject::connect(elements, SIGNAL(attributesUpdated(QString)),this, SLOT(updatedDatabaseElementsAttributes(QString)));
}

void DatabaseElementsUpdater::removeElements( DatabaseElements* elements )
{
	QObject::disconnect(elements, SIGNAL(tablesUpdated(QString)),this, SLOT(updatedDatabaseElementsTables(QString)));
	QObject::disconnect(elements, SIGNAL(attributesUpdated(QString)),this, SLOT(updatedDatabaseElementsAttributes(QString)));
}
