#include "exportfilemodulinterface.h"

#include "excelexportmodul.h"

ExportFileModulInterface::ExportFileModulInterface(  QWidget *parent , Qt::WFlags flags )
	: QWidget(parent , flags)
{
	ui.setupUi(this);
	ui.bExecute->setText("Export");
}

ExportFileModulInterface::~ExportFileModulInterface()
{
}

QWidget* ExportFileModulInterface::exportButton()
{
	return ui.bExecute;
}

