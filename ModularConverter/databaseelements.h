#ifndef databaseelements_h__
#define databaseelements_h__

#include <QtConcurrentRun>
#include <QFuture>
#include <QFutureWatcher>
#include <QSharedPointer>
#include <QVector>
#include <QMap>
#include <QPair>
#include <QString>
#include <QStringList>
#include <QSqlRecord>
#include <QSqlField>

class ConnectionManager;

class DatabaseElements : public QObject {
	Q_OBJECT
public:
	DatabaseElements();
	DatabaseElements(ConnectionManager* connectionManager, const QString& connectionname);
	DatabaseElements(const DatabaseElements& obj);
	DatabaseElements& operator= (const DatabaseElements& obj);
	~DatabaseElements();

	void update();
	void updateBlocking();

	QStringList getAttributes() const;
	QStringList getAttributes(const QString& table) const;
	QVector<QPair<QString,int>> getAttributesTypes(const QString& table);
	QStringList getTables() const;
	
	void updateTables();
	void updateAttributes();

	void waitForUpdate();

	void setConnectionManager(ConnectionManager* connectionManager);
public slots:
	void updatedTables();
	void updatedAttributes();

signals:
	void tablesUpdated(QString connectionname);
	void attributesUpdated(QString connectionname);

private:
	ConnectionManager* connectionManager;

	QSharedPointer<QString> connectionname;
	QSharedPointer<QStringList> tables;
	QSharedPointer<QMap<QString,QVector<QPair<QString,int>>>> attributes;

	QSharedPointer<QMutex> tablesMutex;
	QSharedPointer<QMutex> attributesMutex;
	QSharedPointer<QFutureWatcher<void>> watchTablesUpdate;
	QSharedPointer<QFutureWatcher<void>> watchAttributesUpdate;
};
#endif // databaseelements_h__