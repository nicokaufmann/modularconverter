#ifndef workflow_h__
#define workflow_h__

#include <QString>
#include <QList>

#include "workflowitem.h"

class Workflow {
public:

	Workflow(int uniqueId, QString name, QString description, bool forceOrder);
	~Workflow();
	
	int getUniqueId();
	void setUniqueId(int id);
	QString getName();
	void setName(QString name);
	QString getDescription();
	void setDescription(QString desc);
	bool getForceOrder();
	void setForceOrder(bool forceOrder);

	int count();

	WorkflowItem* getItem(int index);
	QList<WorkflowItem*>& getItems();
private:
	int uniqueId;
	bool forceOrder;
	QString name;
	QString description;
	QList<WorkflowItem*> items;
};

#endif // workflow_h__