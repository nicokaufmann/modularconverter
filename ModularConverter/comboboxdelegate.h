#ifndef comboboxdelegate_h__
#define comboboxdelegate_h__

#include <QItemDelegate>
#include <QStringList>
#include <QComboBox>

class ComboBoxDelegate : public QItemDelegate
{
	Q_OBJECT
public:
	ComboBoxDelegate(QStringList options, QObject *parent = 0);

	QWidget* createEditor (QWidget * parent, const QStyleOptionViewItem& option, const QModelIndex& index ) const;
	void setEditorData(QWidget *editor, const QModelIndex &index) const;
	void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
	void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

	void setOptions(QStringList options);
private:
	QStringList options;
};
#endif // comboboxdelegate_h__