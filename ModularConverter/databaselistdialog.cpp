#include "databaselistdialog.h"

DatabaseListDialog::DatabaseListDialog(ApplicationDatabase* applicationDatabase, QWidget *parent , Qt::WFlags flags )
	: QDialog(parent, flags), applicationDatabase(applicationDatabase), connectionManager(applicationDatabase->getConnectionManager())
{
	ui.setupUi(this);

	listModel = new QStringListModel(connectionManager->connectionList());
	ui.dbListView->setModel(listModel);

	QObject::connect(ui.bAdd, SIGNAL(clicked()), this, SLOT(addDatabase()));
	QObject::connect(ui.bEdit, SIGNAL(clicked()), this, SLOT(editDatabase()));
	QObject::connect(ui.bRemove, SIGNAL(clicked()), this, SLOT(removeDatabase()));
}

DatabaseListDialog::~DatabaseListDialog()
{

}

void DatabaseListDialog::addDatabase()
{
	DatabaseInformation dbi(applicationDatabase ,"Hinzuf�gen",this);
	while(dbi.exec() != QDialog::Rejected) {
		ConnectionInformation connection = dbi.getConnection();

		if(applicationDatabase->addConnection(connection)) {
			listModel->setStringList(connectionManager->connectionList());
			break;
		} else {
			QMessageBox msgBox;
			msgBox.setWindowTitle("Fehler");
			msgBox.setText("Datenbank konnte nicht hinzugef�gt werden.");
			msgBox.setDetailedText(applicationDatabase->getLastError());
			msgBox.setIcon(QMessageBox::Warning);
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::Ok);
			msgBox.exec();
		}
	}
}

void DatabaseListDialog::removeDatabase()
{
	QModelIndexList selectedItems = ui.dbListView->selectionModel()->selectedIndexes();

	if(selectedItems.size() == 1) {
		QMessageBox msgBox(this);
		msgBox.setWindowTitle("Datenbank l�schen?");
		msgBox.setText("Soll die ausgew�hlte Datenbank wirklich gel�scht werden?");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgBox.setDefaultButton(QMessageBox::No);
		if(msgBox.exec() == QMessageBox::Yes) {


			if(connectionManager->containsConnection(selectedItems[0].data().toString())) {
				if(!applicationDatabase->removeConnection(connectionManager->getConnection(selectedItems[0].data().toString()))) {
					QMessageBox errBox(this);
					errBox.setWindowTitle("Fehler beim l�schen der Datenbank.");
					errBox.setText("Die Datenbank konnte nicht gel�scht werden.");
					errBox.setIcon(QMessageBox::Critical);
					errBox.setStandardButtons(QMessageBox::Ok);
					errBox.setDefaultButton(QMessageBox::Ok);
					errBox.exec();
				} else {
					QMessageBox errBox(this);
					errBox.setWindowTitle("Achtung");
					errBox.setText("Das L�schen wird erst nach einem Neustart der Anwendung abgeschlossen.");
					errBox.setIcon(QMessageBox::Warning);
					errBox.setStandardButtons(QMessageBox::Ok);
					errBox.setDefaultButton(QMessageBox::Ok);
					errBox.exec();
				}
				listModel->setStringList(connectionManager->connectionList());
			}
		}
	}

}

void DatabaseListDialog::editDatabase()
{
	QModelIndexList selectedItems = ui.dbListView->selectionModel()->selectedIndexes();

	if(selectedItems.size() == 1) {
		if(connectionManager->containsConnection(selectedItems[0].data().toString())) {
			DatabaseInformation dbi(applicationDatabase ,"�bernehmen",this);

			ConnectionInformation current = connectionManager->getConnection(selectedItems[0].data().toString());
			dbi.setInformation(current);

			if(dbi.exec() != QDialog::Rejected) {
				dbi.setConnectionName(current.connectionname());
				ConnectionInformation connection = dbi.getConnection();

				if(!applicationDatabase->updateConnection(connection)) {
					QMessageBox msgBox;
					msgBox.setWindowTitle("Fehler");
					msgBox.setText("Datenbank konnte nicht aktuallisiert werden.\n");
					msgBox.setIcon(QMessageBox::Warning);
					msgBox.setStandardButtons(QMessageBox::Ok);
					msgBox.setDefaultButton(QMessageBox::Ok);
					msgBox.exec();
				}
				listModel->setStringList(connectionManager->connectionList());
			}
		}
	}
}