#include "sqlinterpreter.h"

SqlInterpreter::SqlInterpreter()
	: numberCheck("[0-9]+(?:\\.[0-9]*)?")
{
	init();
}

SqlInterpreter::SqlInterpreter(DatabaseElements databaseElements, SyntaxElements syntaxElements)
	: syntaxElements(syntaxElements) ,databaseElements(databaseElements), numberCheck("[0-9]+(?:\\.[0-9]*)?")
{
	init();
	initElements();
}

void SqlInterpreter::init()
{
	//build typeMap
	typeMap["base"] = &createInstance<SyntaxNode>;
	typeMap["statement"] = &createInstance<StatementNode>;
	typeMap["select"] = &createInstance<SelectNode>;
	typeMap["delete"] = &createInstance<DeleteNode>;
	typeMap["set"] = &createInstance<SetNode>;
	typeMap["where"] = &createInstance<WhereNode>;
	typeMap["from"] = &createInstance<FromNode>;
	typeMap["update"] = &createInstance<UpdateNode>;
	typeMap["insert into"] = &createInstance<InsertIntoNode>;
	typeMap["values"] = &createInstance<ValuesNode>;
	typeMap["into"] = &createInstance<IntoNode>;
	typeMap["join"] = &createInstance<JoinNode>;
	typeMap["left join"] = &createInstance<JoinNode>;
	typeMap["right join"] = &createInstance<JoinNode>;
	typeMap["inner join"] = &createInstance<JoinNode>;
	typeMap["full join"] = &createInstance<JoinNode>;
	typeMap["on"] = &createInstance<OnNode>;
	typeMap["union"] = &createInstance<UnionNode>;
	typeMap["union all"] = &createInstance<UnionNode>;
	typeMap["group"] = &createInstance<GroupNode>;
	typeMap["group by"] = &createInstance<GroupNode>;
	typeMap["asc"] = &createInstance<OrderModifierNode>;
	typeMap["desc"] = &createInstance<OrderModifierNode>;
	typeMap["order"] = &createInstance<OrderNode>;
	typeMap["order by"] = &createInstance<OrderNode>;

	typeMap["field"] = &createInstance<FieldNode>;
	typeMap["table"] = &createInstance<TableNode>;
	typeMap["text"] = &createInstance<TextNode>;
	typeMap["value"] = &createInstance<ValueNode>;
	typeMap["as"] = &createInstance<AliasNode>;
	typeMap["alias"] = &createInstance<AliasNode>;
	typeMap["."] = &createInstance<LinkNode>;
	typeMap["link"] = &createInstance<LinkNode>;
	typeMap["between"] = &createInstance<BetweenNode>;
	typeMap["in"] = &createInstance<InNode>;
	typeMap["brace"] = &createInstance<BraceNode>;
	typeMap["("] = &createInstance<BraceNode>;
	typeMap["()"] = &createInstance<BraceNode>;
	typeMap["distinct"] = &createInstance<DistinctNode>;
	typeMap["top"] = &createInstance<TopNode>;

	typeMap["negate"] = &createInstance<NegateNode>;
	typeMap["not"] = &createInstance<NegateNode>;

	typeMap["and"] = &createInstance<LogicalNode>;
	typeMap["or"] = &createInstance<LogicalNode>;
	typeMap["like"] = &createInstance<ComparisonNode>;
	typeMap["="] = &createInstance<ComparisonNode>;
	typeMap["is"] = &createInstance<ComparisonNode>;
	typeMap[">="] = &createInstance<ComparisonNode>;
	typeMap["<="] = &createInstance<ComparisonNode>;
	typeMap[">"] = &createInstance<ComparisonNode>;
	typeMap["<"] = &createInstance<ComparisonNode>;
	typeMap["<>"] = &createInstance<ComparisonNode>;

	typeMap["+"] = &createInstance<OperatorNode>;
	typeMap["-"] = &createInstance<OperatorNode>;
	typeMap["*"] = &createInstance<OperatorNode>;
	typeMap["/"] = &createInstance<OperatorNode>;

	typeMap["function"] = &createInstance<FunctionNode>;

	separators<<QChar(' ')<<QChar('\t')<<QChar(',')<<QChar('(')<<QChar(')')
		<<QChar('"')<< QChar('\'')<<QChar('#')<<QChar('[')
		<<QChar('`')<< QChar('.')<<QChar('+')
		<<QChar('-')<<QChar('/')<<QChar('*')
		<<QChar('<')<<QChar('>')<<QChar('=')
		<<QChar('&');
}




void SqlInterpreter::initElements()
{
	currentTables.clear();
	currentFunctions.clear();
	currentAttributes.clear();

	currentTables.append(databaseElements.getTables());
	currentFunctions.append(syntaxElements.getFunctions());
	currentAttributes.append(databaseElements.getAttributes());
}

SqlInterpreter::~SqlInterpreter()
{

}

void SqlInterpreter::setSyntaxElements( SyntaxElements syntaxElements )
{
	this->syntaxElements = syntaxElements;
	initElements();
}

void SqlInterpreter::setDatabaseElements( DatabaseElements databaseElements )
{
	this->databaseElements = databaseElements;
	initElements();
}

int SqlInterpreter::getFirstSequence( QString& syntax )
{
	int tp=0;
	while(tp<syntax.length() && !separators.contains(syntax[tp])) {
		tp++;
	}
	return tp;
}

QString SqlInterpreter::leftTrim(const QString& str )
{
	int n = str.size() - 1;
	for (; n >= 0; --n) {
		if (!str.at(n).isSpace()) {
			return str.left(n + 1);
		}
	}
	return "";
}

int SqlInterpreter::interpret( QString syntax, SyntaxNode* rootNode )
{
	SyntaxNode* lastAddedNode = rootNode;
	QString trimmedSyntax(syntax);
	QChar currentSeparator;
	QChar lastSeparator;

	trimmedSyntax.replace("\n"," ");
	trimmedSyntax.replace("\t"," ");

	do {
		trimmedSyntax = leftTrim(trimmedSyntax);
		
		if(!trimmedSyntax.isEmpty()) {
			
			//check for separator symbol OR end
			int tp = getFirstSequence(trimmedSyntax);

			//separator
			if(currentSeparator!=QChar(' ')) {
				lastSeparator = currentSeparator;
			}
			currentSeparator = trimmedSyntax[tp];

			//process left of separator
			if(tp > 0 && currentSeparator != QChar('(')) {
				//can't be brace node OR function
				SyntaxNode* createdNode = interpretFirstSequence(trimmedSyntax, tp);
				if(placeNode(lastAddedNode,createdNode)) {
					lastAddedNode = createdNode;
				}
			}
			
			//process separator
			if(currentSeparator == QChar('(')) {
				SyntaxNode* createdNode;
				if(tp > 0) {
					createdNode = new FunctionNode(NULL, trimmedSyntax.left(tp));
				} else {
					createdNode = new BraceNode(NULL,"");
				}
				trimmedSyntax.remove(0,tp+1);

				//interpret inside of braces
				trimmedSyntax.remove(0,interpret(trimmedSyntax,createdNode));
				if(placeNode(lastAddedNode,createdNode)) {
					lastAddedNode = createdNode;
				}
			} else if (currentSeparator == QChar('>') || currentSeparator == QChar('<') || currentSeparator == QChar('=')){
				SyntaxNode* createdNode = interpretFirstSequence(trimmedSyntax, tp);
				if(placeNode(lastAddedNode,createdNode)) {
					lastAddedNode = createdNode;
				}
			} else {
				trimmedSyntax.remove(0,1);

				if(currentSeparator == QChar(')')  && (rootNode->type()=="brace" || rootNode->type()=="function")) {
					((BraceNode*)rootNode)->close();
					return syntax.length()-trimmedSyntax.length();
				} else {
					SyntaxNode* createdNode;

					if(currentSeparator == QChar('*')) {
						if(lastAddedNode->type()=="select" || lastAddedNode->type()=="delete" || lastAddedNode->type()=="link"  || (lastAddedNode->type()=="function" && lastAddedNode->hasSpace()) || lastSeparator==QChar(',')) {
							createdNode = new FieldNode(NULL,"*");
						} else {
							createdNode = new OperatorNode(NULL,"*");
						}
					} else {
						createdNode = interpretSymbol(trimmedSyntax, currentSeparator);
					}

					if(createdNode && placeNode(lastAddedNode,createdNode)) {
						lastAddedNode = createdNode;
					}
				}
			}
		}
	} while(trimmedSyntax.length() > 0);

	return syntax.length();
}

SyntaxNode* SqlInterpreter::interpretSymbol( QString& syntax, QChar separator)
{
	SyntaxNode* createdNode = NULL;

	if(separator == QChar('"') || separator == QChar('\'')) {
		int closingPos=syntax.indexOf(separator);

		if(closingPos >= 0) {
			createdNode = new TextNode(NULL,syntax.left(closingPos),syntaxElements.textDelimiter());
			syntax.remove(0,closingPos+1);
		}
	} else if(separator == QChar('#')) {
		int closingPos=syntax.indexOf(separator);

		if(closingPos >= 0) {
			createdNode = new TextNode(NULL,syntax.left(closingPos),"#");
			syntax.remove(0,closingPos+1);
		}
	} else if(separator == QChar('`')) {
		int closingPos=syntax.indexOf(separator);
		
		if(closingPos >= 0) {
			createdNode = getNode(syntax.left(closingPos),NULL);
			syntax.remove(0,closingPos+1);
		}
	} else if(separator == QChar('[')) {
		int closingPos=syntax.indexOf(QChar(']'));
		
		if(closingPos >= 0) {
			createdNode = getNode(syntax.left(closingPos),NULL);
			syntax.remove(0,closingPos+1);
		}
	} else if(separator == QChar('&')) {
		createdNode = new LogicalNode(NULL,separator);
	} else if(separator == QChar('+') || separator == QChar('-') || separator == QChar('/')) {
		createdNode = new OperatorNode(NULL,separator);
	} else if(separator == QChar('.')) {
		createdNode = new LinkNode(NULL,"");
	}

	return createdNode;
}

SyntaxNode* SqlInterpreter::interpretFirstSequence( QString& syntax, int pos )
{
	QString lCase = syntax.toLower();

	//find match
	QStringList keys = typeMap.keys();
	QString currentMatch("");

	for(int i=0; i < keys.size(); ++i) {
		if(keys[i].length() > currentMatch.length() &&
			(keys[i].length() == syntax.length() || 
			 (keys[i].length() < syntax.length()  && 
			  (separators.contains(syntax[keys[i].length()])) || separators.contains(syntax[keys[i].length()-1])))) { 
				
			if(lCase.left(keys[i].length()) == keys[i]) {
				currentMatch = keys[i];
			}
		}
	}

	if(currentMatch.length() > 0 ) {
		SyntaxNode* tempNode = typeMap[currentMatch](NULL,syntax.left(currentMatch.length()));
		syntax.remove(0, currentMatch.length());
		return tempNode;
	} else {
		//nothing found or just parts matched (so that joined_data isn't matched as join)
		SyntaxNode* tempNode = getNode(syntax.left(pos), NULL);
		syntax.remove(0,pos);
		return tempNode;
	}
}

bool SqlInterpreter::placeNode( SyntaxNode* lastAddedNode, SyntaxNode* createdNode )
{
	SyntaxNode* currentNode = lastAddedNode;

	if(createdNode->usePrevious()) {
		while(currentNode->parent()!=NULL && !currentNode->parent()->accepts(createdNode)) {
			currentNode = currentNode->parent();
		}
		
		if(currentNode->parent() != NULL) {
			if(createdNode->needsIntermediate() && currentNode->parent()->type() != createdNode->intermediateType()) {
				currentNode->parent()->replace(currentNode, createdNode->intermediate());
				createdNode->add(currentNode);
			} else {
				currentNode->parent()->replace(currentNode, createdNode);
				createdNode->add(currentNode);
			}		
		}
		return true;
	} else {
		while(currentNode!=NULL && !(currentNode->hasSpace() && currentNode->accepts(createdNode))) {
			currentNode = currentNode->parent();
		}

		if(currentNode!=NULL) {
			if(createdNode->needsIntermediate() && currentNode->type()!=createdNode->intermediateType()) {
				currentNode->add(createdNode->intermediate());
			} else {
				currentNode->add(createdNode);
			}
			return true;
		}
	}

	return false;
}

SyntaxNode* SqlInterpreter::getNode(QString& syntax, SyntaxNode* parent )
{

	if(typeMap.contains(syntax.toLower().trimmed())) {
		return typeMap[syntax.toLower().trimmed()](parent,syntax);
	} else if(currentTables.contains(syntax,Qt::CaseInsensitive)) {
		return new TableNode(parent, syntax, syntaxElements.fieldLeadDelimiter(), syntaxElements.fieldTrailDelimiter());
	} else if(currentFunctions.contains(syntax,Qt::CaseInsensitive)) {
		return new FunctionNode(parent, syntax);
	} else if(currentAttributes.contains(syntax,Qt::CaseInsensitive)) {
		return new FieldNode(parent, syntax, syntaxElements.fieldLeadDelimiter(), syntaxElements.fieldTrailDelimiter());
	} else if(numberCheck.exactMatch(syntax)) {
		return new ValueNode(parent, syntax);
	}

	return new FieldNode(parent, syntax, syntaxElements.fieldLeadDelimiter(), syntaxElements.fieldTrailDelimiter());
}
