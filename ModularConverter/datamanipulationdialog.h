#ifndef datamanipulationdialog_h__
#define datamanipulationdialog_h__

#include <QDialog>
#include <QMessageBox>
#include <QListWidgetItem>

#include <QtConcurrentRun>
#include <QFuture>
#include <QFutureWatcher>

#include "connectionmanager.h"

#include "datainsertmodel.h"
#include "lineeditdelegate.h"

#include "ui_datamanipulationdialog.h"

class DataManipulationDialog : public QDialog
{
	Q_OBJECT

public:
	DataManipulationDialog(ConnectionManager* connectionManager, QString connectionname, QString tablename, QWidget *parent = 0, Qt::WFlags flags = 0);
	~DataManipulationDialog();

public slots:
	void insert();
	void insertionFinished();

private:
	bool insertion();

	QString insertionError;

	ConnectionManager* connectionManager;
	QString connectionname;
	QString tablename;

	QFutureWatcher<bool> watchInsertion;

	LineEditDelegate itemDelegate;
	Ui::DataManipulationDialog ui;
};
#endif // datamanipulationdialog_h__
