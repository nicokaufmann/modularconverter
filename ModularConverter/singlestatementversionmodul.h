#ifndef singlestatementversionmodul_h__
#define singlestatementversionmodul_h__

#include <QSqlDatabase>
#include <QSqlQuery>

#include "executeversionmodulinterface.h"
#include "convertermodul.h"

class SingleStatementVersionModul : public ConverterModul {
public:
	SingleStatementVersionModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	~SingleStatementVersionModul();

	bool execute(int cmd=0);
	bool initialize();
	QWidget* createWidget();

	static QStringList getRequestedParameter();
	static bool useContext();
protected:
	bool generateVersionRequests();

	QString statementConnectionName;
	QString statementText;
	QMap<QString, VersionRequest> versionRequests;
	QVector<QString> versionRequestOrder;
};
#endif // singlestatementversionmodul_h__