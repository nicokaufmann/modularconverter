#include "syntaxnode.h"


SyntaxNode::SyntaxNode( SyntaxNode* parent, QString val )
	: parentNode(parent), childLimit(-1), usePreviousNode(false), intermediateNode(NULL), nodeType("base"), nodeValue(val), editable(false), indent("\t")
{
	
}

SyntaxNode::~SyntaxNode()
{
	for(int i=0; i < childNodes.size();++i) {
		delete childNodes[i];
	}
	childNodes.clear();
}

QString& SyntaxNode::type()
{
	return nodeType;
}

QString& SyntaxNode::value()
{
	return nodeValue;
}

void SyntaxNode::add( SyntaxNode* child )
{
	childNodes.append(child);
	child->setParent(this);
}

void SyntaxNode::add( SyntaxNode* child, int at )
{
	childNodes.insert(at, child);
	child->setParent(this);
}

bool SyntaxNode::remove( SyntaxNode* child )
{
	int pos = childNodes.indexOf(child);
	if(pos >= 0) {
		child->setParent(NULL);
		childNodes.remove(pos);
		return true;
	} else {
		return false;
	}
}

bool SyntaxNode::replace( SyntaxNode* child, SyntaxNode* replacement )
{
	int pos = childNodes.indexOf(child);
	if(pos >= 0) {
		child->setParent(NULL);
		replacement->setParent(this);
		childNodes[pos] = replacement;
		return true;
	} else {
		return false;
	}
}

void SyntaxNode::relocate( SyntaxNode* targetParent )
{
	if(parentNode) {
		parentNode->remove(this);
	}
	targetParent->add(this);
}

void SyntaxNode::insert( SyntaxNode* targetParent )
{
	parentNode->replace(this, targetParent);
	targetParent->add(this);
}

void SyntaxNode::setParent( SyntaxNode* parent )
{
	this->parentNode = parent;
}

bool SyntaxNode::usePrevious()
{
	return usePreviousNode;
}

SyntaxNode* SyntaxNode::parent()
{
	return parentNode;
}

QVector<SyntaxNode*>& SyntaxNode::children()
{
	return childNodes;
}

SyntaxNode* SyntaxNode::child( int index )
{
	return childNodes[index];
}

int SyntaxNode::count()
{
	return childNodes.count();
}

int SyntaxNode::limit()
{
	return childLimit;
}

bool SyntaxNode::hasSpace()
{
	return childLimit==-1 || ((childLimit-childNodes.count()) > 0);
}

bool SyntaxNode::accepts( SyntaxNode* node )
{
	return true;
}

QString SyntaxNode::toString()
{
	return nodeType + " : " + nodeValue;
}

QString SyntaxNode::toStringFormat( int currentLevel )
{
	return nodeType;
}

bool SyntaxNode::needsIntermediate()
{
	return false;
}

SyntaxNode* SyntaxNode::intermediate()
{
	return NULL;
}

QString SyntaxNode::intermediateType()
{
	return "";
}

QString SyntaxNode::childString( int index )
{
	if(index > -1 && index < childNodes.size()) {
		return childNodes[index]->toString();
	} else {
		return "";
	}
}

QString SyntaxNode::childStringFormat( int index, int currentLevel )
{
	if(index > -1 && index < childNodes.size()) {
		return childNodes[index]->toStringFormat(currentLevel);
	} else {
		return "";
	}
}

QString SyntaxNode::slotName( int index )
{
	if(index < slotNames.size()) {
		return slotNames[index];
	} else {
		return "";
	}
}

int SyntaxNode::index( SyntaxNode* child )
{
	return childNodes.indexOf(child,0);
}

bool SyntaxNode::isEditable()
{
	return editable;
}

void SyntaxNode::setValue( const QString& val )
{
	nodeValue = val;
}

QString SyntaxNode::indentation( int level )
{
	QString result;
	for(int i=0; i < level; ++i) {
		result.append(indent);
	}
	return result;
}

void SyntaxNode::indentation( int level, QString& str )
{
	for(int i=0; i < level; ++i) {
		str.append(indent);
	}
}

SyntaxNode* SyntaxNode::clone()
{
	return new SyntaxNode(*this);
}

SyntaxNode* SyntaxNode::copy()
{
	SyntaxNode* tempCopy = clone();
	tempCopy->copyChildren();
	return tempCopy;
}

void SyntaxNode::copyChildren()
{
	for(int i=0; i < childNodes.count(); ++i) {
		childNodes[i] = childNodes[i]->copy();
		childNodes[i]->setParent(this);
	}
}


