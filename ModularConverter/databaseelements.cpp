#include "databaseelements.h"

#include "applicationdatabase.h"
#include <QSqlError>

DatabaseElements::DatabaseElements()
	: connectionname(new QString("")), connectionManager(NULL), tablesMutex(new QMutex), attributesMutex(new QMutex), watchTablesUpdate(new QFutureWatcher<void>), watchAttributesUpdate(new QFutureWatcher<void>), tables(new QStringList), attributes(new QMap<QString,QVector<QPair<QString,int>>>)
{
	QObject::connect(watchTablesUpdate.data(), SIGNAL(finished()),this, SLOT(updatedTables()));
	QObject::connect(watchAttributesUpdate.data(), SIGNAL(finished()),this, SLOT(updatedAttributes()));
}

DatabaseElements::DatabaseElements( ConnectionManager* connectionManager, const QString& connectionname )
	: connectionname(new QString(connectionname)), connectionManager(connectionManager), tablesMutex(new QMutex), attributesMutex(new QMutex), 
	  watchTablesUpdate(new QFutureWatcher<void>), watchAttributesUpdate(new QFutureWatcher<void>), tables(new QStringList), attributes(new QMap<QString,QVector<QPair<QString,int>>>)
{
	QObject::connect(watchTablesUpdate.data(), SIGNAL(finished()),this, SLOT(updatedTables()));
	QObject::connect(watchAttributesUpdate.data(), SIGNAL(finished()),this, SLOT(updatedAttributes()));
}

DatabaseElements::DatabaseElements( const DatabaseElements& obj )
{
	connectionname = obj.connectionname;
	connectionManager = obj.connectionManager;
	tables = obj.tables;
	attributes = obj.attributes;
	tablesMutex = obj.tablesMutex;
	attributesMutex = obj.attributesMutex;
	watchTablesUpdate = obj.watchTablesUpdate;
	watchAttributesUpdate = obj.watchAttributesUpdate;

	QObject::connect(watchTablesUpdate.data(), SIGNAL(finished()),this, SLOT(updatedTables()));
	QObject::connect(watchAttributesUpdate.data(), SIGNAL(finished()),this, SLOT(updatedAttributes()));
}



DatabaseElements& DatabaseElements::operator=( const DatabaseElements& obj )
{
	QObject::disconnect(watchTablesUpdate.data(), SIGNAL(finished()),this, SLOT(updatedTables()));
	QObject::disconnect(watchAttributesUpdate.data(), SIGNAL(finished()),this, SLOT(updatedAttributes()));

	connectionname = obj.connectionname;
	connectionManager = obj.connectionManager;
	tables = obj.tables;
	attributes = obj.attributes;
	tablesMutex = obj.tablesMutex;
	attributesMutex = obj.attributesMutex;
	watchTablesUpdate = obj.watchTablesUpdate;
	watchAttributesUpdate = obj.watchAttributesUpdate;

	QObject::connect(watchTablesUpdate.data(), SIGNAL(finished()),this, SLOT(updatedTables()));
	QObject::connect(watchAttributesUpdate.data(), SIGNAL(finished()),this, SLOT(updatedAttributes()));

	return *this;
}

DatabaseElements::~DatabaseElements()
{
	if(watchAttributesUpdate->future().isRunning()) {
		watchAttributesUpdate->future().waitForFinished();
	}

	if(watchTablesUpdate->future().isRunning()) {
		watchTablesUpdate->future().waitForFinished();
	}
}

void DatabaseElements::update()
{
	if(watchAttributesUpdate->future().isRunning()) {
		watchAttributesUpdate->future().waitForFinished();
	}

	if(watchTablesUpdate->future().isRunning()) {
		watchTablesUpdate->future().waitForFinished();
	}

	QFuture<void> future = QtConcurrent::run(this, &DatabaseElements::updateTables );
	watchTablesUpdate->setFuture(future);
}

void DatabaseElements::updateBlocking()
{
	if(watchAttributesUpdate->future().isRunning()) {
		watchAttributesUpdate->future().waitForFinished();
	}

	if(watchTablesUpdate->future().isRunning()) {
		watchTablesUpdate->future().waitForFinished();
	}

	updateTables();
}

void DatabaseElements::updateTables()
{
	if(connectionManager) {
		QStringList result;
		if(connectionManager->getConnection(*connectionname).updateTables()) {
			AcquiredConnection connection(connectionManager->acquireConnection(*connectionname));

			if(connection->isOpen()) {
				result = connection->tables();
			}
		}

		result.append(connectionManager->getApplicationDatabase()->getTables(*connectionname));

		QMutexLocker tablesLocker(tablesMutex.data());
		tables->clear();
		*tables += result;
	}
}

void DatabaseElements::updatedTables()
{
	emit tablesUpdated(*connectionname);

	if(watchAttributesUpdate->future().isRunning()) {
		watchAttributesUpdate->future().waitForFinished();
	}

	QFuture<void> future = QtConcurrent::run(this, &DatabaseElements::updateAttributes );
	watchAttributesUpdate->setFuture(future);
}

void DatabaseElements::updateAttributes()
{
	if(connectionManager) {
		AcquiredConnection connection(connectionManager->acquireConnection(*connectionname));

		//let's assume this won't need a mutex
		for(int i=0; i < tables->size(); ++i) {
			QString tempTable = tables->at(i);
			QVector<QPair<QString,int>> tempList;

			if(connection->isOpen()) {
				QSqlRecord record = connection->record(tempTable);

				for(int j=0; j < record.count(); ++j) {
					tempList.append(QPair<QString,int>(record.fieldName(j),record.field(j).type()));
				}
			}

			QMutexLocker attributesLocker(attributesMutex.data());
			attributes->insert(tempTable,tempList);
		}
	}
}

void DatabaseElements::updatedAttributes()
{
	emit attributesUpdated(*connectionname);
}

QStringList DatabaseElements::getAttributes(const QString& table ) const
{
	QMutexLocker lockAttributes(attributesMutex.data());

	QStringList result;
	for(int i=0; i < attributes->value(table).count(); ++i){
		result << attributes->value(table)[i].first;
	}
	return result;
}

QStringList DatabaseElements::getAttributes() const
{
	QMutexLocker lockTables(tablesMutex.data());
	QMutexLocker lockAttributes(attributesMutex.data());

	QStringList result;
	for(int i=0; i < tables->count(); ++i) {
		const QString table(tables->at(i));
		for(int j=0; j < attributes->value(table).count(); ++j){
			result << attributes->value(table)[j].first;
		}
	}

	return result;
}

QVector<QPair<QString,int>> DatabaseElements::getAttributesTypes(const QString& table )
{
	QMutexLocker lockAttributes(attributesMutex.data());
	return attributes->value(table);
}

QStringList DatabaseElements::getTables() const
{
	QMutexLocker lockTables(tablesMutex.data());

	return *tables;
}

void DatabaseElements::setConnectionManager( ConnectionManager* connectionManager )
{
	this->connectionManager = connectionManager;
}

void DatabaseElements::waitForUpdate()
{
	watchTablesUpdate->waitForFinished();
	watchAttributesUpdate->waitForFinished();
}
