#include <QtGui/QWidget>
#include <QtSql/QSqlDatabase>
#include <QString>
#include <QMessageBox>

#include "modulslotsetup.h"

#include "ui_modultab.h"

#include "connectionmanager.h"
#include "modulmanager.h"
#include "workflowwindow.h"


class ModulTab : public QWidget
{
	Q_OBJECT

public:
	ModulTab(ConnectionManager* connectionManager, ModulManager* modulManager, QWidget *parent = 0, Qt::WFlags flags = 0);
	~ModulTab();

public slots:
	void selected(int index);
	void executeModul();
	void tableSelected(ModulSlotSetup* modulSlot);
	void closing(WorkflowWindow* window);

signals:
	void startModul(QWidget* modulWidget);

private:
	void clearItemList();
	void buildItemListLayout();

	ConnectionManager* connectionManager;
	ModulManager* modulManager;

	QList<ModulSlotSetup*> itemList;
	QList<WorkflowWindow*> windowList;

	Ui::ModulTab ui;
};