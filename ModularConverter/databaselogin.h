#ifndef DATABASELOGIN_H
#define DATABASELOGIN_H

#include <QtGui/QDialog>
#include <QSettings>
#include <QMessageBox>

#include "ui_databaseinformation.h"

class DatabaseLoginInformation : public QDialog
{
	Q_OBJECT

public:
	DatabaseLoginInformation(QWidget *parent = 0, Qt::WFlags flags = 0);
	~DatabaseLoginInformation();

public slots:
	void submit();
	void showStringEdit();
	void testConnection();

private:
	Ui::DatabaseInformation ui;
};

#endif // DATABASELOGIN_H
