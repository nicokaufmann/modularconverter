#ifndef csvmergemodulinterface_h__
#define csvmergemodulinterface_h__

#include "standardmodulinterface.h"
#include "executemodulinterface.h"

class CsvMergeModul;

class CsvMergeModulInterface : public StandardModulInterface 
{
	Q_OBJECT

public:
	CsvMergeModulInterface(CsvMergeModul* modul, QWidget *parent = 0, Qt::WFlags flags = 0);
	~CsvMergeModulInterface();

	void finished(bool status);

public slots:
	void executeModul();

signals:
	void execute(int cmd=0);

private:
	ExecuteModulInterface* executeInterface;
};
#endif // csvmergemodulinterface_h__