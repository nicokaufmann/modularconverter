#ifndef oraclesyntaxelements_h__
#define oraclesyntaxelements_h__

#include "syntaxelements.h"

class OracleSyntaxElements : public SyntaxElements {
public:
	OracleSyntaxElements();
	OracleSyntaxElements(const OracleSyntaxElements& obj);
	OracleSyntaxElements& operator= (const OracleSyntaxElements& obj);

	virtual void initializeSyntax();
	virtual QString delimitDate(const QString& date) const;
	virtual QString delimitDate(const QDate& date) const;
};
#endif // oraclesyntaxelements_h__