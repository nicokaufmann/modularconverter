#include "excelexportmodul.h"

ExcelExportModul::ExcelExportModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion)
	: BasePathModul(parameterMap, versionMap, connectionManager, context, inheritVersion), exportVersion(parameterMap["Exporttabelle"].first, parameterMap["Exporttabelle"].second,"Exportversion")
{
}

ExcelExportModul::~ExcelExportModul()
{
}

bool ExcelExportModul::initialize()
{
	addVersionRequestInherited(exportVersion);
	processInheritedRequests();

	return true;
}

QWidget* ExcelExportModul::createWidget()
{
	return new ExcelExportModulInterface(this);
}

QStringList ExcelExportModul::getRequestedParameter()
{
	return (QStringList()<<"Exporttabelle"<<"Versionierung");
}

bool ExcelExportModul::execute(int cmd)
{

	XL::ApplicationPtr xlapp(XL::GetApplication()); 
	if(xlapp->good()) {
		XL::WorkbooksPtr books(xlapp->getWorkbooks());
		XL::IWorkbook* tempBook = books->add();
		if(tempBook) {
			XL::WorkbookPtr importBook(tempBook);
			XL::WorksheetsPtr importSheets(importBook->sheets());
			
			if(exportSheet(importSheets, QString::number(exportVersion.versionId()))) {
				XL::Util::deleteEmptySheets(importBook.get());

				if(importBook->saveAs(exportPath.replace("/","\\").toStdString().c_str())) {
					return true;
				} else {
					addUserMessage("Excel Arbeitsmappe kann nicht gespeichert werden",2);
					lastError="Excel Arbeitsmappe kann nicht gespeichert werden:\n" +QString(importBook->getLastError()->getErrorMessage());
					return false;
				}
			} else {
				return false;
			}
		} else {
			addUserMessage("Excel Arbeitsmappe konnte nicht ge�ffnet werden",2);
			lastError="Excel Arbeitsmappe konnte nicht ge�ffnet werden:\n" +QString(books->getLastError()->getErrorMessage());
			return false;
		}
	} else {
		addUserMessage("Problem mit Excel Schnittstelle",2);
		lastError="Problem mit Excel Schnittstelle:\n" +QString(xlapp->getLastError()->getErrorMessage());
		return false;
	}
}

bool ExcelExportModul::exportSheet( XL::WorksheetsPtr sheets, QString versionId )
{
	QString connectionname = parameterMap["Exporttabelle"].first;
	QString table = parameterMap["Exporttabelle"].second;

	AcquiredConnection connection(connectionManager->acquireConnection(connectionname));

	if(connection->isOpen()) {
		QSqlQuery data(*connection);
		data.setForwardOnly(true);
		if(data.exec("select * from "+table+ " where version_id="+versionId)) {
			int rows=0;
			
			QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 5));

			while(!writeSheet(XL::WorksheetPtr(sheets->add()),data, rows)){
			}
			addUserMessage("Export erfolgreich abgeschlossen. ( " + QString::number(rows) + " Zeilen)",0);

			QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 100));

			//synchronize version
			versionMap[connectionname][table] = versionId.toUInt();
			return true;
		} else {
			addUserMessage("Abfrage fehlgeschlagen",2);
			lastError="Abfrage fehlgeschlagen:\n"+data.lastError().text();
			return false;
		}
	} else {
		addUserMessage("Problem mit Datenbankverbindung",2);
		lastError="Problem mit Datenbankverbindung:\n"+connection->lastError().text();
		return false;
	}
}

bool ExcelExportModul::writeSheet( XL::WorksheetPtr sheet, QSqlQuery& query , int& rows)
{
	//get size
	int nrRows = (query.size()-query.at());
	if(nrRows > 65000) {
		nrRows=65000;
	}

	QList<int> relevantIndexes;
	for(int i=0; i < query.record().count();++i) {
		if(query.record().fieldName(i) != "version_id") {
			relevantIndexes<<i;
		}
	}

	XL::BufferedWriter writer(sheet.get(),nrRows+1,relevantIndexes.size());
	XL::VariantPtr variant(XL::GetVariant());

	for(int i=0; i < relevantIndexes.size(); ++i) {
		variant->setChar(query.record().fieldName(relevantIndexes[i]).toStdString().c_str());
		writer.setValue(1,i+1,variant.get());
	}

	int row=2;
	while(query.next()) {
		const QSqlRecord record(query.record());
		for(int i=0; i < relevantIndexes.size(); ++i) {
			const QVariant val(record.value(relevantIndexes[i]));
			
			if(val.type() == QVariant::Date) {
				variant->setChar(val.toDate().toString("dd.MM.yyyy").toStdString().c_str());
			} else if(val.type() == QVariant::DateTime) {
				QDateTime tempTime = val.toDateTime();
				if(tempTime.toString("hh:mm:ss") == QString("00:00:00")) {
					variant->setChar(val.toDateTime().toString("dd.MM.yyyy").toStdString().c_str());
				} else {
					variant->setChar(val.toDateTime().toString("dd.MM.yyyy hh:mm:ss").toStdString().c_str());
				}
			} else {
				variant->setChar(val.toString().toStdString().c_str());
			}
			writer.setValue(row,i+1,variant.get());
		}
		
		if(row==65000) {
			writer.writeBuffer();
			return false;
		}
		rows++;
		row++;
	}
	writer.writeBuffer();
	return true;
}
