#ifndef csvmergemodul_h__
#define csvmergemodul_h__

#include <QString>
#include <QVariant>
#include <QMap>

#include "csvmergemodulinterface.h"
#include "convertermodul.h"

#include "xlutil.h"
#include "xl_buffered_reader.h"

class CsvMergeModul : public ConverterModul {
public:
	CsvMergeModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	virtual ~CsvMergeModul();

	bool execute(int cmd=0);
	QWidget* createWidget();

	void setPath(const QString& path);
	void setPaths(const QMap<QString, QStringList>& paths);

protected:
	QString path;
	QMap<QString, QStringList> paths;
};
#endif // csvmergemodul_h__
