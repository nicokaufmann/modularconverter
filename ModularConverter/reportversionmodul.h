#ifndef reportversionmodul_h__
#define reportversionmodul_h__

#include <QMessageBox>
#include <QFileDialog>
#include <QMap>
#include <QPair>
#include <QDateTime>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>

#include "tableversion.h"

#include "baseexecutionmodul.h"

class ExcelReportModul;

class ReportVersionModul : public BaseExecutionModul
{
	Q_OBJECT

public:
	ReportVersionModul(ExcelReportModul* modul, QWidget *parent = 0, Qt::WFlags flags = 0);
	~ReportVersionModul();

public:
	void execute();
	void initialize();

private:
	void loadVersionMap();

	ExcelReportModul* modul;

	QMap<QString, TableVersion*> versionedTables;
	QMap<QString,QPair<QString, QDateTime>> versionMap;
};
#endif // reportversionmodul_h__
