
#include "csvmergemodul.h"
#include <QFile>

CsvMergeModul::CsvMergeModul( ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion )
	: ConverterModul(parameterMap,versionMap,connectionManager,context,inheritVersion)
{
	
}

CsvMergeModul::~CsvMergeModul()
{
}

QWidget* CsvMergeModul::createWidget()
{
	return new CsvMergeModulInterface(this);
}

void CsvMergeModul::setPath( const QString& path )
{
	this->path = path;
}

void CsvMergeModul::setPaths( const QMap<QString, QStringList>& paths )
{
	this->paths = paths;
}

bool CsvMergeModul::execute( int cmd)
{
	XL::ApplicationPtr app(XL::GetApplication());
	int useHeadline = 1;

	if(app->good()) {
		XL::WorkbooksPtr books(app->getWorkbooks());

		QFile file(path);
		if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
			QTextStream csv(&file);

			const QStringList files(paths.keys());
			for(int i=0; i < files.count(); ++i) {
				const QStringList sheetNames(paths[files[i]]);

				XL::IWorkbook* tempBook = books->open(files[i].toStdString().c_str());

				int filepercentage = (i/(double)files.count())*100;

				if(tempBook) {
					XL::WorkbookPtr importBook(tempBook);
					XL::WorksheetsPtr importSheets(importBook->sheets());

					for(int j=0; j < importSheets->count(); ++j) {
						XL::WorksheetPtr sheet(importSheets->getItem(j+1));

						int sheetpercentage = (j/(double)importSheets->count()/(double)files.count())*100;

						if(sheetNames.contains(QString(sheet->getName()))) {
							XL::BufferedReader bf(sheet.get());
							XL::VariantPtr currentVal(XL::GetVariant());

							for(int row=useHeadline; row <= bf.rows(); ++row) {
								for(int col=1; col <=bf.cols(); ++col) {

									bf.getValue(row,col,currentVal.get());


									char* tempVal = currentVal->getChar();
									csv<<tempVal;
									delete[] tempVal;

									if(col != bf.cols()) {
										csv<<";";
									}
								}
								csv<<"\n";

								if(row%500==0) {
									int rowpercentage = (row/(double)bf.rows()/(double)importSheets->count()/(double)files.count())*100;
									QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, filepercentage + sheetpercentage + rowpercentage));
								}
							}
							file.flush();
							useHeadline = 2;
						}
					}
				} else {
					addUserMessage("Problem beim �ffnen der Datei: " + files[i], 2);
					lastError = "Problem beim �ffnen der Datei: " + files[i];
					return false;
				}
			}

			QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 100));
			addUserMessage("Vorgang abgeschlossen. " + QString::number(files.count()) + " Dateien zusammengefasst.", 0);
			return true;
		} else {
			addUserMessage("Problem beim �ffnen der Zieldatei: " + path, 2);
			lastError = "Problem beim �ffnen der Zieldatei: " + path;
			return false;
		}
	} else {
		addUserMessage("Problem mit Excel Schnittstelle: " + QString(app->getLastError()->getErrorMessage()), 2);
		lastError = "Problem mit Excel Schnittstelle:\nFehler: " + QString(app->getLastError()->getErrorMessage())+ "\nBeschreibung: " + QString(app->getLastError()->getErrorDescription());
		return false;
	}
}
