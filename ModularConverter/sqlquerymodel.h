#ifndef sqlquerymodel_h__
#define sqlquerymodel_h__

#include <QAbstractItemModel>
#include <QVariant>
#include <QStringList>
#include <QMimeData>
#include <QSqlQuery>

#include "sqlinterpreter.h"

class SqlQueryModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	SqlQueryModel(QSqlQuery* sqlQuery, QObject *parent = 0);
	~SqlQueryModel();

	//Qt::ItemFlags flags(const QModelIndex & index) const;
	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole ) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

	int rowCount( const QModelIndex & parent = QModelIndex() ) const;
	int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

	QModelIndex index(int row, int column, const QModelIndex &parent) const;
	QModelIndex parent(const QModelIndex &index) const;

	Qt::DropActions supportedDropActions() const;

	void setBuffer(int startRow, int endRow);
	int getStartRow();
	int getEndRow();

	QVector<QVariant>& getValueBuffer();
	QVector<QVariant>& getColumnHeader();
public slots:

protected:

private:
	void expandValuebuffer(int endrow);

	QSqlQuery* sqlQuery;

	int columns;
	
	int bufferStart;
	int bufferEnd;

	QModelIndex invalidIndex;
	QVariant invalidVariant;
	QVector<QVariant> columnData;
	QVector<QVariant> valueBuffer;

	int bufferedRows;

};
#endif // sqlquerymodel_h__