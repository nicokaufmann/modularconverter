#include "databaselogin.h"

#include <QSqlDatabase>
#include "applicationdatabase.h"
#include "stringeditdialog.h"
#include "simplecrypt.h"

#include <QSqlError>

DatabaseLoginInformation::DatabaseLoginInformation(QWidget *parent, Qt::WFlags flags)
	: QDialog(parent, flags)
{
	//setup ui
	ui.setupUi(this);
	ui.bOk->setText("‹bernehmen");
	ui.cbDriver->addItems(ConnectionManager::getDriverList());
	ui.cbSyntax->addItems(ConnectionManager::getSyntaxList());

	ui.cbParallelExecution->setChecked(true);
	ui.cbUpdateTables->setChecked(true);
	ui.cbUpdateTables->setEnabled(false);
	ui.bTables->setEnabled(false);

	//load settings
	SimpleCrypt crypto(Q_UINT64_C(0x0c3ed6a8fabcf012));
	
	QSettings settings(QSettings::IniFormat, QSettings::UserScope,"NK", "ModularConverter");
	
	ui.leConnectionName->setText(settings.value("connectionname").toString());
	ui.leHost->setText(settings.value("host").toString());
	ui.leDatabaseName->setText(settings.value("databasename").toString());
	ui.leUser->setText(settings.value("databaseusername").toString());
	if(settings.value("databasepassword").toString().length() > 0) {
		ui.lePassword->setText(crypto.decryptToString(settings.value("databasepassword").toString()));
	}
	ui.leOptions->setText(settings.value("options").toString());
	ui.cbParallelExecution->setChecked(settings.value("parallel").toBool());

	int driverIndex = ui.cbDriver->findText(settings.value("drivername").toString());
	if(driverIndex > -1) {
		ui.cbDriver->setCurrentIndex(driverIndex);
	}
	int syntaxIndex = ui.cbSyntax->findText(settings.value("syntaxname").toString());
	if(syntaxIndex > -1) {
		ui.cbSyntax->setCurrentIndex(syntaxIndex);
	}

	//connect signals
	QObject::connect(ui.bOk, SIGNAL(clicked()), this, SLOT(submit()));
	QObject::connect(ui.bAbort, SIGNAL(clicked()), this, SLOT(reject()));
	QObject::connect(ui.bTest, SIGNAL(clicked()), this, SLOT(testConnection()));
	QObject::connect(ui.bODBC, SIGNAL(clicked()), this, SLOT(showStringEdit()));
}

DatabaseLoginInformation::~DatabaseLoginInformation()
{
	
}

void DatabaseLoginInformation::submit()
{
	//save settings
	SimpleCrypt crypto(Q_UINT64_C(0x0c3ed6a8fabcf012));
	QSettings settings(QSettings::IniFormat, QSettings::UserScope,"NK", "ModularConverter");
	settings.setValue("drivername",ui.cbDriver->currentText());
	settings.setValue("syntaxname",ui.cbSyntax->currentText());
	settings.setValue("connectionname",ui.leConnectionName->text());
	settings.setValue("host",ui.leHost->text());
	settings.setValue("databasename",ui.leDatabaseName->text());
	settings.setValue("databaseusername",ui.leUser->text());
	settings.setValue("databasepassword",crypto.encryptToString(ui.lePassword->text()));
	settings.setValue("options",ui.leOptions->text());
	settings.setValue("parallel", ui.cbParallelExecution->isChecked());
	hide();
}

void DatabaseLoginInformation::showStringEdit()
{
	QStringList parts;
	parts<<"Driver={};"<<"Server=;"<<"Srvr=;"<<"Uid=;"<<"User=;"<<"Pwd=;"<<"Password=;"<<"Database=;"<<"Db=;"<<"Dbq=;"<<"Fil={MS Access};"<<"App=;"<<"Port=;"<<":memory:"
		<<"SQLite3 ODBC Driver"<<"Microsoft Access Driver (*.mdb, *.accdb)"<<"MySQL ODBC 5.1 Driver"
		<<"Driver=SQLite3 ODBC Driver;Database=;"
		<<"Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=;"
		<<"Driver={Microsoft Access Driver (*.mdb)};Dbq=;"
		<<"Driver={Adaptive Server Enterprise};App=;Server=;Port=;Db=;Uid=;Pwd=;"
		<<"Driver={SYBASE ASE ODBC Driver};Srvr=;Uid=;Pwd=;"
		<<"Driver={MySQL ODBC 5.1 Driver};Server=;Database=;User=;Password=;Option=3;"
		<<"Driver={MySQL ODBC 5.1 Driver};Server=;Port=;Database=;User=;Password=;Option=3;";
	StringEditDialog sed(ui.leDatabaseName->text(),"Connection String:",parts,this);
	if(sed.exec() == QDialog::Accepted) {
		ui.leDatabaseName->setText(sed.getEditedText());
	}
}

void DatabaseLoginInformation::testConnection()
{
	ConnectionInformation testConn(ConnectionManager::getDriver(ui.cbDriver->currentText()),
		ui.cbSyntax->currentText(),
		ui.leConnectionName->text(),
		ui.leHost->text(),
		ui.leDatabaseName->text(),
		ui.leOptions->text(),
		ui.leUser->text(),
		ui.lePassword->text(),
		ui.cbParallelExecution->isChecked());

	QSqlDatabase testDb = testConn.database();

	if(testDb.open()) {
		testDb.close();
		QMessageBox msg;
		msg.setWindowTitle("Verbindungstest erfolgreich");
		msg.setIcon(QMessageBox::Information);
		msg.setText("Verbindung zur Datenbank konnte hergestellt werden.");
		msg.setStandardButtons(QMessageBox::Ok);
		msg.setDefaultButton(QMessageBox::Ok);
		msg.exec();
	} else {
		QMessageBox msg;
		msg.setWindowTitle("Verbindungstest fehlgeschlagen");
		msg.setIcon(QMessageBox::Critical);
		msg.setText("Verbindung zu Datenbank konnte nicht hergestellt werden.");
		msg.setDetailedText(testDb.lastError().text());
		msg.setStandardButtons(QMessageBox::Ok);
		msg.setDefaultButton(QMessageBox::Ok);
		msg.exec();
	}
}
