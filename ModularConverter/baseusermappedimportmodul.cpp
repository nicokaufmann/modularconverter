#include "baseusermappedimportmodul.h"

BaseUserMappedImportModul::BaseUserMappedImportModul( ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion )
	: ConverterModul(parameterMap,versionMap,connectionManager,context,inheritVersion), mappingPrepared(false)
{
}

bool BaseUserMappedImportModul::isMappingPrepared()
{
	return mappingPrepared;
}

bool BaseUserMappedImportModul::needsCustomMapping()
{
	return targetFields.count() != fieldMapping.count();
}

void BaseUserMappedImportModul::setPath( QString path )
{
	addUserMessage("Datei ausgewählt: " + path.right(path.length()-path.lastIndexOf("/")-1),0);
	this->path = path;
}

void BaseUserMappedImportModul::setSheetnames( QStringList sheetNames )
{
	this->sheetNames = sheetNames;
}

void BaseUserMappedImportModul::setVersionDescription( QString versionDescription )
{
	this->versionDesc = versionDescription;
}

QStringList& BaseUserMappedImportModul::getTargetFields()
{
	return targetFields;
}

QStringList& BaseUserMappedImportModul::getSourceFields()
{
	return sourceFields;
}

QMap<QString, QString>& BaseUserMappedImportModul::getMapping()
{
	return fieldMapping;
}

void BaseUserMappedImportModul::setMapping( const QMap<QString, QString>& mapping )
{
	fieldMapping.clear();
	fieldMapping = mapping;
}
