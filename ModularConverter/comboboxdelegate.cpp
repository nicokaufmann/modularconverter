
#include "comboboxdelegate.h"

ComboBoxDelegate::ComboBoxDelegate(QStringList options, QObject *parent)
	: QItemDelegate(parent), options(options)
{
}

QWidget* ComboBoxDelegate::createEditor( QWidget * parent, const QStyleOptionViewItem& option, const QModelIndex& index ) const
{
	QComboBox* editor= new QComboBox(parent);
	editor->addItem("");
	editor->addItems(options);
	return editor;
}

void ComboBoxDelegate::setEditorData( QWidget *editor, const QModelIndex &index ) const
{
	QString value = index.model()->data(index, Qt::EditRole).toString();

	QComboBox *comboBox = static_cast<QComboBox*>(editor);
	const int pos = comboBox->findText(value);
	if(pos > -1) {
		comboBox->setCurrentIndex(pos);
	}
}

void ComboBoxDelegate::setModelData( QWidget *editor, QAbstractItemModel *model, const QModelIndex &index ) const
{
	QComboBox *comboBox = static_cast<QComboBox*>(editor);
	QString value = comboBox->currentText();

	model->setData(index, value, Qt::EditRole);
}

void ComboBoxDelegate::updateEditorGeometry( QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
	editor->setGeometry(option.rect);
}

void ComboBoxDelegate::setOptions( QStringList options )
{
	this->options = options;
}
