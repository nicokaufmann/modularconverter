#include "excelusermappedimportmodul.h"

#include <QStringBuilder>

ExcelUserMappedImportModul::ExcelUserMappedImportModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion)
	: BaseUserMappedImportModul(parameterMap, versionMap, connectionManager, context, inheritVersion), dataRow(2), titleRow(1)
{
}

ExcelUserMappedImportModul::~ExcelUserMappedImportModul()
{
}

QWidget* ExcelUserMappedImportModul::createWidget()
{
	return new ExcelImportMappingModulInterface(this);
}

QStringList ExcelUserMappedImportModul::getRequestedParameter()
{
	return (QStringList()<<"Zieltabelle"<<"Versionierung");
}


bool ExcelUserMappedImportModul::execute(int cmd)
{
	errorType = 3;
	if(sheetNames.size() > 0) {

		XL::ApplicationPtr xlapp(XL::GetApplication());
		if(xlapp->good()) {
			XL::WorkbooksPtr books(xlapp->getWorkbooks());
			XL::IWorkbook* tempBook = books->open(path.toStdString().c_str());
			if(tempBook) {
				XL::WorkbookPtr importBook(tempBook);
				XL::WorksheetsPtr importSheets(importBook->sheets());

				//first step - fall back if custom mapping needed
				if(cmd==0) {
					mappingPrepared = false;
					XL::WorksheetPtr sheet(importSheets->getItem(1));
					
					if(!prepareImportMapping(sheet)) {
						return false;
					} else {
						mappingPrepared = true;
					}

					if(needsCustomMapping()) {
						addUserMessage("Nicht alle Felder konnten zugeordnet werden.",1);
						lastError = "Nicht alle Felder konnten zugeordnet werden. �berpr�fen Sie die Zuordnung.";
						errorType = 1;
						return false;
					}
				}

				//custom mapping not needed or applied
				int versionId = generateVersion(parameterMap["Versionierung"].first,parameterMap["Versionierung"].second,versionDesc);

				if(versionId > 0) {

					QString statement;
					QVector<int> indexes;
					QVector<int> types;

					if(createImportInstructions(statement,indexes,types)) {
						//loop sheets
						for(int sheetIdx=0; sheetIdx < importSheets->count(); ++sheetIdx) {
							XL::WorksheetPtr sheet(importSheets->getItem(sheetIdx+1));

							QString tempName = sheet->getName();
							if(sheetNames.contains(tempName)) {
								//import sheet

								if(!importSheet(sheet, versionId, statement, indexes, types)) {
									return false;
								}
							}
						}

						QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 100));
						return true;
					} else {
						addUserMessage("Problem beim erstellen des Importanweisungen.",2);
						return false;
					}
				} else {
					addUserMessage("Problem mit Versionserstellung",2);
					return false;
				}
			} else {
				addUserMessage("Excel Arbeitsmappe konnte nicht ge�ffnet werden",2);
				lastError="Excel Arbeitsmappe konnte nicht ge�ffnet werden:\n" +QString(books->getLastError()->getErrorMessage());
				return false;
			}
		} else {
			addUserMessage("Problem mit Excel Schnittstelle",2);
			lastError="Problem mit Excel Schnittstelle:\n" +QString(xlapp->getLastError()->getErrorDescription());
			return false;
		}
	} else {
		addUserMessage("Kein Arbeitsblatt ausgew�hlt",2);
		return false;
	}
}

bool ExcelUserMappedImportModul::prepareImportMapping( XL::WorksheetPtr sheet )
{
	QString connectionname = parameterMap["Zieltabelle"].first;
	QString table = parameterMap["Zieltabelle"].second;

	AcquiredConnection connection(connectionManager->acquireConnection(connectionname));

	if(connection->isOpen()) {
		QSqlRecord record = connection->record(table);

		targetFields.clear();
		sourceFields.clear();
		fieldMapping.clear();
		fieldIndexes.clear();
		fieldTypes.clear();

		//get fields
		for(int fieldIdx=0; fieldIdx < record.count(); ++fieldIdx) {
			const QString field(record.fieldName(fieldIdx).toLower());
			if(field != "version_id") {
				targetFields<<field;
				fieldTypes.insert(field,record.field(fieldIdx).type());
			}
		}

		//map fields
		XL::RangePtr usedRange(sheet->getUsedRange());
		XL::RangePtr columns(usedRange->columns());

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		for(int columnIdx=0; columnIdx < columns->count(); ++columnIdx) {
			XL::VariantPtr val(usedRange->getItem(1,columnIdx+1));
			char* tempVal = val->getChar();
			const QString fieldname = syntax->filterSpecialChars(QString(tempVal)).toLower();
			delete[] tempVal;

			if(fieldname.length() > 0 && !sourceFields.contains(fieldname)) {
				sourceFields<<fieldname;
				fieldIndexes.insert(fieldname, columnIdx+1);
				
				if(targetFields.contains(fieldname)) {
					fieldMapping.insert(fieldname, fieldname);
				}
			}
		}

		return true;
	} else {
		addUserMessage("Problem mit Datenbankverbindung",2);
		lastError="Problem mit Datenbankverbindung:\n"+connection->lastError().text();
		return false;
	}
}

bool ExcelUserMappedImportModul::createImportInstructions( QString& statement, QVector<int>& indexes, QVector<int>& types )
{
	indexes.clear();
	types.clear();

	SyntaxElements* syntax = connectionManager->getConnection(parameterMap["Zieltabelle"].first).syntaxElements();
	
	//build statement
	statement = "insert into " + syntax->delimitTablename(parameterMap["Zieltabelle"].second) + " (";
	
	const QStringList keys(fieldMapping.keys());
	
	for(int fieldIdx=0; fieldIdx < keys.count(); ++fieldIdx) {
		const QString field(keys[fieldIdx]);
		if(fieldMapping[field] != "") {
			statement+=syntax->delimitFieldname(field)+",";
			indexes.append(fieldIndexes[fieldMapping[field]]);
			types.append(fieldTypes[field]);
		}
	}
	statement+="version_id) values (";
	
	addUserMessage(QString::number(fieldMapping.count()) + " von " + QString::number(targetFields.count()) + " Feldern gefunden", (fieldMapping.count() != targetFields.count())?1:0);

	return true;
}

bool ExcelUserMappedImportModul::importSheet( XL::WorksheetPtr sheet, int versionId, QString& statement, QVector<int>& indexes, QVector<int>& types )
{
	QString connectionname = parameterMap["Zieltabelle"].first;
	QString table = parameterMap["Zieltabelle"].second;

	AcquiredConnection connection(connectionManager->acquireConnection(connectionname));
	
	if(connection->isOpen()) {
		connection->transaction();

		QSqlQuery insert(*connection);
		insert.setForwardOnly(true);

		XL::BufferedReader reader(sheet.get());

		const QString emptyField("NULL,");
		const QString comma(",");
		const QString closeBracket(")");
		const QString versionField(formatValueODBC(versionId,false));
		const QRegExp matchInt("[0-9]+");

		const int columnCount = indexes.count();
		const int rowCount = reader.rows()+1;
		int countEmptyCellsPerRow;
		int countEmptyRows = 0;
		int countInsertedRows = 0;

		XL::VariantPtr cellValue(XL::GetVariant());

		for(int row=2; row < rowCount; ++row) {
			QString statementValues;
			countEmptyCellsPerRow = 0;

			for(int column=0; column < columnCount; ++column) {
				const int currentType = types[column];

				if(reader.getValue(row,indexes[column],cellValue.get()) && !cellValue->isEmpty()) {
					if(currentType == QVariant::Double) {
						statementValues+=formatValueODBC(cellValue->getDouble(),false) % comma;
					} else if(currentType == QVariant::Int) {
						statementValues+=formatValueODBC(cellValue->getInt(),false) % comma;
					} else if(currentType == QVariant::LongLong) {
						statementValues+=formatValueODBC(cellValue->getInt(),false) % comma;
					} else if(currentType == QVariant::UInt) {
						statementValues+=formatValueODBC(cellValue->getUInt(),false) % comma;
					} else if(currentType == QVariant::Bool) {
						statementValues+=formatValueODBC(cellValue->getBool(),false) % comma;
					} else {
						char* tempValue = cellValue->getChar();
						const QString temp(tempValue);
						delete[] tempValue;


						if(currentType == QVariant::Date ||currentType == QVariant::DateTime) {
							QStringList parts = temp.split(".");
							if(parts.size()	== 3) {
								statementValues+=formatValueODBC(parts[2]+"-"+parts[1]+"-"+parts[0],false) % comma;
							} else {
								if(matchInt.exactMatch(temp)) {
									statementValues+=temp % comma;
								} else {
									statementValues+=formatValueODBC(temp,false) % comma;
								}
							}
						} else {
							statementValues+=formatValueODBC(temp,false) % comma;
						}
					}
				} else {
					countEmptyCellsPerRow++;
					statementValues+=emptyField;
				}
			}


			if(countEmptyCellsPerRow != columnCount) {
				statementValues+=versionField;

				if(!insert.exec(statement % statementValues % closeBracket)) {
					addUserMessage("Importfehler in Zeile " + QString::number(row),2);
					lastError="Problem beim Import, Zeile " + QString::number(row) + " konnte nicht importiert werden:\n" + insert.lastError().text() + "\n\nStatement:\n" + insert.lastQuery();

					connection->rollback();
					return false;
				} else {
					countInsertedRows++;
				}
			} else {
				countEmptyRows ++;
			}

			if(row%100==0) {
				QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, (row*100)/(rowCount)));
			}
		}

		addUserMessage(QString::number(countInsertedRows) + " Zeilen importiert",0);
		addUserMessage(QString::number(countEmptyRows) + " Leerzeilen ignoriert",0);

		versionMap[connectionname][table] = versionId;
		connection->commit();
		return true;
	} else {
		addUserMessage("Problem mit Datenbankverbindung",2);
		lastError="Problem mit Datenbankverbindung:\n"+connection->lastError().text();
		return false;
	}
}

QString ExcelUserMappedImportModul::formatValueODBC( const QVariant &field, bool trimStrings )
{
	QString r;
	if (field.isNull()) {
		r = QLatin1String("NULL");
	} else if (field.type() == QVariant::DateTime) {
		// Use an escape sequence for the datetime fields
		if (field.toDateTime().isValid()){
			QDate dt = field.toDateTime().date();
			QTime tm = field.toDateTime().time();
			// Dateformat has to be "yyyy-MM-dd hh:mm:ss", with leading zeroes if month or day < 10
			r = QLatin1String("{ ts '") +
				QString::number(dt.year()) + QLatin1Char('-') +
				QString::number(dt.month()).rightJustified(2, QLatin1Char('0'), true) +
				QLatin1Char('-') +
				QString::number(dt.day()).rightJustified(2, QLatin1Char('0'), true) +
				QLatin1Char(' ') +
				tm.toString() +
				QLatin1String("' }");
		} else
			r = QLatin1String("NULL");
	} else if(field.type() == QVariant::Date) {
		if (field.toDate().isValid()){
			QDate dt = field.toDateTime().date();
			// Dateformat has to be "yyyy-MM-dd", with leading zeroes if month or day < 10
			r = QLatin1String("{ d '") +
				QString::number(dt.year()) + QLatin1Char('-') +
				QString::number(dt.month()).rightJustified(2, QLatin1Char('0'), true) +
				QLatin1Char('-') +
				QString::number(dt.day()).rightJustified(2, QLatin1Char('0'), true) +
				QLatin1String("' }");
		} else
			r = QLatin1String("NULL");
	} else if (field.type() == QVariant::ByteArray) {
		QByteArray ba = field.toByteArray();
		QString res;
		static const char hexchars[] = "0123456789abcdef";
		for (int i = 0; i < ba.size(); ++i) {
			uchar s = (uchar) ba[i];
			res += QLatin1Char(hexchars[s >> 4]);
			res += QLatin1Char(hexchars[s & 0x0f]);
		}
		r = QLatin1String("0x") + res;
	} else {
		r = formatValue(field, trimStrings);
	}
	return r;
}

QString ExcelUserMappedImportModul::formatValue( const QVariant &field, bool trimStrings )
{
	const QLatin1String nullTxt("NULL");

	QString r;
	if (field.isNull())
		r = nullTxt;
	else {
		switch (field.type()) {
		case QVariant::Int:
		case QVariant::UInt:
			if (field.type() == QVariant::Bool)
				r = field.toBool() ? QLatin1String("1") : QLatin1String("0");
			else
				r = field.toString();
			break;
		case QVariant::String:
		case QVariant::Char:
			{
				QString result = field.toString();
				if (trimStrings) {
					int end = result.length();
					while (end && result.at(end-1).isSpace()) /* skip white space from end */
						end--;
					result.truncate(end);
				}
				/* escape the "'" character */
				result.replace(QLatin1Char('\''), QLatin1String("''"));
				r = QLatin1Char('\'') + result + QLatin1Char('\'');
				break;
			}
		case QVariant::Bool:
			r = QString::number(field.toBool());
			break;
		case QVariant::ByteArray : {
			QByteArray ba = field.toByteArray();
			QString res;
			static const char hexchars[] = "0123456789abcdef";
			for (int i = 0; i < ba.size(); ++i) {
				uchar s = (uchar) ba[i];
				res += QLatin1Char(hexchars[s >> 4]);
				res += QLatin1Char(hexchars[s & 0x0f]);
			}
			r = QLatin1Char('\'') + res +  QLatin1Char('\'');
			break;
								   }
		default:
			r = field.toString();
			break;
		}
	}
	return r;
}
