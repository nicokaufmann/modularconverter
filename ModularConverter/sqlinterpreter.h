#ifndef sqlinterpreter_h__
#define sqlinterpreter_h__

#include <QMap>
#include <QVector>
#include <QPair>
#include <QString>
#include <QChar>

#include "databaseelements.h"
#include "syntaxelements.h"

#include "syntaxnodes.h"

class SqlInterpreter {
public:
	//extend with possible database information class / tables + attributes
	SqlInterpreter();
	SqlInterpreter(DatabaseElements databaseElements, SyntaxElements syntaxElements);
	~SqlInterpreter();

	void setSyntaxElements(SyntaxElements syntaxElements);
	void setDatabaseElements(DatabaseElements databaseElements);

	int interpret(QString syntax, SyntaxNode* rootNode);

	SyntaxNode* getNode(QString& syntax, SyntaxNode* parent);
private:
	void init();
	void initElements();

	SyntaxNode* interpretFirstSequence(QString& syntax, int pos );
	SyntaxNode* interpretSymbol(QString& syntax, QChar separator);

	bool placeNode( SyntaxNode* lastAddedNode, SyntaxNode* createdNode );
	int getFirstSequence(QString& syntax);
	QString leftTrim(const QString& str);

	SyntaxElements syntaxElements;
	DatabaseElements databaseElements;

	QStringList currentTables;
	QStringList currentAttributes;
	QStringList currentFunctions;

	QRegExp numberCheck;

	QVector<QChar> separators;
	QMap<QString,SyntaxNode*(*)(SyntaxNode*, QString)> typeMap;
};
#endif // sqlinterpreter_h__





