#include "modularconverter.h"

#include "logindialog.h"

ModularConverter::ModularConverter(LoginDialog* loginDialog, ApplicationDatabase* applicationDatabase, QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags), loginDialog(loginDialog), applicationDatabase(applicationDatabase), modulManager(new ModulManager())
{
	ui.setupUi(this);

	simpleSqlEditor = new SimpleSqlEditor(applicationDatabase);

	//tabs
	workflowTab = new WorkflowTab(applicationDatabase,modulManager,this);
	ui.contentWorkflowTab->addWidget(workflowTab);
	
	modulTab = new ModulTab(applicationDatabase->getConnectionManager(),modulManager,this);
	ui.contentModulTab->addWidget(modulTab);

	editWorkflowsTab = new EditWorkflowsTab(applicationDatabase,modulManager,this);
	ui.contentEditWorkflowTab->addWidget(editWorkflowsTab);

	dataTab = new DataTab(applicationDatabase, simpleSqlEditor, this);
	ui.contentDataTab->addWidget(dataTab);

	tableTab = new TableTab(applicationDatabase,this);
	ui.contentTablesTab->addWidget(tableTab);

	//signals
	QObject::connect(applicationDatabase, SIGNAL(databasesUpdated()), editWorkflowsTab, SLOT(databasesUpdated()));
	QObject::connect(applicationDatabase, SIGNAL(databasesUpdated()), dataTab, SLOT(databasesUpdated()));
	QObject::connect(applicationDatabase, SIGNAL(databasesUpdated()), tableTab, SLOT(databasesUpdated()));

	QObject::connect(applicationDatabase, SIGNAL(workflowsUpdated()), workflowTab, SLOT(workflowsUpdated()));
	QObject::connect(applicationDatabase, SIGNAL(workflowsUpdated()), editWorkflowsTab, SLOT(workflowsUpdated()));
	
	QObject::connect(ui.actionDatabaselist, SIGNAL(triggered()), this, SLOT(openDatabaseList()));
	QObject::connect(ui.actionOpenSqlEditor, SIGNAL(triggered()), this, SLOT(openSqlEditor()));
	QObject::connect(ui.actionChangeApplication, SIGNAL(triggered()), this, SLOT(changeApplication()));
	QObject::connect(ui.actionExit, SIGNAL(triggered()), this, SLOT(exitApplication()));
	QObject::connect(ui.actionLogout, SIGNAL(triggered()), this, SLOT(logout()));

	QObject::connect(modulTab, SIGNAL(startModul(QWidget*)), this, SLOT(startModul(QWidget*)));
	QObject::connect(workflowTab, SIGNAL(startWorkflowModul(QList<WorkflowItem*>,int)), this, SLOT(startWorkflowModul(QList<WorkflowItem*>,int)));
}

ModularConverter::~ModularConverter()
{
	delete simpleSqlEditor;
}

void ModularConverter::setPermissions( ApplicationUser user )
{
	//enable tabs
	ui.tabWidget->clear();
	ui.tabWidget->insertTab(1,ui.tWorkflow,"Workflows");

	if(user.hasFeaturePermission("modultab")) {
		ui.tabWidget->insertTab(2,ui.tModul,"Module");
	}
	if(user.hasFeaturePermission("workflowadmintab")) {
		ui.tabWidget->insertTab(3,ui.tWorkflowAdmin,"Workflows verwalten");
	}
	if(user.hasFeaturePermission("tablestab")) {
		ui.tabWidget->insertTab(4,ui.tTables,"Tabellen");
	}
	if(user.hasFeaturePermission("datatab")) {
		ui.tabWidget->insertTab(5,ui.tData,"Daten");
	}

	//enable actions
	ui.actionDatabaselist->setEnabled(user.hasFeaturePermission("databaselist"));
	ui.actionOpenSqlEditor->setEnabled(user.hasFeaturePermission("sqleditor"));

	//set sqleditor permissions
	simpleSqlEditor->setPermissions(user);

	//set tab permissions
	dataTab->setPermissions(user);

	//set workflow permissions
	workflowTab->workflowsUpdated();
}

void ModularConverter::connectionLoss()
{
	ui.statusBar->showMessage("Achtung! Es besteht keine Verbindung zur Anwendung!");

	QMessageBox msgBox;
	msgBox.setWindowTitle("Anwendungsfehler");
	msgBox.setText("Die Verbindung zur Anwendungsdatenbank wurde unterbrochen!");
	msgBox.setIcon(QMessageBox::Critical);
	//msgBox.setDetailedText(databaseManager->getCoreConnection().getLastError());
	msgBox.setStandardButtons(QMessageBox::Ok);
	msgBox.setDefaultButton(QMessageBox::Ok);
	msgBox.exec();
}

void ModularConverter::openDatabaseList()
{
	DatabaseListDialog dbl(applicationDatabase,this);
	dbl.exec();
}

void ModularConverter::openSqlEditor()
{
	simpleSqlEditor->show();
}

void ModularConverter::display()
{
	setPermissions(applicationDatabase->getCurrentUser());
	show();
}

void ModularConverter::changeApplication()
{
	loginDialog->show();
}

void ModularConverter::exitApplication()
{
	close();
}

void ModularConverter::logout()
{
	hide();
	loginDialog->show();
}
