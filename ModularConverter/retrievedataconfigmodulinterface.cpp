#include "retrievedataconfigmodulinterface.h"
#include "retrievedataconfigmodul.h"

#include "datamatchingmodel.h"

RetrieveDataConfigModulInterface::RetrieveDataConfigModulInterface(RetrieveDataConfigModul* rdcm, QWidget *parent, Qt::WFlags flags )
	: QWidget(parent,flags), rdcm(rdcm), comboDelegate(rdcm->getSourceFields())
{
	ui.setupUi(this);

	DataMatchingModel* dmm = new DataMatchingModel(rdcm->getTargetFields());
	dmm->addMatchingRow("Quellmapping", QMap<QString,QString>());
	ui.mapFieldsTable->setModel(dmm);
	ui.mapFieldsTable->setItemDelegateForRow(0,&comboDelegate);

	init();

	QObject::connect(ui.cbContext, SIGNAL(activated(int)), this, SLOT(selectContext(int)));
	QObject::connect(ui.bSave, SIGNAL(clicked()), this, SLOT(saveContext()));
	QObject::connect(ui.bDelete, SIGNAL(clicked()), this, SLOT(deleteContext()));
	QObject::connect(ui.bNewContext, SIGNAL(clicked()), this, SLOT(createContext()));
	QObject::connect(ui.bAddStage, SIGNAL(clicked()), this, SLOT(addStage()));
}

RetrieveDataConfigModulInterface::~RetrieveDataConfigModulInterface()
{

}

void RetrieveDataConfigModulInterface::init()
{
	ui.cbContext->clear();
	ui.cbContext->addItems(rdcm->getContextList());
	selectContext(0);
}

void RetrieveDataConfigModulInterface::selectContext( int index )
{
	QMessageBox msgBox;
	msgBox.setStandardButtons(QMessageBox::Ok);
	msgBox.setDefaultButton(QMessageBox::Ok);
	msgBox.setWindowTitle("Fehler");
	msgBox.setText("Fehler bei der Auswahl des Kontext");
	msgBox.setIcon(QMessageBox::Critical);

	clearStages();

	QMap<QString, QString> fieldMap;
	if(!rdcm->loadFieldMap(ui.cbContext->itemText(index), fieldMap)) {
		msgBox.setDetailedText(rdcm->getLastError());
		msgBox.exec();
		return;
	}
	DataMatchingModel* dmm = (DataMatchingModel*)ui.mapFieldsTable->model(); 
	dmm->setMatchingRow("Quellmapping",fieldMap);

	QMap<int, QMap<QString, QPair<QString, QString>>> stageMap;
	if(!rdcm->loadStageMap(ui.cbContext->itemText(index),stageMap)) {
		msgBox.setDetailedText(rdcm->getLastError());
		msgBox.exec();
		return;
	}

	const QList<int> stages(stageMap.keys());

	for(int i=0; i < stages.size(); ++i) {
		const QMap<QString, QPair<QString, QString>> currentStage(stageMap[stages[i]]);
		RetrieveDataStageConfigModulInterface* rdscm = new RetrieveDataStageConfigModulInterface(i,stages[i],rdcm, currentStage,this);
		
		stageList.append(rdscm);
		ui.stageListLayout->addWidget(rdscm);
		QObject::connect(rdscm, SIGNAL(removeStage(int)), this, SLOT(removeStage(int)));
	}
}

void RetrieveDataConfigModulInterface::createContext()
{
	QString name=QInputDialog::getText(this,"Eingabe","Kontextname:");

	if(!name.isEmpty() && ui.cbContext->findText(name) == -1) {
		ui.cbContext->addItem(name);
		ui.cbContext->setCurrentIndex(ui.cbContext->findText(name));

		clearStages();

		DataMatchingModel* dmm = (DataMatchingModel*)ui.mapFieldsTable->model(); 
		dmm->setMatchingRow("Quellmapping",QMap<QString,QString>());
	}
}

void RetrieveDataConfigModulInterface::addStage()
{
	RetrieveDataStageConfigModulInterface* rdscm;
	if(stageList.size() > 0) {
		rdscm = new RetrieveDataStageConfigModulInterface(stageList.last()->getIndex()+1,stageList.last()->getStage()+1,rdcm,QMap<QString, QPair<QString, QString>>(),this);
	} else {
		rdscm = new RetrieveDataStageConfigModulInterface(1,0,rdcm, QMap<QString, QPair<QString, QString>>(),this);
	}

	stageList.append(rdscm);
	ui.stageListLayout->addWidget(rdscm);

	QObject::connect(rdscm, SIGNAL(removeStage(int)), this, SLOT(removeStage(int)));
}

void RetrieveDataConfigModulInterface::clearStages()
{
	// clear widgets
	for(int i=0; i < stageList.size(); ++i) {
		QObject::disconnect(stageList[i], SIGNAL(removeStage(int)), this, SLOT(removeStage(int)));

		delete stageList[i];
	}
	stageList.clear();

	// clear layout
	QLayoutItem *child;
	while ((child = ui.stageListLayout->takeAt(0)) != 0) {
		delete child;
	}
}

void RetrieveDataConfigModulInterface::saveContext()
{
	QMessageBox msgBox;
	msgBox.setStandardButtons(QMessageBox::Ok);
	msgBox.setDefaultButton(QMessageBox::Ok);
	msgBox.setWindowTitle("Fehler");
	msgBox.setText("Fehler beim speichern des Kontexts");
	msgBox.setIcon(QMessageBox::Critical);

	if(rdcm->deleteContext(ui.cbContext->currentText())) {
		DataMatchingModel* dmm = (DataMatchingModel*)ui.mapFieldsTable->model(); 
		
		if(rdcm->saveFieldMap(ui.cbContext->currentText(),dmm->matchingRow("Quellmapping"))) {
			QMap<int, QMap<QString, QPair<QString, QString>>> stageMap;
			
			for(int i=0; i < stageList.size(); ++i) {
				QMap<QString, QPair<QString, QString>> currentStage;
				stageList[i]->getStageMapping(currentStage);
				stageMap.insert(stageList[i]->getStage(), currentStage);
			}

			if(!rdcm->saveStageMap(ui.cbContext->currentText(), stageMap)) {
				msgBox.setDetailedText(rdcm->getLastError());
				msgBox.exec();
			}
		} else {
			msgBox.setDetailedText(rdcm->getLastError());
			msgBox.exec();
		}
	} else {
		msgBox.setDetailedText(rdcm->getLastError());
		msgBox.exec();
	}
}

void RetrieveDataConfigModulInterface::removeStage( int index )
{
	for(int i=0; i < stageList.size(); ++i) {
		if(stageList[i]->getIndex()==index) {
			QObject::disconnect(stageList[i], SIGNAL(removeStage(int)), this, SLOT(removeStage(int)));
			delete stageList[i];
			stageList.removeAt(i);
			return;
		}
	}
}

void RetrieveDataConfigModulInterface::deleteContext()
{
	if(ui.cbContext->currentText().length() > 0) {
		QMessageBox msgBox;
		msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgBox.setDefaultButton(QMessageBox::No);
		msgBox.setWindowTitle("Achtung");
		msgBox.setText("Soll der Kontext wirklich gel�scht werden?");
		msgBox.setIcon(QMessageBox::Warning);

		if(msgBox.exec() == QMessageBox::Yes) {
			if(rdcm->deleteContext(ui.cbContext->currentText())) {
				init();
			} else {
				QMessageBox errorBox;
				errorBox.setStandardButtons(QMessageBox::Ok);
				errorBox.setDefaultButton(QMessageBox::Ok);
				errorBox.setWindowTitle("Fehler");
				errorBox.setText("Der Kontext konnte nicht gel�scht werden.");
				errorBox.setIcon(QMessageBox::Critical);
				errorBox.setDetailedText(rdcm->getLastError());
				errorBox.exec();
			}
		}
	}
}


