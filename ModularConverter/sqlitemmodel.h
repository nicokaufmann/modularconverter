#ifndef SqlItemModel_h__
#define SqlItemModel_h__

#include <QAbstractItemModel>
#include <QInputDialog>
#include <QVariant>
#include <QStringList>
#include <QIcon>
#include <QMimeData>
#include <QMenu>

#include "sqlinterpreter.h"


enum NodeOperation {
	replaceNode = 1,
	addNode = 2,
	insertNode = 3,
	swapNodes = 4,
	copyNode = 5,
	removeNode = 6,
	editNode = 7,
	pasteNode = 8,
	preorderNode = 9,
	noopNode = 10
};

class SqlItemModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	SqlItemModel(SqlInterpreter* interpreter, QObject *parent = 0);
	~SqlItemModel();

	void setRootNode(SyntaxNode* root);
	void setSqlInterpreter(SqlInterpreter* interpreter);

	bool dropMimeData(const QMimeData * data, Qt::DropAction action, int row, int column, const QModelIndex & parent);
	QMimeData* mimeData(const QModelIndexList & indexes) const;
	QStringList mimeTypes() const;

	Qt::ItemFlags flags(const QModelIndex & index) const;
	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole ) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

	bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
	//bool insertRows(int row, int count, const QModelIndex & parent = QModelIndex());
	//bool removeRows(int row, int count, const QModelIndex & parent = QModelIndex()) ;

	int rowCount( const QModelIndex & parent = QModelIndex() ) const;
	int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

	QModelIndex index(int row, int column, const QModelIndex &parent) const;
	QModelIndex nodeIndex(SyntaxNode* node) const;
	QModelIndex parent(const QModelIndex &index) const;

	Qt::DropActions supportedDropActions() const;

	SyntaxNode* getRootNode();

public slots:
	void dropOperation( QAction* action );
	void contextOperation( QAction* action);

private:
	void checkDropOptions(bool internalMove);
	
	void removeSyntaxNode(SyntaxNode* node);
	void addSyntaxNode(SyntaxNode* target, SyntaxNode* node);
	void addSyntaxNode(SyntaxNode* target, SyntaxNode* node,int at);

	SyntaxNode* operationSource;
	SyntaxNode* operationTarget;

	SyntaxNode* rootNode;
	SqlInterpreter* interpreter;

	QMap<QString,QString> iconMap;
};
#endif // SqlItemModel_h__