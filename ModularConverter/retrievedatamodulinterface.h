#ifndef retrievedatamodulinterface_h__
#define retrievedatamodulinterface_h__

#include "standardmodulinterface.h"
#include "selectversionmodulinterface.h"
#include "executemodulinterface.h"
#include "retrievedetailsmodulinterface.h"

class RetrieveDataModul;

class RetrieveDataModulInterface : public StandardModulInterface 
{
	Q_OBJECT

public:
	RetrieveDataModulInterface(RetrieveDataModul* modul, QWidget *parent = 0, Qt::WFlags flags = 0);
	~RetrieveDataModulInterface();

	void finished(bool status);

public slots:
	void executeModul();

	void showWithResult();
	void showWithoutResult();

signals:
	void execute(int cmd=0);

private:
	ExecuteModulInterface* executeInterface;
	RetrieveDetailsModulInterface* detailsInterface;
};
#endif // retrievedatamodulinterface_h__