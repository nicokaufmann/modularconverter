#include "versionrequest.h"

VersionRequest::VersionRequest()
	: sharedConnectionname(new QString("")), sharedTablename(new QString("")), sharedContext(new QString("")), sharedVersionId(new unsigned int)
{
	*sharedVersionId = 0;
}

VersionRequest::VersionRequest( QString connectionname, QString tablename )
	: sharedConnectionname(new QString(connectionname)), sharedTablename(new QString(tablename)), sharedContext(new QString("")), sharedVersionId(new unsigned int)
{
	*sharedVersionId = 0;
}

VersionRequest::VersionRequest( QString connectionname, QString tablename, QString context )
	: sharedConnectionname(new QString(connectionname)), sharedTablename(new QString(tablename)), sharedContext(new QString(context)), sharedVersionId(new unsigned int)
{
	*sharedVersionId = 0;
}

VersionRequest::VersionRequest( const VersionRequest& obj )
{
	sharedConnectionname = obj.sharedConnectionname;
	sharedContext = obj.sharedContext;
	sharedTablename = obj.sharedTablename;
	sharedVersionId = obj.sharedVersionId;
}

VersionRequest& VersionRequest::operator=( const VersionRequest& obj )
{
	sharedConnectionname = obj.sharedConnectionname;
	sharedContext = obj.sharedContext;
	sharedTablename = obj.sharedTablename;
	sharedVersionId = obj.sharedVersionId;

	return *this;
}

void VersionRequest::setConnectionname( QString connectionname )
{
	*sharedConnectionname = connectionname;
}

QString VersionRequest::connectionname()
{
	return *sharedConnectionname;
}

void VersionRequest::setTablename( QString tablename )
{
	*sharedTablename = tablename;
}

QString VersionRequest::tablename()
{
	return *sharedTablename;
}

void VersionRequest::setContext( QString context )
{
	*sharedContext = context;
}

QString VersionRequest::context()
{
	return *sharedContext;
}

void VersionRequest::setVersionId( unsigned int versionId )
{
	*sharedVersionId = versionId;
}

unsigned int VersionRequest::versionId()
{
	return *sharedVersionId;
}

