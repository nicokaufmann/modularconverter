#include "reportversionmodul.h"

#include "excelreportmodul.h"

ReportVersionModul::ReportVersionModul(ExcelReportModul* modul,  QWidget *parent , Qt::WFlags flags )
	: BaseExecutionModul(parent,flags), modul(modul)
{
	ui.bExecute->setText("Export");

	//QObject::connect(ui.bExecute, SIGNAL(clicked()), this, SLOT(start()));
}

ReportVersionModul::~ReportVersionModul()
{
}

void ReportVersionModul::initialize()
{
	removeAllWidgets();

	for(int i=0; i < versionedTables.keys().size();++i) {
		delete versionedTables[versionedTables.keys()[i]];
	}

	loadVersionMap();

	QMap<QString, QString> idents;
	QMap<QString, QStringList> versions;
	if(modul->getStatementIdent(idents) && modul->getStatementVersions(versions)) {
		

		for(int i=0; i < versions.keys().size(); ++i) {
			QString key = versions.keys()[i];
			
			TableVersion* temp = new TableVersion(&versionMap,this);
			temp->setVersionList(versions[key]);
			temp->setTableContext(idents[key]);

			versionedTables[key] = temp;
			addWidget(temp);
		}
	} else {
		//errormsg
	}
}


void ReportVersionModul::execute()
{
	QMessageBox msgBox;
	msgBox.setWindowTitle("Fehler");
	msgBox.setStandardButtons(QMessageBox::Ok);
	msgBox.setDefaultButton(QMessageBox::Ok);

	QString path = QFileDialog::getSaveFileName(this,"Exportdatei angeben","","Excel Arbeitsmappe (*.xlsx);;Excel Arbeitsmappe 97-2003 (*.xls)");

	if(path.length() > 0) {
		
			ExcelReportModul* reportModul=(ExcelReportModul*)modul;
			QMap<QString, QString> versions;
			for(int i=0; i < versionedTables.keys().size();++i) {
				versions[versionedTables.keys()[i]] = versionedTables[versionedTables.keys()[i]]->getSelectedVersion();
			}

			if(reportModul->execute(path,versions)) {
				msgBox.setWindowTitle("Modul ausgeführt");
				msgBox.setText("Berechnungen abgeschlossen.");
				msgBox.setIcon(QMessageBox::Information);
			} else {
				msgBox.setText("Das Modul hat einen Fehler verursacht.");
				msgBox.setDetailedText(reportModul->getLastError());
				msgBox.setIcon(QMessageBox::Critical);
			}
	} else {
		msgBox.setText("Es muss ein Pfad angegeben werden.");
		msgBox.setIcon(QMessageBox::Warning);
	}

	msgBox.exec();
}

void ReportVersionModul::loadVersionMap()
{
	ParameterMap paramMap = modul->getParameterMap();

	QSqlDatabase connection = paramMap["Versionierung"].first;
	QString table = paramMap["Versionierung"].second;

	if(!connection.isOpen()) {
		connection.open();
	}

	if(connection.isOpen()) {
		QSqlQuery data(connection);
		if(data.exec("select * from "+ DatabaseInterop::isolateName(table))) {
			versionMap.clear();

			while(data.next()) {
				versionMap[data.record().value("id").toString()] = QPair<QString, QDateTime>(data.record().value("description").toString(),data.record().value("created").toDateTime());
			}
		} else {
			QMessageBox msgBox;
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::Ok);
			msgBox.setWindowTitle("Fehler");
			msgBox.setText("Liste konnte nicht aktuallisiert werden, Abfrage fehlgeschlagen.");
			msgBox.setDetailedText(data.lastError().text());
			msgBox.setIcon(QMessageBox::Critical);
			msgBox.exec();
			return;
		}
	} else {
		QMessageBox msgBox;
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.setWindowTitle("Fehler");
		msgBox.setText("Liste konnte nicht aktuallisiert werden, Problem mit Datenbankverbindung.");
		msgBox.setDetailedText(connection.lastError().text());
		msgBox.setIcon(QMessageBox::Critical);
		msgBox.exec();
		return;
	}
}
