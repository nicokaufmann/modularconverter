#ifndef CompletionItemModel_h__
#define CompletionItemModel_h__

#include <QAbstractItemModel>
#include <QVariant>
#include <QStringList>
#include <QIcon>
#include <QMimeData>

#include "syntaxelements.h"
#include "databaseelements.h"

class CompletionItemModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	CompletionItemModel(QObject *parent = 0);

	void setCompletionElements(SyntaxElements syntaxElements, DatabaseElements databaseElements);
	
	Qt::ItemFlags flags(const QModelIndex & index) const;
	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole ) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

	int rowCount( const QModelIndex & parent = QModelIndex() ) const;
	int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

	QModelIndex index(int row, int column, const QModelIndex &parent) const;
	QModelIndex parent(const QModelIndex &index) const;


private:

	QVector<QVariant> completionList;
	QMap<QString, QVariant> completionIcons;

	QVariant invalidVariant;
	QModelIndex invalidModelIndex;
};

#endif // CompletionItemModel_h__