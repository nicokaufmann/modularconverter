#include "listdialog.h"

#include <QStringListModel>

ListDialog::ListDialog(const QString& title, const QStringList& list,QWidget *parent, Qt::WFlags flags)
	: QDialog(parent, flags)
{
	ui.setupUi(this);
	ui.listView->setModel(new QStringListModel(list));

	setWindowTitle(title);
}

ListDialog::~ListDialog()
{
}

void ListDialog::setList( const QStringList& list )
{
	QStringListModel* model = (QStringListModel*)ui.listView->model();
	model->setStringList(list);
}
