#include "datainsertmodel.h"
#include "sqlquerymodel.h"

DataInsertModel::DataInsertModel( ConnectionManager* connectionManager, QString connectionname, QString tablename, QObject *parent)
	: QAbstractItemModel(parent), connectionManager(connectionManager), connectionname(connectionname), tablename(tablename), columns(0)
{
	ConnectionInformation connection(connectionManager->getConnection(connectionname));
	DatabaseElements dbElements(*connection.databaseElements());
	SyntaxElements syntaxElements(*connection.syntaxElements());
	
	QVector<QPair<QString,int>> attributeTypes = dbElements.getAttributesTypes(tablename);
	
	for(int i=0; i < attributeTypes.size(); ++i) {
		columnData<<attributeTypes[i].first;
		columnTypeData<<syntaxElements.getType(attributeTypes[i].second);
	}

	columns = attributeTypes.size();
	columnInsertData.resize(columns);

}

DataInsertModel::~DataInsertModel()
{
}


Qt::ItemFlags DataInsertModel::flags( const QModelIndex & index ) const
{
	if(index.isValid()) {
		if(index.row()==1) {
			return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
		} else {
			return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
		}
	}

	return Qt::NoItemFlags;
}

Qt::DropActions DataInsertModel::supportedDropActions() const
{
	return Qt::IgnoreAction;
}


int DataInsertModel::rowCount( const QModelIndex & parent ) const
{
	return 2;
}

int DataInsertModel::columnCount( const QModelIndex & parent ) const
{
	return columns;
}




QVariant DataInsertModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if(role == Qt::DisplayRole) {
		//section relative to buffer position in
		if(orientation == Qt::Horizontal) {
			if(section < columns) {
				return columnData[section];
			}
		} else if(orientation == Qt::Vertical) {
			if(section == 0) {
				return QVariant("Datentyp");
			} else {
				return QVariant("Datensatz");
			}
		}
	}
	
	return invalidVariant;
}

QVariant DataInsertModel::data( const QModelIndex & index, int role ) const
{
	if(role == Qt::DisplayRole || role == Qt::EditRole) {
		if(index.isValid() && index.column() < columns && index.row() < 2) {
			if(index.row() == 0) {
				return columnTypeData[index.column()];
			} else {
				return columnInsertData[index.column()];
			}
		}
	}

	return invalidVariant;
}

QMimeData* DataInsertModel::mimeData( const QModelIndexList & indexes ) const
{
	if(indexes.size() == 0) {
		return 0;
	} else {
		QMimeData* data = new QMimeData;
		QString textData("");
		int itemCount = 0;
		for(int i=0; i < indexes.size(); ++i) {
			if (indexes[i].isValid()) {
				textData += indexes[i].data(Qt::DisplayRole).toString()+"	";
				itemCount++;
			}
		}

		if(itemCount > 0) {
			textData.chop(1);
		}

		data->setText(textData);
		return data;
	}
}



QModelIndex DataInsertModel::index( int row, int column, const QModelIndex &parent ) const
{
	return createIndex(row, column);
}

QModelIndex DataInsertModel::parent( const QModelIndex &index ) const
{
	return invalidIndex;
}

bool DataInsertModel::setData( const QModelIndex & index, const QVariant & value, int role /*= Qt::EditRole */ )
{
	if(role == Qt::EditRole) {
		if(index.isValid() && index.column() < columns && index.row() == 1) {
			columnInsertData[index.column()] = value;
			dataChanged(index,index);
			return true;
		}
	}
	return false;
}

QVector<QVariant> DataInsertModel::getInsertData()
{
	return columnInsertData;
}
