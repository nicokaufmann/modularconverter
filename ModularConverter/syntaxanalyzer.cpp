#include "syntaxanalyzer.h"


SyntaxAnalyzer::SyntaxAnalyzer( SyntaxNode* rootNode )
	: rootNode(rootNode)
{

}

SyntaxAnalyzer::~SyntaxAnalyzer()
{

}

void SyntaxAnalyzer::reset()
{
}

QVector<SyntaxNode*> SyntaxAnalyzer::getAllDepth( const QString& nodeType )
{
	QVector<SyntaxNode*> nodes;
	getAllDepth(rootNode, nodeType, nodes);
	return nodes;
}

void SyntaxAnalyzer::getAllDepth(SyntaxNode* node, const QString& nodeType, QVector<SyntaxNode*>& nodes )
{
	if(node->type() == nodeType) {
		nodes.append(node);
	}

	for(int i=0; i < node->count(); ++i) {
		getAllDepth(node->children()[i], nodeType, nodes);
	}
}

QVector<SyntaxNode*> SyntaxAnalyzer::getAllBreadth( const QString& nodeType )
{
	QVector<SyntaxNode*> nodes;
	QList<SyntaxNode*> checkList;

	checkList.append(rootNode);
	while(checkList.count() > 0) {
		getAllBreadth(checkList, nodes, nodeType);
	}
	return nodes;
}

void SyntaxAnalyzer::getAllBreadth( QList<SyntaxNode*>& checkList, QVector<SyntaxNode*>& nodes, const QString& nodeType )
{
	SyntaxNode* checkNode = checkList.first();
	checkList.removeFirst();

	if(checkNode->type() == nodeType) {
		nodes.append(checkNode);
	}

	for(int i=0; i < checkNode->count(); ++i) {
		checkList.append(checkNode->children()[i]);
	}
}


SyntaxNode* SyntaxAnalyzer::getCorresponding( const QString& nodeType )
{
	SyntaxNode* parent = rootNode->parent();
	if(parent) {
		for(int i=0; i < parent->count(); ++i) {
			if(parent->children()[i]->type() == nodeType) {
				return parent->children()[i];
			}
		}
	}
	return 0;
}

SyntaxNode* SyntaxAnalyzer::getFirstBreadth( const QString& nodeType )
{
	QList<SyntaxNode*> checkList;

	checkList.append(rootNode);
	while(checkList.count() > 0) {
		SyntaxNode* result = getFirstBreadth(checkList, nodeType);
	
		if(result) {
			return result;
		}
	}
	return 0;
}

SyntaxNode* SyntaxAnalyzer::getFirstBreadth( QList<SyntaxNode*>& checkList, const QString& nodeType )
{
	SyntaxNode* checkNode = checkList.first();
	checkList.removeFirst();

	if(checkNode->type() == nodeType) {
		return checkNode;
	}

	for(int i=0; i < checkNode->count(); ++i) {
		checkList.append(checkNode->children()[i]);
	}

	return 0;
}

SyntaxNode* SyntaxAnalyzer::getFirstDepth( const QString& nodeType )
{
	return getFirstDepth(rootNode, nodeType);
}

SyntaxNode* SyntaxAnalyzer::getFirstDepth( SyntaxNode* node, const QString& nodeType )
{
	if(node->type() == nodeType) {
		return node;
	}

	for(int i=0; i < node->count(); ++i) {
		SyntaxNode* result = getFirstDepth(node->children()[i], nodeType);
		if(result) {
			return result;
		}
	}

	return 0;
}

QVector<SyntaxNode*> SyntaxAnalyzer::getNotContained( const QString& nodeType, const QStringList& elements )
{
	QVector<SyntaxNode*> nodes;
	getNotContained(rootNode,nodes, nodeType,elements);
	return nodes;
}

void SyntaxAnalyzer::getNotContained( SyntaxNode* node, QVector<SyntaxNode*>& nodes, const QString& nodeType, const QStringList& elements )
{
	if(node->type() == nodeType && !elements.contains(node->value(), Qt::CaseInsensitive)) {
		nodes.append(node);
	}

	for(int i=0; i < node->count(); ++i) {
		getNotContained(node->children()[i],nodes, nodeType,elements);
	}
}

