#include "retrievedetailsmodulinterface.h"

RetrieveDetailsModulInterface::RetrieveDetailsModulInterface( QWidget *parent , Qt::WFlags flags )
	: QWidget(parent , flags)
{
	ui.setupUi(this);
}

RetrieveDetailsModulInterface::~RetrieveDetailsModulInterface()
{
}

QWidget* RetrieveDetailsModulInterface::withResultsButton()
{
	return ui.bWithResult;
}

QWidget* RetrieveDetailsModulInterface::withoutResultsButton()
{
	return ui.bWithoutResult;
}
