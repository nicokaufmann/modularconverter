#ifndef syntaxelements_h__
#define syntaxelements_h__

#include <QSharedPointer>
#include <QStringList>
#include <QMap>
#include <QVariant>
#include <QDate>

class SyntaxElements {
public:
	SyntaxElements();
	SyntaxElements(const SyntaxElements& obj);
	SyntaxElements& operator= (const SyntaxElements& obj);
	~SyntaxElements();

	QStringList getKeywords() const;
	QStringList getLogicalFunctions() const;

	QStringList getFunctions() const;
	QStringList getFunctionParameter(const QString& functionname) const;
	int getFunctionParamterCount(const QString& functionname) const;

	QStringList getTypeList() const;
	QString getType(int val) const;

	QString fieldLeadDelimiter() const;
	QString fieldTrailDelimiter() const;
	QString tableLeadDelimiter() const;
	QString tableTrailDelimiter() const;
	QString dateDelimiter() const;
	QString textDelimiter() const;

	virtual QString delimitTablename(const QString& tablename) const;
	virtual QString delimitFieldname(const QString& fieldname) const;
	virtual QString delimitText(const QString& text) const;
	virtual QString delimitDate(const QString& date) const;
	virtual QString delimitDate(const QDate& date) const;

	virtual QString filterSpecialChars(const QString& string);

	virtual QString insertLimit(const QString& statement,const int& limit) const;

	virtual void initializeSyntax();
protected:
	QSharedPointer<QString> sharedFieldLeadDelimiter;
	QSharedPointer<QString> sharedFieldTrailDelimiter;
	QSharedPointer<QString> sharedTableLeadDelimiter;
	QSharedPointer<QString> sharedTableTrailDelimiter;
	QSharedPointer<QString> sharedTextDelimiter;
	QSharedPointer<QString> sharedDateDelimiter;

	QSharedPointer<QRegExp> sharedSpecialChars;
	QSharedPointer<QString> sharedSpecialCharsReplacement;

	QSharedPointer<QStringList> keywords;
	QSharedPointer<QStringList> logicalFunctions;
	QSharedPointer<QMap<QString, QStringList>> functions;

	QSharedPointer<QStringList> typeList;
	QSharedPointer<QMap<int, QString>> typeMapping;

};
#endif // syntaxelements_h__