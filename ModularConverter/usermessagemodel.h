#include <QAbstractItemModel>
#include <QVariant>
#include <QIcon>

#include "convertermodul.h"

class UserMessageModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	UserMessageModel(ConverterModul* converterModul,QObject *parent = 0);

	void setConverterModul(ConverterModul* converterModul);
	void updateMessages();

	Qt::ItemFlags flags(const QModelIndex & index) const;
	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole ) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

	int rowCount( const QModelIndex & parent = QModelIndex() ) const;
	int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

	QModelIndex index(int row, int column, const QModelIndex &parent) const;
	QModelIndex parent(const QModelIndex &index) const;
private:
	ConverterModul* converterModul;

	QVector<int> warningList;
	QVector<QVariant> messageList;
	QVariant invalidVariant;
	QModelIndex invalidModelIndex;

	const QVariant warningIcon;
	const QVariant errorIcon;
};