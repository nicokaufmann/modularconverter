#include "applicationdatabase.h"

#include <QSqlQuery>
#include <QSqlError>

ApplicationDatabase::ApplicationDatabase()
	: connectionManager(new ConnectionManager(this)), workflowManager(new WorkflowManager(this))
{

}

ApplicationDatabase::~ApplicationDatabase()
{
	delete connectionManager;
	delete workflowManager;
}


WorkflowManager* ApplicationDatabase::getWorkflowManager()
{
	return workflowManager;
}

ConnectionManager* ApplicationDatabase::getConnectionManager()
{
	return connectionManager;
}

QString ApplicationDatabase::getLastError()
{
	return lastError;
}

ApplicationUser ApplicationDatabase::getCurrentUser()
{
	return currentUser;
}

bool ApplicationDatabase::checkApplication( ConnectionInformation connection )
{
	AcquiredConnection dbConnection(connection);

	if(dbConnection->isOpen()) {
		if(dbConnection->record("core_connections").isEmpty()) {
			lastError = "core_connections Tabelle nicht gefunden.";
		} else if(dbConnection->record("core_tables").isEmpty()) {
			lastError = "core_tables Tabelle nicht gefunden.";
		} else if(dbConnection->record("core_workflows").isEmpty()) {
			lastError = "core_workflows Tabelle nicht gefunden.";
		} else if(dbConnection->record("core_workflow_item").isEmpty()) {
			lastError = "core_workflow_item Tabelle nicht gefunden.";
		} else if(dbConnection->record("core_workflow_item_params").isEmpty()) {
			lastError = "core_workflow_item_params Tabelle nicht gefunden.";
		} else if(dbConnection->record("core_user").isEmpty()) {
			lastError = "core_user Tabelle nicht gefunden.";
		} else if(dbConnection->record("core_workflow_permissions").isEmpty()) {
			lastError = "core_workflow_permissions Tabelle nicht gefunden.";
		} else if(dbConnection->record("core_feature_permissions").isEmpty()) {
			lastError = "core_feature_permissions Tabelle nicht gefunden.";
		} else {
			return true;
		}
	} else {
		lastError = "Verbindung fehlgeschlagen.\n" + dbConnection->lastError().text();
	}
	return false;
}

bool ApplicationDatabase::createApplication( ConnectionInformation connection )
{
	AcquiredConnection dbConnection(connection);

	if(dbConnection->isOpen()) {
		QSqlQuery createTable(*dbConnection);

		if(dbConnection->databaseName().contains("Microsoft Access Driver")) {
			if(dbConnection->record("core_connections").isEmpty()) {	
				if(!createTable.exec("create table core_connections(drivername VARCHAR(255) NOT NULL, syntaxname VARCHAR(255) NOT NULL, connectionname VARCHAR(255) NOT NULL,"
					"hostname VARCHAR(255),databasename VARCHAR(255),username VARCHAR(255),password VARCHAR(255), options VARCHAR(255),"
					"parallel YESNO NOT NULL, update_tables YESNO NOT NULL, PRIMARY KEY (connectionname))")) {
						lastError = createTable.lastError().text();
						return false;
				}
			}
			if(dbConnection->record("core_tables").isEmpty()) {	
				if(!createTable.exec("create table core_tables(connectionname VARCHAR(255) NOT NULL,tablename VARCHAR(255));")) {
						lastError = createTable.lastError().text();
						return false;
				}
			}
			if(dbConnection->record("core_workflows").isEmpty()) {	
				if(!createTable.exec("create table core_workflows(id COUNTER NOT NULL, name VARCHAR(255)  NOT NULL, "
					"description VARCHAR(255), forceorder YESNO, PRIMARY KEY (id));")) {
						lastError = createTable.lastError().text();
						return false;
				}
			}
			if(dbConnection->record("core_workflow_item").isEmpty()) {	
				if(!createTable.exec("create table core_workflow_item(id COUNTER NOT NULL, workflow_id INT NOT NULL, order_index INT NOT NULL, "
					"description VARCHAR(255), modulname VARCHAR(255) NOT NULL, modulcontext VARCHAR(255) , inheritversion YESNO, PRIMARY KEY (id));")) {
						lastError = createTable.lastError().text();
						return false;
				}
			}
			if(dbConnection->record("core_workflow_item_params").isEmpty()) {	
				if(!createTable.exec("create table core_workflow_item_params(id COUNTER NOT NULL, workflow_item_id INT NOT NULL, param_alias VARCHAR(255) , "
					"param_table VARCHAR(255), param_connectionname VARCHAR(255), PRIMARY KEY (id));")) {
						lastError = createTable.lastError().text();
						return false;
				}
			}
			if(dbConnection->record("core_user").isEmpty()) {	
				if(!createTable.exec("create table core_user(id COUNTER NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, "
					"admin YESNO, PRIMARY KEY (id));")) {
						lastError = createTable.lastError().text();
						return false;
				}
				QSqlQuery insertUser(*dbConnection);
				if(!insertUser.exec("insert into core_user(username, password, admin) values('kaufmann','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',1)")){
					lastError = insertUser.lastError().text();
					return false;
				}
			}
			if(dbConnection->record("core_workflow_permissions").isEmpty()) {	
				if(!createTable.exec("create table core_workflow_permissions(user_id INT NOT NULL, workflow_id INT NOT NULL, allowed YESNO NOT NULL);")) {
					lastError = createTable.lastError().text();
					return false;
				}
			}
			if(dbConnection->record("core_feature_permissions").isEmpty()) {	
				if(!createTable.exec("create table core_feature_permissions(user_id INT NOT NULL, feature VARCHAR(255) NOT NULL, allowed YESNO NOT NULL);")) {
					lastError = createTable.lastError().text();
					return false;
				}
			}
			return true;
		} else {
			if(dbConnection->record("core_connections").isEmpty()) {	
				if(!createTable.exec("create table core_connections(drivername VARCHAR(255) NOT NULL, syntaxname VARCHAR(255) NOT NULL, connectionname VARCHAR(255) NOT NULL,"
					"hostname VARCHAR(255),databasename VARCHAR(255),username VARCHAR(255),password VARCHAR(255), options VARCHAR(255),"
					"parallel BOOLEAN NOT NULL, update_tables BOOLEAN NOT NULL, PRIMARY KEY (connectionname))")) {
						lastError = createTable.lastError().text();
						return false;
				}
			}
			if(dbConnection->record("core_tables").isEmpty()) {	
				if(!createTable.exec("create table core_tables(connectionname VARCHAR(255) NOT NULL,tablename VARCHAR(255));")) {
					lastError = createTable.lastError().text();
					return false;
				}
			}
			if(dbConnection->record("core_workflows").isEmpty()) {	
				if(!createTable.exec("create table core_workflows(id INT NOT NULL AUTO_INCREMENT, name VARCHAR(255)  NOT NULL, "
					"description VARCHAR(255), forceorder BOOLEAN, PRIMARY KEY (id));")) {
						lastError = createTable.lastError().text();
						return false;
				}
			}
			if(dbConnection->record("core_workflow_item").isEmpty()) {	
				if(!createTable.exec("create table core_workflow_item(id INT NOT NULL AUTO_INCREMENT, workflow_id INT NOT NULL, order_index INT NOT NULL, "
					"description VARCHAR(255), modulname VARCHAR(255) NOT NULL, modulcontext VARCHAR(255) , inheritversion BOOLEAN, PRIMARY KEY (id));")) {
						lastError = createTable.lastError().text();
						return false;
				}
			}
			if(dbConnection->record("core_workflow_item_params").isEmpty()) {	
				if(!createTable.exec("create table core_workflow_item_params(id INT NOT NULL AUTO_INCREMENT, workflow_item_id INT NOT NULL, param_alias VARCHAR(255) , "
					"param_table VARCHAR(255), param_connectionname VARCHAR(255), PRIMARY KEY (id));")) {
						lastError = createTable.lastError().text();
						return false;
				}
			}
			if(dbConnection->record("core_user").isEmpty()) {	
				if(!createTable.exec("create table core_user(id INT NOT NULL AUTO_INCREMENT, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, "
					"admin BOOLEAN, PRIMARY KEY (id));")) {
						lastError = createTable.lastError().text();
						return false;
				}
				QSqlQuery insertUser(*dbConnection);
				if(!insertUser.exec("insert into core_user(username, password, admin) values('kaufmann','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',1)")){
					lastError = insertUser.lastError().text();
					return false;
				}
			}
			if(dbConnection->record("core_workflow_permissions").isEmpty()) {	
				if(!createTable.exec("create table core_workflow_permissions(user_id INT NOT NULL, workflow_id INT NOT NULL, allowed BOOLEAN NOT NULL);")) {
					lastError = createTable.lastError().text();
					return false;
				}
			}
			if(dbConnection->record("core_feature_permissions").isEmpty()) {	
				if(!createTable.exec("create table core_feature_permissions(user_id INT NOT NULL, feature VARCHAR(255) NOT NULL, allowed BOOLEAN NOT NULL);")) {
					lastError = createTable.lastError().text();
					return false;
				}
			}
			return true;
		}
	} else {
		lastError = "Verbindung fehlgeschlagen.\n" + dbConnection->lastError().text();
	}
	return false;
}

bool ApplicationDatabase::connect( QString username, QString password, ConnectionInformation connection )
{
	currentUser = loadUser(connection, username, password);

	if(currentUser.isValid() && loadConnections(connection) && loadWorkflows(connection)) {
		applicationName = connection.connectionname();
		connectionManager->addConnection(connection);
		connectionManager->getDatabaseElementsUpdater()->updateElements();
		emit databasesUpdated();
		return true;
	} else {
		return false;
	}
}

ApplicationUser ApplicationDatabase::loadUser(ConnectionInformation connection, QString username, QString password )
{
	AcquiredConnection dbConnection(connection);

	if(dbConnection->isOpen()) {
		connectionManager->clearConnections();

		QSqlQuery queryUser(*dbConnection);
		queryUser.prepare("select * from core_user where username=? and password=?");
		queryUser.bindValue(0,username);
		queryUser.bindValue(1,password);

		if(queryUser.exec()) {
			if(queryUser.next()) {
				ApplicationUser user(queryUser.record().value("username").toString(), queryUser.record().value("admin").toBool());

				QSqlQuery queryWorkflowPermissions(*dbConnection);
				queryWorkflowPermissions.prepare("select * from core_workflow_permissions where user_id=?");
				queryWorkflowPermissions.bindValue(0,queryUser.record().value("id").toInt());

				if(queryWorkflowPermissions.exec()) {
					while(queryWorkflowPermissions.next()) {
						user.setWorkflowPermission(queryWorkflowPermissions.record().value("workflow_id").toInt(),queryWorkflowPermissions.record().value("allowed").toBool());
					}
				}

				QSqlQuery queryFeaturePermissions(*dbConnection);
				queryFeaturePermissions.prepare("select * from core_feature_permissions where user_id=?");
				queryFeaturePermissions.bindValue(0,queryUser.record().value("id").toInt());

				if(queryFeaturePermissions.exec()) {
					while(queryFeaturePermissions.next()) {
						user.setFeaturePermission(queryFeaturePermissions.record().value("feature").toString(),queryFeaturePermissions.record().value("allowed").toBool());
					}
				}

				return user;
			} else {
				lastError = "Benutzerdaten nicht korrekt.";
			}
		} else {
			lastError = queryUser.lastError().text();
		}
	} else {
		lastError = dbConnection->lastError().text();
	}

	return ApplicationUser();
}

bool ApplicationDatabase::loadConnections(ConnectionInformation connection)
{
	AcquiredConnection dbConnection(connection);

	if(dbConnection->isOpen()) {
		connectionManager->clearConnections();

		QSqlQuery query(*dbConnection);
		if(query.exec("select * from core_connections")) {
			SimpleCrypt crypto(Q_UINT64_C(0x0c4ad2a3aab8f012));

			while(query.next()) {
				connectionManager->addConnection(ConnectionInformation(ConnectionManager::getDriver(query.record().value("drivername").toString()),
					query.record().value("syntaxname").toString(),
					query.record().value("connectionname").toString(),
					query.record().value("hostname").toString(),
					query.record().value("databasename").toString(),
					query.record().value("options").toString(),
					query.record().value("username").toString(),
					crypto.decryptToString(query.record().value("password").toString()),
					query.record().value("parallel").toBool(),
					query.record().value("update_tables").toBool()));
			}

			emit databasesUpdated();

			return true;
		} else {
			lastError = query.lastError().text();
		}
	} else {
		lastError = dbConnection->lastError().text();
	}

	return false;
}

bool ApplicationDatabase::addConnection( ConnectionInformation connectionInformation )
{
	AcquiredConnection connection(connectionManager->acquireConnection(applicationName));

	if(connection->isOpen()) {
		//name taken?
		QSqlQuery query(*connection);
		query.prepare("select connectionname from core_connections where connectionname=:value");
		query.bindValue(":value",connectionInformation.connectionname());
		if(query.exec() && !query.next()) {
			//store new
			SimpleCrypt crypto(Q_UINT64_C(0x0c4ad2a3aab8f012));

			QSqlQuery insert(*connection);
			insert.prepare("INSERT INTO core_connections (drivername, syntaxname, connectionname, hostname, databasename, username, password, options, parallel, update_tables) "
				"VALUES (:drivername, :syntaxname, :connectionname, :hostname, :databasename, :username, :password, :options, :parallel, :update_tables)");
			insert.bindValue(0, connectionInformation.driver().alias());
			insert.bindValue(1, connectionInformation.syntaxname());
			insert.bindValue(2, connectionInformation.connectionname());
			insert.bindValue(3, connectionInformation.hostname());
			insert.bindValue(4, connectionInformation.databasename());
			insert.bindValue(5, connectionInformation.username());
			insert.bindValue(6, crypto.encryptToString(connectionInformation.password()));
			insert.bindValue(7, connectionInformation.options());
			insert.bindValue(8, connectionInformation.synchronousUse());
			insert.bindValue(9, connectionInformation.updateTables());
			if(insert.exec()) {
				//put in map
				connectionManager->addConnection(connectionInformation);

				connectionManager->getDatabaseElementsUpdater()->updateElements(connectionInformation.connectionname());
				emit databasesUpdated();

				return true;
			} else {
				lastError = insert.lastError().text();
			}
		} else {
			lastError = query.lastError().text();
		}
	} else {
		lastError = connection->lastError().text();
	}

	return false;
}

bool ApplicationDatabase::removeConnection( ConnectionInformation connectionInformation )
{
	AcquiredConnection connection(connectionManager->acquireConnection(applicationName));

	if(connection->isOpen()) {
		QSqlQuery query(*connection);
		query.prepare("delete from core_connections where connectionname=?");
		query.bindValue(0,connectionInformation.connectionname());
		if(query.exec()) {
			connectionManager->removeConnection(connectionInformation);

			emit databasesUpdated();

			return true;
		} else {
			lastError = query.lastError().text();
		}
	} else {
		lastError = connection->lastError().text();
	}

	return false;
}

bool ApplicationDatabase::updateConnection( ConnectionInformation connectionInformation )
{
	AcquiredConnection connection(connectionManager->acquireConnection(applicationName));

	if(connection->isOpen()) {
		SimpleCrypt crypto(Q_UINT64_C(0x0c4ad2a3aab8f012));

		QSqlQuery update(*connection);
		update.prepare("update core_connections set drivername=:drivername, syntaxname=:syntaxname, hostname=:hostname, databasename=:databasename, "
			"username=:username, password=:password, options=:options, parallel=:parallel, update_tables=:update_tables "
			"where connectionname=:connectionname");
		update.bindValue(0,connectionInformation.driver().alias());
		update.bindValue(1,connectionInformation.syntaxname());
		update.bindValue(2,connectionInformation.hostname());
		update.bindValue(3,connectionInformation.databasename());
		update.bindValue(4,connectionInformation.username());
		update.bindValue(5,crypto.encryptToString(connectionInformation.password()));
		update.bindValue(6,connectionInformation.options());
		update.bindValue(7,connectionInformation.synchronousUse());
		update.bindValue(8,connectionInformation.updateTables());
		update.bindValue(9,connectionInformation.connectionname());
		

		if(update.exec()) {
			if(connectionManager->containsConnection(connectionInformation.connectionname())) {
				//ConnectionInformation tempConnection = connectionManager->getConnection(connectionInformation.connectionname());
				//ConnectionLocker updateLocker(tempConnection);

				AcquiredConnection tempConnection = connectionManager->acquireConnection(connectionInformation.connectionname());
				ConnectionInformation tempConnectionInformation(tempConnection.getConnectionInformation());

				tempConnectionInformation.setDriver(connectionInformation.driver());
				tempConnectionInformation.setSyntaxname(connectionInformation.syntaxname());
				tempConnectionInformation.setHostname(connectionInformation.hostname());
				tempConnectionInformation.setDatabasename(connectionInformation.databasename());
				tempConnectionInformation.setOptions(connectionInformation.options());
				tempConnectionInformation.setUsername(connectionInformation.username());
				tempConnectionInformation.setPassword(connectionInformation.password());
				tempConnectionInformation.setSynchronousUse(connectionInformation.synchronousUse());
				tempConnectionInformation.setUpdateTables(connectionInformation.updateTables());

				connectionManager->getDatabaseElementsUpdater()->updateElements(connectionInformation.connectionname());
				emit databasesUpdated();
			}

			return true;
		} else {
			lastError = update.lastError().text();
		}
	} else {
		lastError = connection->lastError().text();
	}

	return false;
}

bool ApplicationDatabase::saveWorkflow( Workflow* flow )
{
	AcquiredConnection connection(connectionManager->acquireConnection(applicationName));

	if(connection->isOpen()) {
		// update or insert

		if(flow->getUniqueId() != 0) {
			// update core_workflows
			QSqlQuery updateWorkflow(*connection);
			updateWorkflow.prepare("update core_workflows set description=:description, name=:name, forceorder=:forceorder WHERE id=:id");
			updateWorkflow.bindValue(0,flow->getDescription());
			updateWorkflow.bindValue(1,flow->getName());
			updateWorkflow.bindValue(2,flow->getForceOrder());
			updateWorkflow.bindValue(3,flow->getUniqueId());

			if(updateWorkflow.exec()) {
				//delete old entry in workflowlist
				for(int flowIdx=0; flowIdx < workflowManager->workflowCount(); ++ flowIdx) {
					if(workflowManager->getWorkflow(flowIdx)->getUniqueId() == flow->getUniqueId()) {
						workflowManager->removeWorkflow(flowIdx);
						workflowManager->addWorkflow(flow, flowIdx);

						emit workflowsUpdated();
						break;
					}
				}
			} else {
				lastError = updateWorkflow.lastError().text();
				return false;
			}
		} else {
			// insert core_workflows
			QSqlQuery insertWorkflow(*connection);
			insertWorkflow.prepare("insert into core_workflows (name, description, forceorder) values(:name, :description, :forceorder)");
			insertWorkflow.bindValue(0,flow->getName());
			insertWorkflow.bindValue(1,flow->getDescription());
			insertWorkflow.bindValue(2,flow->getForceOrder());
			if(insertWorkflow.exec()) {
				QSqlQuery retrieveId(*connection);
				retrieveId.prepare("select max(id) as maxid from core_workflows where name=:name and description=:description and forceorder=:forceorder");
				retrieveId.bindValue(0,flow->getName());
				retrieveId.bindValue(1,flow->getDescription());
				retrieveId.bindValue(2,flow->getForceOrder());

				if(retrieveId.exec() && retrieveId.next()) {
					flow->setUniqueId(retrieveId.record().value("maxid").toInt());
					workflowManager->addWorkflow(flow);
					emit workflowsUpdated();
				} else {
					lastError = retrieveId.lastError().text();
					return false;
				}
			} else {
				insertWorkflow.clear();
				lastError = insertWorkflow.lastError().text();
				return false;
			}
		}

		removeWorkflowItems(*connection,flow);
		saveWorkflowItems(*connection,flow);

		return true;
	} else {
		lastError = connection->lastError().text();
		return false;
	}
}

bool ApplicationDatabase::saveWorkflowItems( QSqlDatabase& connection, Workflow* flow )
{
	for(int itemIdx=0; itemIdx < flow->getItems().size(); ++itemIdx) {
		connection.transaction();
		QSqlQuery insertWorkflowItem(connection);
		insertWorkflowItem.prepare("insert into core_workflow_item (workflow_id, order_index, description, modulname, modulcontext, inheritversion) values (:workflow_id, :order_index, :description, :modulname, :modulcontext, :inheritversion)");
		insertWorkflowItem.bindValue(0,flow->getUniqueId());
		insertWorkflowItem.bindValue(1,itemIdx);
		insertWorkflowItem.bindValue(2,flow->getItems().at(itemIdx)->getDescription());
		insertWorkflowItem.bindValue(3,flow->getItems().at(itemIdx)->getModulName());
		insertWorkflowItem.bindValue(4,flow->getItems().at(itemIdx)->getModulContext());
		insertWorkflowItem.bindValue(5,flow->getItems().at(itemIdx)->getInheritVersion());

		if(insertWorkflowItem.exec()) {
			QSqlQuery retrieveId(connection);
			retrieveId.prepare("select max(id) as maxid from core_workflow_item");
			if(retrieveId.exec() && retrieveId.next()) {
				flow->getItems().at(itemIdx)->setUniqueId(retrieveId.record().value("maxid").toInt());
				connection.commit();

				QMapIterator<QString, QPair<QString, QString>> i(flow->getItems().at(itemIdx)->getParameterMap());
				while (i.hasNext()) {
					i.next();
					QSqlQuery insertItemParam(connection);
					insertItemParam.prepare("insert into core_workflow_item_params (workflow_item_id,param_alias,param_table,param_connectionname) values (:workflow_item_id,:param_alias,:param_table,:param_connectionname)");
					insertItemParam.bindValue(0,flow->getItems().at(itemIdx)->getUniqueId());
					insertItemParam.bindValue(1,i.key());
					insertItemParam.bindValue(2,i.value().second);
					insertItemParam.bindValue(3,i.value().first);
					insertItemParam.exec();
				}
			} else {
				retrieveId.clear();
				insertWorkflowItem.clear();
				connection.rollback();
				lastError = retrieveId.lastError().text();
				return false;
			}
		} else {
			lastError = insertWorkflowItem.lastError().text();
			return false;
		}
	}
	return true;
}

bool ApplicationDatabase::removeWorkflow( Workflow* flow )
{
	AcquiredConnection connection(connectionManager->acquireConnection(applicationName));

	if(connection->isOpen()) {
		// remove items first
		removeWorkflowItems(*connection, flow);

		// remove vom db
		QSqlQuery cleanup(*connection);
		cleanup.prepare("delete * from core_workflows where id=:id");
		cleanup.bindValue(0,flow->getUniqueId());
		if(cleanup.exec()) {
			//remove from list
			workflowManager->removeWorkflow(flow);

			emit workflowsUpdated();
			return true;
		} else {
			lastError = cleanup.lastError().text();
		}
	} else {
		lastError = connection->lastError().text();
	}

	return false;
}

bool ApplicationDatabase::removeWorkflowItems( QSqlDatabase& connection, Workflow* flow )
{
	//clear core_workflow_item & core_workflow_item_params
	QSqlQuery cleanup(connection);
	cleanup.prepare("delete core_workflow_item_params.* from core_workflow_item_params,core_workflow_item where workflow_item_id=core_workflow_item.id and core_workflow_item.workflow_id=:id");
	cleanup.bindValue(0,flow->getUniqueId());
	bool result = cleanup.exec();

	cleanup.prepare("delete from core_workflow_item where workflow_id=:id");
	cleanup.bindValue(0,flow->getUniqueId());

	return result && cleanup.exec();
}

bool ApplicationDatabase::loadWorkflows(ConnectionInformation connection)
{
	AcquiredConnection dbConnection(connection);

	if(dbConnection->isOpen()) {
		QSqlQuery workflows(*dbConnection);
		if(workflows.exec("select * from core_workflows")) {
			workflowManager->clearWorkflowList();

			while(workflows.next()) {
				//add workflow
				Workflow* tempFlow = new Workflow(workflows.record().value("id").toInt(),workflows.record().value("name").toString(),workflows.record().value("description").toString(),workflows.record().value("forceorder").toBool() );

				//add items
				loadWorkflowItems(*dbConnection ,tempFlow);

				//add to list
				workflowManager->addWorkflow(tempFlow);
			}

			emit workflowsUpdated();
			return true;
		} else {
			lastError = workflows.lastError().text();
		}
	} else {
		lastError = dbConnection->lastError().text();
	}

	return false;
}

bool ApplicationDatabase::loadWorkflowItems( QSqlDatabase& connection, Workflow* flow )
{
	// get workflow items
	QSqlQuery workflowItems(connection);
	workflowItems.prepare("select * from core_workflow_item where workflow_id=:workflow_id order by order_index");
	workflowItems.bindValue(0,flow->getUniqueId());

	if(workflowItems.exec()) {
		while(workflowItems.next()) {
			WorkflowItem* tempItem = new WorkflowItem(workflowItems.record().value("id").toInt(),workflowItems.record().value("modulname").toString());
			tempItem->setDescription(workflowItems.record().value("description").toString());
			tempItem->setModulContext(workflowItems.record().value("modulcontext").toString());
			tempItem->setInheritVersion(workflowItems.record().value("inheritversion").toBool());

			//get workflow item paramsm
			QSqlQuery modulParams(connection);
			modulParams.prepare("select * from core_workflow_item_params where workflow_item_id=:workflow_item_id");
			modulParams.bindValue(0,workflowItems.record().value("id").toInt());

			if(modulParams.exec()) {
				while(modulParams.next()) {
					if(connectionManager->containsConnection(modulParams.record().value("param_connectionname").toString())) {
						tempItem->getParameterMap()[modulParams.record().value("param_alias").toString()]=
							QPair<QString, QString>(modulParams.record().value("param_connectionname").toString(),modulParams.record().value("param_table").toString());
					}
				}
			}
			flow->getItems().push_back(tempItem);
		}
		return true;
	} else {
		lastError = workflowItems.lastError().text();
		return false;
	}
}

bool ApplicationDatabase::updateTables( const QString& connectionname, const QStringList& tables )
{
	AcquiredConnection connection(connectionManager->acquireConnection(applicationName));

	if(connection->isOpen()) {
		QSqlQuery deleteTables(*connection);
		deleteTables.prepare("delete * from core_tables where connectionname=?");
		deleteTables.bindValue(0, connectionname);
		if(deleteTables.exec()) {
			deleteTables.finish();
			connection->transaction();
			QSqlQuery insertTables(*connection);
			insertTables.prepare("insert into core_tables (connectionname, tablename) VALUES (?,?)");
			insertTables.bindValue(0, connectionname);

			for(int i=0; i < tables.count(); ++i) {
				insertTables.bindValue(1, tables[i]);
	
				if(!insertTables.exec()) {
					lastError = "Konnte Tabellen nicht aktuallisieren." + insertTables.lastError().text();
					connection->rollback();
					return false;
				}
			}
			connection->commit();
			return true;
		} else {
			lastError = "Konnte Tabellen nicht l�schen." + deleteTables.lastError().text();
			return false;
		}
	} else {
		lastError = connection->lastError().text();
		return false;
	}
}

QStringList ApplicationDatabase::getTables( const QString& connectionname )
{
	AcquiredConnection connection(connectionManager->acquireConnection(applicationName));
	QStringList result;

	if(connection->isOpen()) {
		QSqlQuery tables(*connection);
		tables.setForwardOnly(true);
		tables.prepare("select tablename from core_tables where connectionname=?");
		tables.bindValue(0, connectionname);
		if(tables.exec()) {
			while(tables.next()) {
				result<<tables.value(0).toString();
			}

			return result;
		} else {
			lastError = "Konnte Tabellen nicht laden." + tables.lastError().text();
		}
	} else {
		lastError = connection->lastError().text();
	}

	return result;
}

QString ApplicationDatabase::getApplicationName()
{
	return applicationName;
}
