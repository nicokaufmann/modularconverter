#include "syntaxnodes.h"

StatementNode::StatementNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "statement";
	this->childLimit = -1;
}

StatementNode* StatementNode::clone()
{
	return new StatementNode(*this);
}

bool StatementNode::accepts( SyntaxNode* node )
{
	return node->type()=="select" || node->type()=="from" || node->type()=="where" || node->type()=="insert" || node->type()=="update" ||
		   node->type()=="group" || node->type()=="order" || node->type()=="into" || node->type()=="delete" || node->type()=="set" || node->type()=="values";
}

QString StatementNode::toString()
{
	QString result("");
	for(int i=0; i < childNodes.size()-1;++i) {
		result+=childNodes[i]->toString()+" ";
	}
	result+=childString(childNodes.size()-1);
	return result;
}

QString StatementNode::toStringFormat( int currentLevel )
{
	QString result("");
	for(int i=0; i < childNodes.size();++i) {
		if(i > 0) {
			result.append(indentation(currentLevel));
		}
		result.append(childNodes[i]->toStringFormat(currentLevel)+"\n");
	}
	
	return result;
}

SelectNode::SelectNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "select";
	this->childLimit = -1;
}

SelectNode* SelectNode::clone()
{
	return new SelectNode(*this);
}

bool SelectNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" || node->type()=="set" || node->type()=="update" ||
			 node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString SelectNode::toString()
{
	QString result("SELECT ");
	for(int i=0; i < childNodes.size();++i) {
		result+=childNodes[i]->toString();
		
		if(i < childNodes.size()-1 && childNodes[i]->type() != "distinct" && childNodes[i]->type() != "top") {	
			result += ",";
		} else {
			result += " ";
		}
	}
	return result;
}

QString SelectNode::toStringFormat( int currentLevel )
{
	QString result("SELECT\n");
	for(int i=0; i < childNodes.size();++i) {
		result.append(indentation(currentLevel+1)+childStringFormat(i,currentLevel+1));
		if(i < childNodes.size()-1) {
			if(childNodes[i]->type() != "distinct" && childNodes[i]->type() != "top") {
				result.append(",\n");
			} else {
				result.append("\n");
			}
		}
	}

	return result;
}


bool SelectNode::needsIntermediate()
{
	return true;
}

SyntaxNode* SelectNode::intermediate()
{
	if(intermediateNode == NULL) {
		intermediateNode = new StatementNode(NULL,"");
		intermediateNode->add(this);
	}
	return intermediateNode;
}

QString SelectNode::intermediateType()
{
	return "statement";
}

DistinctNode::DistinctNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "distinct";
	this->childLimit = 0;
}

DistinctNode* DistinctNode::clone()
{
	return new DistinctNode(*this);
}

bool DistinctNode::accepts( SyntaxNode* node )
{
	return false;
}

QString DistinctNode::toString()
{
	return "DISTINCT ";
}

QString DistinctNode::toStringFormat( int currentLevel )
{
	return "DISTINCT ";
}

TopNode::TopNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "top";
	this->childLimit = 1;
}

TopNode* TopNode::clone()
{
	return new TopNode(*this);
}

bool TopNode::accepts( SyntaxNode* node )
{
	return node->type()=="value";
}

QString TopNode::toString()
{
	return nodeValue + " " + childString(0);
}

QString TopNode::toStringFormat( int currentLevel )
{
	return nodeValue + " " + childString(0);
}

SetNode::SetNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "set";
	this->childLimit = -1;
}

SetNode* SetNode::clone()
{
	return new SetNode(*this);
}

bool SetNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" || node->type()=="set" ||
		node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order" || node->type()=="update");
}

QString SetNode::toString()
{
	QString result("SET ");
	for(int i=0; i < childNodes.size();++i) {
		result+=childNodes[i]->toString();

		if(i < childNodes.size()-1) {	
			result += ",";
		}
	}
	return result;
}

QString SetNode::toStringFormat( int currentLevel )
{
	QString result("SET\n");
	for(int i=0; i < childNodes.size();++i) {
		result.append(indentation(currentLevel+1)+childStringFormat(i,currentLevel+1));
		if(i < childNodes.size()-1) {
			result.append(",\n");
		}
	}

	return result;
}


bool SetNode::needsIntermediate()
{
	return true;
}

SyntaxNode* SetNode::intermediate()
{
	if(intermediateNode == NULL) {
		intermediateNode = new StatementNode(NULL,"");
		intermediateNode->add(this);
	}
	return intermediateNode;
}

QString SetNode::intermediateType()
{
	return "statement";
}


DeleteNode::DeleteNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "delete";
	this->childLimit = -1;
}

DeleteNode* DeleteNode::clone()
{
	return new DeleteNode(*this);
}

bool DeleteNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" || node->type()=="set" ||
		node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order" || node->type()=="update");
}

bool DeleteNode::needsIntermediate()
{
	return true;
}

SyntaxNode* DeleteNode::intermediate()
{
	if(intermediateNode == NULL) {
		intermediateNode = new StatementNode(NULL,"");
		intermediateNode->add(this);
	}
	return intermediateNode;
}

QString DeleteNode::intermediateType()
{
	return "statement";
}

QString DeleteNode::toString()
{
	QString result("DELETE ");
	for(int i=0; i < childNodes.size();++i) {
		result+=childNodes[i]->toString();

		if(i < childNodes.size()-1) {	
			result += ",";
		}
	}
	return result;
}

QString DeleteNode::toStringFormat( int currentLevel )
{
	QString result("DELETE\n");
	for(int i=0; i < childNodes.size();++i) {
		result.append(indentation(currentLevel+1)+childStringFormat(i,currentLevel+1));
		if(i < childNodes.size()-1) {
			result.append(",\n");
		}
	}

	return result;
}

WhereNode::WhereNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "where";
	this->childLimit = 1;
}

WhereNode* WhereNode::clone()
{
	return new WhereNode(*this);
}

bool WhereNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" ||
			 node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString WhereNode::toString()
{
	return "WHERE " + childString(0);
}

QString WhereNode::toStringFormat( int currentLevel )
{
	QString result("WHERE\n");
	result.append(indentation(currentLevel+1) + childStringFormat(0,currentLevel+1));
	return result;
}


bool WhereNode::needsIntermediate()
{
	return true;
}

SyntaxNode* WhereNode::intermediate()
{
	if(intermediateNode == NULL) {
		intermediateNode = new StatementNode(NULL,"");
		intermediateNode->add(this);
	}
	return intermediateNode;
}

QString WhereNode::intermediateType()
{
	return "statement";
}

FromNode::FromNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "from";
	this->childLimit = -1;
}

FromNode* FromNode::clone()
{
	return new FromNode(*this);
}

bool FromNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" ||
			 node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString FromNode::toString()
{
	QString result("FROM ");
	for(int i=0; i < childNodes.size()-1;++i) {
		result+=childNodes[i]->toString()+", ";
	}
	result+=childString(childNodes.size()-1);

	return result;
}

QString FromNode::toStringFormat( int currentLevel )
{
	QString result("FROM\n");

	for(int i=0; i < childNodes.size();++i) {
		result.append(indentation(currentLevel+1)+childStringFormat(i,currentLevel+1));
		if(i < childNodes.size()-1) {
			result.append(",");
		}
	}

	return result;
}

bool FromNode::needsIntermediate()
{
	return true;
}

SyntaxNode* FromNode::intermediate()
{
	if(intermediateNode == NULL) {
		intermediateNode = new StatementNode(NULL,"");
		intermediateNode->add(this);
	}
	return intermediateNode;
}

QString FromNode::intermediateType()
{
	return "statement";
}

UpdateNode::UpdateNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "update";
	this->childLimit = -1;
}

UpdateNode* UpdateNode::clone()
{
	return new UpdateNode(*this);
}

bool UpdateNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" || node->type()=="set" ||
		node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString UpdateNode::toString()
{
	QString result("UPDATE ");
	for(int i=0; i < childNodes.size()-1;++i) {
		result+=childNodes[i]->toString()+", ";
	}
	result+=childString(childNodes.size()-1);

	return result;
}

QString UpdateNode::toStringFormat( int currentLevel )
{
	QString result("UPDATE\n");

	for(int i=0; i < childNodes.size();++i) {
		result.append(indentation(currentLevel+1)+childStringFormat(i,currentLevel+1));
		if(i < childNodes.size()-1) {
			result.append(",\n");
		}
	}

	return result;
}

bool UpdateNode::needsIntermediate()
{
	return true;
}

SyntaxNode* UpdateNode::intermediate()
{
	if(intermediateNode == NULL) {
		intermediateNode = new StatementNode(NULL,"");
		intermediateNode->add(this);
	}
	return intermediateNode;
}

QString UpdateNode::intermediateType()
{
	return "statement";
}

InsertIntoNode::InsertIntoNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "insert";
	this->childLimit = -1;
}

InsertIntoNode* InsertIntoNode::clone()
{
	return new InsertIntoNode(*this);
}

bool InsertIntoNode::accepts( SyntaxNode* node )
{
	return node->type()=="field" || node->type()=="table" || node->type()=="brace" || node->type()=="select";
}

QString InsertIntoNode::toString()
{
	QString result("INSERT INTO ");
	for(int i=0; i < childNodes.size()-1;++i) {
		result+=childNodes[i]->toString()+" ";
	}
	result+=childString(childNodes.size()-1);

	return result;
}

QString InsertIntoNode::toStringFormat( int currentLevel )
{
	QString result("INSERT INTO\n");

	for(int i=0; i < childNodes.size();++i) {
		result.append(indentation(currentLevel+1)+childStringFormat(i,currentLevel+1));
		if(i < childNodes.size()-1) {
			result.append(" \n");
		}
	}

	return result;
}

bool InsertIntoNode::needsIntermediate()
{
	return true;
}

SyntaxNode* InsertIntoNode::intermediate()
{
	if(intermediateNode == NULL) {
		intermediateNode = new StatementNode(NULL,"");
		intermediateNode->add(this);
	}
	return intermediateNode;
}

QString InsertIntoNode::intermediateType()
{
	return "statement";
}

ValuesNode::ValuesNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "values";
	this->childLimit = 1;
}

ValuesNode* ValuesNode::clone()
{
	return new ValuesNode(*this);
}

bool ValuesNode::accepts( SyntaxNode* node )
{
	return node->type()=="brace";
}

QString ValuesNode::toString()
{
	return "VALUES " + childString(0);
}

QString ValuesNode::toStringFormat( int currentLevel )
{
	return "VALUES\n" + indentation(currentLevel+1)+childStringFormat(0,currentLevel+1);
}

bool ValuesNode::needsIntermediate()
{
	return true;
}

SyntaxNode* ValuesNode::intermediate()
{
	if(intermediateNode == NULL) {
		intermediateNode = new StatementNode(NULL,"");
		intermediateNode->add(this);
	}
	return intermediateNode;
}

QString ValuesNode::intermediateType()
{
	return "statement";
}


IntoNode::IntoNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->slotNames<<"target table"<<"in";
	this->usePreviousNode = false;
	this->nodeType = "into";
	this->childLimit = 2;
}

IntoNode* IntoNode::clone()
{
	return new IntoNode(*this);
}

bool IntoNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" ||
			 node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString IntoNode::toString()
{
	QString result("INTO ");
	for(int i=0; i < childNodes.size()-1;++i) {
		result+=childNodes[i]->toString()+", ";
	}
	result+=childString(childNodes.size()-1);

	return result;
}

QString IntoNode::toStringFormat( int currentLevel )
{
	QString result("INTO\n");
	
	for(int i=0; i < childNodes.size();++i) {
		result.append(indentation(currentLevel+1)+childStringFormat(i,currentLevel+1));
		if(i < childNodes.size()-1) {
			result.append(",\n");
		}
	}

	return result;
}

JoinNode::JoinNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->slotNames<<"left"<<"right"<<"on";
	this->usePreviousNode = true;
	this->nodeType = "join";
	this->childLimit = 3;
}

JoinNode* JoinNode::clone()
{
	return new JoinNode(*this);
}

bool JoinNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" ||
			node->type()=="join" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString JoinNode::toString()
{
	return childString(0) +" " + nodeValue + " " + childString(1) + " " + childString(2);
}

QString JoinNode::toStringFormat( int currentLevel )
{
	QString joinStr;
	indentation(currentLevel, joinStr);
	joinStr.append(nodeValue);

	return childStringFormat(0,currentLevel) +"\n" + joinStr + "\n" + indentation(currentLevel) + childStringFormat(1,currentLevel) + " " + childStringFormat(2,currentLevel);
}

UnionNode::UnionNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = true;
	this->nodeType = "union";
	this->childLimit = 2;
}

UnionNode* UnionNode::clone()
{
	return new UnionNode(*this);
}

bool UnionNode::accepts( SyntaxNode* node )
{
	return true;
}

QString UnionNode::toString()
{
	return childString(0) +" " + nodeValue + " " + childString(1);
}

QString UnionNode::toStringFormat( int currentLevel )
{
	QString unionStr;
	indentation(currentLevel, unionStr);
	unionStr.append(nodeValue);

	return childStringFormat(0,currentLevel+1) +"\n" + unionStr + "\n" + indentation(currentLevel+1) + childStringFormat(1,currentLevel+1);
}

GroupNode::GroupNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "group";
	this->childLimit = -1;
}

GroupNode* GroupNode::clone()
{
	return new GroupNode(*this);
}

bool GroupNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" ||
		node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString GroupNode::toString()
{
	QString result("GROUP BY ");
	for(int i=0; i < childNodes.size()-1;++i) {
		result+=childNodes[i]->toString()+", ";
	}
	result+=childString(childNodes.size()-1);

	return result;
}

QString GroupNode::toStringFormat( int currentLevel )
{
	QString result("GROUP BY ");

	for(int i=0; i < childNodes.size();++i) {
		result.append(childStringFormat(i,currentLevel));
		if(i < childNodes.size()-1) {
			result.append(",");
		}
	}
	return result;
}

bool GroupNode::needsIntermediate()
{
	return true;
}

SyntaxNode* GroupNode::intermediate()
{
	if(intermediateNode == NULL) {
		intermediateNode = new StatementNode(NULL,"");
		intermediateNode->add(this);
	}
	return intermediateNode;
}

QString GroupNode::intermediateType()
{
	return "statement";
}

OrderNode::OrderNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "order";
	this->childLimit = -1;
}

OrderNode* OrderNode::clone()
{
	return new OrderNode(*this);
}

bool OrderNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" ||
		node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString OrderNode::toString()
{
	QString result("ORDER BY ");
	for(int i=0; i < childNodes.size()-1;++i) {
		result+=childString(i);
		if(childNodes[i+1]->type()=="order mod") {
			result+=" ";
		} else {
			result+=", ";
		}
	}
	result+=childString(childNodes.size()-1);

	return result;
}

QString OrderNode::toStringFormat( int currentLevel )
{
	QString result("ORDER BY ");

	for(int i=0; i < childNodes.size()-1;++i) {
		result+=childString(i);
		if(childNodes[i+1]->type()=="order mod") {
			result+=" ";
		} else {
			result+=", ";
		}
	}
	result+=childString(childNodes.size()-1);

	return result;
}

bool OrderNode::needsIntermediate()
{
	return true;
}

SyntaxNode* OrderNode::intermediate()
{
	if(intermediateNode == NULL) {
		intermediateNode = new StatementNode(NULL,"");
		intermediateNode->add(this);
	}
	return intermediateNode;
}

QString OrderNode::intermediateType()
{
	return "statement";
}

OrderModifierNode::OrderModifierNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->editable = true;
	this->usePreviousNode = false;
	this->nodeType = "order mod";
	this->childLimit = 0;
}

OrderModifierNode* OrderModifierNode::clone()
{
	return new OrderModifierNode(*this);
}

bool OrderModifierNode::accepts( SyntaxNode* node )
{
	return false;
}

QString OrderModifierNode::toString()
{
	return this->nodeValue;
}

QString OrderModifierNode::toStringFormat( int currentLevel )
{
	return this->nodeValue;
}

FieldNode::FieldNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val) , beginDelimiter("`"), endDelimiter("`")
{
	this->editable = true;
	this->usePreviousNode = false;
	this->nodeType = "field";
	this->childLimit = 0;
}

FieldNode::FieldNode( SyntaxNode* parent, QString val, QString delimiter )
	: SyntaxNode(parent, val) , beginDelimiter(delimiter), endDelimiter(delimiter)
{
	this->editable = true;
	this->usePreviousNode = false;
	this->nodeType = "field";
	this->childLimit = 0;
}

FieldNode::FieldNode( SyntaxNode* parent, QString val, QString beginDelimiter, QString endDelimiter )
	: SyntaxNode(parent, val) , beginDelimiter(beginDelimiter), endDelimiter(endDelimiter)
{
	this->editable = true;
	this->usePreviousNode = false;
	this->nodeType = "field";
	this->childLimit = 0;
}

FieldNode* FieldNode::clone()
{
	return new FieldNode(*this);
}

void FieldNode::setDelimiter( QString delimiter )
{
	this->beginDelimiter = delimiter;
	this->endDelimiter = delimiter;
}

void FieldNode::setDelimiter( QString beginDelimiter, QString endDelimiter )
{
	this->beginDelimiter = beginDelimiter;
	this->endDelimiter = endDelimiter;
}

bool FieldNode::accepts( SyntaxNode* node )
{
	return false;
}

QString FieldNode::toString()
{
	if(nodeValue.contains(QRegExp("[ \\/+-]"))) {
		return beginDelimiter + nodeValue + endDelimiter;
	} else {
		return nodeValue;
	}
}

QString FieldNode::toStringFormat( int currentLevel )
{
	if(nodeValue.contains(QRegExp("[ \\/+-]"))) {
		return beginDelimiter + nodeValue + endDelimiter;
	} else {
		return nodeValue;
	}
}

TableNode::TableNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val), beginDelimiter("`"), endDelimiter("`")
{
	this->editable = true;
	this->usePreviousNode = false;
	this->nodeType = "table";
	this->childLimit = 0;
}

TableNode::TableNode( SyntaxNode* parent, QString val, QString delimiter )
	: SyntaxNode(parent, val), beginDelimiter(delimiter), endDelimiter(delimiter)
{
	this->editable = true;
	this->usePreviousNode = false;
	this->nodeType = "table";
	this->childLimit = 0;
}

TableNode::TableNode( SyntaxNode* parent, QString val, QString beginDelimiter, QString endDelimiter )
	: SyntaxNode(parent, val), beginDelimiter(beginDelimiter), endDelimiter(endDelimiter)
{
	this->editable = true;
	this->usePreviousNode = false;
	this->nodeType = "table";
	this->childLimit = 0;
}

TableNode* TableNode::clone()
{
	return new TableNode(*this);
}

bool TableNode::accepts( SyntaxNode* node )
{
	return false;
}

QString TableNode::toString()
{
	if(nodeValue.contains(QRegExp("[ \\+/-]"))) {
		return beginDelimiter + nodeValue + endDelimiter;
	} else {
		return nodeValue;
	}
}

QString TableNode::toStringFormat( int currentLevel )
{
	if(nodeValue.contains(QRegExp("[ \\+/-]"))) {
		return beginDelimiter + nodeValue + endDelimiter;
	} else {
		return nodeValue;
	}
}

void TableNode::setDelimiter( QString delimiter )
{
	this->beginDelimiter = delimiter;
	this->endDelimiter = delimiter;
}

void TableNode::setDelimiter( QString beginDelimiter, QString endDelimiter )
{
	this->beginDelimiter = beginDelimiter;
	this->endDelimiter = endDelimiter;
}

TextNode::TextNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val), beginDelimiter("\""), endDelimiter("\"")
{
	this->editable = true;
	this->usePreviousNode = false;
	this->nodeType = "text";
	this->childLimit = 0;
}

TextNode::TextNode( SyntaxNode* parent, QString val, QString delimiter )
	: SyntaxNode(parent, val), beginDelimiter(delimiter), endDelimiter(delimiter)
{
	this->editable = true;
	this->usePreviousNode = false;
	this->nodeType = "text";
	this->childLimit = 0;
}

TextNode* TextNode::clone()
{
	return new TextNode(*this);
}

bool TextNode::accepts( SyntaxNode* node )
{
	return false;
}

QString TextNode::toString()
{
	return beginDelimiter+nodeValue+endDelimiter;
}

QString TextNode::toStringFormat( int currentLevel )
{
	return beginDelimiter+nodeValue+endDelimiter;
}

void TextNode::setDelimiter( QString delimiter )
{
	this->beginDelimiter = delimiter;
	this->endDelimiter = delimiter;
}

ValueNode::ValueNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->editable = true;
	this->usePreviousNode = false;
	this->nodeType = "value";
	this->childLimit = 0;
}

ValueNode* ValueNode::clone()
{
	return new ValueNode(*this);
}

bool ValueNode::accepts( SyntaxNode* node )
{
	return false;
}

QString ValueNode::toString()
{
	return nodeValue;
}

QString ValueNode::toStringFormat( int currentLevel )
{
	return nodeValue;
}

AliasNode::AliasNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->slotNames<<"field"<<"name";
	this->usePreviousNode = true;
	this->nodeType = "alias";
	this->childLimit = 2;
}

AliasNode* AliasNode::clone()
{
	return new AliasNode(*this);
}

bool AliasNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" ||
		node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString AliasNode::toString()
{
	return childString(0) + " AS " + childString(1);
}

QString AliasNode::toStringFormat( int currentLevel )
{
	return childStringFormat(0,currentLevel) + " AS " + childStringFormat(1,currentLevel);
}

LinkNode::LinkNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = true;
	this->nodeType = "link";
	this->childLimit = 2;
}

LinkNode* LinkNode::clone()
{
	return new LinkNode(*this);
}

bool LinkNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" || node->type()=="comparison" || node->type()=="logical" || 
		node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString LinkNode::toString()
{
	return childString(0) + "." + childString(1);
}

QString LinkNode::toStringFormat( int currentLevel )
{
	return childStringFormat(0,currentLevel) + "." + childStringFormat(1,currentLevel);
}

BraceNode::BraceNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "brace";
	this->childLimit = -1;
}

BraceNode* BraceNode::clone()
{
	return new BraceNode(*this);
}

bool BraceNode::accepts( SyntaxNode* node )
{
	return true;
}

QString BraceNode::toString()
{
	QString result("(");
	for(int i=0; i < childNodes.size()-1;++i) {
		result+=childNodes[i]->toString()+", ";
	}
	result+=childString(childNodes.size()-1)+")";

	return result;
}

QString BraceNode::toStringFormat( int currentLevel )
{
	QString result("(\n");

	for(int i=0; i < childNodes.size();++i) {
		result.append(indentation(currentLevel+1) + childStringFormat(i, currentLevel+1));
		if(i < childNodes.size()-1) {
			result.append(",\n");
		}
	}
	//result.append(")");
	result.append("\n" + indentation(currentLevel)+")");
	return result;
}

void BraceNode::close()
{
	this->childLimit = childNodes.count();
}

NegateNode::NegateNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "negate";
	this->childLimit = 1;
}

NegateNode* NegateNode::clone()
{
	return new NegateNode(*this);
}

bool NegateNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" ||
		node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString NegateNode::toString()
{
	return "NOT " + childString(0);
}

QString NegateNode::toStringFormat( int currentLevel )
{
	return "NOT " + childStringFormat(0,currentLevel);
}

LogicalNode::LogicalNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->editable = true;
	this->usePreviousNode = true;
	this->nodeType = "logical";
	this->childLimit = 2;
}

LogicalNode* LogicalNode::clone()
{
	return new LogicalNode(*this);
}

bool LogicalNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" ||
		node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString LogicalNode::toString()
{
	return childString(0) + " " + nodeValue + " " + childString(1);
}

QString LogicalNode::toStringFormat( int currentLevel )
{
	if(nodeValue.contains("and", Qt::CaseInsensitive) || nodeValue.contains("or", Qt::CaseInsensitive)) {
		return childStringFormat(0, currentLevel) + " " + nodeValue + "\n" + indentation(currentLevel) + childStringFormat(1, currentLevel);
	} else {
		return childStringFormat(0, currentLevel) + " " + nodeValue + " " + childStringFormat(1, currentLevel);
	}
}

ComparisonNode::ComparisonNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->editable = true;
	this->usePreviousNode = true;
	this->nodeType = "comparison";
	this->childLimit = 2;
}

ComparisonNode* ComparisonNode::clone()
{
	return new ComparisonNode(*this);
}

bool ComparisonNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" || node->type()=="logical" || 
		node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString ComparisonNode::toString()
{
	return childString(0) + " " + nodeValue + " " + childString(1);
}

QString ComparisonNode::toStringFormat( int currentLevel )
{
	return childStringFormat(0, currentLevel) + " " + nodeValue + " " + childStringFormat(1, currentLevel);
}

BetweenNode::BetweenNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "between";
	this->childLimit = 1;
}

BetweenNode* BetweenNode::clone()
{
	return new BetweenNode(*this);
}

bool BetweenNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" ||
		node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString BetweenNode::toString()
{
	return "BETWEEN " + childString(0);
}

QString BetweenNode::toStringFormat( int currentLevel )
{
	return "BETWEEN " + childStringFormat(0,currentLevel);
}

InNode::InNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = true;
	this->nodeType = "in";
	this->childLimit = 2;
}

InNode* InNode::clone()
{
	return new InNode(*this);
}

bool InNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" ||
		node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString InNode::toString()
{
	return childString(0) + " IN " + childString(1);
}

QString InNode::toStringFormat( int currentLevel )
{
	return childStringFormat(0,currentLevel) + " IN " + childStringFormat(1,currentLevel);
}

OperatorNode::OperatorNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->editable = true;
	this->usePreviousNode = true;
	this->nodeType = "operator";
	this->childLimit = 2;
}

OperatorNode* OperatorNode::clone()
{
	return new OperatorNode(*this);
}

bool OperatorNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" ||
		node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString OperatorNode::toString()
{
	return childString(0) + nodeValue + childString(1);
}

QString OperatorNode::toStringFormat( int currentLevel )
{
	return childStringFormat(0, currentLevel) + nodeValue + childStringFormat(1, currentLevel);
}

OnNode::OnNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->usePreviousNode = false;
	this->nodeType = "on";
	this->childLimit = 1;
}

OnNode* OnNode::clone()
{
	return new OnNode(*this);
}

bool OnNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="select" || node->type()=="where" || node->type()=="from" || node->type()=="into" ||
			 node->type()=="join" || node->type()=="on" || node->type()=="union" || node->type()=="group" || node->type()=="order");
}

QString OnNode::toString()
{
	return "ON " + childString(0);
}

QString OnNode::toStringFormat( int currentLevel )
{
	return "ON " + childStringFormat(0, currentLevel);
}

FunctionNode::FunctionNode( SyntaxNode* parent, QString val )
	: SyntaxNode(parent, val)
{
	this->editable = true;
	this->usePreviousNode = false;
	this->nodeType = "function";
	this->childLimit = -1;
}

FunctionNode::FunctionNode( SyntaxNode* parent, QString val, QStringList& paramList )
	: SyntaxNode(parent, val)
{
	this->slotNames+=paramList;
	this->usePreviousNode = false;
	this->nodeType = "function";
	this->childLimit = paramList.size();
}

FunctionNode* FunctionNode::clone()
{
	return new FunctionNode(*this);
}

bool FunctionNode::accepts( SyntaxNode* node )
{
	return !(node->type()=="where" || node->type()=="from" || node->type()=="into" ||
			 node->type()=="on" || node->type()=="group" || node->type()=="order");
}

QString FunctionNode::toString()
{
	QString result = nodeValue + "(";

	for(int i=0; i < childNodes.size();++i) {
		result+=childNodes[i]->toString() + ",";
	}
	result.chop(1);
	return result+")";
}

QString FunctionNode::toStringFormat( int currentLevel )
{
	QString result(nodeValue + "( ");

	if(childNodes.count() > 1) {
		result.append("\n");
	}

	for(int i=0; i < childNodes.size();++i) {
		if(childNodes.count() > 1) {
			result.append(indentation(currentLevel+1) + childStringFormat(i, currentLevel+1));
			if(i < childNodes.size()-1) {
				result.append(",\n");
			}
		} else {
			result.append(childStringFormat(i, currentLevel+1));
		}
	}
	result.append(" )");

	return result;
}