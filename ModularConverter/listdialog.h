#ifndef listdialog_h__
#define listdialog_h__

#include <QDialog>

#include "ui_listdialog.h"

class ListDialog : public QDialog
{
	Q_OBJECT

public:
	ListDialog(const QString& title, const QStringList& list,QWidget *parent = 0, Qt::WFlags flags = 0);
	~ListDialog();

	void setList(const QStringList& list);
public slots:

private:
	Ui::ListDialog ui;
};
#endif // listdialog_h__