#include "basepathmodul.h"

BasePathModul::BasePathModul( ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion )
	: ConverterModul(parameterMap, versionMap, connectionManager, context, inheritVersion)
{

}

void BasePathModul::setPath( QString path )
{
	exportPath = path;
}