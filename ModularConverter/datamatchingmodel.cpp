#include "datamatchingmodel.h"
#include "sqlquerymodel.h"

DataMatchingModel::DataMatchingModel(QStringList originalData, QObject *parent)
	: QAbstractItemModel(parent), originalData(originalData), columns(0), rows(0)
{
	for(int i=0; i < originalData.count(); ++i) {
		columnHeaders<<originalData[i];
	}
	columns = originalData.count();
}

DataMatchingModel::~DataMatchingModel()
{
}


Qt::ItemFlags DataMatchingModel::flags( const QModelIndex & index ) const
{
	if(index.isValid()) {
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
	}

	return Qt::NoItemFlags;
}

Qt::DropActions DataMatchingModel::supportedDropActions() const
{
	return Qt::IgnoreAction;
}


int DataMatchingModel::rowCount( const QModelIndex & parent ) const
{
	return rows;
}

int DataMatchingModel::columnCount( const QModelIndex & parent ) const
{
	return columns;
}

QVariant DataMatchingModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if(role == Qt::DisplayRole) {
		//section relative to buffer position in
		if(orientation == Qt::Horizontal) {
			if(section < columns) {
				return columnHeaders[section];
			}
		} else if(orientation == Qt::Vertical) {
			if(section < rows) {
				return rowAlias[section];
			} 
		}
	}

	return invalidVariant;
}

QVariant DataMatchingModel::data( const QModelIndex & index, int role ) const
{
	if(role == Qt::DisplayRole || role == Qt::EditRole) {
		if(index.isValid() && index.column() < columns && index.row() < rows) {
			return rowData[index.row()][index.column()];
		}
	}

	return invalidVariant;
}

QMimeData* DataMatchingModel::mimeData( const QModelIndexList & indexes ) const
{
	if(indexes.size() == 0) {
		return 0;
	} else {
		QMimeData* data = new QMimeData;
		QString textData("");
		int itemCount = 0;
		for(int i=0; i < indexes.size(); ++i) {
			if (indexes[i].isValid()) {
				textData += indexes[i].data(Qt::DisplayRole).toString()+"	";
				itemCount++;
			}
		}

		if(itemCount > 0) {
			textData.chop(1);
		}

		data->setText(textData);
		return data;
	}
}



QModelIndex DataMatchingModel::index( int row, int column, const QModelIndex &parent ) const
{
	return createIndex(row, column);
}

QModelIndex DataMatchingModel::parent( const QModelIndex &index ) const
{
	return invalidIndex;
}

bool DataMatchingModel::setData( const QModelIndex & index, const QVariant & value, int role /*= Qt::EditRole */ )
{
	if(role == Qt::EditRole) {
		if(index.isValid() && index.column() < columns && index.row() < rows) {
			rowData[index.row()][index.column()] = value;
			dataChanged(index,index);
			return true;
		}
	}
	return false;
}

void DataMatchingModel::addMatchingRow( QString alias, const QMap<QString, QString>& currentMatching )
{
	rowAlias.append(alias);
	
	beginInsertRows(QModelIndex(),rows,rows);
	rowData.append(QVector<QVariant>(columns));

	const QStringList keys(currentMatching.keys());
	for(int i=0; i < keys.count(); ++i) {
		if(originalData.contains(keys[i])) {
			rowData[rowData.count()-1][originalData.indexOf(keys[i])]=currentMatching[keys[i]];
		}
	}

	rows++;
	endInsertRows();
}

void DataMatchingModel::setMatchingRow( QString alias, const QMap<QString, QString>& currentMatching )
{
	for(int i=0; i < rowAlias.count(); ++i) {
		if(rowAlias[i].toString() == alias) {
			for(int j=0; j < columns; ++j) {
				if(currentMatching.contains(originalData[j])) {
					rowData[i][j] = currentMatching[originalData[j]];
				} else {
					rowData[i][j] = "";
				}

				
			}
			dataChanged(createIndex(i, 0), createIndex(i, columns-1));
			return;
		}
	}
}

QMap<QString, QString> DataMatchingModel::matchingRow( QString alias )
{
	QMap<QString, QString> matching;

	for(int i=0; i < rowAlias.count(); ++i) {
		if(rowAlias[i].toString() == alias) {
			for(int j=0; j < columns; ++j) {
				if(rowData[i][j].toString().length() > 0) {
					matching.insert(columnHeaders[j].toString(), rowData[i][j].toString());
				}
			}
			return matching;
		}
	}

	return matching;
}

void DataMatchingModel::setOriginalData( QStringList originalData )
{
	beginRemoveColumns(QModelIndex(),0,columns);
	endRemoveColumns();

	beginInsertColumns(QModelIndex(),0,originalData.count());
	
	this->originalData = originalData;
	columnHeaders.clear();
	for(int i=0; i < originalData.count(); ++i) {
		columnHeaders<<originalData[i];
	}
	columns = originalData.count();
	
	for(int i=0; i < rowAlias.count(); ++i) {
		rowData[i].resize(originalData.count());
	}


	endInsertColumns();
}
