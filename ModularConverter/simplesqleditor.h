#ifndef simplesqleditor_h__
#define simplesqleditor_h__

#include <QMainWindow>
#include <QMessageBox>

#include "filterwindow.h"
#include "datawindow.h"
#include "statementwidget.h"
#include "ui_simplesqleditor.h"

#include "applicationdatabase.h"

class SimpleSqlEditor : public QMainWindow
{
	Q_OBJECT

public:
	SimpleSqlEditor(ApplicationDatabase* applicationDatabase, QWidget *parent = 0, Qt::WFlags flags = 0);
	~SimpleSqlEditor();

	void setPermissions(ApplicationUser user);

	void addStatement(QString connectionname, QString statement);

public slots:
	void addStatement();
	void closeStatement(int index);
	void checkPlausibility();
	void checkExecution();
	void showSimpleFilter();
	void showQueryData();
	void showOverviewSelected();
	void showOverviewExcluded();
	void closing(DataWindow* window);

private:
	Ui::SimpleSqlEditor ui;
	
	ApplicationDatabase* applicationDatabase;
	ConnectionManager* connectionManager;
	FilterWindow* filterWindow;


	QList<DataWindow*> windowList;

	int tabCounter;
};
#endif // simplesqleditor_h__


