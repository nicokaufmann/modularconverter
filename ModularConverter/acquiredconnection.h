
#ifndef acquiredconnection_h__
#define acquiredconnection_h__

#include <QSqlDatabase>

#include "connectionlocker.h"

class ConnectionManager;

class AcquiredConnection {
public:
	AcquiredConnection();
	AcquiredConnection(ConnectionInformation connectionInformation);
	AcquiredConnection(ConnectionInformation connectionInformation, ConnectionManager* connectionManager);
	AcquiredConnection(const AcquiredConnection& obj);
	AcquiredConnection& operator= (const AcquiredConnection& obj);
	~AcquiredConnection();

	QSqlDatabase* operator->();
	QSqlDatabase& operator*();
	QSqlDatabase* getConnection();

	ConnectionInformation getConnectionInformation();
	int referenceCount() const;

	bool isValid();
	bool isSafe();

private:
	QSqlDatabase connection;

	QSharedPointer<int> referenceCounter;
	QSharedPointer<ConnectionLocker> lock;
	ConnectionInformation connectionInformation;

	ConnectionManager* connectionManager;
	bool valid;
};
#endif // acquiredconnection_h__