
#ifndef floatinglabel_h__
#define floatinglabel_h__

#include <QString>

#include "ui_floatinglabel.h"

class FloatingLabel  : public QWidget
{
	Q_OBJECT

public:
	FloatingLabel(int id, QString labelText, QWidget *parent = 0, Qt::WFlags flags = 0);
	~FloatingLabel();

	int getId();
	QString getLabelText();
	
public slots:
	void removeLabel();

signals:
	void removed(int id);
	void removed(FloatingLabel* label);

private:
	int id;
	QString labelText;

	Ui::FloatingLabel ui;
};
#endif // floatinglabel_h__