#ifndef excelmultifileexecutionmodulinterface_h__
#define excelmultifileexecutionmodulinterface_h__

#include "standardmodulinterface.h"
#include "executemodulinterface.h"

class MandatoryFieldsModul;

class ExcelMultifileExecutionModulInterface : public StandardModulInterface 
{
	Q_OBJECT

public:
	ExcelMultifileExecutionModulInterface(MandatoryFieldsModul* modul, QWidget *parent = 0, Qt::WFlags flags = 0);
	~ExcelMultifileExecutionModulInterface();

	void finished(bool status);

public slots:
	void executeModul();

signals:
	void execute(int cmd=0);

private:
	ExecuteModulInterface* executeInterface;
};
#endif // excelmultifileexecutionmodulinterface_h__