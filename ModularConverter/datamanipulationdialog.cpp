#include "datamanipulationdialog.h"

#include <QSqlQuery>
#include <QSqlError>

DataManipulationDialog::DataManipulationDialog( ConnectionManager* connectionManager, QString connectionname, QString tablename, QWidget *parent /*= 0*/, Qt::WFlags flags /*= 0*/ )
	: QDialog(parent, flags), connectionManager(connectionManager), connectionname(connectionname), tablename(tablename)
{
	ui.setupUi(this);

	ui.dataView->setModel(new DataInsertModel(connectionManager, connectionname, tablename));
	ui.dataView->setItemDelegateForRow(1, &itemDelegate);

	QObject::connect(&watchInsertion, SIGNAL(finished()),this, SLOT(insertionFinished()));
	QObject::connect(ui.bUpdate, SIGNAL(clicked()),this, SLOT(insert()));
}

DataManipulationDialog::~DataManipulationDialog()
{
	if(watchInsertion.future().isRunning()) {
		watchInsertion.waitForFinished();
	}
}

void DataManipulationDialog::insert()
{
	QFuture<bool> future = QtConcurrent::run(this, &DataManipulationDialog::insertion );
	watchInsertion.setFuture(future);

	insertionError.clear();
	ui.bUpdate->setEnabled(false);
}

bool DataManipulationDialog::insertion()
{
	AcquiredConnection connection(connectionManager->acquireConnection(connectionname));


	if(connection->isOpen()) {
		QSqlQuery insertQuery(*connection);

		

		DatabaseElements database(*connection.getConnectionInformation().databaseElements());
		SyntaxElements syntax(*connection.getConnectionInformation().syntaxElements());
		QStringList attributes = database.getAttributes(tablename);
		QString statement = "insert into " + syntax.delimitTablename(tablename) + " (";
		QString values = "(";
	
		for(int i=0; i < attributes.size(); ++i) {
			statement +=  syntax.delimitFieldname(attributes[i])+",";
			values += ":"+QString::number(i)+",";
		}
		statement.chop(1);
		values.chop(1);

		statement+=") values "+values+")";

		insertQuery.prepare(statement);

		DataInsertModel* model = (DataInsertModel*)ui.dataView->model();
		QVector<QVariant> insertValues = model->getInsertData();
		for(int i=0; i < insertValues.size(); ++i) {
			insertQuery.bindValue(i,insertValues[i]);
		}

		if(insertQuery.exec()) {
			return true;
		} else {
			insertionError = insertQuery.lastError().text() + "\n\nQuery:\n" + insertQuery.lastQuery();
		}
	} else {
		insertionError = "Verbindung konnte nicht hergestellt werden.\n" + connection->lastError().text();
	}

	return false;
}

void DataManipulationDialog::insertionFinished()
{
	ui.bUpdate->setEnabled(true);

	if(watchInsertion.future().result()) {
		QMessageBox msgBox;
		msgBox.setWindowTitle("Datensatz eingefügt");
		msgBox.setText("Der Datensatz wurde erfolgreich hinzugefügt");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.exec();
	} else {
		QMessageBox msgBox;
		msgBox.setWindowTitle("Fehler");
		msgBox.setText("Problem beim einfügen des Datensatzes.");
		msgBox.setDetailedText(insertionError);
		msgBox.setIcon(QMessageBox::Critical);
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.exec();
	}
}
