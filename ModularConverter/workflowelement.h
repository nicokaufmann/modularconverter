#ifndef workflowelement_h__
#define workflowelement_h__

#include <QtGui/QWidget>

#include "ui_workflowelement.h"

class WorkflowElement : public QWidget
{
	Q_OBJECT

public:
	WorkflowElement(int index, QString modulText, bool startable, QWidget *parent = 0, Qt::WFlags flags = 0);
	~WorkflowElement();

	void setIndex(int index);
	void setModulText(QString modulText);
	void setStartable(bool startable);
	void setStatus(bool status);

public slots:
	void execute();

signals:
	void execute(int index);

private:
	int index;

	Ui::WorkflowElement ui;
};
#endif // workflowelement_h__