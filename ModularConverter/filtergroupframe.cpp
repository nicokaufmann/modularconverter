#include "filtergroupframe.h"

#include <QMouseEvent>
#include <QStringBuilder>

FilterGroupFrame::FilterGroupFrame( QWidget *parent , Qt::WFlags flags )
	: QWidget(parent,flags), numberCheck("[0-9]+(?:\\.[0-9]*)?"), 
		dateCheckDE("^([0]?[1-9]|[1|2][0-9]|[3][0|1])[.]([0]?[1-9]|[1][0-2])[.]([0-9]{4}|[0-9]{2})$"),
		dateCheckENG("^([0-9]{4}|[0-9]{2})[-]([0]?[1-9]|[1][0-2])[-]([0]?[1-9]|[1|2][0-9]|[3][0|1])$")
{
	ui.setupUi(this);
	ui.filterGrid->setRowMinimumHeight(0,100);
}

FilterGroupFrame::~FilterGroupFrame()
{
	for(int i=0; i < filterList.count(); ++i) {
		delete filterList[i];
	}
	for(int i=0; i < buttonList.count(); ++i) {
		delete buttonList[i];
	}
}

void FilterGroupFrame::setFrameShadow( QFrame::Shadow shadow )
{
	ui.frame->setFrameShadow(shadow);
}

void FilterGroupFrame::addFilter( const QString& fieldname )
{
	ui.filterGrid->setRowMinimumHeight(0,0);
	if(ui.filterGrid->count()%2 > 0) {
		buttonList.push_back(new QPushButton("AND"));
		buttonList.last()->setMaximumHeight(50);
		buttonList.last()->setMaximumWidth(40);
		buttonList.last()->installEventFilter(this);
		ui.filterGrid->addWidget(buttonList.last(),ui.filterGrid->count()/6, ui.filterGrid->count()%6);
	}
	filterList.append(new FloatingFilter(0,fieldname));
	QObject::connect(filterList.last(), SIGNAL(removed(FloatingFilter*)),this, SLOT(removeFilter(FloatingFilter*)));
	ui.filterGrid->addWidget(filterList.last(),ui.filterGrid->count()/6, ui.filterGrid->count()%6);
}

bool FilterGroupFrame::eventFilter( QObject* object, QEvent* event )
{
	if(event->type() == QEvent::MouseButtonRelease) {
		QPushButton* tempButton = dynamic_cast<QPushButton*>(object);
		if(buttonList.contains(tempButton)) {
			if(tempButton->text()!="AND") {
				tempButton->setText("AND");
			} else {
				tempButton->setText("OR");
			}
		}
	}
	return false;
}

void FilterGroupFrame::removeFilter( FloatingFilter* filter )
{
	for(int i=0; i < filterList.count(); ++i) {
		ui.filterGrid->removeWidget(filterList[i]);
	}
	for(int i=0; i < buttonList.count(); ++i) {
		ui.filterGrid->removeWidget(buttonList[i]);
	}
	
	int pos = filterList.indexOf(filter);
	if(pos > -1) {
		QObject::disconnect(filter, SIGNAL(removed(FloatingFilter*)),this, SLOT(removeFilter(FloatingFilter*)));
		delete filterList[pos];
		filterList.removeAt(pos);
		
		if(pos < buttonList.count()) {
			delete buttonList[pos];
			buttonList.removeAt(pos);
		} else if( filterList.count() > 0) {
			delete buttonList[pos-1];
			buttonList.removeAt(pos-1);
		}
	}

	for(int i=0; i < filterList.count(); ++i) {
		ui.filterGrid->addWidget(filterList[i],ui.filterGrid->count()/6, ui.filterGrid->count()%6);
		if(i < buttonList.count()) {
			ui.filterGrid->addWidget(buttonList[i],ui.filterGrid->count()/6, ui.filterGrid->count()%6);
		}
	}

	if(filterList.count() < 1) {
		ui.filterGrid->setRowMinimumHeight(0,100);
		emit empty(this);
	}
}

QString FilterGroupFrame::filter(SyntaxElements* syntaxElements, const QStringList& attributes )
{
	QString filterStr("");

	for(int i=0; i < filterList.count(); ++i) {
		filterStr+=syntaxElements->delimitFieldname( filterList[i]->getFieldname() ) % " " % filterList[i]->getMethod() % " ";
		
		const QString checkString(filterList[i]->getCheckString());
		if(attributes.contains(checkString)) {
			filterStr+=syntaxElements->delimitFieldname(checkString);
		} else if(numberCheck.exactMatch(checkString)) {
			filterStr+=checkString;
		} else if(dateCheckDE.exactMatch(checkString) || dateCheckENG.exactMatch(checkString)) {
			filterStr+=syntaxElements->delimitDate(checkString);
		} else if(filterList[i]->getMethod() == "IN") {
			filterStr+="(" % checkString % ")";
		} else {
			filterStr+=syntaxElements->delimitText(checkString);
		}

		if(i < buttonList.count()) {
			filterStr+= " " % buttonList[i]->text() % " ";
		}
	}

	return filterStr;
}

int FilterGroupFrame::filterCount()
{
	return filterList.count();
}

void FilterGroupFrame::clearFilter()
{
	while(filterList.count() > 0) {
		removeFilter(filterList[0]);
	}
}
