#ifndef syntaxnode_h__
#define syntaxnode_h__

#include <QVector>
#include <QString>
#include <QStringList>

class SyntaxNode {
public:
	SyntaxNode(SyntaxNode* parent, QString val);
	~SyntaxNode();

	void add(SyntaxNode* child);
	void add(SyntaxNode* child, int at);
	bool remove(SyntaxNode* child);
	bool replace(SyntaxNode* child, SyntaxNode* replacement);
	void relocate(SyntaxNode* targetParent);
	void insert(SyntaxNode* targetParent);

	SyntaxNode* parent();
	void setParent(SyntaxNode* parent);

	QVector<SyntaxNode*>& children();
	SyntaxNode* child(int index);
	int index(SyntaxNode* child);
	int count();
	int limit();
	bool hasSpace();
	bool usePrevious();
	bool isEditable();
	
	QString& type();
	QString& value();
	void setValue(const QString& val);

	QString slotName(int index);
	
	virtual bool accepts(SyntaxNode* node);
	virtual bool needsIntermediate();
	virtual SyntaxNode* intermediate();
	virtual QString intermediateType();

	virtual QString toString();
	virtual QString toStringFormat(int currentLevel);

	virtual SyntaxNode* clone();
	SyntaxNode* copy();
	void copyChildren();
protected:
	QString childString(int index);
	QString childStringFormat(int index, int currentLevel);

	void indentation(int level, QString& str);
	QString indentation(int level);


	QString nodeType;
	QString nodeValue;
	SyntaxNode* parentNode;
	QStringList slotNames;
	QVector<SyntaxNode*> childNodes;

	int childLimit;
	bool usePreviousNode;
	bool editable;
	SyntaxNode* intermediateNode;
	const QString indent;
};
#endif // syntaxnode_h__