#ifndef filtergroupframe_h__
#define filtergroupframe_h__

#include <QString>
#include <QStringList>
#include <QList>

#include "syntaxelements.h"
#include "floatingfilter.h"
#include "ui_filtergroupframe.h"

class FilterGroupFrame  : public QWidget
{
	Q_OBJECT

public:
	FilterGroupFrame( QWidget *parent = 0, Qt::WFlags flags = 0);
	~FilterGroupFrame();

	int filterCount();

	void addFilter(const QString& fieldname);
	QString filter(SyntaxElements* syntaxElements, const QStringList& attributes = QStringList());
	
	void clearFilter();
	void setFrameShadow(QFrame::Shadow shadow);
public slots:
	void removeFilter(FloatingFilter* filter);
	bool eventFilter(QObject* object, QEvent* event);
signals:
	void empty(FilterGroupFrame* filterGroup);

private:
	QRegExp numberCheck;
	QRegExp dateCheckDE;
	QRegExp dateCheckENG;

	QList<FloatingFilter*> filterList;
	QList<QPushButton*> buttonList;

	Ui::FilterGroupFrame ui;
};
#endif // filtergroupframe_h__
