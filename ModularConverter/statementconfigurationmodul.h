#ifndef statementconfigurationmodul_h__
#define statementconfigurationmodul_h__

#include <QMessageBox>
#include <QLineEdit>
#include <QColorDialog>
#include <QString>
#include <QMap>
#include <QVariant>

#include "ui_statementconfigurationmodul.h"

class ReportConfigModul;

class StatementConfigurationModul : public QWidget
{
	Q_OBJECT

public:
	StatementConfigurationModul(int id, ReportConfigModul* rcm, QWidget *parent = 0, Qt::WFlags flags = 0);
	~StatementConfigurationModul();

	void setStatementValues(QMap<QString, QVariant>& cfg);
	void getStatementValues(QMap<QString, QVariant>& cfg);
	int getId();



public slots:
	void selectDatabase(int index);
	void removeStatement();
	void changeHeaderFontColor();
	void changeHeaderBgColor();
	void changeValuesFontColor();
	void changeValuesBgColor();
	void refreshView();

	
signals:
	void removeStatement(int id);
private:
	void getCurrentMapping(QMap<QString, QVariant>& mapping);
	void setCurrentMapping(QMap<QString, QVariant>& mapping);
	void getTableFromStatement();

	unsigned int getRgbFromStyle(QString style);

	int id;
	ReportConfigModul* rcm;
	Ui::StatementConfigurationModul ui;
};
#endif // statementconfigurationmodul_h__