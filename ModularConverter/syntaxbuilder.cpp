#include "syntaxbuilder.h"

void SyntaxBuilder::addConjunction( SyntaxNode* whereNode, SyntaxNode* conditionNode )
{
	if(whereNode->count() > 0) {
		SyntaxNode* child = whereNode->children()[0];
		whereNode->remove(child);

		SyntaxNode* conjunction = new LogicalNode(0,"and");
		conjunction->add(new BraceNode(0,""));
		conjunction->add(conditionNode);
		conjunction->children()[0]->add(child);

		whereNode->add(conjunction);
	} else {
		whereNode->add(conditionNode);
	}
}
