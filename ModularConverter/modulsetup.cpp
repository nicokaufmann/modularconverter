#include "modulsetup.h"

ModulSetup::ModulSetup( ConnectionManager* connectionManager, ModulManager* modulManager, int index, QWidget *parent, Qt::WFlags flags )
	: QWidget(parent,flags), connectionManager(connectionManager), modulManager(modulManager), index(index)
{
	ui.setupUi(this);
	ui.cbContext->hide();
	ui.lContext->hide();

	ui.cbModul->addItems(modulManager->modulList());
	
	selected(0);

	QObject::connect(ui.cbModul, SIGNAL(activated(int)), this, SLOT(selected(int)));
	QObject::connect(ui.bAddAfter, SIGNAL(clicked()), this, SLOT(addAfter()));
	QObject::connect(ui.bRemoveCurrent, SIGNAL(clicked()), this, SLOT(removeCurrent()));
}

ModulSetup::~ModulSetup()
{

}

void ModulSetup::setIndex( int index )
{
	this->index=index;
}

void ModulSetup::selected( int index )
{
	if(modulManager->contains(ui.cbModul->itemText(index))) {
		clearItemList();
		QStringList requested = modulManager->requestedParameter(ui.cbModul->itemText(index));
		for(int i=0; i < requested.size(); ++i) {
			ModulSlotSetup* tempSlot = new ModulSlotSetup(connectionManager, requested[i], this);
			QObject::connect(tempSlot, SIGNAL(tableSelected(ModulSlotSetup*)), this, SLOT(tableSelected(ModulSlotSetup*)));
			
			itemList.push_back(tempSlot);
		}

		buildItemListLayout();

		if(modulManager->useContext(ui.cbModul->itemText(index))) {
			ui.cbContext->show();
			ui.cbContext->clear();
			ui.lContext->show();
		} else {
			ui.cbContext->hide();
			ui.lContext->hide();
		}
	}
}

void ModulSetup::clearItemList()
{
	// clear layout
	QLayoutItem *child;
	while ((child = ui.modulSlotList->takeAt(0)) != 0) {
		delete child;
	}

	//clear itemlist
	for(int i=0; i < itemList.size();++i) {
		QObject::disconnect(itemList[i], SIGNAL(tableSelected(ModulSlotSetup*)), this, SLOT(tableSelected(ModulSlotSetup*)));
		delete itemList[i];
	}
	itemList.clear();
}

void ModulSetup::buildItemListLayout()
{
	for(int i=0; i < itemList.size(); ++i) {
		ui.modulSlotList->addWidget(itemList[i]);
	}
}

void ModulSetup::addAfter()
{
	emit addAfter(index+1);
}

void ModulSetup::removeCurrent()
{
	emit removeCurrent(index);
}

ParameterMap ModulSetup::getParameterMap()
{
	ParameterMap pm;
	for(int i=0; i < itemList.size(); ++i) {
		pm[itemList[i]->getAlias()]=itemList[i]->getParameter();
	}
	return pm;
}

QString ModulSetup::getModulDescription()
{
	return ui.leDescription->text();
}

void ModulSetup::setModulDescription( QString description )
{
	ui.leDescription->setText(description);
}

QString ModulSetup::getModulName()
{
	return ui.cbModul->currentText();
}

void ModulSetup::setModulName( QString modulName )
{
	int idx = ui.cbModul->findText(modulName);
	if(idx >= 0) {
		selected(idx);
		ui.cbModul->setCurrentIndex(idx);
	}
}

QString ModulSetup::getModulContext()
{
	return ui.cbContext->currentText();
}

void ModulSetup::setModulContext( QString modulContext )
{
	int idx = ui.cbContext->findText(modulContext);
	if(idx >= 0) {
		ui.cbContext->setCurrentIndex(idx);
	}
}

void ModulSetup::setParameterMap( ParameterMap& params )
{
	for(int i=0; i < itemList.size(); ++i) {
		if(params.contains(itemList[i]->getAlias())) {
			itemList[i]->setParameter(params[itemList[i]->getAlias()]);
		}
	}
}

void ModulSetup::tableSelected( ModulSlotSetup* modulSlot )
{
	if(modulManager->useContext(ui.cbModul->currentText())) {
		QStringList contextList = modulSlot->getContextList();

		if(contextList.size() > 0) {
			ui.cbContext->clear();
			ui.cbContext->addItems(contextList);
		}
	}
}

bool ModulSetup::getModulInheritVersion()
{
	return ui.cbInheritVersion->isChecked();
}

void ModulSetup::setModulInheritVersion( bool inheritVersion )
{
	ui.cbInheritVersion->setChecked(inheritVersion);
}




