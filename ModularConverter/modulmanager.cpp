#include "modulmanager.h"

#include "excelimportmodul.h"
#include "excelusermappedimportmodul.h"
#include "excelusermappedimportnoversionmodul.h"
#include "excelexportmodul.h"
#include "excelreportmodul.h"
#include "singlestatementmodul.h"
#include "singlestatementversionmodul.h"
#include "reportconfigmodul.h"
#include "eventtestmodul.h"
#include "retrievedatasourceversionmodul.h"
#include "retrievedataconfigmodul.h"
#include "retrievedatadebugmodul.h"
#include "retrievedatareklamodul.h"
#include "mergeretrievesmodul.h"
#include "mergeretrievesreklamodul.h"
#include "uniquenumbermodul.h"
#include "csvmergemodul.h"
#include "mandatoryfieldsmodul.h"
#include "createversionmodul.h"


ModulManager::ModulManager()
{
	modulMap["Excel Import"] = &createModulInstance<ExcelImportModul>;
	modulMap["Excel Import (CM)"] = &createModulInstance<ExcelUserMappedImportModul>;
	modulMap["Excel Import (CM NV)"] = &createModulInstance<ExcelUserMappedImportNoVersionModul>;
	modulMap["Excel Export"] = &createModulInstance<ExcelExportModul>;
	modulMap["Excel Report"] = &createModulInstance<ExcelReportModul>;
	modulMap["Single Statement"] = &createModulInstance<SingleStatementModul>;
	modulMap["Single Statement (Version)"] = &createModulInstance<SingleStatementVersionModul>;
	modulMap["Report Config"] = &createModulInstance<ReportConfigModul>;
	modulMap["Event Test Modul"] = &createModulInstance<EventTestModul>;
	modulMap["Retrieve Data"] = &createModulInstance<RetrieveDataModul>;
	modulMap["Retrieve Data (Rekla)"] = &createModulInstance<RetrieveDataReklaModul>;
	modulMap["Retrieve Data (Debug)"] = &createModulInstance<RetrieveDataDebugModul>;
	modulMap["Retrieve Data (Source Version)"] = &createModulInstance<RetrieveDataSourceVersionModul>;
	modulMap["Retrieve Data Config"] = &createModulInstance<RetrieveDataConfigModul>;
	modulMap["Merge Retrieves"] = &createModulInstance<MergeRetrievesModul>;
	modulMap["Merge Retrieves (Rekla)"] = &createModulInstance<MergeRetrievesReklaModul>;
	modulMap["Unique Number"] = &createModulInstance<UniqueNumberModul>;
	modulMap["Unique Number (No Config)"] = &createModulInstance<UniqueNumberNoConfigModul>;
	modulMap["CSV Merge"] = &createModulInstance<CsvMergeModul>;
	modulMap["Mandatory Fields"] = &createModulInstance<MandatoryFieldsModul>;
	modulMap["Create Version"] = &createModulInstance<CreateVersionModul>;

	requestMap["Excel Import"] = &ExcelImportModul::getRequestedParameter;
	requestMap["Excel Import (CM)"] = &ExcelUserMappedImportModul::getRequestedParameter;
	requestMap["Excel Import (CM NV)"] = &ExcelUserMappedImportNoVersionModul::getRequestedParameter;
	requestMap["Excel Export"] = &ExcelExportModul::getRequestedParameter;
	requestMap["Excel Report"] = &ExcelReportModul::getRequestedParameter;
	requestMap["Single Statement"] = &SingleStatementModul::getRequestedParameter;
	requestMap["Single Statement (Version)"] = &SingleStatementVersionModul::getRequestedParameter;
	requestMap["Report Config"] = &ReportConfigModul::getRequestedParameter;
	requestMap["Event Test Modul"] = &EventTestModul::getRequestedParameter;
	requestMap["Retrieve Data"] = &RetrieveDataModul::getRequestedParameter;
	requestMap["Retrieve Data (Rekla)"] = &RetrieveDataReklaModul::getRequestedParameter;
	requestMap["Retrieve Data (Debug)"] = &RetrieveDataDebugModul::getRequestedParameter;
	requestMap["Retrieve Data (Source Version)"] = &RetrieveDataSourceVersionModul::getRequestedParameter;
	requestMap["Retrieve Data Config"] = &RetrieveDataConfigModul::getRequestedParameter;
	requestMap["Merge Retrieves"] = &MergeRetrievesModul::getRequestedParameter;
	requestMap["Merge Retrieves (Rekla)"] = &MergeRetrievesReklaModul::getRequestedParameter;
	requestMap["Unique Number"] = &UniqueNumberModul::getRequestedParameter;
	requestMap["Unique Number (No Config)"] = &UniqueNumberNoConfigModul::getRequestedParameter;
	requestMap["CSV Merge"] = &CsvMergeModul::getRequestedParameter;
	requestMap["Mandatory Fields"] = &MandatoryFieldsModul::getRequestedParameter;
	requestMap["Create Version"] = &CreateVersionModul::getRequestedParameter;

	contextMap["Excel Import"] = &ExcelImportModul::useContext;
	contextMap["Excel Import (CM)"] = &ExcelUserMappedImportModul::useContext;
	contextMap["Excel Import (CM NV)"] = &ExcelUserMappedImportNoVersionModul::useContext;
	contextMap["Excel Export"] = &ExcelExportModul::useContext;
	contextMap["Excel Report"] = &ExcelReportModul::useContext;
	contextMap["Single Statement"] = &SingleStatementModul::useContext;
	contextMap["Single Statement (Version)"] = &SingleStatementVersionModul::useContext;
	contextMap["Report Config"] = &ReportConfigModul::useContext;
	contextMap["Event Test Modul"] = &EventTestModul::useContext;
	contextMap["Retrieve Data"] = &RetrieveDataModul::useContext;
	contextMap["Retrieve Data (Rekla)"] = &RetrieveDataReklaModul::useContext;
	contextMap["Retrieve Data (Debug)"] = &RetrieveDataDebugModul::useContext;
	contextMap["Retrieve Data (Source Version)"] = &RetrieveDataSourceVersionModul::useContext;
	contextMap["Retrieve Data Config"] = &RetrieveDataConfigModul::useContext;
	contextMap["Merge Retrieves"] = &MergeRetrievesModul::useContext;
	contextMap["Merge Retrieves (Rekla)"] = &MergeRetrievesReklaModul::useContext;
	contextMap["Unique Number"] = &UniqueNumberModul::useContext;
	contextMap["Unique Number (No Config)"] = &UniqueNumberNoConfigModul::useContext;
	contextMap["CSV Merge"] = &CsvMergeModul::useContext;
	contextMap["Mandatory Fields"] = &MandatoryFieldsModul::useContext;
	contextMap["Create Version"] = &CreateVersionModul::useContext;
}

int ModulManager::count()
{
	return modulMap.keys().count();
}

QStringList ModulManager::modulList()
{
	return modulMap.keys();
}

ConverterModul* ModulManager::modul( QString modulname, ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager , QString context, bool inheritVersion)
{
	if(modulMap.contains(modulname)) {
		return modulMap[modulname](parameterMap, versionMap, connectionManager,context, inheritVersion);
	} else {
		return 0;
	}
}

bool ModulManager::contains( QString modulname )
{
	return modulMap.contains(modulname);
}

QStringList ModulManager::requestedParameter( QString modulname )
{
	if(requestMap.contains(modulname)) {
		return requestMap[modulname]();	
	} else {
		return QStringList();
	}
}

bool ModulManager::useContext( QString modulname )
{
	if(contextMap.contains(modulname)) {
		return contextMap[modulname]();
	} else {
		return false;
	}
	
}
