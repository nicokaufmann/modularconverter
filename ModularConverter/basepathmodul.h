#ifndef basepathmodul_h__
#define basepathmodul_h__

#include <QString>
#include "convertermodul.h"

class BasePathModul : public ConverterModul {
public:
	BasePathModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);

	void setPath(QString path);
protected:
	QString exportPath;
};
#endif // basepathmodul_h__