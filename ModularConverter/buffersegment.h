

class BufferSegment {
public:
	BufferSegment();
	BufferSegment(const int& row, const int& column, const int& length);

	bool fullmatch(const BufferSegment& segment) const;
	bool match(const BufferSegment& segment) const;

	void combine(const BufferSegment& segment);

	int count() const;

	const int& row() const;
	const int& column() const;
	const int& length() const;
	const int& height() const;
private:
	int segmentRow;
	int segmentColumn;
	int segmentLength;
	int segmentHeight;
};