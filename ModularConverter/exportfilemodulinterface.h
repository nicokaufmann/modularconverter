
#ifndef exportfilemodulinterface_h__
#define exportfilemodulinterface_h__

#include <QWidget>

#include "ui_exportfilemodulinterface.h"

class ExportFileModulInterface : public QWidget
{
	Q_OBJECT

public:
	ExportFileModulInterface(QWidget *parent = 0, Qt::WFlags flags = 0);
	~ExportFileModulInterface();

	QWidget* exportButton();

public slots:

private:
	Ui::ExportFileModulInterface ui;
};
#endif // exportfilemodulinterface_h__

