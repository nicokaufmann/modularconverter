#ifndef executemodulinterface_h__
#define executemodulinterface_h__

#include <QWidget>
#include "ui_exportfilemodulinterface.h"

class ExecuteModulInterface : public QWidget
{
	Q_OBJECT

public:
	ExecuteModulInterface(QWidget *parent = 0, Qt::WFlags flags = 0);
	~ExecuteModulInterface();

	QWidget* executeButton();

public slots:

private:
	Ui::ExportFileModulInterface ui;
};
#endif // executemodulinterface_h__
