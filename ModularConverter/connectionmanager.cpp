#include "connectionmanager.h"

#include "applicationdatabase.h"

#include "sqlserversyntaxelements.h"
#include "sybasesyntaxelements.h"
#include "oraclesyntaxelements.h"

#include <QThread>
#include <QMutexLocker>

QMap<QString, DriverInformation> ConnectionManager::driverMap = ConnectionManager::initializeDrivers();
QMap<QString, SyntaxElements*> ConnectionManager::syntaxMap = ConnectionManager::initializeSyntax();

ConnectionManager::ConnectionManager( ApplicationDatabase* applicationDatabase )
	: applicationDatabase(applicationDatabase), databaseElementsUpdater(new DatabaseElementsUpdater(this))
{
	
}

ConnectionManager::~ConnectionManager()
{
	const QStringList keys(connectionMap.keys());
	for(int i=0; i < keys.count(); ++i) {
		connectionMap[keys[i]].databaseElements()->waitForUpdate();
	}

	delete databaseElementsUpdater;
}

QMap<QString, DriverInformation> ConnectionManager::initializeDrivers()
{
	QMap<QString, DriverInformation> drivers;
	drivers.insert("QSQLITE", DriverInformation("QSQLITE",true));
	drivers.insert("QODBC", DriverInformation("QODBC",true));
	drivers.insert("QODBC3", DriverInformation("QODBC3",true));

	return drivers;
}

QMap<QString, SyntaxElements*> ConnectionManager::initializeSyntax()
{
	QMap<QString, SyntaxElements*> syntax;
	syntax.insert("standard", new SyntaxElements());
	syntax.insert("odbc sybase", new SybaseSyntaxElements());
	syntax.insert("odbc sqlserver", new SqlServerSyntaxElements());
	syntax.insert("odbc oracle xe", new OracleSyntaxElements());

	const QList<SyntaxElements*> values(syntax.values());
	for(int i=0; i < values.count(); ++i) {
		values[i]->initializeSyntax();
	}

	return syntax;
}

ApplicationDatabase* ConnectionManager::getApplicationDatabase()
{
	return applicationDatabase;
}

DatabaseElementsUpdater* ConnectionManager::getDatabaseElementsUpdater()
{
	return databaseElementsUpdater;
}

void ConnectionManager::clearConnections()
{
	connectionMap.clear();
}

bool ConnectionManager::containsConnection(const QString connectionname )
{
	return connectionMap.contains(connectionname);
}

QStringList ConnectionManager::connectionList()
{
	QStringList temp(connectionMap.keys());
	if(!applicationDatabase->getCurrentUser().hasFeaturePermission("listappdb")) {
		temp.removeOne(applicationDatabase->getApplicationName());
	}

	for(int i=0; i < removedConnections.count(); ++i) {
		temp.removeOne(removedConnections[i]);
	}

	return temp;
}

void ConnectionManager::addConnection( ConnectionInformation connection )
{
	connectionMap.insert(connection.connectionname(), connection);

	connection.databaseElements()->setConnectionManager(this);

	databaseElementsUpdater->addElements(connection.databaseElements());
}

void ConnectionManager::removeConnection( ConnectionInformation connection )
{
	//connectionMap.remove(connection.connectionname());
	removedConnections<<connection.connectionname();
	//databaseElementsUpdater->removeElements(connection.databaseElements());
}

ConnectionInformation ConnectionManager::getConnection(const QString connectionname )
{
	if(connectionMap.contains(connectionname)) {
		return connectionMap[connectionname];
	} else {
		return ConnectionInformation();
	}
}

DriverInformation ConnectionManager::getDriver(const QString alias )
{
	if(driverMap.contains(alias)) {
		return driverMap[alias];
	} else {
		return DriverInformation();
	}
}

QStringList ConnectionManager::getDriverList()
{
	return driverMap.keys();
}

SyntaxElements* ConnectionManager::getSyntax( const QString syntax )
{
	if(syntaxMap.contains(syntax)) {
		return syntaxMap[syntax];
	} else {
		return syntaxMap["standard"];
	}
}

QStringList ConnectionManager::getSyntaxList()
{
	return syntaxMap.keys();
}

AcquiredConnection ConnectionManager::acquireConnection(const QString connectionname )
{
	if(connectionMap.contains(connectionname)) {
		//only active invocation per connection

		aquireInvocationLock.lock();

		const ConnectionInformation requestedConnection(connectionMap[connectionname]);
		const unsigned long currentThreadId = (unsigned long)QThread::currentThreadId();

		//use active connection
		if(activeConnectionsMap[currentThreadId].contains(connectionname)) {
			AcquiredConnection temp(activeConnectionsMap[currentThreadId][connectionname]);
			aquireInvocationLock.unlock();
			return temp;
		}
		
		const DriverInformation requestedDriver(requestedConnection.driver());
		
		if(!requestedDriver.synchronousUse()) {
			//delete inactive connections BY driver that use same resources + create a request
			removeInactiveDrivers(requestedDriver.alias());
			driverRequests<<requestedDriver.alias();
		} else if(!requestedConnection.synchronousUse()) {
			//delete inactive connections BY connectionname that use same resources + create a request
			removeInactiveConnections(requestedConnection.connectionname());
			connectionRequests<<requestedConnection.connectionname();
		}
		aquireInvocationLock.unlock();

		//acquire new connection + add to map
		AcquiredConnection temp(requestedConnection, this);
	
		aquireInvocationLock.lock();
		activeConnectionsMap[currentThreadId].insert(requestedConnection.connectionname(), temp);

		if(!requestedDriver.synchronousUse()) {
			//remove driver request
			driverRequests.removeOne(requestedDriver.alias());
		} else if(!requestedConnection.synchronousUse()) {
			//remove connection request
			connectionRequests.removeOne(requestedConnection.connectionname());
		}

		aquireInvocationLock.unlock();
		
		return temp;
	} else {
		return AcquiredConnection();
	}
}

void ConnectionManager::removeInactiveConnections( const QString& connectionname )
{
	const QList<unsigned long> keys(activeConnectionsMap.keys());

	for(int i=0; i < keys.count(); ++i) {
		if(activeConnectionsMap[keys[i]].contains(connectionname) && activeConnectionsMap[keys[i]][connectionname].referenceCount() == 1) {
			activeConnectionsMap[keys[i]].remove(connectionname);
		}
	}
}

void ConnectionManager::removeInactiveDrivers( const QString& drivername )
{
	const QList<unsigned long> keys(activeConnectionsMap.keys());

	for(int i=0; i < keys.count(); ++i) {
		const QStringList connections(activeConnectionsMap[keys[i]].keys());
		for(int j=0; j < connections.count(); ++j) {
			if(activeConnectionsMap[keys[i]][connections[j]].getConnectionInformation().driver().alias() == drivername) {
				activeConnectionsMap[keys[i]].remove(connections[j]);
				--j;
			}
		}
	}
}

void ConnectionManager::checkRequests( const QString connectionname, const QString drivername )
{
	QMutexLocker lockActiveConnectionsMap(&aquireInvocationLock);
	
	const unsigned long currentThreadId = (unsigned long)QThread::currentThreadId();

	if(driverRequests.contains(drivername) || connectionRequests.contains(connectionname)) {
		if(activeConnectionsMap[currentThreadId].contains(connectionname)) {
			activeConnectionsMap[currentThreadId].remove(connectionname);
		}
	}
}
