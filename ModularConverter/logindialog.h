#ifndef logindialog_h__
#define logindialog_h__

#include <QDialog>
#include "ui_logindialog.h"

#include "applicationdatabase.h"
#include "modularconverter.h"

class LoginDialog : public QDialog
{
	Q_OBJECT

public:
	LoginDialog(QWidget *parent = 0, Qt::WFlags flags = 0);
	~LoginDialog();

public slots:
	void selectApplication();
	void login();

private:
	bool checkConnection(ConnectionInformation connection);
	ConnectionInformation getAppConnection();

	ApplicationDatabase* applicationDatabase;
	ModularConverter* modConv;

	Ui::LoginDialog ui;
};
#endif // logindialog_h__