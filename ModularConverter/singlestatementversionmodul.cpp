#include "singlestatementversionmodul.h"

#include "syntaxanalyzer.h"
#include "sqlinterpreter.h"
#include "syntaxbuilder.h"
#include <QSqlError>

SingleStatementVersionModul::SingleStatementVersionModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion)
	: ConverterModul(parameterMap, versionMap, connectionManager, context, inheritVersion)
{
}

SingleStatementVersionModul::~SingleStatementVersionModul()
{
}

QStringList SingleStatementVersionModul::getRequestedParameter()
{
	return (QStringList()<<"Statementkonfiguration"<<"Versionierung");
}

bool SingleStatementVersionModul::useContext()
{
	return true;
}

QWidget* SingleStatementVersionModul::createWidget()
{
	return new ExecuteVersionModulInterface(this);
}

bool SingleStatementVersionModul::initialize()
{
	return generateVersionRequests();
}

bool SingleStatementVersionModul::execute(int cmd)
{
	QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 1));

	{
		if(connectionManager->containsConnection(statementConnectionName)) {
			AcquiredConnection connection(connectionManager->acquireConnection(statementConnectionName));

			if(connection->isOpen()) {
				QSqlQuery statement(*connection);
				if(statement.prepare(statementText)) {

					for(int i=0; i < versionRequestOrder.count(); ++i) {
						statement.bindValue(":version_id_" + QString::number(i) ,versionRequests[versionRequestOrder[i]].versionId());
					}

					if(statement.exec()) {
						QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 100));
						return true;
					} else {
						lastError="Statement konnte nicht ausgeführt werden:\n" + statement.lastError().text();
						return false;
					}
				} else {
					lastError="Statement konnte nicht vorbereitet werden:\n" + statement.lastError().text();
					return false;
				}
			} else {
				lastError="Verbindung kann nicht hergestellt werden.";
				return false;
			}
		} else {
			lastError="Verbindungsname unbekannt.";
			return false;
		}
	}
}

bool SingleStatementVersionModul::generateVersionRequests()
{
	QString connectionname = parameterMap["Statementkonfiguration"].first;
	QString table = parameterMap["Statementkonfiguration"].second;

	AcquiredConnection connection(connectionManager->acquireConnection(connectionname));

	if(connection->isOpen()) {

		SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

		QSqlQuery data(*connection);
		data.prepare("select * from "+syntax->delimitTablename(table)+" where context=:context");
		data.bindValue(0,context);

		if(data.exec()) {
			if(data.next()) {
				versionRequests.clear();
				versionRequestOrder.clear();

				statementConnectionName = data.record().value("connectionname").toString();
				statementText = data.record().value("statement").toString();

				connectionManager->getDatabaseElementsUpdater()->updateElementsBlocking(statementConnectionName);

				DatabaseElements databaseElements(*connectionManager->getConnection(statementConnectionName).databaseElements());
				SyntaxElements syntaxElements(*connectionManager->getConnection(statementConnectionName).syntaxElements());

				SqlInterpreter interpreter(databaseElements,syntaxElements);
				StatementNode statement(0,"");
				interpreter.interpret(statementText,&statement);

				SyntaxAnalyzer analyzer(&statement);
				QVector<SyntaxNode*> sourceNodes(analyzer.getAllDepth("update"));
				QVector<SyntaxNode*> fromNodes(analyzer.getAllDepth("from"));
				
				QVector<SyntaxNode*> completedNodes;

				sourceNodes+=fromNodes;
				
				for(int j=sourceNodes.count()-1; j > -1; --j) {
					SyntaxAnalyzer sourceAnalyzer(sourceNodes[j]);
					QVector<SyntaxNode*> tableNodes = sourceAnalyzer.getAllDepth("table");


					SyntaxNode* whereNode = sourceAnalyzer.getCorresponding("where");

					if(!whereNode) {
						whereNode = new WhereNode(0,"where");
					}

					for(int k=0; k < tableNodes.count(); ++k) {
						//check if table has alias
						SyntaxNode* currentNode = tableNodes[k];

						if(!completedNodes.contains(currentNode)) {
							QString tablealias(currentNode->value());
							QString tablename(currentNode->value());

							if(currentNode->parent() != NULL && currentNode->parent()->type()=="alias") {
								if(currentNode->parent()->count() > 1) {
									tablealias = tableNodes[k]->parent()->child(1)->value();
								}
							}

							SyntaxNode* condition = new LogicalNode(0,"=");
							TableNode* versionedTable = new TableNode(0,tablealias);

							condition->add(new LinkNode(0,"."));
							condition->add(new FieldNode(0,":version_id_" + QString::number(versionRequestOrder.count())));
							condition->children()[0]->add(versionedTable);
							condition->children()[0]->add(new FieldNode(0,"version_id"));

							SyntaxBuilder::addConjunction(whereNode, condition);

							versionRequestOrder<<tablename;

							if(!versionRequests.contains(tablename)) {
								versionRequests.insert(tablename, VersionRequest(statementConnectionName,tablename));
								addVersionRequestInherited(versionRequests[tablename]);
							}

							completedNodes.append(currentNode);
							completedNodes.append(versionedTable);
						}
					}

					if(!whereNode->parent() && whereNode->children().count() > 0) {
						if(sourceNodes[j]->type()=="update") {
							sourceNodes[j]->parent()->add(whereNode);
						} else {
							sourceNodes[j]->parent()->add(whereNode,sourceNodes[j]->parent()->index(sourceNodes[j])+1);
						}
					}
				}

				statementText = statement.toString();
				processInheritedRequests();

				return true;
			} else {
				lastError="Konfiguration nicht gefunden.";
				return false;
			}
		} else {
			lastError="Konfigurations-Abfrage fehlgeschlagen:\n"+data.lastError().text();
			return false;
		}
	} else {
		lastError="Problem mit Konfigurations-Datenbankverbindung:\n"+connection->lastError().text();
		return false;
	}
}