#ifndef syntaxnodes_h__
#define syntaxnodes_h__

#include "syntaxnode.h"

// CREATION
template<typename T> SyntaxNode* createInstance(SyntaxNode* parent, QString val) { return new T(parent, val); }


//QMap<QString,SyntaxNode*(*)(SyntaxNode*, QString)> typeMap;
//typeMap["base"] = &createInstance<SyntaxNode>;
//SyntaxNode* root = typeMap["base"](NULL,"");

//Statement Node
//Select Node
//Set Node
//Delete Node
//Where Node
//From Node
//Update Node
//Insert Into Node
//Values Node
//Into Node
//Join Node
//On Node
//Union Node
//Group Node
//Order Node
//Order Modifier Node
//Field Node
//Table Node
//Text Node
//Value Node
//Alias Node
//Link Node
//Brace Node
//NegateNode
//LogicalNode
//ComparisonNode
//Between Node
//In Node
//Operator Node
//Select Modifier Node
//Top
//Function Node


//Statement Node
class StatementNode : public SyntaxNode {
public:
	StatementNode(SyntaxNode* parent, QString val);
	virtual StatementNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//Select Node
class SelectNode : public SyntaxNode {
public:
	SelectNode(SyntaxNode* parent, QString val);
	virtual SelectNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);

	bool needsIntermediate();
	SyntaxNode* intermediate();
	QString intermediateType();
};

//Set Node
class SetNode : public SyntaxNode {
public:
	SetNode(SyntaxNode* parent, QString val);
	virtual SetNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);

	bool needsIntermediate();
	SyntaxNode* intermediate();
	QString intermediateType();
};

//Delete Node
class DeleteNode : public SyntaxNode {
public:
	DeleteNode(SyntaxNode* parent, QString val);
	virtual DeleteNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);

	bool needsIntermediate();
	SyntaxNode* intermediate();
	QString intermediateType();
};

//Where Node
class WhereNode : public SyntaxNode {
public:
	WhereNode(SyntaxNode* parent, QString val);
	virtual WhereNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);

	bool needsIntermediate();
	SyntaxNode* intermediate();
	QString intermediateType();
};

//From Node
class FromNode : public SyntaxNode {
public:
	FromNode(SyntaxNode* parent, QString val);
	virtual FromNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);

	bool needsIntermediate();
	SyntaxNode* intermediate();
	QString intermediateType();
};

//Update Node
class UpdateNode : public SyntaxNode {
public:
	UpdateNode(SyntaxNode* parent, QString val);
	virtual UpdateNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);

	bool needsIntermediate();
	SyntaxNode* intermediate();
	QString intermediateType();
};

//Insert Into Node
class InsertIntoNode : public SyntaxNode {
public:
	InsertIntoNode(SyntaxNode* parent, QString val);
	virtual InsertIntoNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);

	bool needsIntermediate();
	SyntaxNode* intermediate();
	QString intermediateType();
};

//Values Node
class ValuesNode : public SyntaxNode {
public:
	ValuesNode(SyntaxNode* parent, QString val);
	virtual ValuesNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);

	bool needsIntermediate();
	SyntaxNode* intermediate();
	QString intermediateType();
};

//Into Node
class IntoNode : public SyntaxNode {
public:
	IntoNode(SyntaxNode* parent, QString val);
	virtual IntoNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//Join Node
class JoinNode : public SyntaxNode {
public:
	JoinNode(SyntaxNode* parent, QString val);
	virtual JoinNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//On Node
class OnNode : public SyntaxNode {
public:
	OnNode(SyntaxNode* parent, QString val);
	virtual OnNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//Union Node
class UnionNode : public SyntaxNode {
public:
	UnionNode(SyntaxNode* parent, QString val);
	virtual UnionNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//Group Node
class GroupNode : public SyntaxNode {
public:
	GroupNode(SyntaxNode* parent, QString val);
	virtual GroupNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);

	bool needsIntermediate();
	SyntaxNode* intermediate();
	QString intermediateType();
};

//Order Node
class OrderNode : public SyntaxNode {
public:
	OrderNode(SyntaxNode* parent, QString val);
	virtual OrderNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
	
	bool needsIntermediate();
	SyntaxNode* intermediate();
	QString intermediateType();
};

//Order Modifier Node
class OrderModifierNode : public SyntaxNode {
public:
	OrderModifierNode(SyntaxNode* parent, QString val);
	virtual OrderModifierNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//Field Node
class FieldNode : public SyntaxNode {
public:
	FieldNode(SyntaxNode* parent, QString val);
	FieldNode(SyntaxNode* parent, QString val, QString delimiter);
	FieldNode(SyntaxNode* parent, QString val, QString beginDelimiter, QString endDelimiter);
	virtual FieldNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);

	void setDelimiter(QString delimiter);
	void setDelimiter(QString beginDelimiter, QString endDelimiter);

protected:
	QString beginDelimiter;
	QString endDelimiter;
};

//Table Node
class TableNode : public SyntaxNode {
public:
	TableNode(SyntaxNode* parent, QString val);
	TableNode(SyntaxNode* parent, QString val, QString delimiter);
	TableNode(SyntaxNode* parent, QString val, QString beginDelimiter, QString endDelimiter);
	virtual TableNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);

	void setDelimiter(QString delimiter);
	void setDelimiter(QString beginDelimiter, QString endDelimiter);
protected:
	QString beginDelimiter;
	QString endDelimiter;
};

//Text Node
class TextNode : public SyntaxNode {
public:
	TextNode(SyntaxNode* parent, QString val);
	TextNode(SyntaxNode* parent, QString val, QString delimiter);
	virtual TextNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);

	void setDelimiter(QString delimiter);
protected:
	QString beginDelimiter;
	QString endDelimiter;
};

//Value Node
class ValueNode : public SyntaxNode {
public:
	ValueNode(SyntaxNode* parent, QString val);
	virtual ValueNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//Alias Node
class AliasNode : public SyntaxNode {
public:
	AliasNode(SyntaxNode* parent, QString val);
	virtual AliasNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//Link Node
class LinkNode : public SyntaxNode {
public:
	LinkNode(SyntaxNode* parent, QString val);
	virtual LinkNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//Brace Node
class BraceNode : public SyntaxNode {
public:
	BraceNode(SyntaxNode* parent, QString val);
	virtual BraceNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);

	void close();
};

//NegateNode
class NegateNode : public SyntaxNode {
public:
	NegateNode(SyntaxNode* parent, QString val);
	virtual NegateNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//LogicalNode
class LogicalNode : public SyntaxNode {
public:
	LogicalNode(SyntaxNode* parent, QString val);
	virtual LogicalNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//ComparisonNode
class ComparisonNode : public SyntaxNode {
public:
	ComparisonNode(SyntaxNode* parent, QString val);
	virtual ComparisonNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//Between Node
class BetweenNode : public SyntaxNode {
public:
	BetweenNode(SyntaxNode* parent, QString val);
	virtual BetweenNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//In Node
class InNode : public SyntaxNode {
public:
	InNode(SyntaxNode* parent, QString val);
	virtual InNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};


//Operator Node
class OperatorNode : public SyntaxNode {
public:
	OperatorNode(SyntaxNode* parent, QString val);
	virtual OperatorNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//Distinct Node
class DistinctNode : public SyntaxNode {
public:
	DistinctNode(SyntaxNode* parent, QString val);
	virtual DistinctNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//Distinct Node
class TopNode : public SyntaxNode {
public:
	TopNode(SyntaxNode* parent, QString val);
	virtual TopNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};

//Function Node
class FunctionNode : public SyntaxNode {
public:
	FunctionNode(SyntaxNode* parent, QString val);
	FunctionNode(SyntaxNode* parent, QString val, QStringList& paramList);
	virtual FunctionNode* clone();

	bool accepts(SyntaxNode* node);
	QString toString();
	QString toStringFormat(int currentLevel);
};


#endif // syntaxnodes_h__
