#include "singlestatementmodul.h"

#include <QSqlError>

SingleStatementModul::SingleStatementModul(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion)
	: ConverterModul(parameterMap, versionMap, connectionManager, context, inheritVersion)
{
	immediateExecution = true;
}

SingleStatementModul::~SingleStatementModul()
{
}

QStringList SingleStatementModul::getRequestedParameter()
{
	return (QStringList()<<"Statementkonfiguration");
}

bool SingleStatementModul::useContext()
{
	return true;
}

bool SingleStatementModul::execute(int cmd)
{
	QString statementConnectionName;
	QString statementText;

	{
		QString connectionname = parameterMap["Statementkonfiguration"].first;
		QString table = parameterMap["Statementkonfiguration"].second;

		AcquiredConnection connection(connectionManager->acquireConnection(connectionname));

		if(connection->isOpen()) {

			SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

			QSqlQuery data(*connection);
			data.prepare("select * from "+syntax->delimitTablename(table)+" where context=:context");
			data.bindValue(0,context);

			if(data.exec()) {
				if(data.next()) {
					statementConnectionName = data.record().value("connectionname").toString();
					statementText = data.record().value("statement").toString();

				} else {
					lastError="Konfiguration nicht gefunden.";
					return false;
				}
			} else {
				lastError="Konfigurations-Abfrage fehlgeschlagen:\n"+data.lastError().text();
				return false;
			}
		} else {
			lastError="Problem mit Konfigurations-Datenbankverbindung:\n"+connection->lastError().text();
			return false;
		}
	}
	
	QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 1));

	{
		if(connectionManager->containsConnection(statementConnectionName)) {
			AcquiredConnection connection(connectionManager->acquireConnection(statementConnectionName));

			if(connection->isOpen()) {
				QSqlQuery statement(*connection);
				if(statement.exec(statementText)) {
					QMetaObject::invokeMethod(widget, "setProgress", Qt::QueuedConnection, Q_ARG(int, 100));
					return true;
				} else {
					lastError="Statement konnte nicht ausgeführt werden:\n" + statement.lastError().text();
					return false;
				}
			} else {
				lastError="Verbindung kann nicht hergestellt werden.";
				return false;
			}
		} else {
			lastError="Verbindungsname unbekannt.";
			return false;
		}
	}
}

QWidget* SingleStatementModul::createWidget()
{
	return new StandardModulInterface(this);
}
