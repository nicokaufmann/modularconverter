#include "simplesqleditor.h"

#include "listdialog.h"
#include "sqlinterpreter.h"
#include "syntaxanalyzer.h"

#include <QSqlError>

SimpleSqlEditor::SimpleSqlEditor(ApplicationDatabase* applicationDatabase, QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags), applicationDatabase(applicationDatabase), connectionManager(applicationDatabase->getConnectionManager()), tabCounter(0)
{
	ui.setupUi(this);
	ui.statementsTab->clear();

	filterWindow = new FilterWindow(this, connectionManager);

	QObject::connect(ui.actionCheckPlausibility, SIGNAL(triggered()), this, SLOT(checkPlausibility()));
	QObject::connect(ui.actionCheckExecution, SIGNAL(triggered()), this, SLOT(checkExecution()));
	QObject::connect(ui.actionSimpleFilter, SIGNAL(triggered()), this, SLOT(showSimpleFilter()));
	QObject::connect(ui.actionShowData, SIGNAL(triggered()), this, SLOT(showQueryData()));
	QObject::connect(ui.actionOverviewSelected, SIGNAL(triggered()), this, SLOT(showOverviewSelected()));
	QObject::connect(ui.actionOverviewExcluded, SIGNAL(triggered()), this, SLOT(showOverviewExcluded()));
	QObject::connect(ui.actionNewStatement, SIGNAL(triggered()), this, SLOT(addStatement()));
	QObject::connect(ui.statementsTab, SIGNAL(tabCloseRequested(int)),this, SLOT(closeStatement(int)));
}

SimpleSqlEditor::~SimpleSqlEditor()
{
}

void SimpleSqlEditor::setPermissions( ApplicationUser user )
{
	ui.actionCheckExecution->setEnabled(user.hasFeaturePermission("testquery"));
	ui.actionShowData->setEnabled(user.hasFeaturePermission("showquery"));
	ui.actionOverviewSelected->setEnabled(user.hasFeaturePermission("showquery"));
	ui.actionOverviewExcluded->setEnabled(user.hasFeaturePermission("showquery"));
}

void SimpleSqlEditor::addStatement()
{
	StatementWidget* tempWidget = new StatementWidget(applicationDatabase,this);

	tabCounter++;
	ui.statementsTab->insertTab(0,tempWidget,"Statement " + QString::number(tabCounter));
}

void SimpleSqlEditor::addStatement( QString connectionname, QString statement )
{
	StatementWidget* tempWidget = new StatementWidget(applicationDatabase,this);

	tabCounter++;
	ui.statementsTab->insertTab(0,tempWidget,"Statement " + QString::number(tabCounter));
	ui.statementsTab->setCurrentIndex(0);

	tempWidget->setDatabase(connectionname);
	tempWidget->setStatement(statement);
}

void SimpleSqlEditor::closeStatement(int index)
{
	StatementWidget* tempWidget = (StatementWidget*)ui.statementsTab->widget(index);
	ui.statementsTab->removeTab(index);
}

void SimpleSqlEditor::closing( DataWindow* window )
{
	if(windowList.contains(window)) {
		QObject::disconnect(window, SIGNAL(closing(DataWindow*)), this, SLOT(closing(DataWindow*)));
		windowList.removeOne(window);
		window->deleteLater();
	}
}

void SimpleSqlEditor::checkPlausibility()
{
	int currentIdx = ui.statementsTab->currentIndex();
	if(currentIdx > -1 && currentIdx < ui.statementsTab->count()) {
		StatementWidget* currentStatement = (StatementWidget*)ui.statementsTab->widget(currentIdx);
		
		DatabaseElements databaseElements(*(connectionManager->getConnection(currentStatement->getDatabase()).databaseElements()));
		SyntaxElements syntaxElements(*connectionManager->getConnection(currentStatement->getDatabase()).syntaxElements());
		StatementNode rootNode(NULL, "");

		SqlInterpreter interpreter(databaseElements, syntaxElements);
		
		interpreter.interpret(currentStatement->getStatement(),&rootNode);
		
		SyntaxAnalyzer analyzer(&rootNode);

		QStringList tables(databaseElements.getTables());
		QStringList attributes(databaseElements.getAttributes());

		QVector<SyntaxNode*> unknownTables = analyzer.getNotContained("table",tables);
		QVector<SyntaxNode*> unknownAttributes = analyzer.getNotContained("field", attributes);
		//functionnames
		//function parameter
		//expected parameter vs actual
		//select ohne from

		QStringList warnings;

		for(int i=0; i < unknownTables.count(); ++i) {
			warnings.append("Unbekannter Tabellenname \"" + unknownTables[i]->value() + "\"");
		}

		for(int i=0; i < unknownAttributes.count(); ++i) {
			warnings.append("Unbekannter Feldname \"" + unknownAttributes[i]->value() + "\"");
		}

		if(warnings.count() > 0) {
			ListDialog ld("Plausibilitätscheck - Warnungen",warnings);
			ld.exec();
		} else {
			QMessageBox msgBox;
			msgBox.setWindowTitle("Statement plausibel");
			msgBox.setText("Keine Probleme gefunden.");
			msgBox.setIcon(QMessageBox::Information);
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::Ok);
			msgBox.exec();
		}
	}
}

void SimpleSqlEditor::showSimpleFilter()
{
	filterWindow->show();
	QApplication::setActiveWindow(filterWindow);
}

void SimpleSqlEditor::showQueryData()
{
	int currentIdx = ui.statementsTab->currentIndex();
	if(currentIdx > -1 && currentIdx < ui.statementsTab->count()) {
		StatementWidget* currentStatement = (StatementWidget*)ui.statementsTab->widget(currentIdx);

		if(connectionManager->containsConnection(currentStatement->getDatabase())) {	
			DatabaseElements databaseElements(*(connectionManager->getConnection(currentStatement->getDatabase()).databaseElements()));
			SyntaxElements syntaxElements(*connectionManager->getConnection(currentStatement->getDatabase()).syntaxElements());

			StatementNode rootNode(NULL, "");
			SqlInterpreter interpreter(databaseElements, syntaxElements);
			interpreter.interpret(currentStatement->getStatement(),&rootNode);

			SyntaxAnalyzer analyzeAll(&rootNode);

			if(analyzeAll.getFirstDepth("insert") == NULL && analyzeAll.getFirstDepth("update") == NULL && analyzeAll.getFirstDepth("delete") == NULL) {
				//build query
				windowList.append(new DataWindow(connectionManager, currentStatement->getDatabase(), currentStatement->getStatement()));
				QObject::connect(windowList.last(), SIGNAL(closing(DataWindow*)), this, SLOT(closing(DataWindow*)));
				windowList.last()->show();
			} else {
				QMessageBox msgBox;
				msgBox.setStandardButtons(QMessageBox::Ok);
				msgBox.setDefaultButton(QMessageBox::Ok);
				msgBox.setWindowTitle("Anzeige nicht möglich");
				msgBox.setText("Nur SELECT-Statements werden unterstützt.");
				msgBox.setIcon(QMessageBox::Warning);
				msgBox.exec();
			}
		}
	}
}

void SimpleSqlEditor::showOverviewSelected()
{
	int currentIdx = ui.statementsTab->currentIndex();
	if(currentIdx > -1 && currentIdx < ui.statementsTab->count()) {
		StatementWidget* currentStatement = (StatementWidget*)ui.statementsTab->widget(currentIdx);

		if(connectionManager->containsConnection(currentStatement->getDatabase())) {

			DatabaseElements databaseElements(*(connectionManager->getConnection(currentStatement->getDatabase()).databaseElements()));
			SyntaxElements syntaxElements(*connectionManager->getConnection(currentStatement->getDatabase()).syntaxElements());
		
			StatementNode rootNode(NULL, "");
			SqlInterpreter interpreter(databaseElements, syntaxElements);
			interpreter.interpret(currentStatement->getStatement(),&rootNode);

			SyntaxAnalyzer analyzeAll(&rootNode);

			if(analyzeAll.getFirstDepth("insert") == NULL && analyzeAll.getFirstDepth("update") == NULL && analyzeAll.getFirstDepth("delete") == NULL) {
				SyntaxNode* whereNode = analyzeAll.getFirstBreadth("where");
				SyntaxNode* fromNode = analyzeAll.getFirstBreadth("from");

				// TODO: if contains insert / update / delete / drop statements => error

				if(whereNode && fromNode) {
					SyntaxAnalyzer analyzeWhere(whereNode);
					QVector<SyntaxNode*> relevantFields(analyzeWhere.getAllBreadth("field"));
					// TODO: Dialog asking to order relevant fields

					whereNode = whereNode->copy();

					StatementNode overviewStatement(NULL,"");
					overviewStatement.add(new SelectNode(NULL,"select"));
					overviewStatement.add(fromNode->copy());

					overviewStatement.child(0)->add(new DistinctNode(NULL,"distinct"));
					for(int i=0; i < relevantFields.count(); ++i) {				
						overviewStatement.child(0)->add(relevantFields[i]->copy());
					}

					overviewStatement.add(whereNode);

					SyntaxNode* groupNode = new GroupNode(NULL, "group by");
					overviewStatement.add(groupNode);
					for(int i=0; i < relevantFields.count(); ++i) {				
						groupNode->add(relevantFields[i]->copy());
					}

					windowList.append(new DataWindow(connectionManager, currentStatement->getDatabase(), overviewStatement.toString()));
					QObject::connect(windowList.last(), SIGNAL(closing(DataWindow*)), this, SLOT(closing(DataWindow*)));
					windowList.last()->show();
				}
			} else {
				QMessageBox msgBox;
				msgBox.setStandardButtons(QMessageBox::Ok);
				msgBox.setDefaultButton(QMessageBox::Ok);
				msgBox.setWindowTitle("Anzeige nicht möglich");
				msgBox.setText("Nur SELECT-Statements werden unterstützt.");
				msgBox.setIcon(QMessageBox::Warning);
				msgBox.exec();
			}
		}
	}
}

void SimpleSqlEditor::showOverviewExcluded()
{
	int currentIdx = ui.statementsTab->currentIndex();
	if(currentIdx > -1 && currentIdx < ui.statementsTab->count()) {
		StatementWidget* currentStatement = (StatementWidget*)ui.statementsTab->widget(currentIdx);

		if(connectionManager->containsConnection(currentStatement->getDatabase())) {

			DatabaseElements databaseElements(*(connectionManager->getConnection(currentStatement->getDatabase()).databaseElements()));
			SyntaxElements syntaxElements(*connectionManager->getConnection(currentStatement->getDatabase()).syntaxElements());
		
			StatementNode rootNode(NULL, "");
			SqlInterpreter interpreter(databaseElements, syntaxElements);
			interpreter.interpret(currentStatement->getStatement(),&rootNode);

			SyntaxAnalyzer analyzeAll(&rootNode);
			
			if(analyzeAll.getFirstDepth("insert") == NULL && analyzeAll.getFirstDepth("update") == NULL && analyzeAll.getFirstDepth("delete") == NULL) {
				SyntaxNode* whereNode = analyzeAll.getFirstBreadth("where");
				SyntaxNode* fromNode = analyzeAll.getFirstBreadth("from");


				// TODO: if contains insert / update / delete / drop statements => error

				if(whereNode && fromNode) {
					SyntaxAnalyzer analyzeWhere(whereNode);
					QVector<SyntaxNode*> relevantFields(analyzeWhere.getAllBreadth("field"));
					// TODO: Dialog asking to order relevant fields

					whereNode = whereNode->copy();

					StatementNode overviewStatement(NULL,"");
					overviewStatement.add(new SelectNode(NULL,"select"));
					overviewStatement.add(fromNode->copy());
				
					overviewStatement.child(0)->add(new DistinctNode(NULL,"distinct"));
					for(int i=0; i < relevantFields.count(); ++i) {				
						overviewStatement.child(0)->add(relevantFields[i]->copy());
					}
				
					overviewStatement.add(whereNode);


					SyntaxNode* groupNode = new GroupNode(NULL, "group by");
					overviewStatement.add(groupNode);
					for(int i=0; i < relevantFields.count(); ++i) {				
						groupNode->add(relevantFields[i]->copy());
					}

					if(whereNode->count() > 0) {
						whereNode->child(0)->insert(new BraceNode(NULL,""));
						whereNode->child(0)->insert(new NegateNode(NULL,"NOT"));
					}

					windowList.append(new DataWindow(connectionManager, currentStatement->getDatabase(), overviewStatement.toString()));
					QObject::connect(windowList.last(), SIGNAL(closing(DataWindow*)), this, SLOT(closing(DataWindow*)));
					windowList.last()->show();
				}
			} else {
				QMessageBox msgBox;
				msgBox.setStandardButtons(QMessageBox::Ok);
				msgBox.setDefaultButton(QMessageBox::Ok);
				msgBox.setWindowTitle("Anzeige nicht möglich");
				msgBox.setText("Nur SELECT-Statements werden unterstützt.");
				msgBox.setIcon(QMessageBox::Warning);
				msgBox.exec();
			}
		}
	}
}

void SimpleSqlEditor::checkExecution()
{
	int currentIdx = ui.statementsTab->currentIndex();
	if(currentIdx > -1 && currentIdx < ui.statementsTab->count()) {
		StatementWidget* currentStatement = (StatementWidget*)ui.statementsTab->widget(currentIdx);

		if(connectionManager->containsConnection(currentStatement->getDatabase())) {
			QMessageBox msgBox;
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::Ok);

			AcquiredConnection connection(connectionManager->acquireConnection(currentStatement->getDatabase()));

			if(connection->isOpen()) {
				if(connection->transaction()) {
					QSqlQuery query(*connection);

					if(!query.exec(currentStatement->getStatement())) {
						msgBox.setWindowTitle("Statement fehlerhaft");
						msgBox.setText("Es gab ein Problem beim test des Statements.");
						msgBox.setDetailedText(query.lastError().text());
						msgBox.setIcon(QMessageBox::Warning);
					} else {
						msgBox.setWindowTitle("Erfolgreich simuliert");
						msgBox.setText("Das Statement konnte erfolgreich ausgeführt werden.");
						msgBox.setIcon(QMessageBox::Information);
					}

					connection->rollback();
				} else {
					msgBox.setWindowTitle("Test nicht erfolgt");
					msgBox.setText("Verbindung unterstützt keine Transaktionen.");
					msgBox.setIcon(QMessageBox::Warning);
				}
			} else {
				msgBox.setWindowTitle("Verbindungsfehler");
				msgBox.setText("Verbindung zu Datenbank konnte nicht hergestellt werden.");
				msgBox.setDetailedText(connection->lastError().text());
				msgBox.setIcon(QMessageBox::Warning);
			}

			msgBox.exec();
		}
	}
}
