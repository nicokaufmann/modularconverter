#include "tabletab.h"

#include <QSqlQuery>
#include <QSqlError>

TableTab::TableTab( ApplicationDatabase* applicationDatabase, QWidget *parent, Qt::WFlags flags)
	: QWidget(parent,flags), applicationDatabase(applicationDatabase), connectionManager(applicationDatabase->getConnectionManager())
{
	ui.setupUi(this);

	ui.cbDatabases->addItems(connectionManager->connectionList());

	ui.tFields->setColumnCount(1);
	ui.tFields->setHorizontalHeaderItem(0,new QTableWidgetItem("Datentyp"));

	QObject::connect(ui.bChangeTitle, SIGNAL(clicked()), this, SLOT(changeHeader()));
	QObject::connect(ui.bAddField, SIGNAL(clicked()), this, SLOT(addColumn()));
	QObject::connect(ui.bRemoveField, SIGNAL(clicked()), this, SLOT(removeColumn()));

	QObject::connect(ui.bNewTable, SIGNAL(clicked()), this, SLOT(clearTable()));
	QObject::connect(ui.bSaveTable, SIGNAL(clicked()), this, SLOT(createTable()));
	QObject::connect(ui.bSqlStatement, SIGNAL(clicked()), this, SLOT(createStatement()));

	QObject::connect(ui.cbDatabases, SIGNAL(activated(int)), this, SLOT(selectDatabase(int)));
	QObject::connect(ui.cbTables, SIGNAL(activated(int)), this, SLOT(selectTable(int)));

	QObject::connect(ui.bSpreadsheetTemplate, SIGNAL(clicked()), this, SLOT(spreadsheetTemplate()));

	QObject::connect(connectionManager->getDatabaseElementsUpdater(), SIGNAL(databaseElementsUpdated(QString)), this, SLOT(tableDataUpdated(QString)));

	//typelist
	selectDatabase(0);
}

TableTab::~TableTab()
{

}


void TableTab::addColumn()
{
	ConnectionInformation connection(connectionManager->getConnection(ui.cbDatabases->currentText()));
	SyntaxElements syntax(*connection.syntaxElements());

	QComboBox* tempBox = new QComboBox(ui.tFields);
	tempBox->addItems(syntax.getTypeList());

	//table manager
	ui.tFields->setRowCount(ui.tFields->rowCount()+1);
	ui.tFields->setCellWidget(ui.tFields->rowCount()-1,0,tempBox);
}

void TableTab::removeColumn()
{
	if(ui.tFields->currentRow() >= 0) {
		QMessageBox msgBox;
		QString fieldname = "";
		if(ui.tFields->verticalHeaderItem(ui.tFields->currentRow())) {
			fieldname = ui.tFields->verticalHeaderItem(ui.tFields->currentRow())->text();
		}
		msgBox.setWindowTitle("Achtung");
		msgBox.setText("Soll das Feld " + fieldname + " wirklich entfernt werden?");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgBox.setDefaultButton(QMessageBox::Yes);
		if(msgBox.exec() == QMessageBox::Yes) {
			ui.tFields->removeRow(ui.tFields->currentRow());
		}
	}
}

void TableTab::changeHeader()
{
	if(ui.tFields->currentRow() >= 0) {
		bool ok;
		QString text;
		if(ui.tFields->verticalHeaderItem(ui.tFields->currentRow())) {
			text = QInputDialog::getText(this,"Feldname anpassen","",QLineEdit::Normal,ui.tFields->verticalHeaderItem(ui.tFields->currentRow())->text(),&ok);
		} else {
			text = QInputDialog::getText(this,"Feldname anpassen","",QLineEdit::Normal,"",&ok);
		}
		if(ok) {
			ui.tFields->setVerticalHeaderItem(ui.tFields->currentRow(),new QTableWidgetItem(text));
		}
	}
}


void TableTab::selectDatabase(int index)
{
	if(connectionManager->containsConnection(ui.cbDatabases->currentText())) {
		ui.cbTables->clear();
		ui.cbTables->addItems(connectionManager->getConnection(ui.cbDatabases->currentText()).databaseElements()->getTables());

		selectTable(0);
	}
}

void TableTab::selectTable(int index)
{
	if(connectionManager->containsConnection(ui.cbDatabases->currentText())) {
		ConnectionInformation connection(connectionManager->getConnection(ui.cbDatabases->currentText()));
		QVector<QPair<QString,int>> attributeMap(connection.databaseElements()->getAttributesTypes(ui.cbTables->currentText()));
		SyntaxElements syntax(*connection.syntaxElements());

		if(attributeMap.count() > 0) {
			//delete current table
			ui.tFields->setRowCount(0);
			ui.tFields->setRowCount(attributeMap.count());

			for(int i=0; i<attributeMap.count(); ++i ) {
				QComboBox* tempBox = new QComboBox(ui.tFields);
				tempBox->addItems(syntax.getTypeList());
				tempBox->setCurrentIndex(tempBox->findText(syntax.getType(attributeMap[i].second)));
				
				ui.tFields->setCellWidget(i,0,tempBox);
				ui.tFields->setVerticalHeaderItem(i,new QTableWidgetItem(attributeMap[i].first));
			}
		}
	}
}

void TableTab::tableDataUpdated( QString connectionname )
{
	ui.cbTables->clear();
	ui.cbTables->addItems(connectionManager->getConnection(connectionname).databaseElements()->getTables());

	selectTable(0);
}

void TableTab::databasesUpdated()
{
	ui.cbDatabases->clear();
	ui.cbDatabases->addItems(connectionManager->connectionList());
}

void TableTab::clearTable()
{
	ui.tFields->setRowCount(0);
}

void TableTab::createTable()
{
	if(ui.cbTables->findText(ui.leTableName->text()) == -1) {
		if(connectionManager->containsConnection(ui.cbDatabases->currentText())) {
			AcquiredConnection connection(connectionManager->acquireConnection(ui.cbDatabases->currentText()));

			if(connection->isOpen()) {
				SyntaxElements* syntax = connection.getConnectionInformation().syntaxElements();

				QString statement = "create table " + syntax->delimitTablename(ui.leTableName->text()) + " (";

				for(int fieldIdx=0; fieldIdx < ui.tFields->rowCount(); ++fieldIdx) {
					if(ui.tFields->verticalHeaderItem(fieldIdx)) {
						statement+=syntax->delimitFieldname(syntax->filterSpecialChars(ui.tFields->verticalHeaderItem(fieldIdx)->text()))+" ";
					} else {
						statement+=syntax->delimitFieldname(syntax->filterSpecialChars(QString::number(fieldIdx+1)))+" ";
					}

					QComboBox* tempBox = (QComboBox*)ui.tFields->cellWidget(fieldIdx,0);
					statement+=tempBox->currentText();

					if(fieldIdx < ui.tFields->rowCount()-1) {
						statement+=",";
					}
				}

				statement+=")";

				QSqlQuery createStatement(*connection);
				if(createStatement.exec(statement)) {
					connectionManager->getDatabaseElementsUpdater()->updateElements(ui.cbDatabases->currentText());

					QMessageBox msgBox;
					msgBox.setWindowTitle("Tabelle erstellt");
					msgBox.setText("Die Tabelle wurde erfolgreich erstellt.");
					msgBox.setIcon(QMessageBox::Information);
					msgBox.setStandardButtons(QMessageBox::Ok);
					msgBox.setDefaultButton(QMessageBox::Ok);
					msgBox.exec();
				} else {
					QMessageBox msgBox;
					msgBox.setWindowTitle("Fehler beim erstellen");
					msgBox.setText("Die Tabelle konnte nicht erstellt werden.");
					msgBox.setIcon(QMessageBox::Critical);
					msgBox.setDetailedText("Statement:\n" + createStatement.executedQuery() + "\nError:\n"+createStatement.lastError().text());
					msgBox.setStandardButtons(QMessageBox::Ok);
					msgBox.setDefaultButton(QMessageBox::Ok);
					msgBox.exec();
				}
			} else {
				QMessageBox msgBox;
				msgBox.setWindowTitle("Fehler beim erstellen");
				msgBox.setText("Verbindung mit der Datenbank konnte nicht hergestellt werden.");
				msgBox.setIcon(QMessageBox::Critical);
				msgBox.setDetailedText("\nError:\n"+connection->lastError().text());
				msgBox.setStandardButtons(QMessageBox::Ok);
				msgBox.setDefaultButton(QMessageBox::Ok);
				msgBox.exec();
			}
		}
	} else {
		QMessageBox msgBox;
		msgBox.setWindowTitle("Fehler Tabellenname");
		msgBox.setText("Der Tabellenname ist schon vergeben. Tabelle konnte nicht erstellt werden.");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.exec();
	}
}

void TableTab::spreadsheetTemplate()
{
	QMessageBox msgBox;
	msgBox.setStandardButtons(QMessageBox::Ok);
	msgBox.setDefaultButton(QMessageBox::Ok);

	QString path = QFileDialog::getOpenFileName(this,"Arbeitsmappe �ffnen", "", "Excel Arbeitsmappe (*.xls *.xlsx)");

	if(path.length() > 0) {
		XL::ApplicationPtr xlapp(XL::GetApplication());
		if(xlapp->good()) {
			XL::WorkbooksPtr books(xlapp->getWorkbooks());
			XL::IWorkbook* tempBook = books->open(path.toStdString().c_str());

			if(tempBook) {
				XL::WorkbookPtr importBook(tempBook);
				XL::WorksheetsPtr importSheets(importBook->sheets());

				//get sheetname
				QStringList sheetNames;
				
				for(int sheetIdx=0; sheetIdx < importSheets->count(); ++sheetIdx) {
					XL::WorksheetPtr sheet(importSheets->getItem(sheetIdx+1));
					sheetNames<<QString(sheet->getName());
				}

				ChoseElementsDialog choseSheets(sheetNames,false,this);
				if(choseSheets.exec()==QDialog::Accepted && choseSheets.getSelectedElements().size() > 0) {
					//build table definition
					proposeTableDefinition(XL::WorksheetPtr(importSheets->getItem(sheetNames.indexOf(choseSheets.getSelectedElements().at(0))+1)));
				} else {
					msgBox.setWindowTitle("Fehler");
					msgBox.setText("Es muss mindestens ein Arbeitsblatt ausgew�hlt werden.");
					msgBox.setIcon(QMessageBox::Critical);
					msgBox.exec();
					return;
				}
			} else {
				msgBox.setWindowTitle("Fehler");
				msgBox.setText("Excel Arbeitsmappe konnte nicht ge�ffnet werden.");
				msgBox.setDetailedText(books->getLastError()->getErrorMessage());
				msgBox.setIcon(QMessageBox::Critical);
				msgBox.exec();
				return;
			}
		} else {
			msgBox.setWindowTitle("Fehler");
			msgBox.setText("Problem mit Excel Schnittstelle");
			msgBox.setDetailedText(xlapp->getLastError()->getErrorMessage());
			msgBox.setIcon(QMessageBox::Warning);
			msgBox.exec();
			return;
		}
	}
}

void TableTab::proposeTableDefinition( XL::WorksheetPtr sheet )
{
	XL::BufferedReader reader(sheet.get());

	QString matched;
	QRegExp matchDates("^([0]?[1-9]|[1|2][0-9]|[3][0|1])[.]([0]?[1-9]|[1][0-2])[.]([0-9]{4}|[0-9]{2})$");
	QRegExp matchDouble("[0-9]+((\\.[0-9]+)|(\\,[0-9]+))?");
	QRegExp matchInt("[0-9]+");
	
	SyntaxElements syntax(*connectionManager->getConnection(ui.cbDatabases->currentText()).syntaxElements());

	ui.tFields->setRowCount(0);

	QStringList fields;
	for(int col=1; col < reader.cols()+1;++col) {
		//heading
		char* tempHeading = reader.getValue(1,col)->getChar();
		QString heading(syntax.filterSpecialChars(QString(tempHeading)));
		delete[] tempHeading;

		if(heading.length() > 0 && !fields.contains(heading)) {
			fields<<heading;

			QComboBox* tempBox = new QComboBox(ui.tFields);
			tempBox->addItems(syntax.getTypeList());
			
			ui.tFields->setRowCount(ui.tFields->rowCount()+1);
			ui.tFields->setCellWidget(ui.tFields->rowCount()-1,0,tempBox);
			ui.tFields->setVerticalHeaderItem(ui.tFields->rowCount()-1,new QTableWidgetItem(heading));

			//INT => DOUBLE => DATE(restart) => STRING
			matched="INT";
			for(int row=2; row < reader.rows()+1;++row) {
				char* tempVar = reader.getValue(row,col)->getChar();
				QString var(tempVar);
				delete[] tempVar;

				if(var.length() > 0) {
					if(matched == "INT" && !matchInt.exactMatch(var)) {
						matched="DOUBLE";
					}
					if(matched == "DOUBLE" && !matchDouble.exactMatch(var)) {
						matched="DATE";
						row=2;
					}
					if(matched == "DATE" && !matchDates.exactMatch(var)) {
						matched="VARCHAR(255)";
						break;
					}
				}
			}
			tempBox->setCurrentIndex(tempBox->findText(matched));
		}
	}
}

void TableTab::createStatement()
{
	if(connectionManager->containsConnection(ui.cbDatabases->currentText())) {
		SyntaxElements* syntax = connectionManager->getConnection(ui.cbDatabases->currentText()).syntaxElements();

		QString statement = "create table " + syntax->delimitTablename(ui.leTableName->text()) + " (";

		for(int fieldIdx=0; fieldIdx < ui.tFields->rowCount(); ++fieldIdx) {
			if(ui.tFields->verticalHeaderItem(fieldIdx)) {
				statement+=syntax->delimitFieldname(syntax->filterSpecialChars(ui.tFields->verticalHeaderItem(fieldIdx)->text()))+" ";
			} else {
				statement+syntax->delimitFieldname(syntax->filterSpecialChars(QString::number(fieldIdx+1)))+" ";
			}

			QComboBox* tempBox = (QComboBox*)ui.tFields->cellWidget(fieldIdx,0);
			statement+=tempBox->currentText();

			if(fieldIdx < ui.tFields->rowCount()-1) {
				statement+=",";
			}
		}

		statement+=")";

		QMessageBox msgBox;
		msgBox.setWindowTitle("Statement");
		msgBox.setText("Siehe Details.");
		msgBox.setDetailedText(statement);
		msgBox.setIcon(QMessageBox::Information);
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.exec();
	}
}
