#ifndef datainsertmodel_h__
#define datainsertmodel_h__

#include <QAbstractItemModel>
#include <QVariant>
#include <QVector>
#include <QStringList>
#include <QMimeData>

#include "connectionmanager.h"

class DataInsertModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	DataInsertModel(ConnectionManager* connectionManager, QString connectionname, QString tablename, QObject *parent = 0);
	~DataInsertModel();

	Qt::ItemFlags flags( const QModelIndex & index ) const;

	bool setData( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );
	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole ) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
	QMimeData* mimeData( const QModelIndexList & indexes ) const;

	int rowCount( const QModelIndex & parent = QModelIndex() ) const;
	int columnCount ( const QModelIndex & parent = QModelIndex() ) const;

	QModelIndex index(int row, int column, const QModelIndex &parent) const;
	QModelIndex parent(const QModelIndex &index) const;

	Qt::DropActions supportedDropActions() const;

	QVector<QVariant> getInsertData();
public slots:

protected:

private:
	ConnectionManager* connectionManager;
	QString connectionname;
	QString tablename;

	int columns;

	QModelIndex invalidIndex;
	QVariant invalidVariant;
	QVector<QVariant> columnData;
	QVector<QVariant> columnTypeData;
	QVector<QVariant> columnInsertData;
};
#endif // datainsertmodel_h__