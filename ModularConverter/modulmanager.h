#ifndef modulmanager_h__
#define modulmanager_h__

#include <QString>
#include <QMap>

#include "connectionmanager.h"

#include "convertermodul.h"

template<typename T> ConverterModul* createModulInstance(ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion) { return new T(parameterMap, versionMap, connectionManager, context, inheritVersion); }

class ModulManager {

public:
	ModulManager();
	~ModulManager();

	int count();
	QStringList modulList();
	bool contains(QString modulname);
	ConverterModul* modul(QString modulname, ParameterMap parameterMap, VersionMap versionMap, ConnectionManager* connectionManager, QString context, bool inheritVersion);
	QStringList requestedParameter(QString modulname);
	bool useContext(QString modulname);
private:
	QMap<QString,ConverterModul*(*)(ParameterMap, VersionMap, ConnectionManager*,QString,bool)> modulMap;
	QMap<QString,QStringList(*)()> requestMap;
	QMap<QString,bool(*)()> contextMap;
};


#endif // modulmanager_h__