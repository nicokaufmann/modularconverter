#include "buffersegment.h"

BufferSegment::BufferSegment()
	: segmentRow(0), segmentColumn(0), segmentLength(0), segmentHeight(0)
{

}

BufferSegment::BufferSegment( const int& row, const int& column, const int& length )
	: segmentRow(row), segmentColumn(column), segmentLength(length), segmentHeight(1)
{

}

bool BufferSegment::fullmatch( const BufferSegment& segment ) const
{
	return (segment.row()-segmentRow-segmentHeight) == 0 && segment.column() <= segmentColumn && (segment.column() + segment.length()) >= (segmentColumn+segmentLength);
}

bool BufferSegment::match( const BufferSegment& segment ) const
{
	return (segment.row()-segmentRow-segmentHeight) == 0 && segment.column() > (segmentColumn-segment.length()) && segment.column() < (segmentColumn+segmentLength);
}

void BufferSegment::combine( const BufferSegment& segment )
{
	if(segment.column() > segmentColumn) {
		segmentLength-=segment.column()-segmentColumn;
		segmentColumn = segment.column();
	} else if(segment.column() < segmentColumn) {
		if(segment.column()+segment.length() < segmentColumn+segmentLength) {
			segmentLength = segment.column()+segment.length()-segmentColumn;
		}
	} else {
		if(segment.length() < segmentLength) {
			segmentLength = segment.length();
		}
	}

	if(segmentRow < segment.row()) {
		segmentHeight++;
	} else {
		segmentRow = segment.row();
		segmentHeight = segment.height()+1;
	}
}

const int& BufferSegment::row() const
{
	return segmentRow;
}

const int& BufferSegment::column() const
{
	return segmentColumn;
}

const int& BufferSegment::length() const
{
	return segmentLength;
}

const int& BufferSegment::height() const
{
	return segmentHeight;
}

int BufferSegment::count() const
{
	return segmentLength*segmentHeight;
}
