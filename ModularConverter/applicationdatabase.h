#ifndef applicationdatabase_h__
#define applicationdatabase_h__

#include <QStringList>
#include <QVariant>
#include <QList>
#include <QMap>
#include <QComboBox>

#include "applicationuser.h"
#include "connectionmanager.h"
#include "workflowmanager.h"

#include "simplecrypt.h"

class ApplicationDatabase : public QObject {
	Q_OBJECT
public:
	ApplicationDatabase();
	~ApplicationDatabase();
	
	bool checkApplication(ConnectionInformation connection);
	bool createApplication(ConnectionInformation connection);
	bool connect(QString username, QString password, ConnectionInformation connection);

	bool addConnection(ConnectionInformation connectionInformation);
	bool updateConnection(ConnectionInformation connectionInformation);
	bool removeConnection(ConnectionInformation connectionInformation);
	
	bool saveWorkflow(Workflow* flow);
	bool removeWorkflow(Workflow* flow);

	ApplicationUser getCurrentUser();

	QString getApplicationName();

	bool updateTables(const QString& connectionname, const QStringList& tables);
	QStringList getTables(const QString& connectionname);

	ConnectionManager* getConnectionManager();
	WorkflowManager* getWorkflowManager();

	QString getLastError();
signals:
	void databasesUpdated();
	void workflowsUpdated();

private:
	bool loadConnections(ConnectionInformation connection);
	bool loadWorkflows(ConnectionInformation connection);
	bool loadWorkflowItems(QSqlDatabase& connection, Workflow* flow);

	bool saveWorkflowItems(QSqlDatabase& connection, Workflow* flow);
	bool removeWorkflowItems(QSqlDatabase& connection, Workflow* flow);
	
	ApplicationUser loadUser(ConnectionInformation connection, QString username, QString password);

	ApplicationUser currentUser;
	QString applicationName;

	ConnectionManager* connectionManager;
	WorkflowManager* workflowManager;

	QString lastError;
};
#endif // applicationdatabase_h__