#include "connectionlocker.h"

ConnectionLocker::ConnectionLocker( ConnectionInformation connection )
	: connection(connection)
{
	if(!connection.driver().synchronousUse()) {
		connection.driver().mutex()->lock();
	}

	if(!connection.synchronousUse()) {
		connection.lock()->lockForWrite();
	} else {
		connection.lock()->lockForRead();
	}
}

ConnectionLocker::~ConnectionLocker()
{
	if(!connection.driver().synchronousUse()) {
		connection.driver().mutex()->unlock();
	}

	connection.lock()->unlock();
}

